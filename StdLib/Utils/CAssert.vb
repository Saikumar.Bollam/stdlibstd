''' <summary>
''' Helper class to perform assertions.  Interface design was a simple scaled down knockoff from nUnit
''' </summary>
''' <remarks></remarks>
Public Class CAssert

    Public Class AssertionException
        Inherits System.Exception
        Public Sub New(ByVal psErrorMessage As String)
            MyBase.New(psErrorMessage)
        End Sub
    End Class


    Public Shared Sub IsTrue(ByVal condition As Boolean, ByVal message As String)
        If False = condition Then
            Throw New AssertionException(message)
        End If
    End Sub

    Public Shared Sub IsFalse(ByVal condition As Boolean, ByVal message As String)
        If True = condition Then
            Throw New AssertionException(message)
        End If
    End Sub

    Public Shared Sub IsNotNull(ByVal anObject As Object, ByVal message As String)
        If IsNothing(anObject) Then
            Throw New AssertionException(message)
        End If
    End Sub

    Public Shared Sub IsNull(ByVal anObject As Object, ByVal message As String)
        If False = IsNothing(anObject) Then
            Throw New AssertionException(message)
        End If
    End Sub

    Public Shared Sub Contains(ByVal expected As Object, ByVal actual As IList, ByVal message As String)
        If False = actual.Contains(expected) Then
            Throw New AssertionException(message)
        End If

    End Sub


    Public Shared Sub Fail(ByVal message As String)
        Throw New AssertionException(message)
    End Sub

    ''' <summary>
    ''' This routine useful for assertions that should fail (with AssertionException thrown) catastrophically on dev/stage, warn on demo, and simply logged on production (to avoid excessive spam).
    ''' It's useful mainly on convenience features (ie: address importing), whereby a failure should not cause a PUC to end user, but should be caught by developers. 
    ''' On demo site, we generally don't want to just PUC because it might be used during training;  however we log the warning so that we hopefully catch them
    ''' before going to production.
    ''' </summary>
    ''' <param name="message"></param>
    ''' <remarks>Should you find yourself needing to warn/block the user, then you should be using the <see cref="Fail">Fail</see> version of this	
    ''' </remarks>
    Public Shared Sub FailGracefully(ByVal message As String)
        FailGracefully(message, Nothing)
    End Sub

    Public Shared Sub FailGracefully(ByVal message As String, ByVal ex As Exception)
        Dim log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CAssert))

        Select Case CBaseStd.RuntimeEnvironment
            Case CBaseStd.Environments.PRODUCTION
                log.Info("Assertion failure (gracefully): " & message & If(ex IsNot Nothing, ". Exception message: " & ex.Message, ""))
            Case CBaseStd.Environments.DEMO
                log.Warn("Assertion failure (gracefully): " & message, ex)
            Case Else
                log.Warn("Assertion failure (gracefully): " & message, ex)
                Throw New AssertionException(message)
        End Select
    End Sub

End Class


