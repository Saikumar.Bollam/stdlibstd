﻿Option Strict On

Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports StdLib.CBaseStd

Public Class CDataTable2Text
    Protected log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)


    ''' <summary>
    ''' This could be used to skip exporting values for certain columns. E.g. The primary key XXXID is included in the query results and we don't want to export the value of the primary key.
    ''' Note: Please make sure the skipped columns are not the last columns. To be enhanced, make skippied exporting columns to support last columns as well
    ''' </summary>
    Public SkippedExportingColumns As New HashSet(Of String)

    ''' <summary>
    ''' This could be use to customize the value of certain field during exporting. E.g. encrypted SSN need to be decrypted before exporting while the value of data reader can't be modified. 
    ''' By defining the special transformation logic in SpecialFieldsTransformationLogic, this module will transform the value before exporting the text
    ''' </summary>
    Public ColumnsNeedTransformation As New HashSet(Of String)

    ''' <summary>
    ''' Should be used together with ColumnsNeedTransformation. It's used to define special data field transformation logic before exporting.
    ''' </summary>
    Public SpecialFieldsTransformationLogic As IDataFieldsTransformation

    Private _dt As DataTable
    Private _FieldSeparator As Char
    Private _textReader As System.IO.TextReader
    Private _Headers As String()
    Private _MissingColumns As List(Of DataColumn)
    Private _TextFieldParser As Microsoft.VisualBasic.FileIO.TextFieldParser

    Public RequireSameColumnsCount As Boolean = False 'the # of columns defined in the text file being imported must match that of the datatable defined.  By default set to false so that existing customers will not fail.
    Public EnableHeaderText As Boolean = True
    Public IgnoreEmptyLinesReading As Boolean = True
    Public ExportEmptyValueAsBlank As Boolean = False
    Public ExportQuoteValuesWhenNecessary As Boolean = False 'will quote the value and escape quotes if the value contains deliminater, quotes, or new line. Isquoted parameter will override this and always quote
    Public TrimWhitespace As Boolean = True

    Private _CurrentLine As Long = 0
    Public ReadOnly Property CurrentLine() As Long
        Get
            Return _CurrentLine
        End Get
    End Property

    ' Determines whether to parse the text using split function instead of TextFieldParser
    Private _UseSplit As Boolean?
    Public Property UseSplit() As Boolean
        Get
            If Not _UseSplit.HasValue Then
                _UseSplit = AppSettings.Get("USE_SPLIT_TO_IMPORT_CSV") = "Y"
            End If
            Return _UseSplit.Value
        End Get
        Set(ByVal value As Boolean)
            _UseSplit = value
        End Set
    End Property

    Public Sub convertDataTable2Text(ByRef dt As DataTable, ByVal txtWriter As System.IO.TextWriter, Optional ByVal FieldSeparator As String = vbTab, Optional ByVal IsQuoted As Boolean = False)
        Dim bSkippedExportingColumnsEnabled As Boolean = SkippedExportingColumns.Count > 0
        Dim bDataTransformationEnabled As Boolean = ColumnsNeedTransformation.Count > 0

        _dt = dt
        _CurrentLine = 0
        If EnableHeaderText Then
            _CurrentLine += 1
            For c = 0 To _dt.Columns.Count - 1 'Each col In _dt.Columns
                Dim sValue As String = _dt.Columns(c).ColumnName

                If bSkippedExportingColumnsEnabled AndAlso SkippedExportingColumns.Contains(sValue) Then
                    Continue For
                End If

                If TrimWhitespace Then
                    sValue = SafeString(sValue)
                End If

                If ExportEmptyValueAsBlank AndAlso SafeDouble(sValue) = _DefaultEmptyValueNumeric Then
                    txtWriter.Write("")
                Else
                    Dim isQuoteValue As Boolean = IsQuoted
                    If ExportQuoteValuesWhenNecessary Then
                        If sValue.Contains("""") OrElse
                        sValue.Contains(vbNewLine) OrElse
                        sValue.Contains(FieldSeparator) Then
                            isQuoteValue = True
                        End If
                    End If
                    If isQuoteValue Then
                        sValue = ControlChars.Quote & sValue.Replace(ControlChars.Quote, """""") & ControlChars.Quote
                    End If
                    txtWriter.Write(sValue)
                End If

                If c <> _dt.Columns.Count - 1 Then
                    txtWriter.Write(FieldSeparator)
                End If
            Next
            txtWriter.WriteLine()
        End If

        For Each row As DataRow In _dt.Rows
            _CurrentLine += 1
            Dim c As Integer
            For c = 0 To _dt.Columns.Count - 1 'Each col In _dt.Columns
                Dim sColumnName As String = _dt.Columns(c).ColumnName

                If bSkippedExportingColumnsEnabled AndAlso SkippedExportingColumns.Contains(sColumnName) Then
                    Continue For
                End If

                Dim sValue As String
                If row.Item(c) Is Nothing OrElse TypeOf row.Item(c) Is DBNull Then
                    sValue = ""
                Else
                    sValue = CStr(row.Item(c))
                End If

                If bDataTransformationEnabled AndAlso ColumnsNeedTransformation.Contains(sColumnName) AndAlso SpecialFieldsTransformationLogic IsNot Nothing Then
                    sValue = SpecialFieldsTransformationLogic.TransformData(row, sColumnName)
                End If

                'Dim sValue As String = DirectCast(row.Item(c), String)
                If TrimWhitespace Then
                    sValue = SafeString(sValue)
                End If
                If ExportEmptyValueAsBlank AndAlso SafeDouble(sValue) = _DefaultEmptyValueNumeric Then
                    txtWriter.Write("")
                Else
                    Dim isQuoteValue As Boolean = IsQuoted
                    If ExportQuoteValuesWhenNecessary Then
                        If sValue.Contains("""") OrElse
                        sValue.Contains(vbNewLine) OrElse
                        sValue.Contains(FieldSeparator) Then
                            isQuoteValue = True
                        End If
                    End If
                    If isQuoteValue Then
                        sValue = ControlChars.Quote & sValue.Replace(ControlChars.Quote, """""") & ControlChars.Quote
                    End If
                    txtWriter.Write(sValue)
                End If

                If c <> _dt.Columns.Count - 1 Then
                    txtWriter.Write(FieldSeparator)
                End If
            Next
            txtWriter.WriteLine()
        Next
        txtWriter.Flush()

        _dt = Nothing
    End Sub


    Public Sub convertDataTable2Text(pReader As IDataReader, ByVal txtWriter As System.IO.TextWriter, Optional ByVal FieldSeparator As String = vbTab, Optional ByVal IsQuoted As Boolean = False)
        Dim bSkippedExportingColumnsEnabled As Boolean = SkippedExportingColumns.Count > 0
        Dim bDataTransformationEnabled As Boolean = ColumnsNeedTransformation.Count > 0

        _CurrentLine = 0
        If EnableHeaderText Then
            _CurrentLine += 1
            For c = 0 To pReader.FieldCount - 1
                If bSkippedExportingColumnsEnabled AndAlso SkippedExportingColumns.Contains(pReader.GetName(c)) Then
                    Continue For
                End If

                Dim sValue As String = pReader.GetName(c)
                If TrimWhitespace Then
                    sValue = SafeString(sValue)
                End If

                If ExportEmptyValueAsBlank AndAlso SafeDouble(sValue) = _DefaultEmptyValueNumeric Then
                    txtWriter.Write("")
                Else
                    Dim isQuoteValue As Boolean = IsQuoted
                    If ExportQuoteValuesWhenNecessary Then
                        If sValue.Contains("""") OrElse
                        sValue.Contains(vbNewLine) OrElse
                        sValue.Contains(FieldSeparator) Then
                            isQuoteValue = True
                        End If
                    End If
                    If isQuoteValue Then
                        sValue = ControlChars.Quote & sValue.Replace(ControlChars.Quote, """""") & ControlChars.Quote
                    End If
                    txtWriter.Write(sValue)
                End If

                If c <> pReader.FieldCount - 1 Then
                    txtWriter.Write(FieldSeparator)
                End If
            Next
            txtWriter.WriteLine()
        End If


        While pReader.Read
            _CurrentLine += 1
            Dim c As Integer
            For c = 0 To pReader.FieldCount - 1 'Each col In _dt.Columns
                Dim sColumnName As String = pReader.GetName(c)

                If bSkippedExportingColumnsEnabled AndAlso SkippedExportingColumns.Contains(sColumnName) Then
                    Continue For
                End If

                Dim sValue As String
                If pReader(c) Is Nothing OrElse TypeOf pReader(c) Is DBNull Then
                    sValue = ""
                Else
                    sValue = CStr(pReader(c))
                End If

                If bDataTransformationEnabled AndAlso ColumnsNeedTransformation.Contains(sColumnName) AndAlso SpecialFieldsTransformationLogic IsNot Nothing Then
                    sValue = SpecialFieldsTransformationLogic.TransformData(pReader, sColumnName)
                End If

                'Dim sValue As String = DirectCast(row.Item(c), String)
                If TrimWhitespace Then
                    sValue = SafeString(sValue)
                End If
                If ExportEmptyValueAsBlank AndAlso SafeDouble(sValue) = _DefaultEmptyValueNumeric Then
                    txtWriter.Write("")
                Else
                    Dim isQuoteValue As Boolean = IsQuoted
                    If ExportQuoteValuesWhenNecessary Then
                        If sValue.Contains("""") OrElse
                        sValue.Contains(vbNewLine) OrElse
                        sValue.Contains(FieldSeparator) Then
                            isQuoteValue = True
                        End If
                    End If
                    If isQuoteValue Then
                        sValue = ControlChars.Quote & sValue.Replace(ControlChars.Quote, """""") & ControlChars.Quote
                    End If
                    txtWriter.Write(sValue)
                End If

                If c <> pReader.FieldCount - 1 Then
                    txtWriter.Write(FieldSeparator)
                End If
            Next
            txtWriter.WriteLine()
        End While
        txtWriter.Flush()


    End Sub

    Public Sub setUpHeaders()
        _MissingColumns = New List(Of DataColumn)

        If EnableHeaderText = False Then
            Return
        End If

        If Not hasRows() Then
            Throw New ParseException("Missing header row!  If no header is expected, then set ReadHeaderText to False.")
        End If

        _Headers = NextLine()

        If Me.RequireSameColumnsCount AndAlso _Headers.Count <> _dt.Columns.Count Then
            Dim sErr As String = String.Format("Number of columns in text file does not match the defined columns in the datatable.  Culprit line ({0}): {1} Expected # columns: {2} Actual # of columns: {3}", _CurrentLine, vbCrLf, _dt.Columns.Count, _Headers.Count)
            Throw New ParseException(sErr)
        End If

        'uppercase all the column names so we can do case insensitve search 
        For i = 0 To _Headers.Count - 1
            _Headers(i) = UCase(_Headers(i))
        Next

        For Each sHeader As String In _Headers
            If _dt.Columns(sHeader) Is Nothing Then
                If sHeader = "" Then
                    Throw New ParseException("Invalid blank column name in source file.")
                Else
                    Throw New ParseException("Invalid column name in source file: " & sHeader)
                End If
            End If
        Next

        If Not IgnoreMissingColumns Then
            For Each col As DataColumn In _dt.Columns
                If Not _Headers.Contains(UCase(col.ColumnName)) Then
                    _MissingColumns.Add(col)
                End If
            Next
        End If
    End Sub



    Public Sub convertText2DataTable(ByRef dt As DataTable, ByRef txtReader As System.IO.TextReader, Optional ByVal FieldSeparator As Char = CChar(vbTab))
        _dt = dt
        _FieldSeparator = FieldSeparator
        _CurrentLine = 0
        _textReader = txtReader

        If Not UseSplit Then
            Dim oTextFieldParser As New Microsoft.VisualBasic.FileIO.TextFieldParser(_textReader)
            oTextFieldParser.TrimWhiteSpace = TrimWhitespace
            oTextFieldParser.HasFieldsEnclosedInQuotes = True
            oTextFieldParser.SetDelimiters(_FieldSeparator)
            oTextFieldParser.TextFieldType = FileIO.FieldType.Delimited

            _TextFieldParser = oTextFieldParser
        End If

        setUpHeaders()

        'setup the header from the text stream 

        While hasRows()
            Dim newRow As DataRow = dt.NewRow()

            Dim oFields() As String = Nothing

            oFields = NextLine()

            If Me.RequireSameColumnsCount AndAlso oFields.Length <> _dt.Columns.Count Then
                Throw New ParseException(String.Format("Number of columns in text file does not match the defined columns in the datatable.  Culprit line ({0}): {1}", _CurrentLine, Text.CArrayFormatter.FormatAsTextComma(oFields)))
            End If
            Try
                Dim c As Integer
                Dim nUpperBound As Integer

                If EnableHeaderText Then
                    nUpperBound = _Headers.Count - 1
                Else
                    nUpperBound = _dt.Columns.Count - 1
                End If


                For c = 0 To nUpperBound
                    Dim sValue As String = oFields(c)

                    If EnableHeaderText Then
                        'set the row values based on the named column position
                        setFieldValue(newRow, _dt.Columns(_Headers(c)), sValue)
                        'newRow.Item(arrHeaders(c)) = arrCols(c)
                    Else
                        'set the row values based on coordinates
                        setFieldValue(newRow, _dt.Columns(c), sValue)
                        'newRow.Item(c) = arrCols(c)
                    End If
                Next

                Dim colMiss As DataColumn
                For Each colMiss In _MissingColumns
                    setFieldValue(newRow, colMiss, "")
                Next
            Catch ex As Exception
                Throw New ParseException(String.Format("Failed to parse Line: {0} with content: {1}.  Error message: {2}", _CurrentLine, Text.CArrayFormatter.FormatAsTextComma(oFields), ex.Message), ex)
            End Try

            dt.Rows.Add(newRow)
        End While

        _dt = Nothing
    End Sub

    Public Sub convertText2DataReader(ByVal dt As DataTable, ByRef txtReader As System.IO.TextReader, Optional ByVal FieldSeparator As Char = CChar(vbTab))
        _textReader = txtReader
        _FieldSeparator = FieldSeparator
        _dt = dt

        If Not UseSplit Then
            Dim oTextFieldParser As New Microsoft.VisualBasic.FileIO.TextFieldParser(_textReader)
            oTextFieldParser.TrimWhiteSpace = TrimWhitespace
            oTextFieldParser.HasFieldsEnclosedInQuotes = True
            oTextFieldParser.SetDelimiters(_FieldSeparator)
            oTextFieldParser.TextFieldType = FileIO.FieldType.Delimited

            _TextFieldParser = oTextFieldParser
        End If

        setUpHeaders()

    End Sub

    Public Function NextRow() As DataRow
        Dim oFields As String() = NextLine()

        _dt.Rows.Clear()
        Dim newRow As DataRow = _dt.NewRow()

        If RequireSameColumnsCount AndAlso oFields.Count <> _dt.Columns.Count Then
            Throw New ParseException(String.Format("Number of columns in text file does not match the defined columns in the datatable.  Culprit line ({0}): {1}", _CurrentLine, Text.CArrayFormatter.FormatAsTextComma(oFields)))
        End If
        Try

            Dim nUpperBound As Integer

            If EnableHeaderText Then
                nUpperBound = _Headers.Count - 1
            Else
                nUpperBound = _dt.Columns.Count - 1
            End If


            For c = 0 To nUpperBound
                Dim sValue As String = oFields(c)

                If EnableHeaderText Then
                    'set the row values based on the named column position
                    setFieldValue(newRow, _dt.Columns(_Headers(c)), sValue)
                    'newRow.Item(arrHeaders(c)) = arrCols(c)
                Else
                    'set the row values based on coordinates
                    setFieldValue(newRow, _dt.Columns(c), sValue)
                    'newRow.Item(c) = arrCols(c)
                End If
            Next

            Dim colMiss As DataColumn
            For Each colMiss In _MissingColumns
                setFieldValue(newRow, colMiss, "")
            Next
        Catch ex As Exception
            Throw New ParseException(String.Format("Failed to parse Line: {0} with content: {1}.  Error message: {2}", _CurrentLine, Text.CArrayFormatter.FormatAsTextComma(oFields), ex.Message), ex)
        End Try

        Return newRow
    End Function

    Public Function hasRows() As Boolean

        If UseSplit Then
            Return _textReader.Peek <> -1
        Else
            Return Not _TextFieldParser.EndOfData
        End If

    End Function

    Private Function NextLine() As String()
        If UseSplit Then
            Dim oNextLine As New List(Of String)

            For Each sField As String In _textReader.ReadLine().Split(_FieldSeparator)
                oNextLine.Add(sField.Trim(" "c).Trim("'"c).Trim(Chr(34)))
            Next

            _CurrentLine += 1

            Return oNextLine.ToArray
        Else
            _CurrentLine += 1
            Try
                Return _TextFieldParser.ReadFields()
            Catch mal As Microsoft.VisualBasic.FileIO.MalformedLineException
                Throw New ParseException(String.Format("Failed to parse Line: {0} with content: {1}.  Error message: {2}", _CurrentLine, _TextFieldParser.ErrorLine, mal.Message), mal)
            End Try

        End If
    End Function

    'Protected Sub convertText2DataTableUsingSplit(ByRef dt As DataTable, ByRef txtReader As System.IO.TextReader, Optional ByVal FieldSeparator As Char = CChar(vbTab))
    '	UseSplit = True
    '	convertText2DataTable(dt, txtReader, FieldSeparator)
    'End Sub

    'Protected Sub convertText2DataTableUsingParser(ByRef dt As DataTable, ByRef txtReader As System.IO.TextReader, Optional ByVal FieldSeparator As Char = CChar(vbTab))
    '	UseSplit = False
    '	convertText2DataTable(dt, txtReader, FieldSeparator)
    'End Sub

    Private Sub setFieldValue(ByVal row As DataRow, ByVal column As DataColumn, ByVal FieldValue As String)
        Try
            'Must use .Equals because SourceNullInidcator can be Nothing and 
            ' String.Empty = Nothing is true
            If SafeString(FieldValue).Equals(SourceNullIndicator) Then
                row(column.ColumnName) = DBNull.Value
                Return
            End If

            If SafeString(FieldValue) <> "" Then
                row(column.ColumnName) = FieldValue
            Else
                If column.DataType Is GetType(Integer) OrElse
                 column.DataType Is GetType(Double) OrElse
                 column.DataType Is GetType(Decimal) OrElse
                 column.DataType Is GetType(Long) OrElse
                 column.DataType Is GetType(Int16) OrElse
                 column.DataType Is GetType(Int32) OrElse
                 column.DataType Is GetType(Int64) OrElse
                 column.DataType Is GetType(Short) Then

                    row(column.ColumnName) = _DefaultEmptyValueNumeric
                ElseIf column.DataType Is GetType(Date) OrElse
                 column.DataType Is GetType(DateTime) Then
                    row(column.ColumnName) = DBNull.Value
                Else
                    row(column.ColumnName) = ""
                End If
            End If
        Catch ex As System.Exception
            log.Debug("Unable to set column: " & column.ColumnName & " with value: " & FieldValue)
            Throw ex
        End Try
    End Sub

    Public SourceNullIndicator As String = Nothing
    Public IgnoreMissingColumns As Boolean = False

    Private _DefaultEmptyValueNumeric As Integer = 0

    Public Sub New(ByVal pnDefaultEmptyValueNumeric As Integer)
        _DefaultEmptyValueNumeric = pnDefaultEmptyValueNumeric
    End Sub

    Public Class ParseException
        Inherits System.Exception
        Public Sub New(ByVal sErr As String)
            MyBase.New(sErr)
        End Sub
        Public Sub New(ByVal psErr As String, ByVal poEx As Exception)
            MyBase.New(psErr, poEx)
        End Sub
    End Class

End Class


