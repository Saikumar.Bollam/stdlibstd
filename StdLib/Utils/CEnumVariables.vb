'this class pulls data out from the web.config 
Imports System.Xml
Imports StdLib.CBaseStd

Public Class CEnumVariables
    Inherits ArrayList
    'Inherits System.Collections.Hashtable
    'shared member used to cache data 
    ''' <summary>
    ''' Shared member that's initialized on app loadup.  It will be a reference to the 'enumVariables' xml node.
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared EnumVariablesNode As XmlElement

    ''' <summary>
    ''' Shared member used to cache data to optimize look up
    ''' Previously, constructor would run an expensive SelectSingleElement with XPath 
    ''' on the config document each time this class was constructed.
    ''' This will still find the first matching node, but instead does the work 
    ''' only once when the type constuctor runs.
    ''' Private because this field is an implementation detail for efficiency
    ''' and isn't thread safe.
    ''' </summary>
    Private Shared EnumVariablesNodesByName As Dictionary(Of String, XmlElement)

    Shared Sub New()
        RefreshEnumVariablesNode()
    End Sub

    ''' <summary>
    ''' Loads or reloads the cached enumvariables. Intended to be called on class instantiation, and 
    ''' from GlobalShared/Utilities/RefreshEnumVariables.aspx
    ''' </summary>
    ''' <remarks>
    ''' Note this is not threadsafe. It's intended to be called rarely, and the type of concurrency bug it
    ''' is expected to produce is low severity, so it's not worth the hit of synchlocking access to the cached variable.
    ''' </remarks>
    Public Shared Sub RefreshEnumVariablesNode()
        Dim oXML As New XmlDocument
        oXML.Load(ConfigMapFolder & "\enumVariables.config")
        EnumVariablesNode = oXML.DocumentElement

        ' insert first node with given name
        EnumVariablesNodesByName = New Dictionary(Of String, XmlElement)
        For Each oNode As XmlElement In EnumVariablesNode.ChildNodes.OfType(Of XmlElement)
            Dim sNodeName As String = oNode.Name.ToLower
            If Not EnumVariablesNodesByName.ContainsKey(sNodeName) Then
                EnumVariablesNodesByName.Add(sNodeName, oNode)
            End If
        Next
    End Sub

    ''' <summary>
    '''  Indicates the organization that should be taken as the context of this current request, for customization purpose.
    ''' </summary>
    ''' <remarks>
    '''  ThreadStatic effectively changes the scope of this variable- rather than behaving as a normal Shared, it
    '''  is unique to each Thread/AppDomain combination. This will be set at CGlobalPage's inheritors, and should be done at the
    '''  earliest opportunity to ensure it's there when needed. Don't try to initialize it, it'll only be done on one thread.
    ''' </remarks>
    <ThreadStatic()> Public Shared OrganizationName As String

    Public isOtherNodes As ArrayList = New ArrayList

    Public EnumVariableName As String

    Public Shared Function getAllEnums() As CEnumVariables()
        Dim list As New Generic.List(Of CEnumVariables)

        Dim oNode As XmlNode
        For Each oNode In EnumVariablesNode.ChildNodes
            If TypeOf oNode Is XmlElement Then
                list.Add(New CEnumVariables(oNode.Name))
            End If
        Next

        Return list.ToArray()

    End Function

    ''' <summary>
    ''' Applies the simplified Enum customizations, represented as a generic.list of string values to be removed from the
    ''' returned enum. Get the simplified representation at CacheUtils.getEnumCustomizations
    ''' </summary>
    Public Sub ApplyCustomization(ByVal poToRemove As Generic.List(Of String))
        If poToRemove Is Nothing OrElse poToRemove.Count = 0 Then
            Return
        End If

        For i As Integer = Me.Count - 1 To 0 Step -1
            If poToRemove.Contains(SafeString(Me.Item(i).Value)) Then
                Me.RemoveAt(i)
            End If
        Next
    End Sub

    Private Sub New()
        'No-op. Note private status- blank constructor is used only as the basis for methods that return a copy of the current list.
    End Sub

    Public Sub New(ByVal sType As String)

        Me.New(sType, True)
    End Sub

    Public Shadows Function add(ByVal entry As DictionaryEntry) As Integer
        Return MyBase.Add(entry)
    End Function

    Public Sub New(ByVal sType As String, ByVal autoSort As Boolean)
        EnumVariableName = sType

        Dim oTypeNode As XmlElement = Nothing
        ' find first node with the same type
        If Not EnumVariablesNodesByName.TryGetValue(sType.ToLower, oTypeNode) Then
            Throw New IndexOutOfRangeException("Unable to find node:" & sType & " from Enum Variables XML.")
        End If

        Dim langKey As String = Threading.Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName
        Dim sOrgName As String = SafeString(OrganizationName)
        For Each oChild As XmlElement In oTypeNode.SelectNodes("item")

            'First try to get the localized + customized value
            Dim oLocalizedEntry As XmlElement = DirectCast(oChild.SelectSingleNode(String.Format("global_text[@lang='{0}' and @organization_name={1}]", langKey, XPathString(sOrgName))), XmlElement)

            If oLocalizedEntry IsNot Nothing Then
                Me.add(New DictionaryEntry(oLocalizedEntry.GetAttribute("text"), oChild.GetAttribute("value")))
            Else
                'Next try to get the localized but generic value
                oLocalizedEntry = DirectCast(oChild.SelectSingleNode(String.Format("global_text[@lang='{0}' and not(@organization_name)]", langKey)), XmlElement)
                If oLocalizedEntry IsNot Nothing Then
                    Me.add(New DictionaryEntry(oLocalizedEntry.GetAttribute("text"), oChild.GetAttribute("value")))
                Else
                    'Finally revert to the default value
                    Me.add(New DictionaryEntry(oChild.GetAttribute("text"), oChild.GetAttribute("value")))
                End If
            End If
            If oChild.GetAttribute("is_other") <> "" Then
                isOtherNodes.Add(New DictionaryEntry(oChild.GetAttribute("text"), oChild.GetAttribute("value")))
            End If
        Next

        If autoSort Then
            Me.Sort(New DictionaryEntryComparer())
        End If

    End Sub

    ''' <summary>
    ''' Searches enum for a given string 'value' and returns the corresponding 'text'
    ''' </summary>
    ''' <remarks>If not found, empty string will be returned.</remarks>
    Public Function findByValue(ByVal sValue As String) As String
        Dim itemDictEntry As DictionaryEntry
        Dim sResult As String
        sResult = ""
        For Each itemDictEntry In Me
            If SafeString(itemDictEntry.Value) = sValue Then
                sResult = SafeString(itemDictEntry.Key)
                Exit For
            End If
        Next
        Return sResult
    End Function

    ''' <summary>
    ''' Searches enum for a given string 'text' and returns the corresponding 'value'
    ''' </summary>
    ''' <remarks>If not found, empty string will be returned.</remarks>
    Public Function findByText(ByVal sText As String) As String
        Dim itemDictEntry As DictionaryEntry
        Dim sResult As String
        sResult = ""
        For Each itemDictEntry In Me
            If SafeString(itemDictEntry.Key).ToUpper = SafeString(sText).ToUpper Then
                sResult = SafeString(itemDictEntry.Value)
                Exit For
            End If
        Next
        Return sResult
    End Function

    'helper class to compare dictionary entrys by KEY (case insensitive)!
    Private Class DictionaryEntryComparer
        Implements IComparer
        '-1 if x<y, 0 x=y, 1 x>y
        Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
            Dim xEntry As DictionaryEntry = DirectCast(x, DictionaryEntry)
            Dim yEntry As DictionaryEntry = DirectCast(y, DictionaryEntry)

            Return xEntry.Key.ToString.ToUpper.CompareTo(yEntry.Key.ToString.ToUpper())
        End Function
    End Class

    Public Shadows Sub Insert(ByVal index As Integer, ByVal value As DictionaryEntry)
        MyBase.Insert(index, value)
    End Sub

    Default Public Shadows Property Item(ByVal index As Integer) As DictionaryEntry
        Get
            Return CType(MyBase.Item(index), DictionaryEntry)
        End Get
        Set(ByVal Value As DictionaryEntry)
            MyBase.Item(index) = Value
        End Set
    End Property
End Class

Public Class CEnumVariablesSectionHandler
    Implements System.Configuration.IConfigurationSectionHandler

    Protected log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CEnumVariablesSectionHandler))

    Public Function Create(ByVal parent As Object, ByVal configContext As Object, ByVal section As System.Xml.XmlNode) As Object Implements System.Configuration.IConfigurationSectionHandler.Create
        If section.Attributes("file") Is Nothing Then
            log.Debug("Loading enum variables...")
            Return section
        Else
            'load external file 
            Dim docExternal As New XmlDocument
            Dim sFile As String = CStr(section.Attributes("file").Value)
            docExternal.Load(sFile)
            log.Debug("Loading enumvariables from external file: " & sFile)
            Return docExternal.DocumentElement
        End If
    End Function
End Class
