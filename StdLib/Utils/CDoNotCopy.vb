﻿''' <summary>
''' This class is used to mark a field as skipping copy field. 
'''So CBaseStd.CopyFields will skip copying this field to Destination Object.
''' </summary>
''' <remarks></remarks>
Public Class CDoNotCopy
    Inherits System.Attribute
End Class
