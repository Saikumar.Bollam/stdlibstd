﻿Option Strict On

Imports System.Configuration.ConfigurationManager

''' <summary>
''' This class provides basic timezone management lookup/translations.   For a given point in time, it's 
''' not really clear what the offset and daylight savings are.   This class uses an optional external time zone config
''' file that provides all this information and ultimately returns a TimeZoneInfo that provide specific details.
''' </summary>
''' <remarks>
''' The class has 2 external optional config dependencies:
''' MACHINE_TIME_ZONE: Specifies what the timezone of machine the code is currently executing on.  If missing, will default to Los Angeles.
''' CONFIG_MAP_FOLDER: Specifies the configMap folder whereby TimeZones.config is defined.  If blank/non-existing, it 
''' will default to hardcoded XML.
''' </remarks>
Public MustInherit Class CTimeZoneManager

    Public Shared TimeZoneLoader As ITimeZoneLoader = New CTimeZoneLoader()

    Public Enum Zone
        UTC = 0
        LosAngeles = 1
        Phoenix = 2
        London = 3
        Honolulu = 4
        Anchorage = 5
        Denver = 6
        Chicago = 7
        NewYork = 8
        CentralEurope = 9
        Chamorro = 10
        'aliases
        Hawaii = 4
        Alaska = 5
        Pacific = 1
        Arizona = 2
        Mountain = 6
        Central = 7
        Eastern = 8
        UnitedKingdom = 3
    End Enum

    Private Shared ReadOnly Property _TimeZoneConfigPath() As String
        Get
            Return CBaseStd.ConfigMapFolder & "\TimeZones.config"
        End Get
    End Property

    Private Shared _ZoneInfos As New Dictionary(Of String, TimeZoneInfo)
    Private Shared _Lock As New Object()

    Public Shared ReadOnly Property ZoneInfo(ByVal peZone As Zone) As TimeZoneInfo
        Get
            Return ZoneInfo(MapTimeZoneToID(peZone))
        End Get
    End Property

    Private Shared ReadOnly Property ZoneInfo(ByVal psZone As String) As TimeZoneInfo
        Get
            If psZone = MapTimeZoneToID(Zone.UTC) Then
                Return TimeZoneInfo.Utc
            End If

            SyncLock _Lock
                If Not _ZoneInfos.ContainsKey(psZone) Then
                    _ZoneInfos.Add(psZone, LoadTimeZone(psZone))
                End If

                Return _ZoneInfos(psZone)
            End SyncLock
        End Get
    End Property

    Public Shared ReadOnly Property LocalZoneInfo() As TimeZoneInfo
        Get
            If AppSettings.Get("MACHINE_TIME_ZONE") = "" Then
                Return ZoneInfo(Zone.LosAngeles)
            Else
                Return ZoneInfo(AppSettings.Get("MACHINE_TIME_ZONE"))
            End If

        End Get
    End Property

    Public Shared ReadOnly Property LocalZone() As Zone
        Get
            If AppSettings.Get("MACHINE_TIME_ZONE") = "" Then
                Return Zone.LosAngeles
            Else
                Return MapTimeZoneIDToZone(AppSettings.Get("MACHINE_TIME_ZONE"))
            End If

        End Get
    End Property

    Public Shared ReadOnly Property UTCZoneInfo() As TimeZoneInfo
        Get
            Return TimeZoneInfo.Utc
        End Get
    End Property

#Region "Default Timezone info from XML - copied here in sept 2015 for more portability (and gauranteed valid xml)"
    Private Shared Function getDefaultTimezoneConfig() As XDocument
        Dim sResult = <?xml version="1.0" encoding="utf-8"?>
                      <!--Currently only specifies complete historical information for 1967 onwards.-->
                      <TIME_ZONES xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="C:\LoansPQ2\ConfigMap\TimeZones.xsd">
                          <!---time_of_day appears to be local "wall" time rather than local "standard" time or universal time-->
                          <!--
		From tzdata2010j\northamerica (whitespace reformatted for readability):
		# Rule	NAME	FROM	TO	TYPE	IN	ON			AT		SAVE	LETTER/S
			...
			Rule	US		1967	2006	-		Oct	lastSun	2:00	0			S
			Rule	US		1967	1973	-		Apr	lastSun	2:00	1:00	D
			Rule	US		1974	only	-		Jan	6				2:00	1:00	D
			Rule	US		1975	only	-		Feb	23			2:00	1:00	D
			Rule	US		1976	1986	-		Apr	lastSun	2:00	1:00	D
			Rule	US		1987	2006	-		Apr	Sun>=1	2:00	1:00	D
			Rule	US		2007	max		-		Mar	Sun>=8	2:00	1:00	D
			Rule	US		2007	max		-		Nov	Sun>=1	2:00	0			S
	-->
                          <TIME_ZONE id="America/New_York" base_utc_offset="-5" display_name="Eastern Time" standard_display_name="Eastern Standard Time" daylight_display_name="Eastern Daylight Time">
                              <!--Standard US rules-->
                              <ADJUSTMENT_RULES>
                                  <ADJUSTMENT_RULE date_start="1967-01-01" date_end="1973-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="4" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1974-01-01" date_end="1974-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="1" day="6"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1975-01-01" date_end="1975-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="2" day="23"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1976-01-01" date_end="1986-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="4" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1987-01-01" date_end="2006-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="4" week="1" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="2007-01-01">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="3" week="2" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="11" week="1" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                              </ADJUSTMENT_RULES>
                          </TIME_ZONE>

                          <TIME_ZONE id="America/Chicago" base_utc_offset="-6" display_name="Central Time" standard_display_name="Central Standard Time" daylight_display_name="Central Daylight Time">
                              <!--Standard US rules-->
                              <ADJUSTMENT_RULES>
                                  <ADJUSTMENT_RULE date_start="1967-01-01" date_end="1973-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="4" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1974-01-01" date_end="1974-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="1" day="6"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1975-01-01" date_end="1975-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="2" day="23"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1976-01-01" date_end="1986-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="4" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1987-01-01" date_end="2006-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="4" week="1" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="2007-01-01">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="3" week="2" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="11" week="1" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                              </ADJUSTMENT_RULES>
                          </TIME_ZONE>

                          <TIME_ZONE id="America/Denver" base_utc_offset="-7" display_name="Mountain Time" standard_display_name="Mountain Standard Time" daylight_display_name="Mountain Daylight Time">
                              <!--Standard US rules-->
                              <ADJUSTMENT_RULES>
                                  <ADJUSTMENT_RULE date_start="1967-01-01" date_end="1973-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="4" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1974-01-01" date_end="1974-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="1" day="6"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1975-01-01" date_end="1975-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="2" day="23"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1976-01-01" date_end="1986-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="4" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1987-01-01" date_end="2006-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="4" week="1" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="2007-01-01">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="3" week="2" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="11" week="1" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                              </ADJUSTMENT_RULES>
                          </TIME_ZONE>

                          <TIME_ZONE id="America/Los_Angeles" base_utc_offset="-8" display_name="Pacific Time" standard_display_name="Pacific Standard Time" daylight_display_name="Pacific Daylight Time">
                              <!--Standard US rules-->
                              <ADJUSTMENT_RULES>
                                  <ADJUSTMENT_RULE date_start="1967-01-01" date_end="1973-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="4" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1974-01-01" date_end="1974-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="1" day="6"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1975-01-01" date_end="1975-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="2" day="23"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1976-01-01" date_end="1986-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="4" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1987-01-01" date_end="2006-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="4" week="1" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="2007-01-01">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="3" week="2" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="11" week="1" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                              </ADJUSTMENT_RULES>
                          </TIME_ZONE>

                          <TIME_ZONE id="America/Anchorage" base_utc_offset="-9" display_name="Alaska Time" standard_display_name="Alaska Standard Time" daylight_display_name="Alaska Daylight Time">
                              <!--Standard US rules-->
                              <ADJUSTMENT_RULES>
                                  <ADJUSTMENT_RULE date_start="1967-01-01" date_end="1973-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="4" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1974-01-01" date_end="1974-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="1" day="6"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1975-01-01" date_end="1975-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="2" day="23"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1976-01-01" date_end="1986-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="4" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1987-01-01" date_end="2006-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="4" week="1" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="2007-01-01">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="3" week="2" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="11" week="1" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                              </ADJUSTMENT_RULES>
                          </TIME_ZONE>

                          <TIME_ZONE id="Pacific/Honolulu" base_utc_offset="-10" display_name="Hawaii Time" standard_display_name="Hawaii-Aleutian Standard Time" daylight_display_name="Hawaii-Aleutian Daylight Time">
                              <ADJUSTMENT_RULES/>
                          </TIME_ZONE>

                          <TIME_ZONE id="America/Phoenix" base_utc_offset="-7" display_name="Mountain Standard Time - Arizona" standard_display_name="Mountain Standard Time" daylight_display_name="Mountain Daylight Time">
                              <!--From tzdata: Arizona observed DST in 1967, but Arizona Laws 1968, ch. 183 (effective 1968-03-21) repealed DST.-->
                              <ADJUSTMENT_RULES>
                                  <ADJUSTMENT_RULE date_start="1967-01-01" date_end="1968-03-21">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="4" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                              </ADJUSTMENT_RULES>
                          </TIME_ZONE>

                          <TIME_ZONE id="Europe/London" base_utc_offset="0" display_name="Greenwich Time" standard_display_name="Greenwich Mean Time" daylight_display_name="British Summer Time">
                              <!--Technically some of these are floating rules, but they are like "the day following the fourth Saturday" and so don't match the model of "First/Second/Third/Fourth/Last Sunday"-->
                              <!--
				2:00 standard = 3:00 wall in daylight time.
				1:00 universal = 1:00 standard = 2:00 wall in daylight time
			-->
                              <ADJUSTMENT_RULES>
                                  <ADJUSTMENT_RULE date_start="1967-01-01" date_end="1967-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <!--Sun>=19-->
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="3" day="19"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <!--Sun>=23-->
                                          <FIXED_DATE_RULE time_of_day="03:00:00" month="10" day="29"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1968-01-01" date_end="1968-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="2" day="18"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <!--Sun>=23-->
                                          <FIXED_DATE_RULE time_of_day="03:00:00" month="10" day="27"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>

                                  <!--No daylight time 1969-1971 inclusive-->

                                  <!---For 1972-1980, daylight time is Sun>=16 (third Sunday) in Mar to Sun>=23 ("the day following the fourth Saturday") in Oct-->
                                  <ADJUSTMENT_RULE date_start="1972-01-01" date_end="1972-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" week="3" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="03:00:00" month="10" day="29"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1973-01-01" date_end="1973-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" week="3" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="03:00:00" month="10" day="28"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1974-01-01" date_end="1974-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" week="3" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="03:00:00" month="10" day="27"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1975-01-01" date_end="1975-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" week="3" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="03:00:00" month="10" day="26"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1976-01-01" date_end="1976-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" week="3" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="03:00:00" month="10" day="24"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1977-01-01" date_end="1977-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" week="3" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="03:00:00" month="10" day="23"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1978-01-01" date_end="1978-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" week="3" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="03:00:00" month="10" day="29"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1979-01-01" date_end="1979-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" week="3" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="03:00:00" month="10" day="28"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1980-01-01" date_end="1980-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" week="3" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="03:00:00" month="10" day="26"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>

                                  <!---For 1981-1989, daylight time is last Sunday in Mar to Sun>=23 ("the day following the fourth Saturday") in Oct-->
                                  <ADJUSTMENT_RULE date_start="1981-01-01" date_end="1981-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="01:00:00" week="5" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="10" day="25"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1982-01-01" date_end="1982-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="01:00:00" week="5" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="10" day="24"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1983-01-01" date_end="1983-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="01:00:00" week="5" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="10" day="23"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1984-01-01" date_end="1984-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="01:00:00" week="5" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="10" day="28"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1985-01-01" date_end="1985-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="01:00:00" week="5" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="10" day="27"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1986-01-01" date_end="1986-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="01:00:00" week="5" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="10" day="26"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1987-01-01" date_end="1987-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="01:00:00" week="5" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="10" day="25"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1988-01-01" date_end="1988-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="01:00:00" week="5" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="10" day="23"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1989-01-01" date_end="1989-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="01:00:00" week="5" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="10" day="29"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>

                                  <!---For 1990-1995, daylight time is last Sunday in Mar to Sun>=22 ("the day following the fourth Saturday") in Oct-->
                                  <ADJUSTMENT_RULE date_start="1990-01-01" date_end="1990-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="01:00:00" week="5" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="10" day="28"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1991-01-01" date_end="1991-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="01:00:00" week="5" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="10" day="27"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1992-01-01" date_end="1992-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="01:00:00" week="5" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="10" day="25"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1993-01-01" date_end="1993-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="01:00:00" week="5" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="10" day="24"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1994-01-01" date_end="1994-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="01:00:00" week="5" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="10" day="23"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1995-01-01" date_end="1995-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="01:00:00" week="5" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="10" day="22"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>

                                  <!--EU rules from 1996 on-->
                                  <ADJUSTMENT_RULE date_start="1996-01-01">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="01:00:00" week="5" day_of_week="Sunday" month="3"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" week="5" day_of_week="Sunday" month="10"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                              </ADJUSTMENT_RULES>
                          </TIME_ZONE>

                          <!--
			based on the tzdata file in case 21943
			
			Rule	C-Eur	1945	only	-	Sep	16	 2:00s	0	-
			Rule	C-Eur	1977	1980	-	Apr	Sun>=1	 2:00s	1:00	S
			Rule	C-Eur	1977	only	-	Sep	lastSun	 2:00s	0	-
			Rule	C-Eur	1978	only	-	Oct	 1	 2:00s	0	-
			Rule	C-Eur	1979	1995	-	Sep	lastSun	 2:00s	0	-
			Rule	C-Eur	1981	max	-	Mar	lastSun	 2:00s	1:00	S
			Rule	C-Eur	1996	max	-	Oct	lastSun	 2:00s	0	- 
	-->
                          <TIME_ZONE id="Central Europe" base_utc_offset="1" display_name="Central European Time" standard_display_name="Central European Time" daylight_display_name="Central European Summer Time">
                              <ADJUSTMENT_RULES>
                                  <ADJUSTMENT_RULE date_start="1977-01-01" date_end="1977-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="4" week="1" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="9" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1978-01-01" date_end="1978-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="4" week="1" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FIXED_DATE_RULE time_of_day="02:00:00" month="10" day="1"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1979-01-01" date_end="1980-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="4" week="1" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="9" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1981-01-01" date_end="1995-12-31">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="3" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="9" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                                  <ADJUSTMENT_RULE date_start="1996-01-01">
                                      <DAYLIGHT_TRANSITION_START>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="3" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_START>
                                      <DAYLIGHT_TRANSITION_END>
                                          <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                      </DAYLIGHT_TRANSITION_END>
                                  </ADJUSTMENT_RULE>
                              </ADJUSTMENT_RULES>
                          </TIME_ZONE>
                          <TIME_ZONE id="Chamorro" base_utc_offset="10" display_name="Chamorro Time" standard_display_name="Chamorro Standard Time" daylight_display_name="Chamorro Daylight Time">
                              <ADJUSTMENT_RULES/>
                          </TIME_ZONE>
                      </TIME_ZONES>

        Return sResult
    End Function
#End Region

    Private Shared Function LoadTimeZone(ByVal psZone As String) As TimeZoneInfo
        Dim sTimeZoneID As String = psZone

        Dim oTimeZoneConfig As XDocument
        If False = TimeZoneLoader.IsTimeZoneConfigFileExists(_TimeZoneConfigPath) Then
            oTimeZoneConfig = getDefaultTimezoneConfig()
        Else
            Using oFile As IO.FileStream = IO.File.OpenRead(_TimeZoneConfigPath)
                Using oReader As New IO.StreamReader(oFile)
                    oTimeZoneConfig = Xml.Linq.XDocument.Load(oReader)
                End Using
            End Using
        End If

        Return TimeZoneLoader.LoadTimeZoneInfo(sTimeZoneID, oTimeZoneConfig)
    End Function

    Public Shared Function MapTimeZoneToID(ByVal peZone As Zone) As String
        Select Case peZone
            Case Zone.London
                Return "Europe/London"
            Case Zone.LosAngeles
                Return "America/Los_Angeles"
            Case Zone.Phoenix
                Return "America/Phoenix"
            Case Zone.UTC
                Return "Etc/UTC"
            Case Zone.NewYork
                Return "America/New_York"
            Case Zone.Chicago
                Return "America/Chicago"
            Case Zone.Denver
                Return "America/Denver"
            Case Zone.Anchorage
                Return "America/Anchorage"
            Case Zone.Honolulu
                Return "Pacific/Honolulu"
            Case Zone.CentralEurope
                Return "Central Europe"
            Case Zone.Chamorro
                Return "Chamorro"
            Case Else
                Throw New Exception("Unhandled time zone:" & peZone.ToString)
        End Select
    End Function

    Public Shared Function MapTimeZoneIDToZone(ByVal psZone As String) As Zone
        Select Case psZone
            Case "Europe/London"
                Return Zone.London
            Case "America/Los_Angeles"
                Return Zone.LosAngeles
            Case "America/Phoenix"
                Return Zone.Phoenix
            Case "Etc/UTC"
                Return Zone.UTC
            Case "America/New_York"
                Return Zone.NewYork
            Case "America/Chicago"
                Return Zone.Chicago
            Case "America/Denver"
                Return Zone.Denver
            Case "America/Anchorage"
                Return Zone.Anchorage
            Case "Pacific/Honolulu"
                Return Zone.Honolulu
            Case "Central Europe"
                Return Zone.CentralEurope
            Case "Chamorro"
                Return Zone.Chamorro
            Case Else
                Throw New Exception("Unhandled time zone:" & psZone)
        End Select
    End Function

    Public Shared Function GetTimeZoneAbbreviation(ByVal peZone As Zone, ByVal pbIsDaylightTime As Boolean) As String
        Select Case peZone
            Case Zone.London
                Return If(pbIsDaylightTime, "BST", "GMT")
            Case Zone.LosAngeles
                Return If(pbIsDaylightTime, "PDT", "PST")
            Case Zone.Phoenix, Zone.Denver
                Return If(pbIsDaylightTime, "MDT", "MST")
            Case Zone.UTC
                Return "UTC"
            Case Zone.NewYork
                Return If(pbIsDaylightTime, "EDT", "EST")
            Case Zone.Chicago
                Return If(pbIsDaylightTime, "CDT", "CST")
            Case Zone.Anchorage
                Return If(pbIsDaylightTime, "AKDT", "AKST")
            Case Zone.Honolulu
                Return If(pbIsDaylightTime, "HADT", "HAST")
            Case Zone.CentralEurope
                Return If(pbIsDaylightTime, "CEST", "CET")
            Case Zone.Chamorro
                Return "ChST"
            Case Else
                Throw New Exception("Unhandled time zone:" & peZone.ToString)
        End Select
    End Function
End Class
