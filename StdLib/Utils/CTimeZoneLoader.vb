﻿Option Strict On

Imports System.TimeZoneInfo
Imports StdLib.CBaseStd

Public Class CTimeZoneLoader
    Implements ITimeZoneLoader

    Public ReadOnly Property IsTimeZoneConfigFileExists(ByVal psTimeZoneConfigPath As String) As Boolean Implements ITimeZoneLoader.IsTimeZoneConfigFileExists
        Get
            If IO.File.Exists(psTimeZoneConfigPath) Then
                Return True
            End If
            Return False
        End Get
    End Property

    Public Function LoadTimeZoneInfo(ByVal psTimeZoneID As String, ByVal poTimeZoneConfig As XDocument) As TimeZoneInfo Implements ITimeZoneLoader.LoadTimeZoneInfo

        Dim oZones As IEnumerable(Of XElement) = From oNode As XElement In poTimeZoneConfig.<TIME_ZONES>.<TIME_ZONE> Where oNode.Attribute("id").Value = psTimeZoneID Select oNode
        If oZones.Count = 0 Then
            Throw New Exception("Cannot find Time Zone: " & psTimeZoneID)
        ElseIf oZones.Count > 1 Then
            Throw New Exception("Multiple Time Zones found for Time Zone ID: " & psTimeZoneID)
        End If

        Dim oZone As XElement = oZones(0)

        Dim oUTCOffset As New TimeSpan(SafeInteger(oZone.Attribute("base_utc_offset").Value), 0, 0)
        Dim sDisplayName As String = oZone.Attribute("display_name").Value
        Dim sStandardDisplayName As String = oZone.Attribute("standard_display_name").Value
        Dim sDaylightDisplayName As String = oZone.Attribute("daylight_display_name").Value

        Return CreateCustomTimeZone(psTimeZoneID, oUTCOffset, sDisplayName, sStandardDisplayName, sDaylightDisplayName, LoadAdjustmentRules(oZone))
    End Function

    Private Function LoadTransition(ByVal poDateRuleNode As XElement) As TransitionTime
        Select Case poDateRuleNode.Name
            Case "FLOATING_DATE_RULE"
                Dim oTimeOfDay As DateTime = ParseDate(poDateRuleNode.Attribute("time_of_day").Value).Value
                oTimeOfDay = New DateTime(1, 1, 1, oTimeOfDay.Hour, oTimeOfDay.Minute, oTimeOfDay.Second)

                Dim nMonth As Integer = SafeInteger(poDateRuleNode.Attribute("month").Value)
                Dim nWeek As Integer = SafeInteger(poDateRuleNode.Attribute("week").Value)
                Dim eDayOfWeek As DayOfWeek = DirectCast([Enum].Parse(GetType(DayOfWeek), poDateRuleNode.Attribute("day_of_week").Value), DayOfWeek)

                Return TransitionTime.CreateFloatingDateRule(oTimeOfDay, nMonth, nWeek, eDayOfWeek)
            Case "FIXED_DATE_RULE"
                Dim oTimeOfDay As DateTime = ParseDate(poDateRuleNode.Attribute("time_of_day").Value).Value
                oTimeOfDay = New DateTime(1, 1, 1, oTimeOfDay.Hour, oTimeOfDay.Minute, oTimeOfDay.Second)

                Dim nMonth As Integer = SafeInteger(poDateRuleNode.Attribute("month").Value)
                Dim nDay As Integer = SafeInteger(poDateRuleNode.Attribute("day").Value)

                Return TransitionTime.CreateFixedDateRule(oTimeOfDay, nMonth, nDay)
            Case Else
                Throw New Exception("Unexpected Date Rule Name")
        End Select
    End Function

    Private Function LoadAdjustmentRules(ByVal poZone As XElement) As AdjustmentRule()
        Dim oAdjustments As New List(Of AdjustmentRule)

        Dim oAdjustmentRuleNodes As IEnumerable(Of XElement) = poZone.<ADJUSTMENT_RULES>(0).Elements()

        For Each oRuleNode As XElement In oAdjustmentRuleNodes
            Dim oStartDateRuleNode As XElement = oRuleNode.<DAYLIGHT_TRANSITION_START>(0).Elements()(0)
            Dim oTransitionStart As TransitionTime = LoadTransition(oStartDateRuleNode)

            Dim oEndDateRuleNode As XElement = oRuleNode.<DAYLIGHT_TRANSITION_END>(0).Elements()(0)
            Dim oTransitionEnd As TransitionTime = LoadTransition(oEndDateRuleNode)

            Dim oDateStart As DateTime = ParseDate(oRuleNode.Attribute("date_start").Value).Value

            Dim oDateEnd As DateTime = DateTime.MaxValue.Date
            If oRuleNode.Attribute("date_end") IsNot Nothing Then
                oDateEnd = ParseDate(oRuleNode.Attribute("date_end").Value).Value
            End If

            oAdjustments.Add(AdjustmentRule.CreateAdjustmentRule(oDateStart, oDateEnd, New TimeSpan(1, 0, 0), oTransitionStart, oTransitionEnd))
        Next

        Return oAdjustments.ToArray()
    End Function

End Class
