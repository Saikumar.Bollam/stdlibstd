'Imports System.Web.UI
'Imports System.Web.UI.HtmlControls
'Imports System.Web.UI.WebControls
Imports System.Configuration
Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports System.Globalization
Imports System.Net
Imports System.Net.Http
Imports System.Reflection
Imports System.Text
Imports System.Text.RegularExpressions
Imports StdLib.Exceptions
Imports StdLib.SpecializedDataTypes

Public Class CBaseStd
	Protected Shared logger As log4net.ILog = log4net.LogManager.GetLogger(GetType(CBaseStd))



	''' <summary>
	''' Represents our 00000000-0000-0000-0000-000000000000 as a string that's easier to work with than
	''' Guid.Empty since most of our variables are declared as strings.
	''' </summary>
	Public Const EMPTY_GUID_STRING As String = "00000000-0000-0000-0000-000000000000"

	''' <summary>
	''' Represents an empty rule for underwriting purposes.
	''' </summary>
	''' <remarks></remarks>
	Public Const UNDERWRITE_NEGATIVE As Integer = -999999

	Public Enum eSetTypes
		eSetByText
		eSetByValue
	End Enum

	''' <summary>
	''' Returns # of days in a given month/year.   If no year is specified, the current year is assumed.
	''' </summary>
	<Obsolete("Use CDateUtils getDaysInMonth instead")>
	Public Shared Function getDaysInMonth(ByVal month As Integer, Optional ByVal year As Integer = -1) As Integer
		If year = -1 Then
			year = DateTimeStruct.DateNow.Year
		End If

		'add one month then subtract a day
		Return New Date(year, month, 1).AddMonths(1).AddDays(-1).Day
	End Function


	''' <summary>
	''' Returns nicer 32 char GUID w/ no '-'.  This routine used for older columns that use 32 char instead
	''' of the new uniqueidentifier datatypes.  
	''' </summary>
	Public Shared Function newGUID() As String
		Return System.Guid.NewGuid.ToString.Replace("-", "")
	End Function


	''' <summary>
	''' Takes in a 9 digit xxxxxxxxx SSN and format it into xxx-xx-xxxx format.  If cannot be converted to a perfect SSN, then a blank will be returned.
	''' </summary>
	''' <param name="sSSN">The cleaned up value (without hyphens) must be 9 digits;  otherwise, blank result will be returned.</param>
	Public Shared Function formatSSN(ByVal sSSN As String) As String
		If sSSN = "" Then
			Return ""
		Else
			sSSN = sSSN.Replace("-", "")
		End If

		If Len(sSSN) <> 9 Then
			'traceA("SSN cannot be formatted, it's not pure 9 digit format: " & sSSN)
			Return ""
		End If

		Dim SSNHead As String
		Dim SSNBody As String
		Dim SSNFoot As String

		SSNHead = Mid(sSSN, 1, 3)
		SSNBody = Mid(sSSN, 4, 2)
		SSNFoot = Mid(sSSN, 6, 4)

		Return SSNHead & "-" & SSNBody & "-" & SSNFoot
	End Function


	''' <summary>
	''' Overload that allows for handling of defaults.  Usefull for handling when year =0  means blank instead of 2000.
	''' </summary>
	''' <param name="pnYear"></param>
	''' <param name="pnEmptyYearValue">If pnYear = pnEmptyYearValue, then blank string will be returned.</param>
	Public Overloads Shared Function FormatYear(ByVal pnYear As Integer, ByVal pnEmptyYearValue As Integer) As String
		If pnYear = pnEmptyYearValue Then
			Return ""
		ElseIf safeYear(pnYear) <> 0 Then
			Return safeYear(pnYear).ToString
		Else
			Return ""
		End If
	End Function

	''' <summary>
	''' This routine is designed to make sure our year makes sense.  This routine handles guessing from 2 digit years, but it's recommended
	''' the client app not store things in 2 digits to avoid future problems.  2 digit Years from 00-15 will be prefixed by 20.  Anything after that
	''' is prefixed by 19 (ie: 98 -> 1998).  Any years before 1800 or after now + 100 is considered invalid year.
	''' If an invalid year is encountered, a 0 will be returned.
	''' </summary>
	''' <param name="pnYear"></param>
	''' <remarks>
	''' Be carefull if your year is stored as integer.   Defaults for integer is 0, and calling safeYear(0)  will yield 2000!
	''' </remarks>
	Public Shared Function safeYear(ByVal pnYear As Integer) As Integer
		If pnYear >= 0 AndAlso pnYear <= 15 Then
			Return pnYear + 2000
		ElseIf pnYear > 15 AndAlso pnYear <= 99 Then
			Return pnYear + 1900
		ElseIf pnYear >= 1800 And pnYear <= DateTimeStruct.DateNow.Year + 100 Then
			Return pnYear
		Else
			Return 0
		End If
	End Function

#Region "getSQLDateRange"



	'********************************************************************************
	'* Name: getSQLDateRange
	'*
	'* Description: returns partial SQL query statement that retrieves records between sStartDate and sEndDate (invlusive!)
	'* Parameters: sFieldName - required string specifying field name
	'*             sStartDate - optional starting date ;  empty/invalid sStartDate omits Starting date from SQL query
	'*             sEndDate   - optional ending date   ;  empty/invalid sStartDate omits ending date from SQL query
	'*             DBType     -  "DB_ACCESS" OR "DB_ODBC"
	'* Sample Usage: ? getSQLDateRange("DateCreated", "6/29/00", "7/3/00", 0) -> (DateCreated >= '6/29/00' AND DateCreated < '07/04/00')
	'*               ? getSQLDateRange("DateCreated", "6/29/00", "", 0)       ->(DateCreated >= '6/29/00')
	'*               ? getSQLDateRange("DateCreated", "", "", 0)              -> (blank)
	'*               ? getSQLDateRange("DateCreated", "", "2/2/00", 0)        -> (DateCreated < '2/3/00')
	'* Created: 7/5/00 1:04:54 PM
	'********************************************************************************
	Public Enum eDBTypes
		eDBType_ODBC
		eDBType_ACCESSS
	End Enum
	Public Overloads Shared Function getSQLDateRange(ByVal sFieldName As String, ByVal sStartDate As String, ByVal sEndDate As String, Optional ByVal DBType As eDBTypes = eDBTypes.eDBType_ODBC) As String

		Dim dStartDate As Date
		Dim dEndDate As Date

		If sStartDate = "" Then
			dStartDate = #1/1/1000#
		Else
			dStartDate = CDate(sStartDate)
		End If

		If sEndDate = "" Then
			dEndDate = #1/1/9999#
		Else
			dEndDate = CDate(sEndDate)
		End If

		Return getSQLDateRange(sFieldName, dStartDate, dEndDate, DBType)
	End Function

	Public Overloads Shared Function getSQLDateRange(ByVal sFieldName As String, Optional ByVal sStartDate As Date = #1/1/1000#, Optional ByVal sEndDate As Date = #1/1/9999#, Optional ByVal DBType As eDBTypes = eDBTypes.eDBType_ODBC) As String
		CAssert.IsTrue(DBType = eDBTypes.eDBType_ODBC Or DBType = eDBTypes.eDBType_ACCESSS, "Invalid DBType:" & DBType)

		Dim sSQL As String = ""
		Dim sDelim As String = CStr(IIf(DBType = eDBTypes.eDBType_ACCESSS, "#", "'"))

		If sStartDate <> #1/1/1000# Then
			sSQL = sFieldName & " >= " & sDelim & FormatDateTime(sStartDate, DateFormat.ShortDate) & sDelim
		End If

		If sEndDate <> #1/1/9999# Then
			If Len(sSQL) > 0 Then
				sSQL = sSQL & " AND " & sFieldName & " < " & sDelim & FormatDateTime(sEndDate.AddDays(1), DateFormat.ShortDate) & sDelim
			Else
				sSQL = sSQL & sFieldName & " < " & sDelim & FormatDateTime(sEndDate.AddDays(1), DateFormat.ShortDate) & sDelim
			End If
		End If

		If Len(sSQL) > 0 Then
			sSQL = "(" & sSQL & ")"
		End If

		Return sSQL
	End Function

	Public Overloads Shared Function getSQLDateTimeRange(ByVal sFieldName As String, ByVal sStartDate As String, ByVal sEndDate As String, Optional ByVal DBType As eDBTypes = eDBTypes.eDBType_ODBC) As String

		Dim dStartDate As Date
		Dim dEndDate As Date

		If sStartDate = "" Then
			dStartDate = #1/1/1000#
		Else
			dStartDate = CDate(sStartDate)
		End If

		If sEndDate = "" Then
			dEndDate = #1/1/9999#
		Else
			dEndDate = CDate(sEndDate)
		End If

		Return getSQLDateTimeRange(sFieldName, dStartDate, dEndDate, DBType)
	End Function

	Public Overloads Shared Function getSQLDateTimeRange(ByVal sFieldName As String, Optional ByVal sStartDate As Date = #1/1/1000#, Optional ByVal sEndDate As Date = #1/1/9999#, Optional ByVal DBType As eDBTypes = eDBTypes.eDBType_ODBC) As String
		CAssert.IsTrue(DBType = eDBTypes.eDBType_ODBC Or DBType = eDBTypes.eDBType_ACCESSS, "Invalid DBType:" & DBType)

		Dim sSQL As String = ""
		Dim sDelim As String = CStr(IIf(DBType = eDBTypes.eDBType_ACCESSS, "#", "'"))

		If sStartDate <> #1/1/1000# Then
			sSQL = sFieldName & " >= " & sDelim & FormatDateTime(sStartDate, DateFormat.GeneralDate) & sDelim
		End If

		If sEndDate <> #1/1/9999# Then
			'If the time portion of the end date is not set, use the next day of end date as end date
			If sEndDate.Hour = 0 AndAlso sEndDate.Minute = 0 AndAlso sEndDate.Second = 0 Then
				sEndDate = sEndDate.AddDays(1)
			End If

			sSQL = sSQL & If(Len(sSQL) > 0, " AND ", "") & sFieldName & " < " & sDelim & FormatDateTime(sEndDate, DateFormat.GeneralDate) & sDelim
		End If

		If Len(sSQL) > 0 Then
			sSQL = "(" & sSQL & ")"
		End If

		Return sSQL
	End Function
#End Region

#Region "SQLDate - checks to ensure the data we're attempting to save is valid and savable"
	Public Overloads Shared Function SQLDate(ByVal sDate As String) As String
		If IsDate(sDate) = False Then
			Return "NULL"
		End If

		Return SQLDate(CDate(sDate))

	End Function
	Public Overloads Shared Function SQLDate(ByVal theDate As Date) As String
		If theDate = Date.MinValue Then
			Return "NULL"
		ElseIf theDate < #1/1/1753# Or theDate > #12/31/9999# Then
			'check date within range for sql server.  Anything beyond this is probably dumb anyways 
			Return "NULL"
		Else
			Return SQLString(theDate)
		End If
	End Function

#End Region

#Region "SQLString overloads"
	''' <summary>
	''' only used when fine control is needed  over how a string should be escaped w/ or w/o trimming
	''' avoid using if possiblel
	''' </summary>	
	Public Overloads Shared Function SQLString(ByVal s As String, ByVal pbTrim As Boolean) As String
		If s = "" Then
			Return "''"
		End If
		If pbTrim Then
			Return "'" & s.Trim().Replace("'", "''") & "'"
		Else
			Return "'" & s.Replace("'", "''") & "'"
		End If
	End Function

	Public Overloads Shared Function SQLString(ByVal s As Object) As String
		If TypeOf s Is Guid Then
			Return SQLGuid(CType(s, Guid)) 'this will avoid our warning traces from safestring
		Else
			Return "'" & SafeString(s).Replace("'", "''") & "'"
		End If

	End Function
#End Region

	''' <summary>
	''' Escapes backslashes and single quotes, and then wraps the result in single quotes.
	''' </summary>
	''' <param name="psString"></param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Shared Function JSString(ByVal psString As String) As String
		If psString Is Nothing Then
			Return "null"
		End If
		Return "'" & psString.Replace("\", "\\").Replace("'", "\'").Replace(vbLf, "\n").Replace(vbCr, "\r") & "'"
	End Function

	Public Shared Function JSString(ByVal poObject As Object) As String
		If poObject Is Nothing Then
			Return "null"
		End If
		Return JSString(SafeString(poObject))
	End Function

	Public Shared Function JSBool(ByVal pValue As Boolean?) As String
		If Not pValue.HasValue Then
			Return "null"
		End If
		Return IIfStrict(pValue.Value, "true", "false")
	End Function

#Region "SQLGuid"
	''' <summary>
	''' Convenience routine that's the equivalent of SQLString but is safer when working with native guids.  Result will be in hyphenated format.  
	''' </summary>    
	Public Overloads Shared Function SQLGuid(ByVal value As Guid) As String
		Return "'" & value.ToString() & "'"
	End Function

	''' <summary>
	''' Convenience routine that will force a blank string to -> 000-00-0000000
	''' </summary>    
	Public Overloads Shared Function SQLGuid(ByVal value As String) As String
		If value Is Nothing OrElse value = "" Then
			Return SQLGuid(Guid.Empty)
		Else
			Return SQLGuid(New Guid(value))
		End If
	End Function

	''' <summary>
	''' Similar to SQLGuid, but used in filter expressions which may be picky about having a Guid type rather than a String.
	''' </summary>
	''' <param name="poValue"></param>
	''' <returns>A safe quoted Guid string wrapped in a CONVERT call.</returns>
	''' <remarks></remarks>
	Public Shared Function SQLGuidConvert(ByVal poValue As Guid) As String
		Return "CONVERT(" & SQLGuid(poValue) & ", 'System.Guid')"
	End Function

	''' <summary>
	''' Convenience version of SQLGuidConvert that automatically converts the String to a Guid for you.
	''' </summary>
	Public Shared Function SQLGuidConvert(ByVal psValue As String) As String
		Return SQLGuidConvert(SafeGUID(psValue))
	End Function
#End Region

#Region "Format<DataTypes>"

#Region "FormatGuid"


	Public Enum GUIDFormats
		''' <summary>
		''' 32 digits: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.  Usefull for legacy fields that used 32 char instead of native guid.
		''' </summary>		
		DigitsOnly
		''' <summary>
		''' 32 digits separated by hyphens (THIS IS THE FORMAT USED BY DB):  xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx 
		''' </summary>		
		Hyphen
		''' <summary>
		''' 32 digits separated by hyphens, enclosed in brackets: {xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx} 
		''' </summary>		
		HyphenAndBrackets
		''' <summary>
		''' 32 digits separated by hyphens, enclosed in parentheses: (xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx) 
		''' </summary>		
		HyphenAndParenthesis
	End Enum

	''' <summary>
	''' Takes a GUID datatype and converts it into a string.
	''' </summary>
	''' <param name="pGUID"></param>
	''' <param name="pFormatType">If true, then it will remove all dashs, brackets, parenthesis from guid.  
	''' Otherwise, will return as 32 digit seperated by hyphens.
	''' </param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Overloads Shared Function FormatGuid(ByVal pGUID As Object, ByVal pFormatType As GUIDFormats) As String
		If TypeOf pGUID Is DBNull Then
			Return ""
		End If

		Dim theGUID As Guid = Guid.Empty
		If TypeOf pGUID Is String Then
			If CStr(pGUID) = "" Then
				Return ""
			Else
				theGUID = New Guid(CStr(pGUID))
			End If

		Else
			theGUID = CType(pGUID, Guid)
		End If


		Select Case pFormatType
			Case GUIDFormats.DigitsOnly
				Return theGUID.ToString("N")
			Case GUIDFormats.Hyphen
				Return theGUID.ToString("D")
			Case GUIDFormats.HyphenAndBrackets
				Return theGUID.ToString("B")
			Case GUIDFormats.HyphenAndParenthesis
				Return theGUID.ToString("P")
			Case Else
				Return theGUID.ToString("D")
		End Select
	End Function
#End Region


	Public Overloads Shared Function FormatDateSimple(ByVal d As Date, Optional ByVal showType As safeDatePortions = safeDatePortions.eDate) As String
		If d = Date.MinValue Then
			Return ""
		Else
			Select Case showType
				Case safeDatePortions.eDate
					Return d.ToShortDateString
				Case safeDatePortions.eTime
					Return d.ToShortTimeString
				Case safeDatePortions.eBoth
					Return d.ToString
			End Select
		End If

		Return ""
	End Function


	Public Overloads Shared Function FormatDateSimple(ByVal d As Nullable(Of Date), Optional ByVal showType As safeDatePortions = safeDatePortions.eDate) As String

		If False = d.HasValue Then
			Return ""
		End If

		Return FormatDateSimple(d.Value, showType)
	End Function


	'Robustness routine to handle format dates
	Public Overloads Shared Function FormatDateSimple(ByVal d As DateStruct) As String
		If d = DateStruct.MinValue Then
			Return ""
		Else
			Return d.ToString()
		End If
	End Function

	Public Overloads Shared Function FormatDateSimple(ByVal d As Nullable(Of DateStruct)) As String
		If False = d.HasValue Then
			Return ""
		Else
			Return d.Value.ToString
		End If
	End Function


#End Region



#Region "Safe<DataType> Functions - Note we generally have at least 1 overload to support the DBNull case because when working with dataset, the dataset will return a DBNULL datatype instead of Object."


#Region "Depracated Routines - Various junk to be simply nuked after ccnet detects no longer used "
	''' <summary>
	''' Lets you safely pull out a date into a specific format.
	''' </summary>    
	<Obsolete("Use FormatDate or SafeDate")> Public Overloads Shared Function SafeFormatDateTime(ByVal d As Date, ByVal dateFormat As DateFormat) As String
		Return FormatDateTime(d, dateFormat)
	End Function

	''' <summary>
	''' Null overload for convenience so calling client doesn't need to know what function to call.
	''' </summary>
	<Obsolete("Use FormatDate or SafeDate")> Public Overloads Shared Function SafeFormatDateTime(ByVal oNull As DBNull, ByVal dateFormat As DateFormat) As String
		Return ""
	End Function

	''' <summary>
	''' Alias of ParseDate for consistency and IntelliSense convenience.
	''' </summary>
	<Obsolete("Please use ParseDate")> Public Shared Function SafeDateNullable(ByVal pValue As String) As Nullable(Of Date)
		Return ParseDate(pValue)
	End Function

	<Obsolete("Please use ConvertToDSDate")> Public Shared Function SafeDate4DataSet(ByVal pDate As Nullable(Of Date)) As Object
		If DBUtils.CSQLParamValue.IsValidDate(pDate) Then
			Return pDate.Value
		End If

		Return System.Convert.DBNull
	End Function
#End Region

#Region "Gibberish XML Utils function that shoulda been in CXMLUtils, or client code should have used CXmlElement"


	'depracted - use CXmlElement
	Public Overloads Shared Function SafeInnerText(ByVal poNode As Xml.XmlNode) As String
		If poNode Is Nothing Then
			Return ""
		End If
		Return poNode.InnerText
	End Function
	'depracted - use CXmlElement
	Public Overloads Shared Function SafeInnerText(ByVal poElement As Xml.XmlElement) As String
		If poElement Is Nothing Then
			Return ""
		End If
		Return poElement.InnerText
	End Function

#End Region


#Region "Depracated but kept for now: SafeString Overloads to handle old style dates stored as strings"
	Public Enum safeDatePortions
		eDate
		eTime
		eBoth
	End Enum
#End Region

	Private Shared _SafeDateRegEx As New RegularExpressions.Regex("(\d{1,2}[-,/]){2}\d{2,4}")
	''' <summary>
	''' Takes a given string and will return it only if it's a logically formatted and valid date.  Valid dates:
	''' 1. Greater than Date.MinValue are considered valid.
	''' 2. 01/01/2007, 1/1/2007, 1/1/07 are all valid 
	''' If sDate can be determined to be a valid date, then sDate will be returned as is, preserving any time portion.
	''' </summary>
	''' <remarks> 
	''' This routine useful mainly for OLD date storage style whereby date was stored as string. 
	''' </remarks>
	Public Shared Function SafeDate(ByVal sDate As String) As String
		sDate = SafeString(sDate)

		'Dim match As RegularExpressions.Match = regex.Match(sDate)

		If IsDate(sDate) = False OrElse CDate(sDate) = Date.MinValue OrElse _SafeDateRegEx.IsMatch(sDate) = False Then
			Return ""
		Else
			Return sDate
		End If
	End Function

	''' <summary>
	''' Wrapper of CDate and speficy the timezone info as UTC
	''' </summary>
	''' <param name="pDate"></param>
	''' <returns>Date type in UTC</returns>
	Public Shared Function CDateUTC(ByVal pDate As Date) As DateTime
		Return pDate.ToUniversalTime()
	End Function

	Public Shared Function CDateUTC(ByVal pDate As String) As DateTime
		Return DateTimeOffset.Parse(pDate).UtcDateTime
	End Function

	<Obsolete("This can potentially mask errors. Use ParseDate instead.", True)>
	Public Shared Function SafeDateTime(ByVal pDateTime As String) As Nullable(Of DateTime)
		Return ParseDate(pDateTime).GetValueOrDefault()
	End Function

	<Obsolete("This can potentially mask errors. Use ParseDate instead.", True)>
	Public Shared Function SafeDateTime(ByVal pDateTime As Object) As Nullable(Of DateTime)
		Return ParseDate(pDateTime).GetValueOrDefault()
	End Function

	<Obsolete("This can potentially mask errors. Use ParseDateStruct instead.", True)>
	Public Shared Function SafeDateStruct(ByVal pDate As String) As DateStruct
		Return ParseDateStruct(pDate).GetValueOrDefault()
	End Function

	<Obsolete("This can potentially mask errors. Use ParseDateStruct instead.", True)>
	Public Shared Function SafeDateStruct(ByVal pDate As Object) As DateStruct
		Return ParseDateStruct(pDate).GetValueOrDefault()
	End Function

	''' <summary>
	''' Returns a given date in the specified format.  Please note if the optional parameter is not specified 
	''' and 1st paramater is of type object that holds a Date type, the client will end up invoking 
	''' SafeString(Object) instead of this routine!  For that reason, it's best to always specify the 2nd parameter.
	''' </summary>
	''' <remarks> 
	''' This routine useful mainly for OLD date storage style whereby date was stored as string. 
	''' </remarks>
	Public Overloads Shared Function SafeString(ByVal d As Date, Optional ByVal showType As safeDatePortions = safeDatePortions.eDate) As String
		If d = Date.MinValue Then
			Return ""
		Else
			Select Case showType
				Case safeDatePortions.eDate
					Return d.ToShortDateString
				Case safeDatePortions.eTime
					Return d.ToShortTimeString
				Case safeDatePortions.eBoth
					Return d.ToString
			End Select
		End If

		Return ""
	End Function

	''' <summary>
	''' Overload to handle dbnulls.
	''' </summary>
	''' <returns></returns>
	''' <remarks> 
	''' This routine useful mainly for OLD date storage style whereby date was stored as string. 
	''' </remarks>
	Public Overloads Shared Function SafeString(ByVal dNull As DBNull, ByVal showType As safeDatePortions) As String
		Return ""
	End Function

	''' <summary>
	''' Convenience routine for reading dates out of a row with explicit formating option.  
	''' </summary>
	''' <remarks>
	''' Useful for case when we store dates as string, but should be avoided if FormatDate!	
	''' </remarks>
	Public Overloads Shared Function SafeString(ByVal d As Object, ByVal showType As safeDatePortions) As String
		If IsDate(d) = False Then
			Return ""
		ElseIf CDate(d) = Date.MinValue Then
			Return ""
		Else
			Select Case showType
				Case safeDatePortions.eDate
					Return CDate(d).ToShortDateString
				Case safeDatePortions.eTime
					Return CDate(d).ToShortTimeString
				Case safeDatePortions.eBoth
					Return CDate(d).ToString
				Case Else
					Return d.ToString
			End Select
		End If
	End Function


#End Region 'depracted SafeStrings

#Region "SafeString"


	Public Overloads Shared Function Safestring(ByVal oNull As DBNull) As String
		Return ""
	End Function

	''' <summary>
	''' This is the ultimate SafeString that gets used when reading data out of a dataset, 
	''' since most of the time the data is of type Object.
	''' </summary>
	''' <remarks>
	''' Note for GUID, it will return in the form of xxx-xx-xxxxx with hyphens.   However, if working with GUID, you should probably consider using the guid datatype to start with and use SafeGuid!
	''' </remarks>
	Public Overloads Shared Function SafeString(ByVal s As Object) As String
		' special case to handle Recordset Field objects
		If s Is Nothing OrElse TypeOf s Is DBNull Then
			Return ""
			'ElseIf TypeOf s Is System.Web.UI.WebControls.ListItem Then
			'    Return CType(s, System.Web.UI.WebControls.ListItem).Value
		ElseIf TypeOf s Is Guid Then
			Return s.ToString() 'this will return result with hyphen (ie: xxx-xx-xxxx etc. )
		Else
			Const tab As Char = CChar(vbTab)
			'Trim all space/tabs from front and back
			'Unlike CStr, .ToString() will append 12:00:00 AM for dates even when no time portion is set, and will return an Enum constant's name rather than its underlying value
			Return CStr(s).Trim(" "c, tab)
		End If
	End Function

	Public Shared Function SafeBytes(ByVal s As Object) As Byte()
		If s Is Nothing OrElse s Is DBNull.Value Then
			Return New Byte() {}
		Else
			Return CType(s, Byte())
		End If
	End Function
#End Region

#Region "SafeStringGuid (not recommended;  should use safeGuid and store as guid (instead of string) if possible)"
	''' <summary>
	''' DO NOT USE THIS ANYMORE.  You should store a guid as a guid! 
	''' Routine used to translate good/bad guid for use as string (OLD style).   Equivalent of calling SafeGuid(s).ToString().  This is the preferred method when using with GUID16 as it will not blank out 
	''' GUID's into blanks.  Guid objects will be returned in it's native format with hyphens.  Non-guid's will return a EMPTY_GUID_STRING (0's with hyphens).
	''' </summary>
	''' <remarks>
	''' Use this instead of SafeString if you need to ensure that the return type is a valid guid string.  SafeString ( dbnull) will always return a blank string,
	''' which is not a valid GUID.  For native guids, it's recommended to use safeGuid and store the data as a native guid.
	''' </remarks>
	Public Overloads Shared Function SafeStringGuid(ByVal s As Object) As String
		' special case to handle Recordset Field objects
		If s Is Nothing OrElse TypeOf s Is DBNull Then
			Return EMPTY_GUID_STRING
		ElseIf TypeOf s Is Guid Then
			'need this b/c cstr(guid) will fail! 
			Return s.ToString() 'this will return result with hyphen (ie: xxx-xx-xxxx etc. )

		Else

			Try
				Return (New Guid(Trim(s.ToString()))).ToString()
			Catch ex As Exception
				Return EMPTY_GUID_STRING
			End Try

			'Const tab As Char = CChar(vbTab)
			''trim all space/tabs from front and back 
			'Return CStr(s).Trim(" "c, tab) 'very odd: if u return trim(s.toString()), then for dates, it will append 12:00:00 AM if there is no time :-T
		End If
	End Function
#End Region

#Region "SafeLong"


	Public Overloads Shared Function SafeLong(ByVal oNull As DBNull) As Long
		Return 0
	End Function

	Public Overloads Shared Function SafeLong(ByVal o As Object) As Long
		Return SafeLong(SafeDecimal(o))
	End Function

	Public Overloads Shared Function SafeLong(ByVal s As String) As Long
		Return SafeLong(SafeDecimal(s)) 'Use SafeDecimal since SafeDouble doesn't have enough precision for some Long values
	End Function

	Public Overloads Shared Function SafeLong(ByVal d As Decimal) As Long
		Dim nResult As Long
		If d > Long.MaxValue Then
			'log.Debug("Returning 0 because number is larger than int max value: " & d)
			nResult = 0
		ElseIf d < Long.MinValue Then
			'log.Debug("Returning 0 because number is smaller than int min value: " & d)
			nResult = 0
		Else
			nResult = CType(CBaseStd.Round(d), Long)
		End If

		Return nResult
	End Function
#End Region

#Region "SafeInteger"


	Public Overloads Shared Function SafeInteger(ByVal oNull As DBNull) As Long
		Return 0
	End Function

	''' <summary>
	''' Convert to an integer.
	''' </summary>
	''' <param name="d"></param>
	''' <returns>Value of param rounded to integer.</returns>
	''' <remarks>Rounds away from zero at midpoint.</remarks>
	Public Overloads Shared Function SafeInteger(ByVal d As Double) As Integer
		Dim nResult As Integer
		If d > Integer.MaxValue Then
			'log.Debug("Returning 0 because number is larger than int max value: " & d)
			nResult = 0
		ElseIf d < Integer.MinValue Then
			'log.Debug("Returning 0 because number is smaller than int min value: " & d)
			nResult = 0
		ElseIf Double.IsNaN(d) Then
			nResult = 0
		Else
			nResult = CType(CBaseStd.Round(d), Integer)
		End If

		Return nResult
	End Function

	''' <summary>
	''' Convert to an integer.
	''' </summary>
	''' <param name="o"></param>
	''' <returns>Integer if param can be converted, or 0 if not.</returns>
	''' <remarks>Rounds away from zero at midpoint.</remarks>
	Public Overloads Shared Function SafeInteger(ByVal o As Object) As Integer
		Dim d As Double = SafeDouble(o)

		Return SafeInteger(d)
	End Function
#End Region


#Region "SafeDouble"


	Public Const DOUBLE_STYLE As System.Globalization.NumberStyles = System.Globalization.NumberStyles.Currency And Not (System.Globalization.NumberStyles.AllowParentheses Or System.Globalization.NumberStyles.AllowTrailingSign)
	Public Shared Function SafeDouble(ByVal s As String) As Double
		Dim d As Double = 0

		If SafeString(ConfigurationManager.AppSettings("VB_DOUBLE_CONVERSION")) = "Y" AndAlso IsNumeric(s) Then
			'Delete this section if the app setting is no longer relevant
			d = CDbl(s)
		Else
			Double.TryParse(s, DOUBLE_STYLE, Nothing, d)
		End If

		Return d
	End Function

	Public Shared Function SafeDouble(ByVal o As Object) As Double
		Dim s As String = SafeString(o)

		Return SafeDouble(s)
	End Function
#End Region

#Region "SafeDecimal"
	''' <summary>
	''' Converts from String to Decimal.  This is for when we need more precision than a Double can provide.
	''' </summary>
	Public Shared Function SafeDecimal(ByVal s As String) As Decimal
		Dim d As Decimal = 0

		If SafeString(ConfigurationManager.AppSettings("VB_DOUBLE_CONVERSION")) = "Y" AndAlso IsNumeric(s) Then
			'Delete this section if the app setting is no longer relevant
			d = CDec(s)
		Else
			Decimal.TryParse(s, DOUBLE_STYLE, Nothing, d)
		End If

		Return d
	End Function

	Public Shared Function SafeDecimal(ByVal o As Object) As Decimal
		Return SafeDecimal(SafeString(o))
	End Function

	Public Shared Function SafeDecimal(ByVal poNull As DBNull) As Decimal
		Return 0
	End Function
#End Region

#Region "SafeGuid"
	''' <summary>
	''' Robust routine to guarantee a guid is returned.  If value is blank or non-guid compatible, a guid.empty will be returned.  
	''' This routine most useful when working with datasets.
	''' </summary>
	''' <param name="value"></param>
	Public Shared Function SafeGUID(ByVal value As Object) As Guid
		If IsNothing(value) Then
			Return Guid.Empty
		ElseIf TypeOf value Is DBNull Then
			Return Guid.Empty
		ElseIf TypeOf value Is Guid Then
			Return CType(value, Guid) 'faster than trying to parse out the guid in the else clause
		ElseIf value.ToString() = String.Empty Then
			Return Guid.Empty
		Else
			Try
				Dim tempguid As String = Trim(value.ToString).Replace("-", "").Replace("{", "").Replace("}", "")
				If tempguid.Length = 32 Then
					Return New Guid(tempguid)
				Else
					Return Guid.Empty
				End If
			Catch ex As Exception
				Return Guid.Empty
			End Try
		End If
	End Function
#End Region

#Region "SafeSanitizedString"
	''' <summary>
	''' Use this existing .NET library for the sanitazation.
	''' Default True for param bUsedNameEntity to use html 4.0 named entity
	''' Noted that this method would also sanitize &, single quote and double quote marks
	''' Devs need to be awared of the situation whereby this method might affect the output, eg, some Of the thirdParty's response might contain single or double quotes
	''' If have to sanitize the field while preserving quote marks, the inner func 'replace' would be needed.
	''' </summary>

	Public Shared Function SafeSanitizedString(ByVal psUnclean As String, Optional ByVal pbUseNameEntity As Boolean = True) As String
		Dim sResult As String

		' sResult = AntiXss.AntiXssEncoder.HtmlEncode(sResult, pbUseNameEntity)
		sResult = WebUtility.HtmlEncode(SafeString(psUnclean))

		Return sResult
	End Function

	Public Shared Function SafeSanitizedString(ByVal poUnclean As Object, Optional ByVal pbUseNameEntity As Boolean = True) As String
		Dim sResult As String = SafeString(poUnclean)

		' sResult = AntiXss.AntiXssEncoder.HtmlEncode(sResult, pbUseNameEntity)
		sResult = WebUtility.HtmlEncode(SafeString(poUnclean))

		Return sResult
	End Function


#End Region



#Region "Parse<dataTypes> used to support data extraction with null support"
#Region "ParseInteger"
	Public Const INTEGER_STYLE As System.Globalization.NumberStyles = DOUBLE_STYLE And Not System.Globalization.NumberStyles.AllowDecimalPoint
	''' <summary>
	''' Parse an integer from a string.
	''' </summary>
	''' <returns>The integer parsed from the string, or null if the string's value is not an integer or the string is null or empty.</returns>
	''' <remarks>This uses Integer.TryParse.</remarks>
	Public Shared Function ParseInteger(ByVal psValue As String) As Nullable(Of Integer)
		If String.IsNullOrEmpty(psValue) Then
			Return Nothing
		End If

		Dim i As Integer
		If Integer.TryParse(psValue, INTEGER_STYLE, Nothing, i) Then
			Return i
		End If

		Return Nothing
	End Function

	''' <summary>
	''' This overload used frequently when working with datasets; DBNulls returned from datasets as objects will fall into here and yield a null as well
	''' </summary>
	Public Shared Function ParseInteger(ByVal poValue As Object) As Nullable(Of Integer)
		If TypeOf poValue Is DBNull OrElse poValue Is Nothing Then
			Return Nothing
		End If

		Return ParseInteger(poValue.ToString)
	End Function
#End Region

#Region "ParseLong"
	''' <summary>
	''' Parse a long integer from a string.
	''' </summary>
	''' <returns>The long integer parsed from the string, or null if the string's value is not an integer or the string is null or empty.</returns>
	''' <remarks>This uses Long.TryParse.</remarks>
	Public Shared Function ParseLong(ByVal psValue As String) As Nullable(Of Long)
		If String.IsNullOrEmpty(psValue) Then
			Return Nothing
		End If

		Dim l As Long
		If Long.TryParse(psValue, l) Then
			Return l
		End If

		Return Nothing
	End Function

	''' <summary>
	''' This overload used frequently when working with datasets; DBNulls returned from datasets as objects will fall into here and yield a null as well
	''' </summary>
	Public Shared Function ParseLong(ByVal poValue As Object) As Nullable(Of Long)
		If TypeOf poValue Is DBNull OrElse poValue Is Nothing Then
			Return Nothing
		End If

		Return ParseLong(poValue.ToString)
	End Function

#End Region

#Region "ParseDouble"
	''' <summary>
	''' Parse a double from a string.
	''' </summary>
	''' <returns>The double parsed from the string, or null if the string's value is not a double or the string is null or empty.</returns>
	Public Shared Function ParseDouble(ByVal psValue As String) As Nullable(Of Double)
		If String.IsNullOrEmpty(psValue) Then
			Return Nothing
		End If

		Dim d As Double
		If Double.TryParse(psValue, DOUBLE_STYLE, Nothing, d) Then
			Return d
		End If

		Return Nothing
	End Function

	''' <summary>
	''' This overload used frequently when working with datasets; DBNulls returned from datasets as objects will fall into here and yield a null as well
	''' </summary>
	Public Shared Function ParseDouble(ByVal poValue As Object) As Nullable(Of Double)
		If TypeOf poValue Is DBNull OrElse poValue Is Nothing Then
			Return Nothing
		End If

		Return ParseDouble(poValue.ToString)
	End Function
#End Region

#Region "ParseDecimal"
	''' <summary>
	''' Parse a Decimal from a String.
	''' </summary>
	''' <returns>The Decimal parsed from the String, or null if the String's value is not a Decimal or the String is null or empty.</returns>
	''' <remarks>Use this when the precision may be higher than a Double allows.</remarks>
	Public Shared Function ParseDecimal(ByVal psValue As String) As Nullable(Of Decimal)
		If String.IsNullOrEmpty(psValue) Then
			Return Nothing
		End If

		Dim d As Decimal
		If Decimal.TryParse(psValue, DOUBLE_STYLE, Nothing, d) Then
			Return d
		End If

		Return Nothing
	End Function

	''' <summary>
	''' This overload used frequently when working with datasets; DBNulls returned from datasets as objects will fall into here and yield a null as well
	''' </summary>
	Public Shared Function ParseDecimal(ByVal poValue As Object) As Nullable(Of Decimal)
		If TypeOf poValue Is DBNull OrElse poValue Is Nothing Then
			Return Nothing
		End If

		Return ParseDecimal(poValue.ToString)
	End Function
#End Region

#Region "ParseDate"
	Public Const DATE_STYLES As DateTimeStyles = DateTimeStyles.AllowLeadingWhite Or DateTimeStyles.AllowTrailingWhite

	Public Shared ReadOnly Property DATE_FORMATS(ByVal pFormatInfo As DateTimeFormatInfo) As String()
		Get
			'Public Shared ReadOnly DATE_FORMATS_STANDARD As String() = {"d", "D", "M"}
			With pFormatInfo
				Return New String() { .ShortDatePattern, .LongDatePattern, .MonthDayPattern, "M/d", "MM/dd", "MM/dd/yy", "M/d/yy", "yyyy'-'MM'-'dd", "dd' 'MMM' 'yyyy"}
			End With
		End Get
	End Property

	Public Shared ReadOnly Property TIME_FORMATS(ByVal pFormatInfo As DateTimeFormatInfo) As String()
		Get
			'Public Shared ReadOnly TIME_FORMATS_STANDARD As String() = {"t", "T"}
			With pFormatInfo
				Return New String() { .ShortTimePattern, .LongTimePattern, "HH:mm:ss", "HH:mm"}
				'Currently there is no format with time zone info (.NET 2.0 supports UTC offsets, but not full time zones in DateTime format strings)
			End With
		End Get
	End Property

	''' <summary>
	''' Custom DateTime format strings containing both date and time components.
	''' </summary>
	Public Shared ReadOnly Property DATE_TIME_FORMATS(ByVal pFormatInfo As DateTimeFormatInfo) As Generic.IEnumerable(Of String)
		Get
			Dim standardFormats As String() = {"f", "F", "g", "G", "o", "R", "s", "u", "U"}
			Dim formats As New CSet(Of String)
			formats.AddRange(standardFormats)

			For Each dateFormat As String In DATE_FORMATS(pFormatInfo)
				For Each timeFormat As String In TIME_FORMATS(pFormatInfo)
					formats.Add(dateFormat & "' '" & timeFormat)
				Next
			Next

			Return formats.AsReadOnly()
		End Get
	End Property

	''' <summary>
	''' Parse a date from a string.
	''' </summary>
	''' <returns>The date parsed from the string, or null if the string's value is not a date or the string is null or empty.</returns>
	Public Shared Function ParseDate(ByVal pValue As String, ByVal pKind As DateTimeKind) As Nullable(Of Date)
		If String.IsNullOrEmpty(pValue) Then
			Return Nothing
		End If

		Dim formats As New CSet(Of String)
		With formats
			.AddRange(DATE_FORMATS(_culture_us.DateTimeFormat))
			.AddRange(DATE_TIME_FORMATS(_culture_us.DateTimeFormat))
			.AddRange(TIME_FORMATS(_culture_us.DateTimeFormat))
		End With

		Dim d As Date
		If Date.TryParseExact(pValue, formats.ToArray, _culture_us, DATE_STYLES, d) Then
			If d.Kind = DateTimeKind.Utc Then
				pKind = DateTimeKind.Utc
			End If

			Return DateTime.SpecifyKind(d, pKind)
		End If

		Dim dDateTimeOffset As DateTimeOffset

		' XML Date time format with offset (we support up to 7 significant digits in the seconds fraction because the .NET framework
		' only supports up to 7 "F"s as a valid custom datetime format specifier (see http://msdn.microsoft.com/en-us/library/8kb3ddd4%28v=vs.80%29.aspx)
		If DateTimeOffset.TryParseExact(pValue, "yyyy'-'MM'-'dd'T'HH':'mm':'ss.FFFFFFFK", _culture_us, DATE_STYLES, dDateTimeOffset) Then
			Return dDateTimeOffset.UtcDateTime
		End If

		Return Nothing
	End Function

	''' <summary>
	''' This overload used frequently when working with datasets; DBNulls returned from datasets as objects will fall into here and yield a null as well
	''' </summary>
	Public Shared Function ParseDate(ByVal pValue As Object, ByVal pKind As DateTimeKind) As Nullable(Of Date)
		If TypeOf pValue Is DBNull OrElse pValue Is Nothing Then
			Return Nothing
		End If

		Return ParseDate(pValue.ToString(), pKind)
	End Function

	''' <summary>
	''' Parse a date from a string, assuming a Kind of DateTimeKind.Unspecified.
	''' </summary>
	''' <returns>The date parsed from the string, or null if the string's value is not a date or the string is null or empty.</returns>
	''' <remarks>
	''' If the Kind is known, the overload where it can be specified should be used. 
	''' Otherwise, ToLocalTime will adjust assuming the time is in UTC and 
	''' ToUniversalTime will adjust assuming the time is Local.
	''' </remarks>
	Public Shared Function ParseDate(ByVal pValue As String) As Nullable(Of Date)
		Return ParseDate(pValue, DateTimeKind.Unspecified)
	End Function

	''' <summary>
	''' This overload used frequently when working with datasets; DBNulls returned from datasets as objects will fall into here and yield a null as well
	''' </summary>
	Public Shared Function ParseDate(ByVal poValue As Object) As Nullable(Of Date)
		If TypeOf poValue Is DBNull OrElse poValue Is Nothing Then
			Return Nothing
		End If

		Return ParseDate(poValue.ToString)
	End Function
#End Region

#Region "ParseDateStruct"

	Private Shared _culture_us As CultureInfo = New CultureInfo("en-US")
	''' <summary>
	''' Parse a date from a string.
	''' </summary>
	''' <returns>The date parsed from the string, or null if the string's value is not a date or the string is null or empty.</returns>
	Public Shared Function ParseDateStruct(ByVal pValue As String) As Nullable(Of DateStruct)
		If String.IsNullOrEmpty(pValue) Then
			Return Nothing
		End If

		'Assume this culture rather than using CultureInfo.CurrentCulture
		Dim formats As New CSet(Of String)
		With formats
			.AddRange(DATE_FORMATS(_culture_us.DateTimeFormat))
			.AddRange(DATE_TIME_FORMATS(_culture_us.DateTimeFormat))
			'.AddRange(TIME_FORMATS(culture.DateTimeFormat))
		End With

		Dim d As Date
		If Date.TryParseExact(pValue, formats.ToArray, _culture_us, DATE_STYLES, d) Then
			Return New DateStruct(d)
		End If

		Return Nothing
	End Function

	''' <summary>
	''' This overload used frequently when working with datasets; DBNulls returned from datasets as objects will fall into here and yield a null as well
	''' </summary>
	''' <remarks>
	''' Be careful when passing in from a strong-typed dataset, since the codegen:nullValue will yield a valid date
	''' Use ParseDateStruct(dr.Item("ColumnName")) instead of ParseDateStruct(dr.ColumnName) if you want it to handle NULLs correctly.
	''' </remarks>
	Public Shared Function ParseDateStruct(ByVal poValue As Object) As Nullable(Of DateStruct)
		If TypeOf poValue Is DBNull OrElse poValue Is Nothing Then
			Return Nothing
		End If

		Return ParseDateStruct(poValue.ToString)
	End Function
#End Region

#Region "ParseDateTimeStruct"
	''' <summary>
	''' Parse a date time struct from a string.
	''' </summary>
	''' <param name="peZone">Timezone that psValue is in.</param>
	Public Shared Function ParseDateTimeStruct(ByVal psValue As String, ByVal peZone As CTimeZoneManager.Zone) As Nullable(Of DateTimeStruct)
		Dim dDate As Nullable(Of DateTime) = ParseDate(psValue)
		If dDate.HasValue Then
			Return New DateTimeStruct(dDate.Value, peZone)
		Else
			Return Nothing
		End If
	End Function

	Public Shared Function ParseDateTimeStruct(ByVal poValue As Object, ByVal peZone As CTimeZoneManager.Zone) As Nullable(Of DateTimeStruct)
		If TypeOf poValue Is DBNull OrElse poValue Is Nothing Then
			Return Nothing
		End If

		Return ParseDateTimeStruct(poValue.ToString, peZone)
	End Function

#End Region


#Region "ParseTimeStruct "

	''' <summary>
	''' Parse a Time from a string.   Any date portion will be lost. 
	''' </summary>
	''' <returns>The Time parsed from the string, or null if the string's value is not a Time or the string is null or empty.</returns>
	Public Shared Function ParseTimeStruct(ByVal pValue As String) As Nullable(Of TimeStruct)
		If String.IsNullOrEmpty(pValue) Then
			Return Nothing
		End If

		Dim formats As New CSet(Of String)
		With formats
			'.AddRange(DATE_FORMATS(culture.DateTimeFormat))
			.AddRange(DATE_TIME_FORMATS(_culture_us.DateTimeFormat))
			.AddRange(TIME_FORMATS(_culture_us.DateTimeFormat))
		End With

		Dim d As Date
		If Date.TryParseExact(pValue, formats.ToArray, _culture_us, DATE_STYLES, d) Then
			Return New TimeStruct(d)
		End If

		Return Nothing
	End Function
#End Region



#End Region



#Region "ConvertToDS<DataTypes> used to store nullable values into dataset"


	''' <summary>
	''' Convenience routine to write to dataset and simply creating dbNull values.
	''' </summary>
	''' <remarks> 
	''' Sample Usage:
	''' dim d as nullable (of Date) 
	''' row("col1") = ConvertToDSValue (d) 
	''' </remarks>	
	Public Overloads Shared Function ConvertToDSValue(Of T As Structure)(ByVal pValue As Nullable(Of T)) As Object
		If pValue.HasValue Then
			Return pValue.Value
		Else
			Return Convert.DBNull
		End If
	End Function

	''' <summary>
	''' We have an explicit overload for date because this is a common occurance that dates captured can easily go out of bound from db range. 
	''' </summary>
	''' <param name="pValue"></param>
	''' <remarks> 
	''' It's arguable that an exception should be thrown when value is out of bound and the UI should be validating.    However, this is done for backward compatibility. 	
	''' </remarks>	
	Public Overloads Shared Function ConvertToDSValue(ByVal pValue As Nullable(Of DateTime)) As Object
		If pValue.HasValue AndAlso DBUtils.CSQLParamValue.IsValidDate(pValue) Then
			If pValue.Value.Kind = DateTimeKind.Utc Then
				logger.Warn("All DateTimes stored in DB should be in server Local timezone, not UTC.")
			End If
			Return pValue.Value
		Else
			Return Convert.DBNull
		End If
	End Function

	''' <summary>
	''' We have an explicit overload for DateTimeStruct because this is a common occurance that dates captured can easily go out of bound from db range. 
	''' </summary>
	Public Overloads Shared Function ConvertToDSValue(ByVal pValue As Nullable(Of DateTimeStruct)) As Object
		If pValue.HasValue AndAlso DBUtils.CSQLParamValue.IsValidDate(pValue) Then
			Return pValue.Value.ToTimeZone(CTimeZoneManager.LocalZone).ToDateTime
		Else
			Return Convert.DBNull
		End If
	End Function


	''' <summary>
	''' We have an explicit overload for date because this is a common occurance that dates captured can easily go out of bound from db range. 
	''' </summary>
	''' <param name="pValue"></param>
	''' <remarks> 
	''' It's arguable that an exception should be thrown when value is out of bound and the UI should be validating.    However, this is done for backward compatibility. 	
	''' </remarks>	
	Public Overloads Shared Function ConvertToDSValue(ByVal pValue As Nullable(Of DateStruct)) As Object
		If pValue.HasValue AndAlso DBUtils.CSQLParamValue.IsValidDate(pValue) Then
			' We need to CType this to a Date since our datasets don't know about DateStruct
			' We want code like dr("SomeDate") = ConvertToDSValue(someNullableOfDateStruct) to work
			Return CType(pValue.Value, Date)
		Else
			Return Convert.DBNull
		End If
	End Function



#Region "convertToDSGuid"
	''' <summary>
	''' Used to stored a GUID datatype into a column of a datatable.  Assumes data column 
	''' allows for nulls (otherwise, use New Guid(myVar) ). 
	''' </summary>
	''' <param name="psString">If blank or guid.empty, then function will return dbnull</param>
	''' <returns>Guid or DBNull (if psString is empty/invalid)</returns>
	''' <remarks>
	''' Note we do NOT have an overload that takes in a System.Guid data type b/c that can
	''' be stored directly into the data table with more fine grain control.
	''' 
	''' Also as a db design note, it's often better to use NULLL for guid columns as opposed to 
	''' 32 charactors of "0"s because:
	''' 1. It's easier to construct sql that compare for null vs 000000000...
	''' 2. Code will be more consistent with how we handle dates.
	''' </remarks>
	Public Shared Function convertToDSGuid(ByVal psString As String) As Object
		'<Obsolete("Not preferred since we rather have nulls as 0's")> 
		If psString = "" Then
			Return System.Convert.DBNull
		Else
			Try
				Dim oTmp As Guid = New Guid(psString)
				If oTmp.Equals(Guid.Empty) Then
					Return System.Convert.DBNull
				Else
					Return oTmp
				End If
			Catch ex As System.Exception
				logger.Debug("Invalid guid (DBNull will be returned instead): " & psString)
				Return System.Convert.DBNull
			End Try
		End If
	End Function
#End Region

#Region "convertToDSDate"
	''' <summary>
	'''routine converts a given date into a safe object that can be placed inside a dataset -- the reverse for this can be found in the overload version of safeString
	'''sample usage: oRow.Item("EmploymentStartDate") = convertToDSDate(.FinancialInfo.EmploymentStartDate).
	'''note: we cannot use strong typed properties for this, we have to use the collection indexor instead
	''' </summary>
	''' <param name="sDate"></param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Overloads Shared Function convertToDSDate(ByVal sDate As String) As Object
		If IsDate(sDate) Then
			Return convertToDSDate(CDate(sDate))
		Else
			Return System.Convert.DBNull
		End If
	End Function

	Public Overloads Shared Function convertToDSDate(ByVal theDate As Date) As Object
		If DBUtils.CSQLParamValue.IsValidDate(theDate) Then
			If theDate.Kind = DateTimeKind.Utc Then
				logger.Warn("All DateTimes stored in DB should be in server Local timezone, not UTC.")
			End If
			Return theDate
		Else
			Return Convert.DBNull
		End If
	End Function

	Public Overloads Shared Function convertToDSDate(ByVal theDate As Nullable(Of Date)) As Object
		If theDate.HasValue Then
			Return convertToDSDate(theDate.Value)
		Else
			Return Convert.DBNull
		End If
	End Function

	Public Overloads Shared Function convertToDSDate(ByVal poDateTimeStruct As DateTimeStruct) As Object
		Return ConvertToDSValue(poDateTimeStruct)
	End Function
#End Region



#End Region

	''' <summary>
	''' Convenient function to quickly compare if pValue is part of poChecks. Going to be deprecated.
	''' <para>Revise <code>SelectCaseShort(a, b, c, d)</code>  to  <code>{b, c, d}.Contains(a)</code>, and make sure you import System.Linq namespace.</para>
	''' </summary>
	''' <typeparam name="T"></typeparam>
	''' <param name="pValue"></param>
	''' <param name="poChecks"></param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Shared Function SelectCaseShort(Of T)(ByVal pValue As T, ByVal ParamArray poChecks() As T) As Boolean
		For Each item As T In poChecks
			If item.Equals(pValue) Then
				Return True
			End If
		Next

		Return False
	End Function

	''' <summary>
	''' Type-safe generic version of IIf.  This is also more Option Strict friendly.
	''' </summary>
	''' <typeparam name="T"></typeparam>
	''' <param name="expression"></param>
	''' <param name="truePart"></param>
	''' <param name="falsePart"></param>
	''' <returns></returns>
	''' <remarks>Note   The expressions in the argument list can include function calls. 
	''' As part of preparing the argument list for the call to IIf, the Visual Basic compiler calls every function in every expression. 
	''' This means that you cannot rely on a particular function not being called if the other argument is selected by Expression.
	''' 
	''' It's recommended to use the new .net If(exp,trueResult,falseResult) as it's short circuited.
	''' </remarks>
	Public Shared Function IIfStrict(Of T)(ByVal expression As Boolean, ByVal truePart As T, ByVal falsePart As T) As T
		If expression Then
			Return truePart
		Else
			Return falsePart
		End If
	End Function

	''' <summary>
	''' Same as TryCast, but for value types.
	''' </summary>
	''' <returns>The input object casted to a Nullable(of T) if the object is of type T or type Nullable(of T).  Else, Nothing.</returns>
	''' <remarks>This will not work for accessing nullable value types from strongly-typed properties on DataSets.</remarks>
	Public Shared Function NullableCast(Of T As Structure)(ByVal poObject As Object) As Nullable(Of T)
		If TypeOf poObject Is T Then
			Return DirectCast(poObject, T)
		End If
		If TypeOf poObject Is Nullable(Of T) Then
			Return DirectCast(poObject, Nullable(Of T))
		End If

		Return Nothing
	End Function
	<Obsolete("Use CDateUtils GetFirstDayOfMonth instead")>
	Public Shared Function GetFirstDayOfMonth(ByVal pDate As DateTime) As DateTime
		Return New DateTime(pDate.Year, pDate.Month, 1)
	End Function
	<Obsolete("Use CDateUtils GetLastDayOfMonth instead")>
	Public Shared Function GetLastDayOfMonth(ByVal pDate As DateTime) As DateTime
		Dim lastDay As Integer = DateTime.DaysInMonth(pDate.Year, pDate.Month)
		Return New DateTime(pDate.Year, pDate.Month, lastDay)
	End Function

	''' <summary>
	''' Gets the age in years from now.
	''' </summary>
	''' <param name="DOB"></param>
	''' <returns></returns>
	''' <remarks>Uses AgeAt</remarks>
	<Obsolete("Use CDateUtils AgeNow instead")>
	Public Shared Function AgeNow(ByVal DOB As Date) As Integer
		Return CDateUtils.AgeAt(DOB, DateTimeStruct.DateNow)
	End Function

	''' <summary>
	''' Get the age in years at the given date.
	''' </summary>
	''' <param name="DOB"></param>
	''' <param name="dAgeOn">The date with which to compare</param>
	''' <returns>Age in years, or -1 if dAgeOn is less than DOB</returns>
	''' <remarks></remarks>
	<Obsolete("Use CDateUtils AgeAt instead")>
	Public Shared Function AgeAt(ByVal DOB As Date, ByVal dAgeOn As Date) As Integer
		Return CDateUtils.NumYearsBetween(DOB, dAgeOn)
	End Function

	''' <summary>
	''' Get the number of total years between 2 dates
	''' </summary>
	''' <param name="pStartDate"></param>
	''' <param name="pEndDate"></param>
	''' <returns>The number of years between the dates, or -1 if pEndDate is less than pStartDate</returns>
	''' <remarks></remarks>
	<Obsolete("Use CDateUtils NumYearsBetween instead")>
	Public Shared Function NumYearsBetween(ByVal pStartDate As DateTime, ByVal pEndDate As DateTime) As Integer
		If pEndDate < pStartDate Then
			Return -1
		End If

		Dim nYearDiff As Integer = pEndDate.Year - pStartDate.Year

		' In order to avoid leap year complications, we're just going to do a straight month / day comparison
		' to determine whether or not to subtract a year from the year difference
		Dim bSubtractYear As Boolean = False

		If pStartDate.Month > pEndDate.Month Then
			bSubtractYear = True
		ElseIf pStartDate.Month = pEndDate.Month AndAlso pStartDate.Day > pEndDate.Day Then
			bSubtractYear = True
		End If

		Return nYearDiff - IIfStrict(bSubtractYear, 1, 0) 'Subtract 1 if birthday has not yet occured this year
	End Function

	''' <summary>
	''' Get the number of total years since the StartDate
	''' </summary>
	''' <param name="pStartDate"></param>
	''' <returns></returns>
	''' <remarks></remarks>
	<Obsolete("Use CDateUtils NumYearsSince instead")>
	Public Shared Function NumYearsSince(ByVal pStartDate As DateTime) As Integer
		Return CDateUtils.NumYearsBetween(pStartDate, DateTimeStruct.DateNow)
	End Function

	''' <summary>
	''' Calculates the number of full months that have elapsed between two dates. This means that if the start date is
	''' 1/30/2011 and the end date is 2/1/2011, it will return 0 since a full month has not passed.
	''' </summary>
	<Obsolete("Use CDateUtils NumMonthsBetween instead")>
	Public Shared Function NumMonthsBetween(ByVal pStartDate As DateTime, ByVal pEndDate As DateTime) As Integer
		Return ((pEndDate.Year - pStartDate.Year) * 12) + pEndDate.Month - pStartDate.Month - If(pStartDate.Day > pEndDate.Day, 1, 0)
	End Function

	Public Shared Function formatPhone(ByVal psPhone As String, ByVal pbIsForeign As Boolean) As String
		psPhone = SafeString(psPhone)
		If pbIsForeign Then
			'In the future we may do some formatting on international phone numbers; for now, we just return as-is
			Return psPhone
		Else
			If psPhone = "" Then
				Return ""
			End If

			Dim oPhone As New Text.CPhoneNumber()
			Try
				oPhone.Parse(psPhone)
			Catch ex As System.Exception
				Return ""
			End Try
			oPhone.Extension = ""
			Return oPhone.ToString
		End If
	End Function

	Public Shared Function formatPhone(ByVal sPhone As String) As String
		Return formatPhone(sPhone, False)
	End Function

	Public Shared Function eclipseIt(ByVal s As String, ByVal nLen As Integer, Optional ByVal sMoreText As String = "...") As String
		Dim sResult As String = s
		If Len(sResult) > nLen Then
			sResult = sResult.Substring(0, nLen - sMoreText.Length) & sMoreText
		End If
		Return sResult
	End Function

	Public Shared Function roundToNearestX(ByRef nInput As Double, ByRef nIntervalX As Double) As Double
		If nIntervalX <= 0 Then
			Throw New ArgumentException("Interval must be greater than 0")
		End If

		Dim A As Double
		Dim B As Double
		Dim C As Double

		A = nIntervalX * Math.Floor(nInput / nIntervalX)
		C = nIntervalX * (Math.Floor(nInput / nIntervalX) + 1)
		B = (A + C) / 2

		If nInput >= B Then
			roundToNearestX = C
		Else
			roundToNearestX = A
		End If
	End Function

	Public Shared oRand As New Random()
	Public Shared Function RandomLimit(ByVal LowerBound As Long, ByVal UpperBound As Long) As Long
		Dim Diff As Long = UpperBound - LowerBound + 1
		Return CLng(Math.Floor(LowerBound + (oRand.NextDouble * Diff)))
	End Function

	''' <summary>
	''' True if parameter is "Y", False in ALL other cases, including DBNull
	''' </summary>
	''' <param name="sYN"></param>
	''' <returns>True if parameter is "Y", False in ALL other cases, including DBNull</returns>
	''' <remarks></remarks>
	Public Shared Function YNStringToBool(ByVal sYN As Object) As Boolean
		Return "Y" = SafeString(sYN)
	End Function

	Public Shared Function YNStringToBoolNullable(ByVal pYN As Object) As Boolean?
		Select Case SafeString(pYN)
			Case "Y"
				Return True
			Case "N"
				Return False
			Case Else
				Return Nothing
		End Select
	End Function

	Public Shared Function BoolToYNString(ByVal bBool As Boolean) As String
		If bBool Then
			Return "Y"
		Else
			Return "N"
		End If
	End Function

	Public Shared Function BoolToYNString(ByVal bBool As Nullable(Of Boolean)) As String
		If Not bBool.HasValue() Then
			Return ""
		ElseIf bBool.Value Then
			Return "Y"
		Else
			Return "N"
		End If
	End Function

	''' <summary>
	''' Returns True for "Y", False for "N", and UseDefault for any other value, such as null or empty string.
	''' </summary>
	Public Shared Function YNToTristate(ByVal psYN As Object) As TriState
		Dim sYN As String = SafeString(psYN)
		If sYN = "Y" Then
			Return TriState.True
		ElseIf sYN = "N" Then
			Return TriState.False
		Else
			Return TriState.UseDefault
		End If
	End Function

	''' <summary>
	''' Returns "Y" for True, "N" for False, and the empty string for UseDefault
	''' </summary>
	Public Shared Function TristateToYN(ByVal peTri As TriState) As String
		If peTri = TriState.False Then
			Return "N"
		ElseIf peTri = TriState.True Then
			Return "Y"
		Else
			Return ""
		End If
	End Function

	Public Enum Environments
		''' <summary>
		''' Development environments
		''' </summary>		
		LOCAL
		''' <summary>
		''' Represents beta, demo, jhademo, eplq, test servers.
		''' </summary>		
		DEMO 'demo site runs in an isolated server w/ it's own DB and web server
		''' <summary>
		''' This is the integration server (and staging server) before it goes into production/demo sites
		''' </summary>		
		STAGING
		''' <summary>
		''' This is oddly default behavior for offsite servers since ENVIRONMENT is rarely setup there. 
		''' </summary>		
		PRODUCTION
	End Enum

	''' <summary>
	''' Based on "ENVIRONMENT" appsetting usually set on machine.config, returns relevant environment code is running on.
	''' </summary>
	''' <remarks>
	''' By default, it's assumed PRODUCTION if no key is defined. 
	''' </remarks>
	Public Shared ReadOnly Property RuntimeEnvironment() As Environments
		Get
			Dim sEnvironment As String = SafeString(System.Configuration.ConfigurationManager.AppSettings.Get("ENVIRONMENT"))
			Select Case sEnvironment
				Case "LOCAL"
					Return Environments.LOCAL
				Case "DEMO"
					Return Environments.DEMO
				Case "STAGING"
					Return Environments.STAGING
				Case "PRODUCTION", ""
					Return Environments.PRODUCTION
				Case Else
					logger.Info("INVALID ENVIRONMENT:" & sEnvironment)
					Return Environments.PRODUCTION
			End Select
		End Get
	End Property

	''' <summary>
	''' This delegate is used by validurl to get the actual list of valid urls. 
	''' This variable should be setup in application start in global asax of each application.
	''' </summary>
	''' <remarks></remarks>
	Public Delegate Function whiteList() As Concurrent.ConcurrentBag(Of String)
	Public Shared WhiteListDelegate As whiteList = Nothing

	Public Delegate Function isRelativePath(ByVal pURL As String) As Boolean

	Public Shared IsRelativePathDelegate As isRelativePath = Nothing

	Private Shared ReadOnly PROHIBIT_URL_SRC_EXP As String = System.Configuration.ConfigurationManager.AppSettings.Get("PROHIBIT_URL_SRC_EXP")
	''' <summary>
	''' This routine validates a URL to make sure it's safe for use.   Should be used on tags that support dynamic source and javascript execution (NOT a complete list): 
	''' img, frame, iframe, frameset
	''' </summary>
	''' <param name="psURL"></param>
	''' <param name="pUseRelativePathCheck"></param>
	''' <returns>psURL if it's safe to use, otherwise an exception will be thrown.</returns>
	''' <remarks>This is defined as a function for convenience use so that we can invoke it directly where we use it (ie: frameset src) instead of decoupling the validation
	''' from the use.
	''' Refer to http://msdn2.microsoft.com/en-us/library/ms998274.aspx for more info on cross site script attacks.
	''' </remarks>
	Public Shared Function ValidateURL(ByVal psURL As String, ByVal pUseRelativePathCheck As Boolean) As String
		CAssert.IsNotNull(psURL, "Required parameter: " & psURL)

		If PROHIBIT_URL_SRC_EXP = "" Then
			Return psURL
		End If

		Static InvalidMatcher As System.Text.RegularExpressions.Regex = Nothing

		If InvalidMatcher Is Nothing Then InvalidMatcher = New Regex(PROHIBIT_URL_SRC_EXP)

		If InvalidMatcher.IsMatch(psURL) Then

			' Throw New System.Web.HttpRequestException(CHTTPExceptionCodes.INVALID_URL, "A potentially unsafe URL was detected and rejected: " & psURL)
			Throw New HttpRequestException(CHTTPExceptionCodes.INVALID_URL, New Exception("A potentially unsafe URL was detected and rejected: " & psURL))
		End If

		Static ValidUrls As Concurrent.ConcurrentBag(Of String) = New Concurrent.ConcurrentBag(Of String)
		' this list won't get refreshed since its not likely that our own file system is changing dynamically per app recyle.
		Static ValidRelativePathUrls As Concurrent.ConcurrentBag(Of String) = New Concurrent.ConcurrentBag(Of String)
		Dim CacheTimeThreshold As Integer = SafeInteger(ConfigurationManager.AppSettings.Get("URL_VALIDATION_CACHE_TIME"))
		If CacheTimeThreshold = 0 Then
			CacheTimeThreshold = 15
		End If

		If WhiteListDelegate IsNot Nothing Then
			ValidUrls = WhiteListDelegate.Invoke
		End If


		For Each URL As String In ValidUrls
			If psURL.ToUpper.StartsWith(URL) Then Return psURL
		Next

		'This check is just to make sure if relative path is passed in its under our LPQwebsite's folder subdirectory
		If pUseRelativePathCheck AndAlso IsRelativePathDelegate IsNot Nothing Then

			For Each url In ValidRelativePathUrls
				If psURL.StartsWith(url) Then Return psURL
			Next

			If Not IsRelativePathDelegate(psURL) Then
				Throw New HttpRequestException(CHTTPExceptionCodes.INVALID_URL, New Exception("A potentially unsafe URL was detected and rejected: " & psURL))
			End If

			Dim CleanRelativePath As String = CleanRelativeSubPathForValidateURL(psURL)
			If CleanRelativePath <> "" Then
				ValidRelativePathUrls.Add(CleanRelativePath)
			End If

		Else
			If psURL.ToUpper.StartsWith("HTTP:") OrElse psURL.ToUpper.StartsWith("HTTPS:") OrElse psURL.ToUpper.StartsWith("FTP:") Then
				Throw New HttpRequestException(CHTTPExceptionCodes.INVALID_URL, New Exception("A potentially unsafe URL was detected and rejected: " & psURL))
			End If
		End If

		Return psURL
	End Function


	Public Shared Function CleanRelativeSubPathForValidateURL(ByVal psURL As String) As String
		'check for /*/
		Dim secondIndex As Integer = psURL.IndexOf("/", 2)

		If secondIndex > 0 Then
			Return psURL.Substring(0, secondIndex + 1)
		Else
			'check for .aspx and ashx
			secondIndex = psURL.IndexOf(".", 2)
			If secondIndex > 0 Then
				Dim RelativePath As String = psURL.Substring(0, secondIndex)
				Select Case True
					Case psURL.Contains(RelativePath & ".aspx")
						Return RelativePath & ".aspx"
					Case psURL.Contains(RelativePath & ".ashx")
						Return RelativePath & ".ashx"
				End Select
			End If
		End If
		Return ""
	End Function

	''' <summary>
	''' Convinient overload of validateURL that defaults relative path whitelist to true.
	''' </summary>
	''' <param name="psURL"></param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Shared Function ValidateURL(ByVal psURL As String) As String
		Return ValidateURL(psURL, True)
	End Function

	Public Shared Function IsSomething(ByVal poObject As Object) As Boolean
		Return Not IsNothing(poObject)
	End Function

	'Obsolete, the reverse of this would be:
	'Uri.UnescapeDataString
	Public Shared Function escape(ByVal encodeString As String) As String
		'******************************************************************************** 
		'* Name: urlEncode             
		'* Description: This function recieves a string and returns it in encoded, url friendly form.  
		'                    Usefull when used with unescape('http%3A%2F%2Ftest%2Ecom%2Fpage%2Easp'); on the client 
		'                    so you don't have to store into a temp hidden field 
		'* Parameters: 
		'* Sample Usage: 
		'Print urlEncode("http://test.com/page.asp?msg=how youdo") 'usually, you only pass the URL parameter "how youdo" instead of the entire URL 
		'http%3A%2F%2Ftest%2Ecom%2Fpage%2Easp%3Fmsg%3Dhow%20youdo 
		'* Created: 9/29/2000 5:56:57 PM 
		'******************************************************************************** 

		Dim sb As New System.Text.StringBuilder()
		Dim currentChar As String
		Dim i As Integer

		For i = 1 To Len(encodeString)
			currentChar = Mid(encodeString, i, 1)


			If Asc(currentChar) < 91 And Asc(currentChar) > 64 Then
				'character is between A to Z so leave it 
				' 
				sb.Append(currentChar)
			ElseIf Asc(currentChar) < 123 And Asc(currentChar) > 96 Then
				'character is between a to z so leave it 
				' 
				sb.Append(currentChar)
			ElseIf Asc(currentChar) < 58 And Asc(currentChar) > 47 Then
				'character is between 0 to 9 so leave it 
				' 
				sb.Append(currentChar)
				'netcape uses %20 instead of + 
				'        ElseIf Asc(currentChar) = 32 Then 
				'            sb = sb + "+" 
				'            'character is a space so change it to a 
				'            '     plus 
			Else
				'character needs a hex conversion 
				sb.Append("%" + Hex(Asc(currentChar)).PadLeft(2, "0"c))
			End If
		Next
		'Return the url encoded string 
		Return sb.ToString
	End Function


	'Public Shared Function isPhoneValid(ByVal phoneNumber As String) As Boolean
	'    'this should match area codes within parentheses or without, as well as - or . or spaces for separators
	'    'ie (714)542.3423 or 234.432.2343 or 934 334 2342
	'    Dim regex As New RegularExpressions.Regex("^((\(\d\d\d\))|\d\d\d)?\s*[\.-]?\s*(\d\d\d)\s*[\.-]?\s*(\d\d\d\d)$")
	'    Return regex.IsMatch(phoneNumber)
	'End Function

	''' <summary>
	''' Copies all column data from copySource into copyTarget.  If copyTarget doesn't contain any source columns, 
	''' then those will be skipped.   Please make sure you double check any primary/foreign keys after cloning
	''' rows to ensure data integrity.
	''' </summary>
	''' <param name="copysource"></param>
	''' <param name="copyTarget"></param>    
	Public Shared Sub cloneRow(ByVal copysource As DataRow, ByVal copyTarget As DataRow)
		Dim col As DataColumn

		For Each col In copysource.Table.Columns
			If copyTarget.Table.Columns.Contains(col.ColumnName) Then
				copyTarget(col.ColumnName) = copysource(col.ColumnName)
			End If
		Next
	End Sub

#Region "ONE WAY HASH ROUTINES"
	''' <summary>
	''' This version of routine is case senstivie  and hence allows for case sensitive passwords :X 
	''' </summary>
	Public Shared Function getOneWayHashValueCaseSensitive(ByVal str As String) As String
		Dim sHashed As String = Nothing
		Using md5 As New System.Security.Cryptography.MD5CryptoServiceProvider()

			Dim oBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(str)
			Dim oHashed As Byte() = md5.ComputeHash(oBytes)
			Dim sBase64 As String = Replace(Convert.ToBase64String(oHashed), "+", ";") 'the plus sign chokes when user clicks on the URL. This is technically still not 100% URL-safe as ";" is still a reserved char.

			sHashed = Left(sBase64, 22)  'Chops off the == that ends up at the end of every base64 string.

			Return sHashed
		End Using
	End Function


	Public Shared Function getOneWayHashValue(ByVal str As String) As String
		Dim sHashed As String = Nothing
		Using md5 As New System.Security.Cryptography.MD5CryptoServiceProvider()

			Dim oBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(str.ToUpper())
			Dim oHashed As Byte() = md5.ComputeHash(oBytes)
			Dim sBase64 As String = Replace(Convert.ToBase64String(oHashed), "+", ";") 'the plus sign chokes when user clicks on the URL. This is technically still not 100% URL-safe as ";" is still a reserved char.

			sHashed = Left(sBase64, 22)  'Chops off the == that ends up at the end of every base64 string.

			Return sHashed
		End Using
	End Function

	''' <summary>
	''' This version required because cookies simply don't like ';' as part of their name/values.
	''' </summary>
	Public Shared Function getOneWayHashCookie(ByVal str As String) As String
		Dim sHashed As String = Nothing
		Using md5 As New System.Security.Cryptography.MD5CryptoServiceProvider()

			Dim oBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(str.ToUpper())
			Dim oHashed As Byte() = md5.ComputeHash(oBytes)
			Dim sBase64 As String = Convert.ToBase64String(oHashed) 'note that + signs that are generated (ie: try hashing juliob@meridianlink.com and you'll see a plus in there) will be seen by iewatch as a ' '.  However it still does work and get seen by the server as a '+'.  I think this is an ieWatch glitch.

			sHashed = Left(sBase64, 22)   'Chops off the == that ends up at the end of every base64 string.

			Return sHashed
		End Using
	End Function
#End Region

#Region "ENCRYPT/DECRYPT ROUTINES"
	''' <summary>
	''' Deprecated - Encrypts the given string using a very simple algorithm. This is very low security; for new applications
	''' please use CCryptoUtilsKMS.EncryptAES Instead.
	''' </summary>
	Public Shared Function Encrypt(ByVal key As String, ByVal s As String) As String
		Dim ret As String = ""
		Dim nLen As Integer = Len(s)
		Dim i As Integer, j As Integer = 0

		'pad the key so its at least same length as what we're trying to hash
		If Len(s) > Len(key) Then
			key = key.PadRight(Len(s), "*"c)
		End If

		For i = nLen To 1 Step -1
			j += 1

			Dim k As Integer = Asc(key.Chars(j - 1)) + 129

			ret += Hex(Asc(Mid(s, i, 1)) Xor (k))
			'traceA(Mid(s, i, 1) & " ++ " & k)
		Next

		Return ret
	End Function

	''' <summary>
	''' Deprecated. Please use the other Decrypt function in CBaseApplicant 
	'''	and provide an Encryption version and DEKID to decrypt strings encrypted with KMS.
	''' </summary>
	''' <param name="key"></param>
	''' <param name="s"></param>
	''' <returns></returns>
	Public Shared Function Decrypt(ByVal key As String, ByVal s As String) As String
		Dim ret As String = ""
		Dim nLen As Integer = Len(s)
		Dim i As Integer, j As Integer = 0

		'pad the key so its at least same length as what we're trying to hash
		If Len(s) > Len(key) Then
			key = key.PadRight(Len(s), "*"c)
		End If

		For i = (nLen - 1) To 1 Step -2
			j += 1

			Dim k As Integer = Asc(key.Chars(SafeInteger(nLen / 2 - j))) + 129

			ret += Chr(CInt("&H" & Mid(s, i, 2)) Xor CInt(k))
			'traceA(Mid(s, i, 2) & " ** " & k)
		Next

		Return ret
	End Function
#End Region

#Region "CopyFields"
	''' <summary>
	''' Gets the most-specific common class type.
	''' </summary>
	Public Shared Function GetCommonType(ByVal pTypes As IList(Of Type)) As Type
		If pTypes Is Nothing Then
			Throw New ArgumentNullException
		End If

		If pTypes.Count = 0 Then
			Return Nothing
		ElseIf pTypes.Count = 1 Then
			Return pTypes(0)
		End If

		Dim typeSets As New List(Of ICollection(Of Type))
		For Each type As Type In pTypes
			typeSets.Add(GetBaseTypes(type))
		Next
		typeSets.Sort(AddressOf CollectionCountComparisonReverse(Of Type)) 'Want the largest set of base types first

		Dim commonTypes As COrderedSet(Of Type) = DirectCast(typeSets(0), COrderedSet(Of Type))
		For i As Integer = 1 To typeSets.Count - 1
			commonTypes.IntersectWith(DirectCast(typeSets(i), COrderedSet(Of Type)))
			If commonTypes.Count = 1 Then 'Only common type is Object
				Exit For
			End If
		Next

		'Since this is an ordered set and we started with the set with the most base types, the most-specific common type should be first
		Return commonTypes(0)
	End Function

	''' <summary>
	''' Get the set of all types that a type inherits from (includes the type itself).
	''' </summary>
	Private Shared Function GetBaseTypes(ByVal pType As System.Type) As COrderedSet(Of Type)
		Dim types As New COrderedSet(Of Type)
		Dim type As Type = pType

		While Not type Is GetType(Object)
			types.Add(type)
			type = type.BaseType
		End While
		types.Add(GetType(Object))

		Return types
	End Function

	Private Shared Function CollectionCountComparisonReverse(Of T)(ByVal x As ICollection(Of T), ByVal y As ICollection(Of T)) As Integer
		Return y.Count.CompareTo(x.Count)
	End Function

	''' <summary>
	''' Version of CopyFields which automatically determines type commonalities between the source and target.
	''' </summary>
	Public Shared Sub CopyFields(ByVal pSource As Object, ByVal pDestination As Object, Optional ByVal pIncludePrivate As Boolean = True, Optional ByVal pCopyReferenceTypes As Boolean = False)
		Dim sourceType As Type = pSource.GetType
		Dim destType As Type = pDestination.GetType
		Dim commonType As Type

		If sourceType Is destType Then 'Handle the simple case
			commonType = sourceType
		Else
			commonType = GetCommonType(New Type() {sourceType, destType})
			If commonType Is GetType(Object) Then
				Throw New ArgumentException("The types of source and destination share no common base type")
			End If
		End If

		CopyFields(pSource, pDestination, commonType, pIncludePrivate, pCopyReferenceTypes)
	End Sub

	''' <summary>
	''' Copies all the intrinsic data types ( types w/ isValueType = true or is String) from the theType type interface to the target object
	'''  the reason we don't uset he source object's type is b/c we might want to only set a subset of properties from the source to the target
	''' if source inherit from A, target inherit from B, but they are not compatable types, then copying blindly from A to B would cause runtime error!
	''' </summary>
	Public Shared Sub CopyFields(ByVal source As Object, ByVal target As Object, ByVal theType As Type, Optional ByVal includePrivate As Boolean = True, Optional ByVal pbCopyReferenceTypes As Boolean = False)
		If Not source.GetType Is theType And source.GetType.IsSubclassOf(theType) = False Then
			Throw New ArgumentException("Source object is not the same type or a subclass of the type:" & theType.ToString)
		End If
		If Not target.GetType Is theType And target.GetType.IsSubclassOf(theType) = False Then
			Throw New ArgumentException("Target object is not the same type or a subclass of the type:" & theType.ToString)
		End If

		While theType IsNot Nothing
			Dim fieldInfos() As FieldInfo
			If includePrivate Then
				fieldInfos = theType.GetFields(BindingFlags.Instance Or BindingFlags.NonPublic Or BindingFlags.Public Or BindingFlags.DeclaredOnly)
			Else
				fieldInfos = theType.GetFields(BindingFlags.Instance Or BindingFlags.Public Or BindingFlags.DeclaredOnly)
			End If

			Dim fi As FieldInfo
			For Each fi In fieldInfos
				Dim val As Object
				val = fi.GetValue(source)

				Dim bIsCopyOK As Boolean = True
				If Not pbCopyReferenceTypes Then
					bIsCopyOK = fi.FieldType.IsValueType OrElse fi.FieldType.Equals(GetType(String)) OrElse (fi.FieldType.IsClass AndAlso TypeOf fi.GetValue(source) Is ICopyFields)
				End If

				If bIsCopyOK Then  'val.GetType.Namespace = "System" Then
					For Each attr As Attribute In Attribute.GetCustomAttributes(fi)
						If TypeOf attr Is CDoNotCopy Then
							bIsCopyOK = False
							Exit For
						End If
					Next

					If Not bIsCopyOK Then
						Continue For
					End If

					Dim targetType As Type = target.GetType
					'the BindingFlags.FlattenHiearchy doesn't work as we'd expect so we have to recursively search the object hiearchy
					Do
						Dim targetField As FieldInfo
						targetField = targetType.GetField(fi.Name, BindingFlags.NonPublic Or BindingFlags.Instance Or BindingFlags.Public)
						If IsNothing(targetField) = False Then
							If Not pbCopyReferenceTypes AndAlso fi.FieldType.IsClass AndAlso TypeOf fi.GetValue(source) Is ICopyFields Then
								'only call copyFields re-cursively for non copy reference mode
								CopyFields(fi.GetValue(source), targetField.GetValue(target), fi.FieldType, True)
							Else
								targetField.SetValue(target, val)
							End If
							Exit Do
						End If
						targetType = targetType.BaseType
					Loop While IsNothing(targetType.BaseType) = False
					'theType.InvokeMember(fi.Name, BindingFlags.Public Or BindingFlags.NonPublic Or BindingFlags.Instance Or BindingFlags.SetField Or BindingFlags.FlattenHierarchy, Nothing, target, New Object() {val})
				End If
			Next

			theType = theType.BaseType
		End While
	End Sub
#End Region

	''' <summary>
	''' Calls math.round with the awayfromzero parameter set.  The reason we need this is because by default, Math.Round rounds to the nearest
	''' even number (e.g. 1.5 => 2, 2.5 => 2, 3.5 => 4), which is pretty weird behavior.
	''' </summary>
	''' <param name="pnNumber">A double-precision floating-point number to be rounded</param>
	Public Shared Function Round(ByVal pnNumber As Double) As Double
		Return Math.Round(pnNumber, MidpointRounding.AwayFromZero)
	End Function

	Public Shared Function Round(ByVal pnNumber As Decimal) As Decimal
		Return Math.Round(pnNumber, MidpointRounding.AwayFromZero)
	End Function

	''' <summary>
	''' Calls math.round with the awayfromzero parameter set.  The reason we need this is because by default, Math.Round rounds to the nearest
	''' even number (e.g. 1.5 => 2, 2.5 => 2, 3.5 => 4), which is pretty weird behavior.
	''' </summary>
	''' <param name="pnNumber">A double-precision floating-point number to be rounded</param>
	''' <param name="pnDigit">The number of significant digits (precision) in the return value</param>
	''' <returns></returns>
	''' <remarks>
	''' Rounding is generally recommened when we need accurate results.
	''' More detail can be found at: http://portal.loanspq.com/wiki/ow.asp?NumberComparisonAndRounding#preview
	'''  'decimals are *supposedly* better suited for financial apps, but they still have flaws :-/
	''' Dim dividend As Decimal = Decimal.One
	''' Dim divisor As Decimal = 3
	''' Response.Write(dividend / divisor * divisor) 'produces 0.9999999999999999999999999999 to the console
	''' Response.Write("<BR/>")
	''' 
	''' 'doubles having floating point precision problem, but in the same scenario, they work fine... 
	''' Dim dividend2 As Double = 1
	''' Dim divisor2 As Double = 3
	''' Response.Write(dividend2 / divisor2 * divisor2)  '' The following displays 1 to the console
	''' Response.Write("<BR />")
	''' 
	''' 'some more examples of floating point problems 
	''' Dim s1 As Single = 1300.4F
	''' Dim s2 As Single = 1359.48F
	''' Dim s As Single = s2 - s1
	''' 
	''' Dim d1 As Double = 1300.4
	''' Dim d2 As Double = 1359.48
	''' Dim d As Double = d2 - d1
	''' 
	''' Dim e1 As Decimal = 1300.4
	''' Dim e2 As Decimal = 1359.48
	''' Dim e As Decimal = e2 - e1
	''' 
	''' Response.Write(s) 'yields 59.07996
	''' Response.Write("<br />")
	''' Response.Write(Round(s, 2))  ' yields 59.08
	''' Response.Write("<br />")
	''' 
	''' Response.Write(d) 'yields 59.0799999999999
	''' Response.Write("<br />")
	''' Response.Write(Round(d, 2))  ' yields 59.08
	''' Response.Write("<br />")
	''' 
	''' 
	''' Response.Write(e)
	''' Response.Write("<br />")
	''' Response.Write(Round(e, 2))  ' yields 59.08
	''' Response.Write("<br />")
	''' 
	''' Response.Write(s = d) 'yields false
	''' Response.Write("<br />")
	''' 'really, the proper way to compare is to have to round it first...
	''' Response.Write(Round(s, 2) = Round(d, 2)) 'yields true 
	'''  
	''' Response.End()
	''' </remarks>
	Public Shared Function Round(ByVal pnNumber As Double, ByVal pnDigit As Integer) As Double
		' we will double round here to prevent floating point numbers from throwing off the results
		' ex. 
		'	A. (2961.72 + 300) / 8000 * 100.0 
		'	B. ((2961.72 + 300)* 100.0) / 8000 
		'  Both A and B produce the same mathmatical result: 40.7715.  However, A produces 40.7714999999999999999999
		'  in memory.  If you round A and B with a precision of 3, A will result in 40.771 due to the floating point issue
		'   while B will result in 40.772 which is correct.
		'  
		'  Solution
		'  We will double round.  The first round will be at max precision (15) so that we nuke the floating point issue.
		'  The second will be at the precision requested to get the correct final result

		Dim nRoundOne As Double = Math.Round(pnNumber, 15, MidpointRounding.AwayFromZero)
		Return Math.Round(nRoundOne, pnDigit, MidpointRounding.AwayFromZero)
	End Function


	''' <summary>
	''' Strong typed enum values to represent results of comparison.  The numeric values of the enums have been set to match those of found in the .net framework 
	''' but using enums is the preferred and more readible way.
	''' </summary>	
	Public Enum NumberCompareResults
		'note first letter of all of these values are different to avoid intellisense typo (ie: using BIsBigger instead of BothEqual because both have starting B) and enhance readability
		AIsBigger = -1
		BIsBigger = 1
		EqualBoth = 0
	End Enum

	''' <summary>
	''' Compares 2 floating doubles in safe manner to prevent floating precision number problems (whereby a value 5 may end up getting stored internally as 5.00....1.  
	''' Two values are considered equal if the difference between them is below the given threshold (default of  .0000001).    
	''' </summary>
	''' <remarks> 
	''' Note: A number written to the database may be read back out with floating precision problem.   For instance, you may write 15, but it may read back out
	''' 14.9999999.................9.    To be safe, you should use this routine when doing comparisons with floating point numbers. 
	''' 
	''' Epsilon choosen here is the lowest denominator: 7.  This is to reduce impact in case client code is working with mixture of datatypes.
	''' decimal: precision = 28, range = 10^28
	''' double : precision = 15, range = 10^308
	''' float  : precision =  7, range = 10^38
	''' 
	''' If you need better precision, you may need to do so manually.
	''' 
	''' Example thresholds:
	''' Assert.IsTrue(std.CompareNumber(Round(1 / 3, 6), 1 / 3) = NumberCompareResults.BIsBigger)
	''' Assert.IsTrue(std.CompareNumber(Round(1 / 3, 7), 1 / 3) = NumberCompareResults.EqualBoth)	
	'''
	''' Refer to here for some more discussion on float (and decimal): 
	''' http://stackoverflow.com/questions/803225/when-should-i-use-double-instead-of-decimal/803260#803260
	''' </remarks>
	Public Shared Function CompareNumber(ByVal a As Double, ByVal b As Double) As NumberCompareResults
		Dim nDiff As Double = a - b
		Const EPSILON_THRESHOLD As Single = 0.0000001
		If Math.Abs(nDiff) <= EPSILON_THRESHOLD Then
			Return NumberCompareResults.EqualBoth
		ElseIf nDiff > 0 Then
			Return NumberCompareResults.AIsBigger
		Else
			Return NumberCompareResults.BIsBigger
		End If
	End Function
	''' <summary>
	''' This overload allows us to control how many digits are signficant.     Numbers are rounded prior to comparison.  
	''' Example: Say that 1.045% and 1.047% are the same, a call such as CompareNumber(1.045, 1.047, 2) will return BOTH_EQUAL.
	''' </summary>
	''' <param name="pnFractionalDigits">The number of fractional digits to round to before comparing</param>
	Public Shared Function CompareNumber(ByVal a As Double, ByVal b As Double, ByVal pnFractionalDigits As Integer) As NumberCompareResults

		Return CompareNumber(Round(a, pnFractionalDigits), Round(b, pnFractionalDigits))

	End Function

	Public Shared Sub CopyStreamContents(ByVal pInput As IO.Stream, ByVal pOutput As IO.Stream)
		Dim buffer(4095) As Byte '4KB buffer
		Do
			Dim numRead As Integer = pInput.Read(buffer, 0, buffer.Length)
			If numRead = 0 Then Exit Do
			pOutput.Write(buffer, 0, numRead)
		Loop
	End Sub

	Private Shared _XPathQuoteRegex As System.Text.RegularExpressions.Regex = New System.Text.RegularExpressions.Regex("([\'\""])", RegexOptions.Compiled)
	''' <summary>
	''' Takes a .NET string and returns it as a safe XPath literal. 
	''' </summary>
	''' <param name="psString"></param>
	''' <returns></returns>
	''' <remarks>
	''' XPath (stupidly) contains no provision for escaping single/double quotes other than to use the other type for the string in question.
	''' Thus, the only safe way to use a string which may contain both single and double quotes is to build up a call to the XPath concat() function.
	'''</remarks>
	Public Shared Function XPathString(ByVal psString As String) As String
		If Not psString.Contains("'"c) Then
			Return "'" & psString & "'"
		Else
			Dim tokens As String() = _XPathQuoteRegex.Split(psString)
			Dim sToReturn As String = "concat("
			For Each sToken As String In tokens
				If sToken = """" Then 'Literal string "
					sToReturn &= "'""', "   ' literal string '"',
				ElseIf sToken = "'" Then
					sToReturn &= """'"", " 'literal string "'", 
				Else
					sToReturn &= "'" & sToken & "', "
				End If
			Next
			sToReturn = sToReturn.TrimEnd(New Char() {","c, " "c})
			sToReturn &= ")"
			Return sToReturn
		End If
	End Function

	''' <summary>
	''' Splits a string into chunks of a specified maximum length.
	''' </summary>
	''' <param name="psSource">The source string to split into chunks</param>
	''' <param name="pnChunkLength">The maximum chunk length</param>
	''' <param name="psDelimiters">If specified, forces the remaining substring after each delimiter to be the start of a new chunk (if any remains)</param>
	''' <param name="pbPreserveWords">Whether to attempt to split at word boundaries rather than exact chunk length</param>
	''' <returns>A list of string chunks, in order</returns>
	''' <remarks>
	''' Chunks will be filled in from source greedily until a delimiter is hit or there is no more remaining source.
	''' The delimiters will not be included in the return value.
	''' </remarks>
	Public Shared Function ChunkSplit(ByVal psSource As String, ByVal pnChunkLength As Integer, Optional ByVal psDelimiters As IList(Of String) = Nothing, Optional ByVal pbPreserveWords As Boolean = False) As List(Of String)
		Dim sWordBoundaries As IList(Of Char) = New Char() {" "c, CChar(vbTab), CChar(vbCr), CChar(vbLf), ChrW(&HAD)}
		Dim sResults As New List(Of String)
		Dim sSubdividedSources As New List(Of String)(New String() {psSource})

		If psDelimiters IsNot Nothing Then
			For Each sDelimiter As String In psDelimiters
				sSubdividedSources = Subdivide(sSubdividedSources, sDelimiter)  'Split source by delimiter
			Next
		End If

		For Each sSource As String In sSubdividedSources
			Dim i As Integer = 0
			While i < sSource.Length
				Dim nSubLength As Integer
				Dim nRemainingSourceLength As Integer = sSource.Length - i

				If nRemainingSourceLength <= pnChunkLength Then
					nSubLength = nRemainingSourceLength
				Else
					nSubLength = pnChunkLength
					If pbPreserveWords Then
						For nCharIndex As Integer = nSubLength + i To i + 1 Step -1 'Start at char after the raw chunk, and go to the second char in the raw chunk
							If sWordBoundaries.Contains(sSource.Chars(nCharIndex)) Then
								nSubLength = nCharIndex - i
								Exit For
							End If
						Next
					End If
				End If

				sResults.Add(sSource.Substring(i, nSubLength))
				i += nSubLength
			End While
		Next

		Return sResults
	End Function

	''' <summary>
	''' Subdivides a list of strings into further parts.
	''' </summary>
	Public Shared Function Subdivide(ByVal psSourceList As IList(Of String), ByVal psDelimiter As String) As List(Of String)
		Dim sResults As New List(Of String)
		For Each sSource As String In psSourceList
			sResults.AddRange(Split(sSource, psDelimiter))
		Next

		Return sResults
	End Function

	Public Shared ReadOnly IsOffsite As Boolean = UCase(AppSettings.Get("IS_OFFSITE")) = "TRUE"

	Private Shared _MachineName As String = Nothing
	''' <summary>
	''' Attempts to get machine name from System.Net.Dns.GetHostName.
	''' Falls back to Environment.MachineName if that doesnt work.
	''' If neither work, returns "unknown"
	''' </summary>
	Public Shared ReadOnly Property MachineName As String
		Get
			'initialize the machinename the first time
			If _MachineName Is Nothing Then
				Try
					_MachineName = System.Net.Dns.GetHostName
				Catch ex As System.Net.Sockets.SocketException
					logger.Warn("Socket Exception occurred while getting the DNS hostname.", ex)
				Catch ex As System.Security.SecurityException
					logger.Warn("Security Exception occurred while getting the DNS hostname", ex)
				Catch ex As Exception
					logger.Warn("Exception occurred while getting the DNS hostname: " & ex.Message, ex)
				End Try

				If _MachineName Is Nothing OrElse _MachineName.Length = 0 Then
					Try
						_MachineName = Environment.MachineName
					Catch ex As Exception
						logger.Warn("Exception occurred while getting the Environment MachineName: " & ex.Message, ex)
					End Try
				End If
				If _MachineName Is Nothing OrElse _MachineName.Length = 0 Then
					logger.Warn("MachineName is Unknown")
					_MachineName = "Unknown"
				End If
			End If
			Return _MachineName
		End Get
	End Property

	Private Shared _ConfigMapFolder As String

	''' <summary>
	''' Returns configMap folder with priority to (listed in order) : 
	''' 1. CONFIG_MAP_FOLDER appsetting key (set on testproject/app.config or machine.config on system level)
	''' 2. If folder exists: c:\loanspq2\configmap
	''' 3. default c:\configmap
	''' </summary>
	Public Shared ReadOnly Property ConfigMapFolder() As String
		Get
			If _ConfigMapFolder <> "" Then
				Return _ConfigMapFolder
			End If

			Dim sResult As String = AppSettings.Get("CONFIG_MAP_FOLDER")
			If sResult = "" Then
				Const NEW_CONFIGMAP = "c:\LoansPQ2\ConfigMap"
				If IO.Directory.Exists(NEW_CONFIGMAP) Then
					sResult = NEW_CONFIGMAP
				Else
					sResult = "c:\configmap"
				End If
			End If

			_ConfigMapFolder = sResult
			Return _ConfigMapFolder
		End Get
	End Property

	Public Shared Function IsRoutingNumberValid(ByVal psNumber As String) As Boolean

		If 9 < psNumber.Length Then
			Return False
		End If

		' alg:
		' 1st routing number digit * 3 +
		' 2nd routing number digit * 7 +
		' 3rd routing number digit * 1 +
		' 4th routing number digit * 3 +
		' 5th routing number digit * 7 +
		' 6th routing number digit * 1 +
		' 7th routing number digit * 3 +
		' 8th routing number digit * 7 +
		' 9th routing number digit * 1 = total
		' Routing number is valid if total is evenly divisible by 10

		' ex. 011202936
		' = 0 + 7 + 1 + 6 + 0 + 2 + 27 + 21 + 6 = 70 which is divisible by 10
		' thus valid routing number
		Dim nMultipliers As Integer() = {3, 7, 1, 3, 7, 1, 3, 7, 1}

		Dim nSum As Integer = 0
		Dim sRouting As String = SafeString(psNumber)

		Dim cRoutingArray As Char() = sRouting.ToArray

		For nIndex As Integer = 0 To sRouting.Length - 1
			nSum += nMultipliers(nIndex) * SafeInteger(cRoutingArray(nIndex))
		Next

		Return nSum Mod 10 = 0
	End Function

	Public Shared Function SafeSubString(ByVal psInputString As String, ByVal pnExpectedLength As Integer) As String

		If SafeString(psInputString) = "" Then
			Return psInputString
		End If
		If pnExpectedLength < 0 Then
			Return psInputString
		End If

		pnExpectedLength = CInt(IIf(psInputString.Length > pnExpectedLength, pnExpectedLength, psInputString.Length))

		Return psInputString.Substring(0, pnExpectedLength)

	End Function

End Class
