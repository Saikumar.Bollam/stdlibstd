﻿'Imports System.Web.UI
'Imports System.Web.UI.HtmlControls
'Imports System.Web.UI.WebControls
Imports StdLib.SpecializedDataTypes

Public Class CDateUtils


    Public Shared Function GetFirstDayOfMonth(ByVal pDate As DateTime) As DateTime
        Return New DateTime(pDate.Year, pDate.Month, 1)
    End Function

    Public Shared Function GetLastDayOfMonth(ByVal pDate As DateTime) As DateTime
        Dim lastDay As Integer = DateTime.DaysInMonth(pDate.Year, pDate.Month)
        Return New DateTime(pDate.Year, pDate.Month, lastDay)
    End Function
    ''' <summary>
    ''' Gets the age in years from now.
    ''' </summary>
    ''' <param name="DOB"></param>
    ''' <returns></returns>
    ''' <remarks>Uses AgeAt</remarks>
    Public Shared Function AgeNow(ByVal DOB As DateTime) As Integer
        Return AgeAt(DOB, DateTimeStruct.DateNow)
    End Function
    ''' <summary>
    ''' Get the age in years at the given date.
    ''' </summary>
    ''' <param name="DOB"></param>
    ''' <param name="dAgeOn">The date with which to compare</param>
    ''' <returns>Age in years, or -1 if dAgeOn is less than DOB</returns>
    ''' <remarks></remarks>
    Public Shared Function AgeAt(ByVal DOB As DateTime, ByVal dAgeOn As DateTime) As Integer
        Return NumYearsBetween(DOB, dAgeOn)
    End Function
    ''' <summary>
    ''' Get the number of total years between 2 dates
    ''' </summary>
    ''' <param name="pStartDate"></param>
    ''' <param name="pEndDate"></param>
    ''' <returns>The number of years between the dates, or -1 if pEndDate is less than pStartDate</returns>
    ''' <remarks></remarks>
    Public Shared Function NumYearsBetween(ByVal pStartDate As DateTime, ByVal pEndDate As DateTime) As Integer
        If pEndDate < pStartDate Then
            Return -1
        End If

        Dim nYearDiff As Integer = pEndDate.Year - pStartDate.Year

        ' In order to avoid leap year complications, we're just going to do a straight month / day comparison
        ' to determine whether or not to subtract a year from the year difference
        Dim bSubtractYear As Boolean = False

        If pStartDate.Month > pEndDate.Month Then
            bSubtractYear = True
        ElseIf pStartDate.Month = pEndDate.Month AndAlso pStartDate.Day > pEndDate.Day Then
            bSubtractYear = True
        End If

        Return nYearDiff - If(bSubtractYear, 1, 0) 'Subtract 1 if birthday has not yet occured this year
    End Function

    ''' <summary>
    ''' Get the number of total years since the StartDate
    ''' </summary>
    ''' <param name="pStartDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function NumYearsSince(ByVal pStartDate As DateTime) As Integer
        Return NumYearsBetween(pStartDate, DateTimeStruct.DateNow)
    End Function
    ''' <summary>
    ''' Calculates the number of full months that have elapsed between two dates. This means that if the start date is
    ''' 1/30/2011 and the end date is 2/1/2011, it will return 0 since a full month has not passed.
    ''' </summary>
    Public Shared Function NumMonthsBetween(ByVal pStartDate As DateTime, ByVal pEndDate As DateTime) As Integer
        Return ((pEndDate.Year - pStartDate.Year) * 12) + pEndDate.Month - pStartDate.Month - If(pStartDate.Day > pEndDate.Day, 1, 0)
    End Function

    ''' <summary>
    ''' Returns # of days in a given month/year.   If no year is specified, the current year is assumed.
    ''' </summary>
    Public Shared Function getDaysInMonth(ByVal month As Integer, Optional ByVal year As Integer = -1) As Integer
        If year = -1 Then
            year = DateTimeStruct.DateNow.Year
        End If

        'add one month then subtract a day
        Return New Date(year, month, 1).AddMonths(1).AddDays(-1).Day
    End Function

    Public Shared Function GetLocalDate(ByVal Optional timeZoneId As String = "Pacific Standard Time") As DateTime
        Dim pstZone As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId)
        Dim pstTime As DateTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, pstZone)
        Return pstTime
    End Function
End Class
