﻿Option Strict On

Public Interface ITimeZoneLoader

    ReadOnly Property IsTimeZoneConfigFileExists(ByVal psTimeZoneConfigPath As String) As Boolean

    Function LoadTimeZoneInfo(ByVal psTimeZoneID As String, ByVal poTimeZoneConfig As XDocument) As TimeZoneInfo

End Interface
