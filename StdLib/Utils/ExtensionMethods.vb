﻿Imports System.Data
Imports System.Runtime.CompilerServices
Imports StdLib.DBUtils

''' <summary>
'''
''' Remember to suffix all methods defined here with an underscore (_). This is just in case the base class ever gets
''' extended with a method of the same name as one of our extension methods.
''' </summary>
''' <remarks>
''' Before adding any new extension, please consult with seniors on team to make sure the addition would be useful.   
''' Becase this class is used everywhere, it's important we be careful to avoid adding junk extensions that are only 
''' useful in very small cases. 
''' </remarks>
Public Module ExtensionMethods
    Private log As log4net.ILog = log4net.LogManager.GetLogger("ExtensionMethods")

#Region "Datatable Select SQl Unescape functions"
    <Extension()> Public Function Select_(ByVal pDataTable As DataTable, ByVal pFilter As String) As DataRow()
        Return pDataTable.Select(CSQLDBUtils.UnescapeSQLBraces(pFilter))
    End Function

    <Extension()> Public Function Select_(ByVal pDataTable As DataTable) As DataRow()
        Return pDataTable.Select()
    End Function

    <Extension()> Public Function Select_(ByVal pDataTable As DataTable, ByVal pFilter As String, ByVal pSort As String) As DataRow()
        Return pDataTable.Select(CSQLDBUtils.UnescapeSQLBraces(pFilter), pSort)
    End Function

    <Extension()> Public Function Select_(ByVal pDataTable As DataTable, ByVal pFilter As String, ByVal pSort As String, ByVal pRecordStates As System.Data.DataViewRowState) As DataRow()
        Return pDataTable.Select(CSQLDBUtils.UnescapeSQLBraces(pFilter), pSort, pRecordStates)
    End Function

#End Region

    <Extension>
    Public Function Contains_(ByVal str As String, ByVal value As String, ByVal comparisonType As StringComparison) As Boolean
        Return str.IndexOf(value, comparisonType) > -1
    End Function

End Module
