Imports System.Data

''' <summary>
''' Interface that provides functions to transform data in the data table or data reader before exporting to csv for CDataTable2Text class
''' </summary>

Public Interface IDataFieldsTransformation
    ''' <summary>
    ''' Used when data source is data table. Please define all columns that need transformation before exporting here.
    ''' </summary>
    Function TransformData(ByVal pDr As DataRow, ByVal psColumnName As String) As String

    ''' <summary>
    ''' Used when data source is data reader. Please define all columns that need transformation before exporting here.
    ''' </summary>
    Function TransformData(ByVal pReader As IDataReader, ByVal psColumnName As String) As String
End Interface
