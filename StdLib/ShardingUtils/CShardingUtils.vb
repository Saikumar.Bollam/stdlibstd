Option Strict On

Imports StdLib.DBUtils
Imports StdLib.SpecializedDataTypes

Namespace ShardingUtils
	''' <summary>
	''' This is the utils class for sharding project which provides shard mapping info, adding sync entry and check sharding preference
	''' </summary>
	Public Class CShardingUtils
		Public Enum ShardingSourceTypes
			Loan
			LoanLastModifiedOnly
			XpressApp
			XpressAppLastModifiedOnly
			LoanTracker
			LoanOfficerAppSubscription
			Lender
			WebMS
			BookingAccountNumber
			Branch
			LoanOfficer
			Unknown
		End Enum

		Protected Shared Log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CShardingUtils))

		Public Shared Function IsReferenceTableSync(ByVal pShardingSourceType As ShardingSourceTypes) As Boolean
			Select Case pShardingSourceType
				Case ShardingSourceTypes.Branch, ShardingSourceTypes.LoanOfficer, ShardingSourceTypes.Lender
					Return True
				Case Else
					Return False
			End Select
		End Function

		Public Shared ReadOnly Property ShardingUserCredDecryptionPasscode As String
			Get
				Return "LPQShardingUser"
			End Get
		End Property

		Public Shared ReadOnly Property ShardingAdminCredDecryptionPasscode As String
			Get
				Return "LPQShardingMaster"
			End Get
		End Property

		Public Shared Function MapShardingSourceTypesToLongString(ByVal psShardingSourceType As String) As String
			Return MapShardingSourceTypesToLongString(MapShardingSourceTypesToEnum(psShardingSourceType))
		End Function

		Public Shared Function MapShardingSourceTypesToLongString(ByVal pShardingSourceType As ShardingSourceTypes) As String
			Select Case pShardingSourceType
				Case ShardingSourceTypes.Loan
					Return "Loan"
				Case ShardingSourceTypes.LoanLastModifiedOnly
					Return "Loan Last Modified Only"
				Case ShardingSourceTypes.XpressApp
					Return "XpressApp"
				Case ShardingSourceTypes.XpressAppLastModifiedOnly
					Return "XpressApp Last Modified Only"
				Case ShardingSourceTypes.WebMS
					Return "WebMS"
				Case ShardingSourceTypes.LoanTracker
					Return "Loan Tracker"
				Case ShardingSourceTypes.LoanOfficerAppSubscription
					Return "LoanOfficer App Subscription"
				Case ShardingSourceTypes.Lender
					Return "Lender"
				Case ShardingSourceTypes.BookingAccountNumber
					Return "Booking Account Number"
				Case ShardingSourceTypes.Branch
					Return "Branch"
				Case ShardingSourceTypes.LoanOfficer
					Return "Loan Officer"
				Case Else
					Return ""
			End Select
		End Function

		Public Shared Function MapShardingSourceTypesToString(ByVal pShardingSourceType As ShardingSourceTypes) As String
			Select Case pShardingSourceType
				Case ShardingSourceTypes.Loan
					Return "LOAN"
				Case ShardingSourceTypes.LoanLastModifiedOnly
					Return "LOANLM"
				Case ShardingSourceTypes.XpressApp
					Return "XA"
				Case ShardingSourceTypes.XpressAppLastModifiedOnly
					Return "XALM"
				Case ShardingSourceTypes.WebMS
					Return "WEBMS"
				Case ShardingSourceTypes.LoanTracker
					Return "LT"
				Case ShardingSourceTypes.LoanOfficerAppSubscription
					Return "LAS"
				Case ShardingSourceTypes.Lender
					Return "LENDER"
				Case ShardingSourceTypes.BookingAccountNumber
					Return "BKACCNUM"
				Case ShardingSourceTypes.Branch
					Return "BRANCH"
				Case ShardingSourceTypes.LoanOfficer
					Return "LO"
				Case Else
					Return ""
			End Select
		End Function

		Public Shared Function MapShardingSourceTypesToEnum(ByVal psShardingSourceType As String) As ShardingSourceTypes
			Select Case psShardingSourceType
				Case "LOAN"
					Return ShardingSourceTypes.Loan
				Case "LOANLM"
					Return ShardingSourceTypes.LoanLastModifiedOnly
				Case "XA"
					Return ShardingSourceTypes.XpressApp
				Case "XALM"
					Return ShardingSourceTypes.XpressAppLastModifiedOnly
				Case "WEBMS"
					Return ShardingSourceTypes.WebMS
				Case "LT"
					Return ShardingSourceTypes.LoanTracker
				Case "LAS"
					Return ShardingSourceTypes.LoanOfficerAppSubscription
				Case "LENDER"
					Return ShardingSourceTypes.Lender
				Case "BKACCNUM"
					Return ShardingSourceTypes.BookingAccountNumber
				Case "LO"
					Return ShardingSourceTypes.LoanOfficer
				Case "BRANCH"
					Return ShardingSourceTypes.Branch
				Case Else
					Return ShardingSourceTypes.Unknown
			End Select
		End Function

		Public Enum SyncOptions
			INSERT
			UPDATE
			DELETE
			SYNC
			Unknown
		End Enum

		Public Shared Function MapSyncOptionsToString(ByVal pSyncOption As SyncOptions) As String
			Select Case pSyncOption
				Case SyncOptions.INSERT
					Return "INSERT"
				Case SyncOptions.UPDATE
					Return "UPDATE"
				Case SyncOptions.DELETE
					Return "DELETE"
				Case SyncOptions.SYNC
					Return "SYNC"
				Case Else
					Return ""
			End Select
		End Function

		Public Shared Function MapSyncOptionsToEnum(ByVal psSyncOption As String) As SyncOptions
			Select Case psSyncOption
				Case "INSERT"
					Return SyncOptions.INSERT
				Case "UPDATE"
					Return SyncOptions.UPDATE
				Case "DELETE"
					Return SyncOptions.DELETE
				Case "SYNC"
					Return SyncOptions.SYNC
				Case Else
					Return SyncOptions.Unknown
			End Select
		End Function

		Public Enum TargetShardTypes
			DataShard
			RecentShard
			ALL
			None
		End Enum

		Public Shared Function MapTargetShardTypesToString(ByVal pTargetShardType As TargetShardTypes) As String
			Select Case pTargetShardType
				Case TargetShardTypes.DataShard
					Return "DS"
				Case TargetShardTypes.RecentShard
					Return "RS"
				Case TargetShardTypes.ALL
					Return "ALL"
				Case Else
					Return ""
			End Select
		End Function

		Public Shared Function MapTargetShardTypesToEnum(ByVal pTargetShardType As String) As TargetShardTypes
			Select Case pTargetShardType
				Case "DS"
					Return TargetShardTypes.DataShard
				Case "RS"
					Return TargetShardTypes.RecentShard
				Case "ALL"
					Return TargetShardTypes.ALL
				Case Else
					Return TargetShardTypes.None
			End Select
		End Function

		Public Shared Sub AddShardingSyncEntry(ByVal pShardingSourceType As ShardingSourceTypes, ByVal pSyncOption As SyncOptions, ByVal psSourceKey As String)
			AddShardingSyncEntry(pShardingSourceType, pSyncOption, psSourceKey, TargetShardTypes.ALL, "")
		End Sub

		Public Shared Sub AddShardingSyncEntry(ByVal pShardingSourceType As ShardingSourceTypes, ByVal pSyncOption As SyncOptions, ByVal psSourceKey As String,
			ByVal pTargetShardType As TargetShardTypes, ByVal pShardName As String)
			Using oDB As New CSQLDBUtils(CSQLConnectionString.CONNECTIONSTRING)
				AddShardingSyncEntry(pShardingSourceType, pSyncOption, psSourceKey, oDB, False, pTargetShardType, pShardName)
			End Using
		End Sub

		Public Shared Sub AddShardingSyncEntry(ByVal pShardingSourceType As ShardingSourceTypes, ByVal pSyncOption As SyncOptions, ByVal psSourceKey As String, ByVal pDB As IDBUtils)
			AddShardingSyncEntry(pShardingSourceType, pSyncOption, psSourceKey, pDB, False)
		End Sub

		Public Shared Sub AddShardingSyncEntry(ByVal pShardingSourceType As ShardingSourceTypes, ByVal pSyncOption As SyncOptions, ByVal psSourceKey As String, ByVal pDB As IDBUtils,
			ByVal pbIsTransactioned As Boolean)
			AddShardingSyncEntry(pShardingSourceType, pSyncOption, psSourceKey, pDB, pbIsTransactioned, TargetShardTypes.ALL, "")
		End Sub

		Public Shared Sub AddShardingSyncEntry(ByVal pShardingSourceType As ShardingSourceTypes, ByVal pSyncOption As SyncOptions, ByVal psSourceKey As String,
			ByVal pDB As IDBUtils, ByVal pbIsTransactioned As Boolean, ByVal pTargetShardType As TargetShardTypes, ByVal pShardName As String)

			Dim sAppID As String = ""
			Select Case pShardingSourceType
				Case ShardingSourceTypes.BookingAccountNumber, ShardingSourceTypes.Loan, ShardingSourceTypes.LoanLastModifiedOnly,
					ShardingSourceTypes.WebMS, ShardingSourceTypes.XpressApp, ShardingSourceTypes.XpressAppLastModifiedOnly
					sAppID = psSourceKey
				Case ShardingSourceTypes.Branch, ShardingSourceTypes.Lender, ShardingSourceTypes.LoanOfficer
				Case Else
					Throw New Exception("Invalid ShardingSourceType: AppID field will be missing by calling this method")
			End Select

			AddShardingSyncEntry(pShardingSourceType, pSyncOption, psSourceKey, pDB, pbIsTransactioned, pTargetShardType, pShardName, sAppID)
		End Sub

		Public Shared Sub AddShardingSyncEntry(ByVal pShardingSourceType As ShardingSourceTypes, ByVal pSyncOption As SyncOptions, ByVal psSourceKey As String,
			ByVal pDB As IDBUtils, ByVal pbIsTransactioned As Boolean, ByVal pTargetShardType As TargetShardTypes, ByVal pShardName As String, ByVal pAppID As String)
			If Not IsShardingEnabled Then
				Return
			End If

			Dim oInsert As New cSQLInsertStringBuilder("ShardingSyncs")
			oInsert.AppendValue("SourceType", New CSQLParamValue(MapShardingSourceTypesToString(pShardingSourceType)))
			oInsert.AppendValue("SourceKey ", New CSQLParamValue(psSourceKey))
			oInsert.AppendValue("SyncOptions", New CSQLParamValue(MapSyncOptionsToString(pSyncOption)))
			oInsert.AppendValue("lastUpdatedTime", New CSQLParamValue(DateTimeStruct.DateNow))
			oInsert.AppendValue("TargetShardType", New CSQLParamValue(MapTargetShardTypesToString(pTargetShardType)))

			If Not String.IsNullOrEmpty(pShardName) Then
				oInsert.AppendValue("ShardName", New CSQLParamValue(pShardName))
			End If

			If Not String.IsNullOrEmpty(pAppID) Then
				oInsert.AppendValue("AppID", New CSQLParamValue(pAppID))
			End If

			pDB.executeNonQuery(oInsert.SQL, pbIsTransactioned)
		End Sub

		Public Shared ReadOnly Property IsShardingEnabled As Boolean
			Get
				Using oDB As New CSQLDBUtils(CSQLConnectionString.CONNECTIONSTRING)
					Dim oWhere As New CSQLWhereStringBuilder
					oWhere.AppendAndCondition("ShardingEnabled = {0}", New CSQLParamValue("Y"))
					Return CBaseStd.SafeInteger(oDB.getScalarValue("Select count(*) from ShardingPreferences" & oWhere.SQL)) > 0
				End Using
			End Get
		End Property


		Public Shared Sub SetShardingPreference(ByVal pbIsEnabled As Boolean?, ByVal pDB As CSQLDBUtils, ByVal psUserName As String)
			If pbIsEnabled.Value Then
				Log.InfoFormat("{0} set Sharding {1}", If(pbIsEnabled, "enabled", "disabled"))

				Dim oUpdate As cSQLUpdateStringBuilder = cSQLUpdateStringBuilder.CreateUpdateWithoutWhere("ShardingPreferences")
				oUpdate.AppendValue("ShardingEnabled", New CSQLParamValue("Y"))
				Log.Info("SQL to enable sharding: " & oUpdate.SQL)
				If pDB.executeNonQuery(oUpdate.SQL) = 0 Then
					Log.Warn("ShardingPreferences is empty")

					Dim oInsert As New cSQLInsertStringBuilder("ShardingPreferences")
					oInsert.AppendValue("ShardingPreferenceID", New CSQLParamValue(Guid.NewGuid))
					oInsert.AppendValue("ShardingEnabled", New CSQLParamValue("Y"))
					Log.Info("SQL to initialize ShardingPreferences to enable sharding: " & oInsert.SQL)
					pDB.executeNonQuery(oInsert.SQL, False)
				End If

				pDB.executeNonQuery("Exec dbo.spMigrateShardingData")
				Log.Info("Sharding Data Migration Entries Added")

				pDB.executeNonQuery("exec dbo.spEnableShardingTriggers Y")
				Log.Info("Sharding Triggers added.")
			Else
				Dim oUpdate As cSQLUpdateStringBuilder = cSQLUpdateStringBuilder.CreateUpdateWithoutWhere("ShardingPreferences")
				oUpdate.AppendValue("ShardingEnabled", New CSQLParamValue("N"))
				Log.Info("SQL to disable sharding: " & oUpdate.SQL)
				pDB.executeNonQuery(oUpdate.SQL)

				pDB.executeNonQuery("exec dbo.spEnableShardingTriggers N")
				Log.Info("Sharding Triggers added.")
			End If
		End Sub
	End Class
End Namespace
