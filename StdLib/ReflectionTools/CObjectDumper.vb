﻿Imports System.IO
Imports System.Reflection

Namespace ReflectionTools

    ''' <summary>
    ''' Class to help print out details of object similar to how VS IDE does it, but in plain text.   Useful for debugging
    ''' purposes.
    ''' </summary>
    ''' <remarks>
    ''' Original code taken from: http://www.developerfusion.com/tools/convert/csharp-to-vb/
    ''' </remarks>
    Public Class CObjectDumper

        ''' <summary>
        ''' Writes first level properties to console.
        ''' </summary>
        Public Shared Sub Write(ByVal element As Object)
            Write(element, 0)
        End Sub

        ''' <summary>
        ''' Writes object to console.
        ''' </summary>
        Public Shared Sub Write(ByVal element As Object, ByVal depth As Integer)
            Write(element, depth, Console.Out)
        End Sub

        ''' <summary>
        ''' Writes object to output streatm.
        ''' </summary>
        Public Shared Sub Write(ByVal element As Object, ByVal depth As Integer, ByVal log As TextWriter)
            Dim dumper As New CObjectDumper(depth)
            dumper.writer = log
            dumper.WriteObject(Nothing, element)
        End Sub

        Public Shared Function ConvertToString(ByVal element As Object, ByVal depth As Integer) As String
            Using sw As New System.IO.StringWriter()


                Write(element, depth, sw)
                Return sw.ToString
            End Using
        End Function

        Private writer As TextWriter
        Private pos As Integer
        Private level As Integer
        Private depth As Integer

        Private Sub New(ByVal depth As Integer)
            Me.depth = depth
        End Sub

        Private Sub Write(ByVal s As String)
            If s IsNot Nothing Then
                writer.Write(s)
                pos += s.Length
            End If
        End Sub

        Private Sub WriteIndent()
            For i As Integer = 0 To level - 1
                writer.Write("  ")
            Next
        End Sub

        Private Sub WriteLine()
            writer.WriteLine()
            pos = 0
        End Sub

        Private Sub WriteTab()
            Write("  ")
            While pos Mod 8 <> 0
                Write(" ")
            End While
        End Sub

        Private Sub WriteObject(ByVal prefix As String, ByVal element As Object)
            If element Is Nothing OrElse TypeOf element Is ValueType OrElse TypeOf element Is String Then
                WriteIndent()
                Write(prefix)
                WriteValue(element)
                WriteLine()
            Else
                Dim enumerableElement As IEnumerable = TryCast(element, IEnumerable)
                If enumerableElement IsNot Nothing Then
                    For Each item As Object In enumerableElement
                        If TypeOf item Is IEnumerable AndAlso Not (TypeOf item Is String) Then
                            WriteIndent()
                            Write(prefix)
                            Write("...")
                            WriteLine()
                            If level < depth Then
                                level += 1
                                WriteObject(prefix, item)
                                level -= 1
                            End If
                        Else
                            WriteObject(prefix, item)
                        End If
                    Next
                Else
                    Dim members As MemberInfo() = element.[GetType]().GetMembers(BindingFlags.[Public] Or BindingFlags.Instance)
                    WriteIndent()
                    Write(prefix)
                    Dim propWritten As Boolean = False
                    For Each m As MemberInfo In members
                        Dim f As FieldInfo = TryCast(m, FieldInfo)
                        Dim p As PropertyInfo = TryCast(m, PropertyInfo)
                        If f IsNot Nothing OrElse p IsNot Nothing Then
                            If propWritten Then
                                WriteTab()
                            Else
                                propWritten = True
                            End If
                            Write(m.Name)
                            Write("=")
                            Dim t As Type = If(f IsNot Nothing, f.FieldType, p.PropertyType)
                            If t.IsValueType OrElse t Is GetType(String) Then
                                WriteValue(If(f IsNot Nothing, f.GetValue(element), p.GetValue(element, Nothing)))
                            Else
                                If GetType(IEnumerable).IsAssignableFrom(t) Then
                                    Write("...")
                                Else
                                    Write("{ }")
                                End If
                            End If
                        End If
                    Next
                    If propWritten Then
                        WriteLine()
                    End If
                    If level < depth Then
                        For Each m As MemberInfo In members
                            Dim f As FieldInfo = TryCast(m, FieldInfo)
                            Dim p As PropertyInfo = TryCast(m, PropertyInfo)
                            If f IsNot Nothing OrElse p IsNot Nothing Then
                                Dim t As Type = If(f IsNot Nothing, f.FieldType, p.PropertyType)
                                If Not (t.IsValueType OrElse t Is GetType(String)) Then
                                    Dim value As Object = If(f IsNot Nothing, f.GetValue(element), p.GetValue(element, Nothing))
                                    If value IsNot Nothing Then
                                        level += 1
                                        WriteObject(m.Name & ": ", value)
                                        level -= 1
                                    End If
                                End If
                            End If
                        Next
                    End If
                End If
            End If
        End Sub

        Private Sub WriteValue(ByVal o As Object)
            If o Is Nothing Then
                Write("null")
            ElseIf TypeOf o Is DateTime Then
                Write(CType(o, DateTime).ToShortDateString())
            ElseIf TypeOf o Is ValueType OrElse TypeOf o Is String Then
                Write(o.ToString())
            ElseIf TypeOf o Is IEnumerable Then
                Write("...")
            Else
                Write("{ }")
            End If
        End Sub
    End Class

End Namespace