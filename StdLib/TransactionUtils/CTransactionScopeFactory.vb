﻿Imports System.Transactions

Namespace TransactionUtils
    Public Class CTransactionScopeFactory

        ''' <summary>
        ''' Creates TransactionScope with default .NET options
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>
        ''' http://blogs.msdn.com/b/dbrowne/archive/2010/05/21/using-new-transactionscope-considered-harmful.aspx
        ''' 
        ''' In case in future, if we have issues with Transaction scope, we can modify the default implementation in here
        ''' </remarks>
        Public Shared Function CreateTransactionScope() As TransactionScope

            Return New TransactionScope()
        End Function

        ''Create TransactionScope with ReadCommitted option. Not needed now unless we are having problems with default constructor
        'Public Shared Function CreateTransactionScope() As TransactionScope
        '	Dim transactionOptions = New TransactionOptions()
        '	TransactionOptions.IsolationLevel = IsolationLevel.ReadCommitted
        '	TransactionOptions.Timeout = TransactionManager.MaximumTimeout
        '	return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        'End Function

    End Class
End Namespace


