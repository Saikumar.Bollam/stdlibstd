Imports System.Xml.Serialization

Namespace Text
    ''' <summary>
    ''' Class to help build up error messages.
    ''' </summary>
    ''' <remarks></remarks>
    <Serializable()> _
    Public Class CErrorFormatter
        '    Implements IEnumerable

        'this will make the error message a text value of the current element during serialization rather than create another sub node
        <XmlText()> Public Text As String = ""

        Public Sub New()
        End Sub

        Public Sub New(ByVal sErr As String)
            Text = sErr
        End Sub

        Public Shadows Function equals(ByVal s As String) As Boolean
            Return Text.Equals(s)
        End Function

        Public Function isEmpty() As Boolean
            Return Text = ""
        End Function


        'conveninience function that works like the String.Format
        Public Overloads Sub append(ByVal ErrorFormat As String, ByVal ParamArray args() As Object)
            append(String.Format(ErrorFormat, args))
        End Sub

        Public Overloads Sub append(ByVal err As String, Optional ByVal allowDuplicateErrorMessages As Boolean = True)
            If Text = "" Then
                Text = err
            ElseIf allowDuplicateErrorMessages OrElse Text.IndexOf(err) = -1 Then 'don't insert duplicate err messages
                Text += vbCrLf & err
            End If
        End Sub

        Public Overloads Sub append(ByVal poList As IEnumerable)
            Dim itm As Object
            For Each itm In poList
                append(itm.ToString, True)
            Next
        End Sub

        Public Function toArray() As String()
            Dim arrList As New ArrayList()
            Dim arr() As String = Text.Split(vbCrLf.ToCharArray)
            Dim i As Integer
            For i = 0 To arr.Length - 1
                If arr(i) <> "" Then
                    arrList.Add(arr(i))
                End If
            Next
            Return CType(arrList.ToArray(GetType(String)), String())
        End Function

        Public Function getFormatedError(ByVal seperator As String) As String
            Return Text.Replace(vbCrLf, seperator)
        End Function

    End Class
End Namespace
