Imports System.Text.RegularExpressions
Namespace Text
    'phone number data structure that helps parsing phone #'s
    'sample usage: 
    'dim phone as new CPhoneNumber()
    'try 
    ' phone.parse ("713-123-1234")
    ' msgbox (phone.fullPhoneNumber)
    'catch ex as system.formatexception 
    ' 'do something 
    'end try 
    <Serializable()> _
    Public Class CPhoneNumber
        Public AreaCode As String
        Public First3 As String
        Public Second4 As String
        Public Extension As String
        Public Const EXTENSION_SEPERATOR As String = "x"

        ''' <summary>
        ''' use this and then use the Parse routine -- preferred over handling inside constructor b/c more consistent 
        ''' </summary>
        Public Sub New()

        End Sub

        ''' <summary>
        ''' please don't use this constructor b/c it is not consistent and clear as the example shown above.  Client app may not be as likely to catch a constructor exception as it would for a call to Parse()
        ''' </summary>
        Public Sub New(ByVal sFullPhoneNumber As String)
            parsePhoneNumber(sFullPhoneNumber)
        End Sub
        Public Property FullPhoneNumber() As String
            Get
                Dim sResult As String = ""
                If AreaCode <> "" Then
                    sResult = "(" & AreaCode & ") "
                End If
                sResult += First3 & "-" & Second4

                If Extension <> "" Then
                    sResult += EXTENSION_SEPERATOR & Extension
                End If

                Return sResult
            End Get
            Set(ByVal Value As String)
                parsePhoneNumber(Value)
            End Set
        End Property

        Public Overrides Function ToString() As String
            Return FullPhoneNumber
        End Function

        Public Shared Function isPhoneNumber(ByVal input As String) As Boolean
            If input = "" Then
                Return False
            End If

            input = input.Replace(" ", "")

            ' Regex match
            Dim options As RegexOptions = RegexOptions.None
            Dim regex As Regex = New Regex(_PhonePattern, options)
            'Dim input As String = "asdf (123)456-1234" & Chr(10) & Chr(13) & "asdf (123)456-4234x55" & Chr(10) & Chr(13) & "123)456-4234x55" & Chr(10) & Chr(13) & "123456-4234x55" & Chr(10) & Chr(13) & "1234564234x55" & Chr(10) & Chr(13) & "1234564234x" & Chr(10) & Chr(13) & "123" & Chr(10) & Chr(13) & "" & Chr(10) & Chr(13) & ""

            ' Check for match
            Return regex.IsMatch(input)
        End Function


        Public Sub Parse(ByVal psInput As String)
            parsePhoneNumber(psInput)
        End Sub
        Private Shared _PhonePattern As String = "^((\((?<area>\d{3})\))|((?<area>\d{3})-?))\s?(?<first3>\d{3})-?(?<second4>\d{4})(?:x(?<ext>\d*)|)$" '"\(?(?<area>\d{3})\)?(?<first3>\d{3})-?(?<second4>\d{4})(?:x(?<ext>\d*)|)"
        Private Sub parsePhoneNumber(ByVal input As String)
            If input = "" Then
                Throw New ArgumentException("Input cannot be empty.")
            End If
            input = input.Replace(" ", "")

            ' Regex match
            Dim options As RegexOptions = RegexOptions.None
            Dim regex As Regex = New Regex(_PhonePattern, options)
            'Dim input As String = "asdf (123)456-1234" & Chr(10) & Chr(13) & "asdf (123)456-4234x55" & Chr(10) & Chr(13) & "123)456-4234x55" & Chr(10) & Chr(13) & "123456-4234x55" & Chr(10) & Chr(13) & "1234564234x55" & Chr(10) & Chr(13) & "1234564234x" & Chr(10) & Chr(13) & "123" & Chr(10) & Chr(13) & "" & Chr(10) & Chr(13) & ""

            ' Check for match
            Dim isMatch As Boolean = regex.IsMatch(input)
            If isMatch = False Then
                Throw New FormatException(String.Format("Unable to parse phone number: {0}", input))
                'System.Windows.Forms.MessageBox.Show(input, "IsMatch")
            End If

            ' Get match
            Dim match As Match = regex.Match(input)

            ' Named groups
            Dim areaGroup As String = match.Groups("area").Value
            Dim first3Group As String = match.Groups("first3").Value
            Dim second4Group As String = match.Groups("second4").Value
            Dim extGroup As String = match.Groups("ext").Value

            Me.AreaCode = areaGroup
            Me.First3 = first3Group
            Me.Second4 = second4Group
            Me.Extension = extGroup

        End Sub
    End Class

End Namespace