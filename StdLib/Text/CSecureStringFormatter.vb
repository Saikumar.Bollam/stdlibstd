Imports System.Configuration.ConfigurationManager
Imports System.Text.RegularExpressions
Imports System.Xml

Namespace Text
    ''' <summary>
    ''' This class provides a means to quickly mask out sensitive data inside strings/xml documents.  It's not designed to be super efficient
    ''' as it does rely heavily on regex and brute force checking.  See UnitTester.TextTests for sample usage.
    ''' </summary>		
    Public Class CSecureStringFormatter
        Private Shared log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CSecureStringFormatter))

        Private Const ENCRYPTED_PW As String = "xxxxx"

        ''' <summary>
        ''' Masks sensitive data using the RegEx defined in the SENSITIVE_DATA_REGEX app setting.
        ''' </summary>
        ''' <param name="psData"></param>
        ''' <returns>The input string with sensitive data masked with 'x' characters.</returns>
        ''' <remarks>Note if the regex 'sensitive' group only captures 5 char, then only the first 5 characters are masked out!</remarks>
        Public Shared Function MaskSensitiveData(ByVal psData As String) As String
			Dim oMatches As MatchCollection = Regex.Matches(psData, AppSettings("SENSITIVE_DATA_REGEX"), RegexOptions.IgnoreCase Or RegexOptions.ExplicitCapture)

			For Each oMatch As Match In oMatches
                psData = psData.Replace(oMatch.Value, oMatch.Groups("prefix").Value & ENCRYPTED_PW) 'New String("x"c, oMatch.Groups("sensitive").Value.Length))
            Next

            Return psData
        End Function

        ''' <summary>
        ''' Checks the Sensitive Names inside the XML Node name.
        ''' It will return true if Sensitive Tag Name is SSN and XMLNodeName contains SSN word
        ''' this will make sure that we identify and mask the XML Tags with name SubjectSSN, MDBSubjectSSN, InquirySubjectSSN etc.
        ''' </summary>
        ''' <param name="psNodeName">XML Node Name</param>
        ''' <param name="psSensitiveNames">Sensitive Field/Tag Names</param>
        ''' <returns>True/False based on match</returns>
        Private Shared Function CheckXMLNodeName(ByVal psNodeName As String, ByVal psSensitiveNames As String()) As Boolean
            For Each sSensitiveName As String In psSensitiveNames
                If psNodeName.ToLower.Contains(sSensitiveName.ToLower) Then
                    Return True
                End If
            Next
            Return False
        End Function

        ''' <summary>
        ''' Masks sensitive XML data from nodes or attributes which match the names in the SENSITIVE_DATA_XML_NAMES app setting. 
        ''' This function is private so that we don't support taking in a DOM document to avoid potentially creating any side effects.
        ''' The routine internally does modify the DOM.
        ''' </summary>
        ''' <param name="psXMLData">Note we don't support taking in a DOM document since we don't want to potentially create any side affects.  The routine internally 
        ''' does modify the DOM.</param>
        ''' <returns>If no error, the XML data with the sensitive information masked with 'x' characters.  Else, the input string.</returns>
        ''' <remarks>Note the search here is not case insensitive.</remarks>
        Private Shared Function MaskSensitiveXMLData(ByVal psXMLData As XDocument) As String
            Dim sensitive_names As String() = AppSettings("SENSITIVE_DATA_XML_NAMES").Split(","c)

            Dim sensitiveElements As IEnumerable(Of XElement) = From ele As XElement In psXMLData.Descendants
                                                                Where sensitive_names.Contains(ele.Name.ToString, StringComparer.InvariantCultureIgnoreCase) OrElse
                                                                CheckXMLNodeName(Convert.ToString(ele.Name), sensitive_names)

            For Each ele As XElement In sensitiveElements
                If ele.Descendants.Count = 0 Then 'Don't want to replace nested nodes
                    ele.Value = ENCRYPTED_PW
                End If
            Next

            Dim sensitiveAttributes As IEnumerable(Of XAttribute) = From attr As XAttribute In psXMLData.Descendants.Attributes
                                                                    Where sensitive_names.Contains(attr.Name.ToString, StringComparer.InvariantCultureIgnoreCase) OrElse
                                                                CheckXMLNodeName(Convert.ToString(attr.Name), sensitive_names)
            For Each attr As XAttribute In sensitiveAttributes
                attr.Value = ENCRYPTED_PW
            Next

            Dim sensitiveCData As IEnumerable(Of XNode) = From ele As XNode In psXMLData.DescendantNodes Where ele.NodeType = XmlNodeType.CDATA Select ele
            For Each node As XNode In sensitiveCData
                Try
                    ' Recursive call, 
                    ' try to mask CDATA as though it were an xml document since 
                    ' that seems to be a common usage of CDATA 
                    Dim oDoc As XDocument = XDocument.Parse(CType(node, XCData).Value)
                    CType(node, XCData).Value = MaskSensitiveXMLData(oDoc)
                Catch ex As Exception
                    ' potentially expensive especially if CDATA is large + multiple replacements
                    CType(node, XCData).Value = MaskSensitiveData(CType(node, XCData).Value)
                End Try
            Next

            Return psXMLData.ToString(SaveOptions.DisableFormatting)
        End Function

        ''' <summary>
        ''' Masks sensitive XML data from nodes or attributes which match the names in the SENSITIVE_DATA_XML_NAMES app setting.
        ''' </summary>
        ''' <param name="psXMLData">Note we don't support taking in a DOM document since we don't want to potentially create any side affects.  The routine internally 
        ''' does modify the DOM.</param>
        ''' <returns>If no error, the XML data with the sensitive information masked with 'x' characters.  Else, the input string.</returns>
        ''' <remarks>Note the search here is not case insensitive.</remarks>
        Public Shared Function MaskSensitiveXMLData(ByVal psXMLData As String) As String
            If String.IsNullOrWhiteSpace(psXMLData) Then
                Return psXMLData
            End If

            Try
                Dim doc As XDocument = XDocument.Parse(psXMLData)
                Return MaskSensitiveXMLData(doc)
            Catch ex As Exception
                'log.Warn("Unable to strip sensitive XML data", ex)
                log.Info("Unable to strip sensitive XML data", ex)
                Return psXMLData
            End Try
        End Function
    End Class

End Namespace
