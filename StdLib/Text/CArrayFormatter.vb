﻿
Namespace Text
    ''' <summary>
    ''' This class will help generate text formatted data for arrays/list or anything IEnumerable.  
    ''' Usefull obvious samples are generating &lt;li&gt; tags for html, vbcrlf, etc. 
    ''' Note: It's not responsible for storing content to build (it has no member variables).
    ''' </summary>	
    Public Class CArrayFormatter

        Public Delegate Function ItemFormatter(ByVal poItem As Object) As String

        'common functions 
        'generates list items but no surrounding list definition tags (ie: <ol>...)
        Public Shared Function FormatAsHTMLItems(ByVal poList As IEnumerable) As String

            Return Format(poList, "<li>", "</li>")


        End Function

        ''' <summary>
        ''' Shorthand to generate html list types (defined by psListTypeTag).  
        ''' </summary>
        ''' <param name="poList"></param>
        ''' <param name="psListTypeTag"></param>
        ''' <returns></returns>
        ''' <remarks>Empty string returned if no item in list</remarks>
        Public Shared Function FormatAsHTMList(ByVal poList As IEnumerable, Optional ByVal psListTypeTag As String = "ol") As String

            Dim sTmp As String = Format(poList, "<li>", "</li>")
            If sTmp = "" Then
                Return ""
            End If

            Dim sb As New System.Text.StringBuilder()
            sb.Append("<").Append(psListTypeTag).Append(">")
            sb.Append(sTmp)
            sb.Append("</").Append(psListTypeTag).Append(">")

            Return sb.ToString

        End Function

        ''' <summary>
        ''' Shorthand to generate list seperated by comma.
        ''' </summary>
        Public Shared Function FormatAsTextComma(ByVal poList As IEnumerable) As String
            Return Format(poList, ",")
        End Function

        ''' <summary>
        ''' Shorthand to generate list seperated by carraige return/line feed.
        ''' </summary>
        ''' <param name="poList"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function FormatAsTextLineBreak(ByVal poList As IEnumerable) As String
            Return Format(poList, vbCrLf)
        End Function

        ''' <summary>
        ''' Seperates a list of values by a given seperator.
        ''' ie: apple;orange;peach)
        ''' </summary>
        ''' <param name="poList"></param>
        ''' <param name="psSeperator"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Format(ByVal poList As IEnumerable, ByVal psSeperator As String) As String

            Return Format(poList, psSeperator, _
              Function(o As Object) If(o Is Nothing, "", o.ToString()) _
              )

        End Function



        ''' <summary>
        ''' Flexible formatting function that will iterate through a list and use a delegate to handle the 
        ''' formatting of the item before appending to the resulting list, seperated by psSeperator.
        ''' </summary>		
        Public Shared Function Format(ByVal pdFormatter As ItemFormatter, ByVal poList As IEnumerable, ByVal psSeperator As String) As String

            Return Format(poList, psSeperator, _
               Function(o As Object) If(o Is Nothing, "", pdFormatter(o)) _
               )
        End Function



        ''' <summary>
        ''' This generates a list with each item encapsulated w/ data before AND after 
        ''' (ie: <li>apple</li><li>orange</li><li>peach</li>)
        ''' </summary>		
        Public Shared Function Format(ByVal poList As IEnumerable, ByVal psPreFix As String, ByVal psPostFix As String) As String
            Return Format(poList, Function(o As Object) psPreFix & o.ToString() & psPostFix)
        End Function

#Region "Fundamental building blocks for other routines"



        ''' <summary>
        ''' Generic formatter that takes in a formatting routine to format each item.
        ''' Sample call:		
        ''' response.write ( Format(poList, Function(o As Object) o.ToString() )
        ''' </summary>
        Public Shared Function Format(Of T)(ByVal poList As IEnumerable, ByVal poFormatter As Func(Of T, String)) As String

            If poList Is Nothing Then
                Return ""
            End If

            Dim sb As New System.Text.StringBuilder()

            Dim itm As Object
            For Each itm In poList
                sb.Append(poFormatter(CType(itm, T)))
            Next

            Return sb.ToString
        End Function

        ''' <summary>
        ''' Generic formatter that takes in a formatting routine and provides insertion of separator.  
        ''' Sample call:		
        ''' response.write ( Format(poList, ";", Function(o As Object) o.ToString() )
        ''' </summary>
        Public Shared Function Format(Of T)(ByVal poList As IEnumerable, ByVal psSeperator As String, ByVal poFormatter As Func(Of T, String)) As String

            If poList Is Nothing Then
                Return ""
            End If

            Dim sb As New System.Text.StringBuilder()
            Dim oEnum As IEnumerator = poList.GetEnumerator

            If oEnum.MoveNext = False Then
                Return ""
            Else
                sb.Append(poFormatter(CType(oEnum.Current, T)))
            End If

            While oEnum.MoveNext
                sb.Append(psSeperator)
                sb.Append(poFormatter(CType(oEnum.Current, T)))
            End While

            Return sb.ToString

        End Function
#End Region

    End Class


End Namespace