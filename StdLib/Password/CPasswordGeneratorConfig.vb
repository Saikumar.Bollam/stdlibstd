﻿Option Strict On

Namespace Security
	<Serializable()>
	Public Class CPasswordGeneratorConfig
		<Flags()>
		Public Enum ECharacterType
			Unknown = 0
			UpperCase = 1
			LowerCase = 2
			Number = 4
			SpecialChar = 8
			Letter = UpperCase Or LowerCase     ' either upper case or lower case
			AlphaNumeric = UpperCase Or LowerCase Or Number
			All = UpperCase Or LowerCase Or Number Or SpecialChar
		End Enum

		Public MinTotalLength As Integer    ' 0 or less means not used
		Public MaxTotalLength As Integer    ' 0 or less means not used
		Public StartWith As ECharacterType = ECharacterType.Unknown
		Public MinUpperCase As Integer = 1
		Public MinLowerCase As Integer = 1
		Public MinSpecialChar As Integer = 1
		Public MinNumber As Integer = 1
		Public PasswordReuseNumber As Integer = 0
		Public HashKeyList As New List(Of String)(30)
		Public userLogin As String = ""
		Public ValidCharSet As CPasswordGeneratorConfig.ECharacterType = CPasswordGeneratorConfig.ECharacterType.All
		Public Regex1 As String = ""
		Public Regex2 As String = ""
		Public Regex3 As String = ""
		Public RegexDescription1 As String = ""
		Public RegexDescription2 As String = ""
		Public RegexDescription3 As String = ""

		' if this flag is set, then minUpper and minLower is ignored
		Public MinAlpha As Integer = 0

		Public Function ValidateConditions() As List(Of String)
			Dim errorList As New List(Of String)

			If MinTotalLength > 0 AndAlso MaxTotalLength > 0 AndAlso MinTotalLength > MaxTotalLength Then
				errorList.Add("Min total length cannot be greater than max total length")
			End If
			If MaxTotalLength > 0 AndAlso MinLowerCase + MinUpperCase + MinSpecialChar + MinNumber > MaxTotalLength Then
				errorList.Add("The total minimum characters exceeds the max total length")
			End If
			If MinLowerCase < 0 Then
				errorList.Add("MinLowerCase must be a positive number")
			End If
			If MinUpperCase < 0 Then
				errorList.Add("MinUpperCase must be a positive number")
			End If
			If MinSpecialChar < 0 Then
				errorList.Add("MinSpecialChar must be a postiive number")
			End If
			If MinNumber < 0 Then
				errorList.Add("MinNumber must be a positive number")
			End If
			If MinAlpha < 0 Then
				errorList.Add("MinAlpha must be a positive number")
			End If
			If MinAlpha > 0 Then
				If (ValidCharSet And ECharacterType.LowerCase) <> ECharacterType.LowerCase AndAlso
				(ValidCharSet And ECharacterType.UpperCase) <> ECharacterType.UpperCase Then
					errorList.Add("MinAlpha is specified but the valid charset does not allow it")
				End If
			End If
			If MinLowerCase > 0 Then
				If (ValidCharSet And ECharacterType.LowerCase) <> ECharacterType.LowerCase Then
					errorList.Add("MinLowerCase is specified but the valid charset does not allow it")
				End If
			End If

			If MinUpperCase > 0 Then
				If (ValidCharSet And ECharacterType.UpperCase) <> ECharacterType.UpperCase Then
					errorList.Add("MinUpperCase is specified but the valid charset does not allow it")
				End If
			End If
			If MinSpecialChar > 0 Then
				If (ValidCharSet And ECharacterType.SpecialChar) <> ECharacterType.SpecialChar Then
					errorList.Add("MinSpecialChar is specified but the valid charset does not allow it")
				End If
			End If

			If MinNumber > 0 Then
				If (ValidCharSet And ECharacterType.Number) <> ECharacterType.Number Then
					errorList.Add("MinNumber is specified but the valid charset does not allow it")
				End If
			End If

			Select Case True
				Case StartWith = ECharacterType.AlphaNumeric AndAlso MinLowerCase <= 0 AndAlso MinUpperCase <= 0 AndAlso MinNumber <= 0 AndAlso MinAlpha <= 0
					errorList.Add("Cannot start with an alpha numeric character and require 0 or less of these character types: lower case, upper case, and number")
				Case StartWith = ECharacterType.Letter AndAlso MinLowerCase <= 0 AndAlso MinUpperCase <= 0 AndAlso MinAlpha <= 0
					errorList.Add("Cannot start with a letter and require 0 or less of these character types: lower case and upper case")
				Case StartWith = ECharacterType.LowerCase AndAlso MinLowerCase <= 0
					errorList.Add("Cannot start with a lower case letter and require 0 or less lower case letters")
				Case StartWith = ECharacterType.Number AndAlso MinNumber <= 0
					errorList.Add("Cannot start with a number and require 0 or less numbers")
				Case StartWith = ECharacterType.SpecialChar AndAlso MinSpecialChar <= 0
					errorList.Add("Cannot start with a special character and require 0 or less special characters")
				Case StartWith = ECharacterType.UpperCase AndAlso MinUpperCase <= 0
					errorList.Add("Cannot start with a upper case letter and require 0 or less upper case letters")
			End Select
			Return errorList
		End Function
	End Class
End Namespace