﻿Option Strict On
Imports System.Text.RegularExpressions

Namespace Security
    Public Class CPasswordGenerator
        <Serializable()> _
        Public Class CError
            Public ResultCode As PasswordValidationErrors
            Private _Context As String

            Sub New(ByVal pResultCode As PasswordValidationErrors, ByVal pContext As String)
                ResultCode = pResultCode
                _Context = pContext
            End Sub

            Public ReadOnly Property Message() As String
                Get
                    Select Case ResultCode
                        Case PasswordValidationErrors.InvalidChars
                            Return String.Format("{0} is an invalid character.", _Context)
                        Case PasswordValidationErrors.InvalidConfig
                            Return "Password configuration is invalid."
                        Case PasswordValidationErrors.ReusedPassword
                            Return String.Format("Password has already been used.", _Context)
                        Case PasswordValidationErrors.EmptyReusedPasswordList
                            Return "No Passwords are stored in the database."
                        Case PasswordValidationErrors.InvalidStartChar
                            Return "Password does not start with " & _Context & "."
                        Case PasswordValidationErrors.NotEnoughAlpha
                            Return String.Format("Password contains fewer than {0} letter(s).", _Context)
                        Case PasswordValidationErrors.NotEnoughLower
                            Return String.Format("Password contains fewer than {0} lower case letter(s).", _Context)
                        Case PasswordValidationErrors.NotEnoughNumeric
                            Return String.Format("Password contains fewer than {0} number(s).", _Context)
                        Case PasswordValidationErrors.NotEnoughSymbol
                            Return String.Format("Password contains fewer than {0} special character(s).", _Context)
                        Case PasswordValidationErrors.NotEnoughUpper
                            Return String.Format("Password contains fewer than {0} upper case letter(s).", _Context)
                        Case PasswordValidationErrors.TooLong
                            Return String.Format("Password is longer than {0} character(s).", _Context)
                        Case PasswordValidationErrors.TooShort
                            Return String.Format("Password is shorter than {0} character(s).", _Context)
                        Case PasswordValidationErrors.ContainsLogin
                            Return String.Format("Password contains login", _Context)
                        Case PasswordValidationErrors.Regex1
                            Return _Context
                        Case PasswordValidationErrors.Regex2
                            Return _Context
                        Case PasswordValidationErrors.Regex3
                            Return _Context
                        Case Else
                            Throw New Exception("Unhandled error code.")
                    End Select
                End Get
            End Property
        End Class

        <Serializable()> _
        Public Class CErrorList
            Inherits List(Of CError)

            Public Function ContainsResultCode(ByVal pResultCode As PasswordValidationErrors) As Boolean
                Return Me.Find(Function(item As CError) item.ResultCode = pResultCode) IsNot Nothing
            End Function

            Public Function ToMessageList() As List(Of String)
                Dim result As New List(Of String)
                For Each e As CError In Me
                    result.Add(e.Message)
                Next
                Return result
            End Function
        End Class


        Public Enum PasswordValidationErrors
            TooShort
            TooLong
            NotEnoughAlpha
            NotEnoughNumeric
            NotEnoughSymbol
            NotEnoughLower
            NotEnoughUpper
            InvalidStartChar
            InvalidChars
            InvalidConfig
            ReusedPassword
            EmptyReusedPasswordList
            ContainsLogin
            Regex1
            Regex2
            Regex3
        End Enum

        ' these character sets aren't const so you can overwrite them to fit your needs
        ' for example, lower case l and the number 1 look similar and you might want to exclude them from the set
        Public UPPERCASE As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        Public LOWERCASE As String = "abcdefghijklmnopqrstuvwxyz"
        Public NUMBER As String = "0123456789"
        Public SPECIALCHARS As String = "!@#$^*()-+=_.,;"

        Public Errors As New CErrorList

        Private _config As CPasswordGeneratorConfig
        Protected Shared log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CPasswordGenerator))

        Public Sub New(ByVal pConfig As CPasswordGeneratorConfig)
            _config = pConfig
        End Sub

        ''' <summary>
        ''' generate password based on condition specified by the config.
        ''' returns nothing if there are errors
        ''' check error list for details of errors.	
        ''' To make sure that the generatedPassword is not in the previous password history, please 
        ''' pull the previous password history, hash it and set it to the Hashkeylist in CPasswordGeneratorConfig			
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GeneratePassword() As String
            'ErrorList.Clear()
            Errors.Clear()

            Dim password As String = ""
            Dim random1 As New Random()

            Dim configerrors As List(Of String) = _config.ValidateConditions()
            If configerrors.Count > 0 Then
                Errors.Add(New CError(PasswordValidationErrors.InvalidConfig, ""))
                Return Nothing
            End If

            If _config.MinAlpha > 0 Then
                For i As Integer = 0 To _config.MinAlpha
                    Dim charset As String = UPPERCASE & LOWERCASE
                    password &= charset(random1.Next(0, charset.Count - 1))
                Next
            Else
                If _config.MinUpperCase > 0 Then
                    For i As Integer = 0 To _config.MinUpperCase - 1
                        password &= UPPERCASE(random1.Next(0, UPPERCASE.Length - 1))
                    Next
                End If
                If _config.MinLowerCase > 0 Then
                    For i As Integer = 0 To _config.MinLowerCase - 1
                        password &= LOWERCASE(random1.Next(0, LOWERCASE.Length - 1))
                    Next
                End If

            End If
            If _config.MinSpecialChar > 0 Then
                For i As Integer = 0 To _config.MinSpecialChar - 1
                    password &= SPECIALCHARS(random1.Next(0, SPECIALCHARS.Length - 1))
                Next
            End If

            If _config.MinNumber > 0 Then
                For i As Integer = 0 To _config.MinNumber - 1
                    password &= NUMBER(random1.Next(0, NUMBER.Length - 1))
                Next
            End If

            If _config.StartWith <> CPasswordGeneratorConfig.ECharacterType.Unknown Then
                StartPasswordWith(password, random1, GetCharacterSet(_config.StartWith))
            End If

            If _config.MinTotalLength > 0 AndAlso password.Length < _config.MinTotalLength Then
                PadNCharacters(password, _config.MinTotalLength - password.Length)
            End If

            If _config.MaxTotalLength > 0 AndAlso password.Length < _config.MaxTotalLength Then
                PadNCharacters(password, random1.Next(0, _config.MaxTotalLength - password.Length))
            End If

            Dim Tries As Integer = 0
            While isReusedPassword(password) AndAlso Tries < 1000
                RandomizePasswordPosition(password)
                Tries += 1
            End While

            If Not String.IsNullOrEmpty(_config.userLogin) AndAlso password.IndexOf(_config.userLogin) <> -1 Then
                RandomizePasswordPosition(password)
            End If

            Return password
        End Function


        Private Function GetCharacterSet(ByVal pCharacterType As CPasswordGeneratorConfig.ECharacterType) As String
            Dim charset As String = ""

            Dim temp As CPasswordGeneratorConfig.ECharacterType = pCharacterType And CPasswordGeneratorConfig.ECharacterType.LowerCase
            If temp = CPasswordGeneratorConfig.ECharacterType.LowerCase Then
                charset &= LOWERCASE
            End If
            temp = pCharacterType And CPasswordGeneratorConfig.ECharacterType.UpperCase
            If temp = CPasswordGeneratorConfig.ECharacterType.UpperCase Then
                charset &= UPPERCASE
            End If

            temp = pCharacterType And CPasswordGeneratorConfig.ECharacterType.Number
            If temp = CPasswordGeneratorConfig.ECharacterType.Number Then
                charset &= NUMBER
            End If

            temp = pCharacterType And CPasswordGeneratorConfig.ECharacterType.SpecialChar
            If temp = CPasswordGeneratorConfig.ECharacterType.SpecialChar Then
                charset &= SPECIALCHARS
            End If

            Return charset
        End Function
        Private Sub PadNCharacters(ByRef pPassword As String, ByVal pNumChar As Integer)
            If pNumChar <= 0 Then
                Return
            End If
            Dim charSet As String = GetCharacterSet(_config.ValidCharSet)
            Dim r As New Random
            For i As Integer = 0 To pNumChar - 1
                pPassword &= charSet(r.Next(0, charSet.Length - 1))
            Next
        End Sub

        Private Sub StartPasswordWith(ByRef pPassword As String, ByVal pRandom As Random, ByVal pCharacterSet As String)
            Dim passwordArray As Char() = pPassword.ToCharArray
            Dim index As Integer = Array.FindIndex(passwordArray, Function(c As Char) pCharacterSet.Contains(c))
            If index >= 0 Then
                SwapChar(passwordArray(0), passwordArray(index))
                pPassword = New String(passwordArray)
            ElseIf _config.MaxTotalLength = 0 OrElse _
              (_config.MaxTotalLength > 0 AndAlso pPassword.Length < _config.MaxTotalLength) Then
                ' index should always be found since we added the characters already, but just in case
                pPassword = pCharacterSet(pRandom.Next(0, pCharacterSet.Length - 1)) & pPassword
            End If
        End Sub


        Private Sub RandomizePasswordPosition(ByRef pPassword As String)
            Dim ran As New Random
            ' no point in randomizing if there is only 1 character
            If pPassword.Length <= 1 Then
                Return
            End If
            Dim passwordArray As Char() = pPassword.ToCharArray
            ' leave the first character alone for the startwith condition

            For i As Integer = 1 To pPassword.Length - 1
                SwapChar(passwordArray(i), passwordArray(ran.Next(1, passwordArray.Length - 1)))
            Next
            pPassword = New String(passwordArray)
        End Sub

        Private Sub SwapChar(ByRef pChar1 As Char, ByRef pchar2 As Char)
            Dim temp As Char = pChar1
            pChar1 = pchar2
            pchar2 = temp
        End Sub

        Public Function ValidatePassword(ByVal pPassword As String) As Boolean
            Errors.Clear()
            ' fatal errors
            If _config Is Nothing Then
                Errors.Add(New CError(PasswordValidationErrors.InvalidConfig, ""))
                Return False
            End If
            If String.IsNullOrEmpty(pPassword) Then
                Return False
            End If
            Dim configerrors As List(Of String) = _config.ValidateConditions
            If configerrors.Count > 0 Then
                Errors.Add(New CError(PasswordValidationErrors.InvalidConfig, ""))
                Return False
            End If


            ' non fatal errors
            If _config.MinTotalLength > 0 AndAlso pPassword.Length < _config.MinTotalLength Then
                Errors.Add(New CError(PasswordValidationErrors.TooShort, _config.MinTotalLength.ToString))
            End If
            If _config.MaxTotalLength > 0 AndAlso pPassword.Length > _config.MaxTotalLength Then
                Errors.Add(New CError(PasswordValidationErrors.TooLong, _config.MaxTotalLength.ToString))
            End If

            'If _config.PasswordReuseNumber = 0 Then
            '    Errors.Add(New CError(PasswordValidationErrors.EmptyReusedPasswordList, ""))
            'End If

            If _config.HashKeyList.Count >= 1 Then
                If isReusedPassword(pPassword) Then
                    Errors.Add(New CError(PasswordValidationErrors.ReusedPassword, _config.PasswordReuseNumber.ToString))
                End If
            End If

            If Not String.IsNullOrEmpty(_config.userLogin) AndAlso pPassword.IndexOf(_config.userLogin) <> -1 Then
                Errors.Add(New CError(PasswordValidationErrors.ContainsLogin, ""))
            End If

            Select Case _config.StartWith
                Case CPasswordGeneratorConfig.ECharacterType.AlphaNumeric
                    If False = Char.IsLetterOrDigit(pPassword(0)) Then
                        Errors.Add(New CError(PasswordValidationErrors.InvalidStartChar, "letter or digit."))
                    End If
                Case CPasswordGeneratorConfig.ECharacterType.Letter
                    If False = Char.IsLetter(pPassword(0)) Then
                        Errors.Add(New CError(PasswordValidationErrors.InvalidStartChar, "letter."))
                    End If
                Case CPasswordGeneratorConfig.ECharacterType.LowerCase
                    If False = Char.IsLower(pPassword(0)) Then
                        Errors.Add(New CError(PasswordValidationErrors.InvalidStartChar, "lower case."))
                    End If
                Case CPasswordGeneratorConfig.ECharacterType.Number
                    If False = Char.IsNumber(pPassword(0)) Then
                        Errors.Add(New CError(PasswordValidationErrors.InvalidStartChar, "number."))
                    End If
                Case CPasswordGeneratorConfig.ECharacterType.SpecialChar
                    If False = SPECIALCHARS.Contains(pPassword(0)) Then
                        Errors.Add(New CError(PasswordValidationErrors.InvalidStartChar, "special character."))
                    End If
                Case CPasswordGeneratorConfig.ECharacterType.UpperCase
                    If False = Char.IsUpper(pPassword(0)) Then
                        Errors.Add(New CError(PasswordValidationErrors.InvalidStartChar, "upper case."))
                    End If
            End Select

            Dim lCaseCount As Integer = 0
            Dim uCaseCount As Integer = 0
            Dim numberCount As Integer = 0
            Dim specialCharCount As Integer = 0
            Dim charset As String = GetCharacterSet(_config.ValidCharSet)

            For Each c As Char In pPassword
                If charset.Contains(c) = False Then
                    Errors.Add(New CError(PasswordValidationErrors.InvalidChars, c.ToString))
                End If
                If Char.IsLower(c) Then
                    lCaseCount += 1
                End If
                If Char.IsUpper(c) Then
                    uCaseCount += 1
                End If
                If Char.IsNumber(c) Then
                    numberCount += 1
                End If
                If SPECIALCHARS.Contains(c) Then
                    specialCharCount += 1
                End If
            Next

            If _config.MinAlpha > 0 AndAlso lCaseCount + uCaseCount < _config.MinAlpha Then
                Errors.Add(New CError(PasswordValidationErrors.NotEnoughAlpha, _config.MinAlpha.ToString))
            Else
                If _config.MinLowerCase > 0 AndAlso lCaseCount < _config.MinLowerCase Then
                    Errors.Add(New CError(PasswordValidationErrors.NotEnoughLower, _config.MinLowerCase.ToString))
                End If

                If _config.MinUpperCase > 0 AndAlso uCaseCount < _config.MinUpperCase Then
                    Errors.Add(New CError(PasswordValidationErrors.NotEnoughUpper, _config.MinUpperCase.ToString))
                End If
            End If
            If _config.MinNumber > 0 AndAlso numberCount < _config.MinNumber Then
                Errors.Add(New CError(PasswordValidationErrors.NotEnoughNumeric, _config.MinNumber.ToString))
            End If

            If _config.MinSpecialChar > 0 AndAlso specialCharCount < _config.MinSpecialChar Then
                Errors.Add(New CError(PasswordValidationErrors.NotEnoughSymbol, _config.MinSpecialChar.ToString))
            End If

            If _config.Regex1 <> "" AndAlso Not Regex.IsMatch(pPassword, _config.Regex1) Then
                Errors.Add(New CError(PasswordValidationErrors.Regex1, _config.RegexDescription1))
            End If

            If _config.Regex2 <> "" AndAlso Not Regex.IsMatch(pPassword, _config.Regex2) Then
                Errors.Add(New CError(PasswordValidationErrors.Regex2, _config.RegexDescription2))
            End If

            If _config.Regex3 <> "" AndAlso Not Regex.IsMatch(pPassword, _config.Regex3) Then
                Errors.Add(New CError(PasswordValidationErrors.Regex3, _config.RegexDescription3))
            End If

            Return Errors.Count = 0
        End Function

        Private Function isReusedPassword(ByVal pPassword As String) As Boolean

            Dim HashedPassword As String = CBaseStd.getOneWayHashValue(pPassword)

            For n As Integer = 0 To _config.PasswordReuseNumber - 1
                If HashedPassword = CBaseStd.SafeString(_config.HashKeyList(n)) Then
                    Return True
                End If
            Next

            Return False

        End Function

    End Class
End Namespace
