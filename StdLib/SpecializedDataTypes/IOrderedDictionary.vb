Namespace SpecializedDataTypes
    Public Interface IOrderedDictionary(Of TKey, TValue)
        Inherits IDictionary(Of TKey, TValue), IList(Of KeyValuePair(Of TKey, TValue))

        Shadows Property Item(ByVal index As Integer) As TValue
        Overloads Function IndexOf(ByVal key As TKey) As Integer
    End Interface
End Namespace