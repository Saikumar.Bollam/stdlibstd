Namespace SpecializedDataTypes
    ''' <summary>
    ''' Covariance adapter for IDictionary.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class COrderedDictionaryAdapter(Of TKey, TSub As TBase, TBase)
        Implements IOrderedDictionary(Of TKey, TBase)

        Private _SubDictionary As IOrderedDictionary(Of TKey, TSub)

        Private ReadOnly Property SubList() As IList(Of KeyValuePair(Of TKey, TSub))
            Get
                Return DirectCast(_SubDictionary, IList(Of KeyValuePair(Of TKey, TSub)))
            End Get
        End Property

        Public Sub New(ByVal poDictionary As IOrderedDictionary(Of TKey, TSub))
            _SubDictionary = poDictionary
        End Sub

		Private Sub Add(ByVal item As KeyValuePair(Of TKey, TBase)) Implements ICollection(Of KeyValuePair(Of TKey, TBase)).Add
			_SubDictionary.Add(New KeyValuePair(Of TKey, TSub)(item.Key, DirectCast(item.Value, TSub)))
		End Sub

		Public Sub Clear() Implements ICollection(Of KeyValuePair(Of TKey, TBase)).Clear
			_SubDictionary.Clear()
		End Sub

		Private Function Contains(ByVal item As KeyValuePair(Of TKey, TBase)) As Boolean Implements ICollection(Of KeyValuePair(Of TKey, TBase)).Contains
			Return _SubDictionary.Contains(New KeyValuePair(Of TKey, TSub)(item.Key, DirectCast(item.Value, TSub)))
		End Function

		Private Sub CopyTo(ByVal array() As KeyValuePair(Of TKey, TBase), ByVal arrayIndex As Integer) Implements ICollection(Of KeyValuePair(Of TKey, TBase)).CopyTo
			For Each oPair As KeyValuePair(Of TKey, TBase) In Me
				array(arrayIndex) = oPair
				arrayIndex += 1
			Next
		End Sub

		Public ReadOnly Property Count() As Integer Implements ICollection(Of KeyValuePair(Of TKey, TBase)).Count
			Get
				Return _SubDictionary.Count
			End Get
		End Property

		Public ReadOnly Property IsReadOnly() As Boolean Implements ICollection(Of KeyValuePair(Of TKey, TBase)).IsReadOnly
			Get
				Return _SubDictionary.IsReadOnly
			End Get
		End Property

		Private Function Remove1(ByVal item As KeyValuePair(Of TKey, TBase)) As Boolean Implements ICollection(Of KeyValuePair(Of TKey, TBase)).Remove
			_SubDictionary.Remove(New KeyValuePair(Of TKey, TSub)(item.Key, DirectCast(item.Value, TSub)))
		End Function

		Private Sub Add1(ByVal key As TKey, ByVal value As TBase) Implements IDictionary(Of TKey, TBase).Add
			_SubDictionary.Add(key, DirectCast(value, TSub))
		End Sub

		Public Function ContainsKey(ByVal key As TKey) As Boolean Implements IDictionary(Of TKey, TBase).ContainsKey
			Return _SubDictionary.ContainsKey(key)
		End Function

		Default Public Property Item(ByVal key As TKey) As TBase Implements IDictionary(Of TKey, TBase).Item
			Get
				Return DirectCast(_SubDictionary, IDictionary(Of TKey, TSub)).Item(key)
			End Get
			Set(ByVal value As TBase)
				DirectCast(_SubDictionary, IDictionary(Of TKey, TSub))(key) = DirectCast(value, TSub)
			End Set
		End Property

		Public ReadOnly Property Keys() As ICollection(Of TKey) Implements IDictionary(Of TKey, TBase).Keys
			Get
				Return _SubDictionary.Keys
			End Get
		End Property

		Public Function Remove(ByVal key As TKey) As Boolean Implements IDictionary(Of TKey, TBase).Remove
			Return _SubDictionary.Remove(key)
		End Function

		Public Function TryGetValue(ByVal key As TKey, ByRef value As TBase) As Boolean Implements IDictionary(Of TKey, TBase).TryGetValue
			Return _SubDictionary.TryGetValue(key, DirectCast(value, TSub))
		End Function

		Public ReadOnly Property Values() As ICollection(Of TBase) Implements IDictionary(Of TKey, TBase).Values
			Get
				Return New CCollectionAdapter(Of TSub, TBase)(_SubDictionary.Values)
			End Get
		End Property

		Public Function GetEnumerator() As IEnumerator(Of KeyValuePair(Of TKey, TBase)) Implements IEnumerable(Of KeyValuePair(Of TKey, TBase)).GetEnumerator
			Return New CKeyValuePairEnumeratorAdapter(Of TKey, TSub, TBase)(_SubDictionary.GetEnumerator())
		End Function

		Private Function GetEnumerator_NonGeneric() As IEnumerator Implements IEnumerable.GetEnumerator
			Return GetEnumerator()
		End Function

		Public Overloads Function IndexOf(ByVal item As KeyValuePair(Of TKey, TBase)) As Integer Implements IList(Of KeyValuePair(Of TKey, TBase)).IndexOf
			Dim itemAdapted As New KeyValuePair(Of TKey, TSub)(item.Key, DirectCast(item.Value, TSub))
			Return SubList.IndexOf(itemAdapted)
		End Function

		Public Sub Insert(ByVal index As Integer, ByVal item As KeyValuePair(Of TKey, TBase)) Implements IList(Of KeyValuePair(Of TKey, TBase)).Insert
			SubList.Insert(index, New KeyValuePair(Of TKey, TSub)(item.Key, DirectCast(item.Value, TSub)))
		End Sub

		Private Property Item_IList(ByVal index As Integer) As KeyValuePair(Of TKey, TBase) Implements IList(Of KeyValuePair(Of TKey, TBase)).Item
			Get
				Dim item As KeyValuePair(Of TKey, TSub) = SubList.Item(index)
				Return New KeyValuePair(Of TKey, TBase)(item.Key, item.Value)
			End Get
			Set(ByVal value As KeyValuePair(Of TKey, TBase))
				SubList(index) = New KeyValuePair(Of TKey, TSub)(value.Key, DirectCast(value.Value, TSub))
			End Set
		End Property

		Public Sub RemoveAt(ByVal index As Integer) Implements IList(Of KeyValuePair(Of TKey, TBase)).RemoveAt
			SubList.RemoveAt(index)
		End Sub

		Public Overloads Function IndexOf(ByVal key As TKey) As Integer Implements IOrderedDictionary(Of TKey, TBase).IndexOf
            Return _SubDictionary.IndexOf(key)
        End Function

        Default Public Property Item(ByVal index As Integer) As TBase Implements IOrderedDictionary(Of TKey, TBase).Item
            Get
                Return _SubDictionary.Item(index)
            End Get
            Set(ByVal value As TBase)
                _SubDictionary.Item(index) = DirectCast(value, TSub)
            End Set
        End Property
    End Class
End Namespace
