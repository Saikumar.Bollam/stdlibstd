Namespace SpecializedDataTypes
    ''' <summary>
    ''' Same as CSet, but with an generic IList implementation for indexed access.
    ''' </summary>
    ''' <remarks>This uses COrderesDictionary internally, and it therefore has the same running time caveats comared to Dictionary.</remarks>
    Public Class COrderedSet(Of T)
        Inherits CSet(Of T)
        Implements IList(Of T)

        Public Sub New()
            MyBase.New()
            _Items = New COrderedDictionary(Of T, Object)
        End Sub

        Public Sub New(ByVal pSource As IEnumerable(Of T))
            MyBase.New()
            _Items = New COrderedDictionary(Of T, Object)

            Me.UnionWith(pSource)
        End Sub

        Public Sub New(ByVal poComparer As IEqualityComparer(Of T))
            MyBase.New()
            _Items = New COrderedDictionary(Of T, Object)(poComparer)
        End Sub

        Public Sub New(ByVal pSource As IEnumerable(Of T), ByVal poComparer As IEqualityComparer(Of T))
            MyBase.New()
            _Items = New COrderedDictionary(Of T, Object)(poComparer)

            Me.UnionWith(pSource)
        End Sub

        Private ReadOnly Property ItemsOrdered() As COrderedDictionary(Of T, Object)
            Get
                Return DirectCast(_Items, COrderedDictionary(Of T, Object))
            End Get
        End Property

        Public Function IndexOf(ByVal item As T) As Integer Implements System.Collections.Generic.IList(Of T).IndexOf
            Return ItemsOrdered.IndexOf(Item)
        End Function

        Public Sub Insert(ByVal index As Integer, ByVal item As T) Implements System.Collections.Generic.IList(Of T).Insert
            ItemsOrdered.Insert(index, New KeyValuePair(Of T, Object)(Item, Nothing))
        End Sub

        Default Public Property Item(ByVal index As Integer) As T Implements System.Collections.Generic.IList(Of T).Item
            Get
                Return DirectCast(_Items, IList(Of KeyValuePair(Of T, Object)))(index).Key
            End Get
            Set(ByVal value As T)
                DirectCast(_Items, IList(Of KeyValuePair(Of T, Object)))(index) = New KeyValuePair(Of T, Object)(value, Nothing)
            End Set
        End Property

        Public Sub RemoveAt(ByVal index As Integer) Implements System.Collections.Generic.IList(Of T).RemoveAt
            ItemsOrdered.RemoveAt(index)
        End Sub
    End Class
End Namespace
