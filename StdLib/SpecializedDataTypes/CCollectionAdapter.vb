Namespace SpecializedDataTypes
    ''' <summary>
    ''' Covariance adapter for ICollection
    ''' </summary>
    Public Class CCollectionAdapter(Of TSub As TBase, TBase)
        Implements ICollection(Of TBase)

        Private _SubCollection As ICollection(Of TSub)
        Public Sub New(ByVal poCollection As ICollection(Of TSub))
            _SubCollection = poCollection
        End Sub

        Private Sub Add(ByVal item As TBase) Implements System.Collections.Generic.ICollection(Of TBase).Add
            _SubCollection.Add(DirectCast(item, TSub))
        End Sub

        Public Sub Clear() Implements System.Collections.Generic.ICollection(Of TBase).Clear
            _SubCollection.Clear()
        End Sub

        Public Function Contains(ByVal item As TBase) As Boolean Implements System.Collections.Generic.ICollection(Of TBase).Contains
            Return _SubCollection.Contains(DirectCast(item, TSub))
        End Function

        Private Sub CopyTo(ByVal array() As TBase, ByVal arrayIndex As Integer) Implements System.Collections.Generic.ICollection(Of TBase).CopyTo
            For Each oItem As TBase In Me
                array(arrayIndex) = oItem
                arrayIndex += 1
            Next
        End Sub

        Public ReadOnly Property Count() As Integer Implements System.Collections.Generic.ICollection(Of TBase).Count
            Get
                Return _SubCollection.Count
            End Get
        End Property

        Public ReadOnly Property IsReadOnly() As Boolean Implements System.Collections.Generic.ICollection(Of TBase).IsReadOnly
            Get
                Return _SubCollection.IsReadOnly
            End Get
        End Property

        Private Function Remove(ByVal item As TBase) As Boolean Implements System.Collections.Generic.ICollection(Of TBase).Remove
            Return _SubCollection.Remove(DirectCast(item, TSub))
        End Function

        Public Function GetEnumerator() As System.Collections.Generic.IEnumerator(Of TBase) Implements System.Collections.Generic.IEnumerable(Of TBase).GetEnumerator
            Return New CEnumeratorAdapter(Of TSub, TBase)(_SubCollection.GetEnumerator())
        End Function

        Private Function GetEnumerator1() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Return GetEnumerator()
        End Function
    End Class
End Namespace