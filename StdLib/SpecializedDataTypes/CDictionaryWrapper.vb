Namespace SpecializedDataTypes
    ''' <summary>
    ''' Wraps a non-generic IDictionary in an IDictionary(Of TKey, TValue)
    ''' </summary>
    ''' <typeparam name="TKey"></typeparam>
    ''' <typeparam name="TValue"></typeparam>
    ''' <remarks></remarks>
    <Serializable()> _
    Public Class CDictionaryWrapper(Of TKey, TValue)
        Implements IDictionary(Of TKey, TValue)

        Private _Dictionary As IDictionary

        Public Sub Add(ByVal item As System.Collections.Generic.KeyValuePair(Of TKey, TValue)) Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).Add
            _Dictionary.Add(item.Key, item.Value)
        End Sub

        Public Sub Clear() Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).Clear
            _Dictionary.Clear()
        End Sub

        Public Function Contains(ByVal item As System.Collections.Generic.KeyValuePair(Of TKey, TValue)) As Boolean Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).Contains
            Return _Dictionary.Contains(Item.Key) AndAlso Item.Value.Equals(DirectCast(_Dictionary(Item.Key), TValue))
        End Function

        Public Sub CopyTo(ByVal array() As System.Collections.Generic.KeyValuePair(Of TKey, TValue), ByVal arrayIndex As Integer) Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).CopyTo
            For Each oPair As KeyValuePair(Of TKey, TValue) In Me
                array(arrayIndex) = oPair
                arrayIndex += 1
            Next
        End Sub

        Public ReadOnly Property Count() As Integer Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).Count
            Get
                Return _Dictionary.Count
            End Get
        End Property

        Public ReadOnly Property IsReadOnly() As Boolean Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).IsReadOnly
            Get
                Return _Dictionary.IsReadOnly
            End Get
        End Property

        Public Function Remove(ByVal item As System.Collections.Generic.KeyValuePair(Of TKey, TValue)) As Boolean Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).Remove
            If Not Me.Contains(Item) Then
                Return False
            End If

            _Dictionary.Remove(Item.Key)
            Return True
        End Function

        Public Sub Add(ByVal key As TKey, ByVal value As TValue) Implements System.Collections.Generic.IDictionary(Of TKey, TValue).Add
            _Dictionary.Add(key, value)
        End Sub

        Public Function ContainsKey(ByVal key As TKey) As Boolean Implements System.Collections.Generic.IDictionary(Of TKey, TValue).ContainsKey
            Return _Dictionary.Contains(key)
        End Function

        Default Public Property Item(ByVal key As TKey) As TValue Implements System.Collections.Generic.IDictionary(Of TKey, TValue).Item
            Get
                If Not _Dictionary.Contains(key) Then
                    Throw New KeyNotFoundException 'Generic.IDictionary interface specifies that this should be thrown
                End If
                Return DirectCast(_Dictionary(key), TValue)
            End Get
            Set(ByVal value As TValue)
                _Dictionary(key) = value
            End Set
        End Property

        Public ReadOnly Property Keys() As System.Collections.Generic.ICollection(Of TKey) Implements System.Collections.Generic.IDictionary(Of TKey, TValue).Keys
            Get
                Return New CCollectionWrapper(Of TKey)(_Dictionary.Keys)
            End Get
        End Property

        Public Function Remove(ByVal key As TKey) As Boolean Implements System.Collections.Generic.IDictionary(Of TKey, TValue).Remove
            If Not Me.ContainsKey(key) Then
                Return False
            End If

            _Dictionary.Remove(key)
            Return True
        End Function

        Public Function TryGetValue(ByVal key As TKey, ByRef value As TValue) As Boolean Implements System.Collections.Generic.IDictionary(Of TKey, TValue).TryGetValue
            If Not _Dictionary.Contains(key) Then
                Return False
            End If

            value = DirectCast(_Dictionary(key), TValue)
            Return True
        End Function

        Public ReadOnly Property Values() As System.Collections.Generic.ICollection(Of TValue) Implements System.Collections.Generic.IDictionary(Of TKey, TValue).Values
            Get
                Return New CCollectionWrapper(Of TValue)(_Dictionary.Values)
            End Get
        End Property

        Public Function GetEnumerator() As System.Collections.Generic.IEnumerator(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)) Implements System.Collections.Generic.IEnumerable(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).GetEnumerator
            Return New CDictionaryEntryEnumeratorWrapper(Of TKey, TValue)(New CEnumeratorWrapper(Of DictionaryEntry)(_Dictionary.GetEnumerator))
        End Function

        Public Function GetEnumerator1() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Return GetEnumerator()
        End Function

        Public Sub New(ByVal poDictionary As IDictionary)
            _Dictionary = poDictionary
        End Sub
    End Class
End Namespace
