﻿Namespace SpecializedDataTypes
    ''' <summary>
    ''' This interface is used to copy fields (types w/ isClass = true) for non copy reference mode from source object to target object
    ''' Note that we should always copy the privite fields for the class implements ICopyFields
    ''' </summary>
    Public Interface ICopyFields
        Sub Clear()
    End Interface
End Namespace