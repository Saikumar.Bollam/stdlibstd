Namespace SpecializedDataTypes
    Public Interface ISet(Of T)
        Inherits ICollection(Of T)

        ''' <summary>
        ''' Adds the item to the set.
        ''' </summary>
        ''' <returns>True if the item was added; false if it already exists within the set.</returns>
        Overloads Function Add(ByVal item As T) As Boolean

        Sub IntersectWith(ByVal other As IEnumerable(Of T))

        Sub UnionWith(ByVal other As IEnumerable(Of T))

        Function Overlaps(ByVal other As IEnumerable(Of T)) As Boolean

#Region "Convenience routines" 'These aren't technically needed for a Set class, but are nice to have
        Function ToArray() As T()
#End Region
    End Interface
End Namespace
