Imports System.Collections.Specialized

Namespace SpecializedDataTypes
    ''' <summary>
    ''' Wraps a StringDictionary into an IDictionary(Of String, String)
    ''' </summary>
    ''' <remarks>
    ''' This does not make a copy; it references the original StringDictionary.
    '''
    ''' Due to the fact that StringDictionary lowercases its keys, it is advisable to change to a Dictionary(Of String, String) implementation with 
    ''' a case-insensitive comparer rather than using this wrapper.
    ''' </remarks>
    Public Class CStringDictionaryWrapper
        Implements IDictionary(Of String, String)

        Private _StringDictionary As StringDictionary

        Public Sub Add(ByVal item As System.Collections.Generic.KeyValuePair(Of String, String)) Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of String, String)).Add
            _StringDictionary.Add(item.Key, item.Value)
        End Sub

        Public Sub Clear() Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of String, String)).Clear
            _StringDictionary.Clear()
        End Sub

        Public Function Contains(ByVal item As System.Collections.Generic.KeyValuePair(Of String, String)) As Boolean Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of String, String)).Contains
            Return _StringDictionary.ContainsKey(Item.Key) AndAlso _StringDictionary(Item.Key) = Item.Value
        End Function

		Public Sub CopyTo(ByVal array() As KeyValuePair(Of String, String), ByVal arrayIndex As Integer) Implements ICollection(Of KeyValuePair(Of String, String)).CopyTo
			For Each oPair As KeyValuePair(Of String, String) In Me
				array(arrayIndex) = oPair
				arrayIndex = arrayIndex + 1
			Next
		End Sub

		Public ReadOnly Property Count() As Integer Implements ICollection(Of KeyValuePair(Of String, String)).Count
			Get
				Return _StringDictionary.Count
			End Get
		End Property

		Public ReadOnly Property IsReadOnly() As Boolean Implements ICollection(Of KeyValuePair(Of String, String)).IsReadOnly
			Get
				Return False
			End Get
		End Property

		Public Function Remove(ByVal item As KeyValuePair(Of String, String)) As Boolean Implements ICollection(Of KeyValuePair(Of String, String)).Remove
			If Not Me.Contains(item) Then
				Return False
			End If

			_StringDictionary.Remove(item.Key)
			Return True
		End Function

		Public Sub Add(ByVal key As String, ByVal value As String) Implements IDictionary(Of String, String).Add
			_StringDictionary.Add(key, value)
		End Sub

		Public Function ContainsKey(ByVal key As String) As Boolean Implements IDictionary(Of String, String).ContainsKey
			Return _StringDictionary.ContainsKey(key)
		End Function

		Default Public Property Item(ByVal key As String) As String Implements IDictionary(Of String, String).Item
			Get
				If Not _StringDictionary.ContainsKey(key) Then
					Throw New KeyNotFoundException 'Generic.IDictionary interface specifies that this should be thrown
				End If
				Return _StringDictionary(key)
			End Get
			Set(ByVal value As String)
				_StringDictionary(key) = value
			End Set
		End Property

		Public ReadOnly Property Keys() As ICollection(Of String) Implements IDictionary(Of String, String).Keys
			Get
				'Return New CListWrapper(Of String)(New ArrayList(_StringDictionary.Keys))
				Return New CCollectionWrapper(Of String)(_StringDictionary.Keys)
			End Get
		End Property

		Public Function Remove(ByVal key As String) As Boolean Implements IDictionary(Of String, String).Remove
			If Not _StringDictionary.ContainsKey(key) Then
				Return False
			End If
			_StringDictionary.Remove(key)
			Return True
		End Function

		Public Function TryGetValue(ByVal key As String, ByRef value As String) As Boolean Implements IDictionary(Of String, String).TryGetValue
			If Not _StringDictionary.ContainsKey(key) Then
				Return False
			End If

			value = _StringDictionary(key)
			Return True
		End Function

		Public ReadOnly Property Values() As ICollection(Of String) Implements IDictionary(Of String, String).Values
			Get
				Return New CCollectionWrapper(Of String)(_StringDictionary.Values)
			End Get
		End Property

		Public Function GetEnumerator() As IEnumerator(Of KeyValuePair(Of String, String)) Implements IEnumerable(Of KeyValuePair(Of String, String)).GetEnumerator
			Return New CDictionaryEntryEnumeratorWrapper(Of String, String)(New CEnumeratorWrapper(Of DictionaryEntry)(_StringDictionary.GetEnumerator))
		End Function

		Public Function GetEnumerator1() As IEnumerator Implements IEnumerable.GetEnumerator
			Return GetEnumerator()
		End Function
	End Class
End Namespace
