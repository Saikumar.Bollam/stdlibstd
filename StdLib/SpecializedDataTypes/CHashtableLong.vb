Namespace SpecializedDataTypes
    <Obsolete("Just use a generic.dictionary(of string, long)")> _
    Public Class CHashTableLong
        Inherits Hashtable

        Public Shadows Sub Add(ByVal key As String, ByVal value As Long)
            MyBase.Add(key, value)
        End Sub

        Default Public Shadows Property Item(ByVal key As String) As Long
            Get
                Return CLng(MyBase.Item(key))
            End Get
            Set(ByVal Value As Long)
                MyBase.Item(key) = Value
            End Set
        End Property
    End Class
End Namespace