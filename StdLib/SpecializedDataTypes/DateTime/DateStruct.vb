
Namespace SpecializedDataTypes
    ''' <summary>
    ''' This date struct is designed to be a slim down version so that we don't have to deal with the extra baggage of the native .net DateTime structure.     When comparing with 
    ''' .net DateTime, the 2 are considered equal if their date portions are equal. 
    ''' </summary>
    ''' <remarks> 
    ''' Note that while we do provide a lot of operator overloads to simplify working with native .net dates, some indirect calls that may invoke the .net's native Equals to compare 
    ''' with DateStruct will FAIL.  
    ''' </remarks>
    <Serializable()> _
    Public Structure DateStruct
        Implements IComparable
        Implements IComparable(Of DateStruct)
        Implements IEquatable(Of DateStruct)
        Implements IEquatable(Of Date)
        Implements System.Runtime.Serialization.ISerializable

#Region "Constructors"

        ''' <summary>
        ''' Wrapper that will take in native .net date.  Please note the time portion will be discarded!
        ''' </summary>
        Sub New(ByVal poDate As Date)
            Me.New(poDate.Year, poDate.Month, poDate.Day)
        End Sub
        Sub New(ByVal poDate As DateTimeStruct)
            Me.New(poDate.Year, poDate.Month, poDate.Day)
        End Sub
        Sub New(ByVal year As Integer, ByVal month As Integer, ByVal day As Integer)
            _Date = New Date(year, month, day)
        End Sub

#End Region

        Public Shared Function Now(ByVal peZone As CTimeZoneManager.Zone) As DateStruct
            Return New DateStruct(DateTimeStruct.Now(peZone))
        End Function

        Public Shared Function Now() As DateStruct
            Return New DateStruct(DateTimeStruct.Now())
        End Function

#Region "Arithmetic routines"

        Public Function AddDays(ByVal pnValue As Integer) As DateStruct
            Return New DateStruct(_Date.AddDays(pnValue))
        End Function

        Public Function AddMonths(ByVal pnValue As Integer) As DateStruct
            Return New DateStruct(_Date.AddMonths(pnValue))
        End Function

        Public Function AddYears(ByVal pnValue As Integer) As DateStruct
            Return New DateStruct(_Date.AddYears(pnValue))
        End Function
#End Region

        Private _Date As Date

#Region "Readonly properties"
        Public ReadOnly Property Month() As Integer
            Get
                Return _Date.Month
            End Get
        End Property

        Public ReadOnly Property Year() As Integer
            Get
                Return _Date.Year
            End Get
        End Property

        Public ReadOnly Property Day() As Integer
            Get
                Return _Date.Day
            End Get
        End Property
#End Region

        Public Shared ReadOnly MinValue As DateStruct = New DateStruct(Date.MinValue)
        Public Shared ReadOnly MaxValue As DateStruct = New DateStruct(Date.MaxValue)

        Public Overloads Overrides Function ToString() As String
            Return _Date.ToShortDateString
        End Function

        Public Overloads Function ToString(ByVal psFormat As String) As String
            Return _Date.ToString(psFormat)
        End Function

        ''' <summary>
        ''' Convenience routine to make switch between .net date and DateStruct easier transparent.
        ''' </summary>
        Public Function ToShortDateString() As String
            Return _Date.ToShortDateString
        End Function

#Region "Conversion Overloads"
        'we're going to be losing time information so this is considered narrowing ... 
        Public Shared Narrowing Operator CType(ByVal d As Date) As DateStruct
            Return New DateStruct(d.Year, d.Month, d.Day)
        End Operator

        Public Shared Narrowing Operator CType(ByVal s As String) As DateStruct
            If IsDate(s) = False Then
                Throw New FormatException("String is not of date type:" & s)
            End If

            Dim d As Date = CDate(s)
            Return New DateStruct(d.Year, d.Month, d.Day)
        End Operator

        Public Shared Widening Operator CType(ByVal d As DateStruct) As Date
            Return d._Date
        End Operator


        Public Shared Widening Operator CType(ByVal d As DateStruct) As String
            Return d.ToString()
        End Operator
#End Region

#Region "Comparison Routines to mimic what normal date does"
        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            Return _Date.CompareTo(obj)
        End Function

        Public Function CompareTo(ByVal other As DateStruct) As Integer Implements System.IComparable(Of DateStruct).CompareTo
            Return _Date.CompareTo(other._Date)
        End Function
        Public Overrides Function Equals(ByVal obj As Object) As Boolean
            If TypeOf obj Is Date Then
                Return _Date.Equals((New DateStruct(CType(obj, Date)))._Date) 'need to wrap with DateStruct to wipe out the time portions :P 
            Else
                Return _Date.Equals(CType(obj, DateStruct)._Date)
            End If

        End Function
        Public Overloads Function Equals(ByVal other As DateStruct) As Boolean Implements System.IEquatable(Of DateStruct).Equals
            Return _Date.Equals(other._Date)
        End Function

        Public Overloads Function Equals(ByVal other As Date) As Boolean Implements System.IEquatable(Of Date).Equals
            Return _Date.Equals((New DateStruct(other))._Date)
        End Function
#End Region

#Region "Operator Overloads to make comparison of DateStruct and Date more seamless"
        Public Shared Operator =(ByVal lhs As DateStruct, ByVal rhs As DateStruct) As Boolean
            Return lhs.Equals(rhs)
        End Operator

        Public Shared Operator <>(ByVal lhs As DateStruct, ByVal rhs As DateStruct) As Boolean
            Return Not lhs.Equals(rhs)
        End Operator

        Public Shared Operator <(ByVal lhs As DateStruct, ByVal rhs As DateStruct) As Boolean
            Return lhs._Date < rhs._Date
        End Operator

        Public Shared Operator >(ByVal lhs As DateStruct, ByVal rhs As DateStruct) As Boolean
            Return lhs._Date > rhs._Date
        End Operator


        Public Shared Operator <=(ByVal lhs As DateStruct, ByVal rhs As DateStruct) As Boolean
            Return lhs._Date <= rhs._Date
        End Operator

        Public Shared Operator >=(ByVal lhs As DateStruct, ByVal rhs As DateStruct) As Boolean
            Return lhs._Date >= rhs._Date
        End Operator

#Region "Transparency equality check to make working with native dates easier."
        'equality ...   note we have to do things 2x to handle case of Date appearing on left or right
        Public Shared Operator =(ByVal lhs As DateStruct, ByVal rhs As Date) As Boolean
            Return lhs.Equals(New DateStruct(rhs))
        End Operator

        Public Shared Operator <>(ByVal lhs As DateStruct, ByVal rhs As Date) As Boolean
            Return Not lhs.Equals(New DateStruct(rhs))
        End Operator

        Public Shared Operator =(ByVal lhs As Date, ByVal rhs As DateStruct) As Boolean
            Return rhs.Equals(New DateStruct(lhs))
        End Operator

        Public Shared Operator <>(ByVal lhs As Date, ByVal rhs As DateStruct) As Boolean
            Return Not rhs.Equals(New DateStruct(lhs))
        End Operator


        'less than or greater ... 

        Public Shared Operator <(ByVal lhs As DateStruct, ByVal rhs As Date) As Boolean
            Return lhs._Date < (New DateStruct(rhs))._Date
        End Operator

        Public Shared Operator >(ByVal lhs As DateStruct, ByVal rhs As Date) As Boolean
            Return lhs._Date > (New DateStruct(rhs))._Date
        End Operator


        Public Shared Operator <=(ByVal lhs As DateStruct, ByVal rhs As Date) As Boolean
            Return lhs._Date <= (New DateStruct(rhs))._Date
        End Operator

        Public Shared Operator >=(ByVal lhs As DateStruct, ByVal rhs As Date) As Boolean
            Return lhs._Date >= (New DateStruct(rhs))._Date
        End Operator

        'inverse 
        Public Shared Operator <(ByVal lhs As Date, ByVal rhs As DateStruct) As Boolean
            Return (New DateStruct(lhs))._Date < rhs._Date
        End Operator

        Public Shared Operator >(ByVal lhs As Date, ByVal rhs As DateStruct) As Boolean
            Return (New DateStruct(lhs))._Date > rhs._Date
        End Operator


        Public Shared Operator <=(ByVal lhs As Date, ByVal rhs As DateStruct) As Boolean
            Return (New DateStruct(lhs))._Date <= rhs._Date
        End Operator

        Public Shared Operator >=(ByVal lhs As Date, ByVal rhs As DateStruct) As Boolean
            Return (New DateStruct(lhs))._Date >= rhs._Date
        End Operator


#End Region

#End Region




#Region "ISerializable Implementation (delegation)"

        Private Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)
            _Date = info.GetDateTime("_date")
        End Sub

        Public Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext) Implements System.Runtime.Serialization.ISerializable.GetObjectData
            info.AddValue("_date", _Date)
        End Sub
#End Region


    End Structure
End Namespace
