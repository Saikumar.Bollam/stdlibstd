﻿Option Strict On

Namespace SpecializedDataTypes
    ''' <summary>
    ''' Represents a DateTime with Time Zone information
    ''' </summary>
    ''' <remarks>Instances of this class are immutable</remarks>
    ''' 
    <Serializable()> _
    Public Structure DateTimeStruct
        Implements IComparable(Of DateTimeStruct)
        Implements IEquatable(Of DateTimeStruct)
        Implements IComparable(Of DateTime)
        Implements IEquatable(Of DateTime)
        Implements IComparable(Of DateStruct)
        Implements IEquatable(Of DateStruct)

        Private _DateTimeUTC As DateTime
        Private _TimeZoneInfo As TimeZoneInfo

        Private ReadOnly Property DateTimeConverted() As DateTime
            Get
                Return TimeZoneInfo.ConvertTimeFromUtc(_DateTimeUTC, _TimeZoneInfo)
            End Get
        End Property

        Private Sub New(ByVal poDateTime As DateTime, ByVal poTimeZoneInfo As TimeZoneInfo, Optional pbIsStripMilliSecond As Boolean = True)
            _TimeZoneInfo = poTimeZoneInfo

            Select Case poDateTime.Kind
                Case DateTimeKind.Local
                    _DateTimeUTC = ConvertTimeToUTCSafe(DateTime.SpecifyKind(poDateTime, DateTimeKind.Unspecified), CTimeZoneManager.LocalZoneInfo)
                Case DateTimeKind.Utc
                    _DateTimeUTC = poDateTime
                Case DateTimeKind.Unspecified
                    'Assume source date time zone is _TimeZoneInfo
                    _DateTimeUTC = ConvertTimeToUTCSafe(DateTime.SpecifyKind(poDateTime, DateTimeKind.Unspecified), _TimeZoneInfo)
            End Select

            'Copy existing datetime to new datetime but set milliseconds to 0
            If pbIsStripMilliSecond = True Then
                _DateTimeUTC = New DateTime(_DateTimeUTC.Year, _DateTimeUTC.Month, _DateTimeUTC.Day, _DateTimeUTC.Hour, _DateTimeUTC.Minute, _DateTimeUTC.Second, 0, _DateTimeUTC.Kind)
            End If

        End Sub

        Public Sub New(ByVal poDateTime As DateTime, ByVal peTimeZone As CTimeZoneManager.Zone, Optional pbIsStripMilliSecond As Boolean = True)
            Me.New(poDateTime, CTimeZoneManager.ZoneInfo(peTimeZone), pbIsStripMilliSecond)
        End Sub

        Public Sub New(ByVal poLocalDateTime As DateTime, Optional pbIsStripMilliSecond As Boolean = True)
            Me.New(poLocalDateTime, CTimeZoneManager.LocalZoneInfo, pbIsStripMilliSecond)
        End Sub


        ''' <summary>
        ''' Wraps .NETs ConvertTimeToUTC to account for invalid timezone dates. EG. 3/13/2016  2:05:00 AM PST, such time would be invalid because 
        ''' after 2 AM on 3/13 it should automatically bumped to 3AM. This will automatically try to bump the time up for 1 hour.
        ''' </summary>
        ''' <param name="pDate"></param>
        ''' <param name="poTimeZoneInfo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function ConvertTimeToUTCSafe(ByVal pDate As DateTime, ByVal poTimeZoneInfo As TimeZoneInfo) As DateTime
            Dim UTCDate As DateTime = Nothing
            Try
                UTCDate = TimeZoneInfo.ConvertTimeToUtc(DateTime.SpecifyKind(pDate, DateTimeKind.Unspecified), poTimeZoneInfo)
            Catch ex As System.ArgumentException
                'if this doesn't work we'll just let the exception flow up.
                UTCDate = TimeZoneInfo.ConvertTimeToUtc(DateTime.SpecifyKind(pDate.AddHours(1), DateTimeKind.Unspecified), poTimeZoneInfo)
            End Try
            Return UTCDate
        End Function


#Region "IComparable and IEquatable"
        Public Function CompareTo(ByVal other As DateTimeStruct) As Integer Implements System.IComparable(Of DateTimeStruct).CompareTo
            Return Me._DateTimeUTC.CompareTo(other._DateTimeUTC)
        End Function

        Public Overloads Function Equals(ByVal other As DateTimeStruct) As Boolean Implements System.IEquatable(Of DateTimeStruct).Equals
            Return Me._DateTimeUTC.Equals(other._DateTimeUTC)
        End Function

        Public Function CompareTo(ByVal other As DateTime) As Integer Implements System.IComparable(Of DateTime).CompareTo
            'Assume other is in the same time zone if kind is unspecified
            Return CompareTo(New DateTimeStruct(other, _TimeZoneInfo))
        End Function

        Public Overloads Function Equals(ByVal other As DateTime) As Boolean Implements System.IEquatable(Of DateTime).Equals
            'Assume other is in the same time zone if kind is unspecified
            Return Equals(New DateTimeStruct(other, _TimeZoneInfo))
        End Function

        Public Function CompareTo(ByVal other As DateStruct) As Integer Implements System.IComparable(Of DateStruct).CompareTo
            Dim oDateStruct As New DateStruct(DateTimeConverted)

            'Assume other is in the same time zone
            Return oDateStruct.CompareTo(other)
        End Function

        Public Overloads Function Equals(ByVal other As DateStruct) As Boolean Implements System.IEquatable(Of DateStruct).Equals
            Dim oDateStruct As New DateStruct(DateTimeConverted)

            'Assume other is in the same time zone
            Return oDateStruct.Equals(other)
        End Function

        Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
            Return Equals(DirectCast(obj, DateTimeStruct))
        End Function

        Public Overrides Function GetHashCode() As Integer
            Return _DateTimeUTC.GetHashCode()
        End Function
#End Region

        ''' <summary>
        ''' Converts the value of the current DateTimeStruct object to its equivalent string representation in the Time Zone specified at construction
        ''' </summary>
        Public Overrides Function ToString() As String
            Return DateTimeConverted.ToString()
        End Function

        ''' <summary>
        ''' Converts the value of the current DateTimeStruct object to its equivalent string representation in the Time Zone specified at construction
        ''' </summary>
        ''' <remarks>Note: since DateTime is not aware of the specified time zone, any format string that diplays time zone information will be inaccurate</remarks>
        Public Overloads Function ToString(ByVal psFormat As String) As String
            Return DateTimeConverted.ToString(psFormat)
        End Function

        Public Shared ReadOnly Property Now(ByVal peZone As CTimeZoneManager.Zone) As DateTimeStruct
            Get
                Return New DateTimeStruct(DateTime.UtcNow, peZone)
            End Get
        End Property

        Public Shared ReadOnly Property Now As DateTimeStruct
            Get
                Return Now(CTimeZoneManager.LocalZone)
            End Get
        End Property

        Public Shared ReadOnly Property DateNow(ByVal peZone As CTimeZoneManager.Zone) As DateTime
            Get
                Return Now(peZone).ToDateTime()
            End Get
        End Property

        Public Shared ReadOnly Property DateNow() As DateTime
            Get
                Return DateNow(CTimeZoneManager.LocalZone)
            End Get
        End Property


        ' DateNow and Now with milliseconds

        Public Shared ReadOnly Property NowWithMilliSecond(ByVal peZone As CTimeZoneManager.Zone) As DateTimeStruct
            Get
                Return New DateTimeStruct(DateTime.UtcNow, peZone, False)
            End Get
        End Property

        Public Shared ReadOnly Property NowWithMilliSecond As DateTimeStruct
            Get
                Return NowWithMilliSecond(CTimeZoneManager.LocalZone)
            End Get
        End Property

        Public Shared ReadOnly Property DateNowWithMilliSecond(ByVal peZone As CTimeZoneManager.Zone) As DateTime
            Get
                Return NowWithMilliSecond(peZone).ToDateTime()
            End Get
        End Property

        Public Shared ReadOnly Property DateNowWithMilliSecond() As DateTime
            Get
                Return DateNowWithMilliSecond(CTimeZoneManager.LocalZone)
            End Get
        End Property

#Region "Zone Info"
        Public ReadOnly Property ZoneDisplayName() As String
            Get
                Return _TimeZoneInfo.DisplayName
            End Get
        End Property

        Public ReadOnly Property ZoneStandardName() As String
            Get
                Return _TimeZoneInfo.StandardName
            End Get
        End Property

        Public ReadOnly Property ZoneDaylightName() As String
            Get
                Return _TimeZoneInfo.DaylightName
            End Get
        End Property

        Public ReadOnly Property ZoneCurrentName() As String
            Get
                If IsDaylightTime Then
                    Return ZoneDaylightName
                Else
                    Return ZoneStandardName
                End If
            End Get
        End Property

        Public ReadOnly Property IsDaylightTime() As Boolean
            Get
                Return _TimeZoneInfo.IsDaylightSavingTime(_DateTimeUTC)
            End Get
        End Property

        Public ReadOnly Property ZoneCurrentAbbreviation() As String
            Get
                Return CTimeZoneManager.GetTimeZoneAbbreviation(CTimeZoneManager.MapTimeZoneIDToZone(_TimeZoneInfo.Id), IsDaylightTime)
            End Get
        End Property
#End Region

#Region "Conversion"
        Public Function ToDateTime() As DateTime
            Select Case _TimeZoneInfo.Id
                Case CTimeZoneManager.UTCZoneInfo.Id
                    Return _DateTimeUTC
                Case CTimeZoneManager.LocalZoneInfo.Id
                    Return DateTime.SpecifyKind(DateTimeConverted, DateTimeKind.Local)
                Case Else
                    Return DateTime.SpecifyKind(DateTimeConverted, DateTimeKind.Unspecified)
            End Select
        End Function

        Public Function ToDateTimeOffset() As DateTimeOffset
            Dim dDate As DateTime = DateTimeConverted

            Return New DateTimeOffset(dDate, _TimeZoneInfo.GetUtcOffset(dDate))
        End Function

        ''' <summary>
        ''' Create a new DateTimeStruct using the current instance's absolute time and the given time zone.
        ''' </summary>
        ''' <param name="poNewZone"></param>
        ''' <returns></returns>
        ''' <remarks>
        '''   No time shifting will occur.  This just change time zone.  Meaning, if the current instance is 1:00 PST, then 
        '''   the new instance will be 4:00 EST (Assuming EST is specified)
        ''' </remarks>
        Public Function ToTimeZone(ByVal poNewZone As CTimeZoneManager.Zone) As DateTimeStruct
            Return New DateTimeStruct(_DateTimeUTC, poNewZone)
        End Function

        Public Function ToShortDateString() As String
            Return ToString("d")
        End Function

        Public Function ToLongDateString() As String
            Return ToString("D")
        End Function

        Public Function ToShortTimeString() As String
            Return ToString("t")
        End Function

        Public Function ToLongTimeString() As String
            Return ToString("T")
        End Function

        Public Function ToFullDateTimeString() As String
            Return ToString("F")
        End Function

        Public Function ToStringWithZone() As String
            Return ToString() & " " & ZoneCurrentAbbreviation
        End Function

        Public Function ToStringWithZone(ByVal poNewZone As CTimeZoneManager.Zone) As String
            Return New DateTimeStruct(_DateTimeUTC, poNewZone).ToStringWithZone()
        End Function

        ''' <summary>
        ''' Append time portion of noon (12:00 pm) to a date string and convert it to a date object
        ''' </summary>
        ''' <param name="psDateOnly"></param>
        ''' <returns></returns>
        Public Shared Function ConvertUserShortDateStringToDateTime(ByVal psDateOnly As String) As Date
            If IsDate(psDateOnly) Then
                Return CDate(psDateOnly).AddHours(12)
            Else
                Throw New Exception(String.Format("Error in ConvertStringDateToDateTime():  {0} is not a valid date", psDateOnly))
            End If
        End Function

        ''' <summary>
        ''' Load date from first date and fall back to backup date if the first date is invalid (like default date when a column is null)
        ''' </summary>
        ''' <param name="pPrimaryDate"></param>
        ''' <param name="pBackupDate"></param>
        ''' <returns></returns>
        Public Shared Function LoadDateWithFallbackIfDateIsInvalid(ByVal pPrimaryDate As Date, ByVal pBackupDate As Date) As Date
            ' Default value for date is 0001-01-01
            If pPrimaryDate.Year < 1900 Then
                Return pBackupDate
            Else
                Return pPrimaryDate
            End If
        End Function
#End Region

#Region "Arithmetic routines"
        Public Function AddDays(ByVal pnValue As Integer) As DateTimeStruct
            Return New DateTimeStruct(_DateTimeUTC.AddDays(pnValue), _TimeZoneInfo)
        End Function

        Public Function AddMonths(ByVal pnValue As Integer) As DateTimeStruct
            Return New DateTimeStruct(_DateTimeUTC.AddMonths(pnValue), _TimeZoneInfo)
        End Function

        Public Function AddYears(ByVal pnValue As Integer) As DateTimeStruct
            Return New DateTimeStruct(_DateTimeUTC.AddYears(pnValue), _TimeZoneInfo)
        End Function

        Public Function AddHours(ByVal pnValue As Integer) As DateTimeStruct
            Return New DateTimeStruct(_DateTimeUTC.AddHours(pnValue), _TimeZoneInfo)
        End Function

        Public Function AddMinutes(ByVal pnValue As Integer) As DateTimeStruct
            Return New DateTimeStruct(_DateTimeUTC.AddMinutes(pnValue), _TimeZoneInfo)
        End Function

        Public Function AddSeconds(ByVal pnValue As Integer) As DateTimeStruct
            Return New DateTimeStruct(_DateTimeUTC.AddSeconds(pnValue), _TimeZoneInfo)
        End Function
#End Region

#Region "Readonly properties"
        Public ReadOnly Property Month() As Integer
            Get
                Return DateTimeConverted.Month
            End Get
        End Property

        Public ReadOnly Property Year() As Integer
            Get
                Return DateTimeConverted.Year
            End Get
        End Property

        Public ReadOnly Property Day() As Integer
            Get
                Return DateTimeConverted.Day
            End Get
        End Property

        Public ReadOnly Property Hour() As Integer
            Get
                Return DateTimeConverted.Hour
            End Get
        End Property

        Public ReadOnly Property Minute() As Integer
            Get
                Return DateTimeConverted.Minute
            End Get
        End Property

        Public ReadOnly Property Second() As Integer
            Get
                Return DateTimeConverted.Second
            End Get
        End Property

        Public ReadOnly Property MilliSecond() As Integer
            Get
                Return DateTimeConverted.Millisecond
            End Get
        End Property
#End Region

#Region "Operator Overloads to make comparison of DateTimeStruct and DateTime more seamless"
        Public Shared Operator =(ByVal lhs As DateTimeStruct, ByVal rhs As DateTimeStruct) As Boolean
            Return lhs.Equals(rhs)
        End Operator

        Public Shared Operator <>(ByVal lhs As DateTimeStruct, ByVal rhs As DateTimeStruct) As Boolean
            Return Not lhs.Equals(rhs)
        End Operator

        Public Shared Operator <(ByVal lhs As DateTimeStruct, ByVal rhs As DateTimeStruct) As Boolean
            Return lhs.CompareTo(rhs) < 0
        End Operator

        Public Shared Operator >(ByVal lhs As DateTimeStruct, ByVal rhs As DateTimeStruct) As Boolean
            Return lhs.CompareTo(rhs) > 0
        End Operator

        Public Shared Operator <=(ByVal lhs As DateTimeStruct, ByVal rhs As DateTimeStruct) As Boolean
            Return Not (lhs > rhs)
        End Operator

        Public Shared Operator >=(ByVal lhs As DateTimeStruct, ByVal rhs As DateTimeStruct) As Boolean
            Return Not (lhs < rhs)
        End Operator

        Public Shared Operator -(ByVal lhs As DateTimeStruct, ByVal rhs As DateTimeStruct) As TimeSpan
            Return lhs._DateTimeUTC - rhs._DateTimeUTC
        End Operator

        Public Shared Operator -(ByVal lhs As DateTimeStruct, ByVal rhs As TimeSpan) As DateTimeStruct
            Return New DateTimeStruct(lhs._DateTimeUTC - rhs, lhs._TimeZoneInfo)
        End Operator

        Public Shared Operator +(ByVal lhs As DateTimeStruct, ByVal rhs As TimeSpan) As DateTimeStruct
            Return New DateTimeStruct(lhs._DateTimeUTC + rhs, lhs._TimeZoneInfo)
        End Operator

#Region "Transparency equality check to make working with native dates easier."
        'equality ...   note we have to do things 2x to handle case of Date appearing on left or right
        Public Shared Operator =(ByVal lhs As DateTimeStruct, ByVal rhs As DateTime) As Boolean
            Return lhs.Equals(New DateTimeStruct(rhs, lhs._TimeZoneInfo))
        End Operator

        Public Shared Operator <>(ByVal lhs As DateTimeStruct, ByVal rhs As DateTime) As Boolean
            Return Not lhs.Equals(New DateTimeStruct(rhs, lhs._TimeZoneInfo))
        End Operator

        Public Shared Operator =(ByVal lhs As DateTime, ByVal rhs As DateTimeStruct) As Boolean
            Return rhs.Equals(New DateTimeStruct(lhs, rhs._TimeZoneInfo))
        End Operator

        Public Shared Operator <>(ByVal lhs As DateTime, ByVal rhs As DateTimeStruct) As Boolean
            Return Not rhs.Equals(New DateTimeStruct(lhs, rhs._TimeZoneInfo))
        End Operator

        'less than or greater ... 
        Public Shared Operator <(ByVal lhs As DateTimeStruct, ByVal rhs As DateTime) As Boolean
            Return lhs < New DateTimeStruct(rhs, lhs._TimeZoneInfo)
        End Operator

        Public Shared Operator >(ByVal lhs As DateTimeStruct, ByVal rhs As DateTime) As Boolean
            Return lhs > New DateTimeStruct(rhs, lhs._TimeZoneInfo)
        End Operator

        Public Shared Operator <=(ByVal lhs As DateTimeStruct, ByVal rhs As DateTime) As Boolean
            Return Not (lhs > rhs)
        End Operator

        Public Shared Operator >=(ByVal lhs As DateTimeStruct, ByVal rhs As DateTime) As Boolean
            Return Not (lhs < rhs)
        End Operator

        'inverse 
        Public Shared Operator <(ByVal lhs As DateTime, ByVal rhs As DateTimeStruct) As Boolean
            Return New DateTimeStruct(lhs, rhs._TimeZoneInfo) < rhs
        End Operator

        Public Shared Operator >(ByVal lhs As DateTime, ByVal rhs As DateTimeStruct) As Boolean
            Return New DateTimeStruct(lhs, rhs._TimeZoneInfo) > rhs
        End Operator

        Public Shared Operator <=(ByVal lhs As DateTime, ByVal rhs As DateTimeStruct) As Boolean
            Return Not (lhs > rhs)
        End Operator

        Public Shared Operator >=(ByVal lhs As DateTime, ByVal rhs As DateTimeStruct) As Boolean
            Return Not (lhs < rhs)
        End Operator
#End Region
#End Region

    End Structure
End Namespace
