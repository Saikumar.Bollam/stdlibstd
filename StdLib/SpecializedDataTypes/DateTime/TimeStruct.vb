Namespace SpecializedDataTypes
    ''' <summary>
    ''' This TimeStruct is designed to be a slim down version so that we don't have to deal with the extra baggage of the native .net DateTime structure. 
    ''' For the most part, it's a copy/paste of DateStruct. 
    ''' When comparing with .net DateTime structure, the 2 are equal if the time portion are equal (regardless if the date portion are completely different).   
    ''' </summary>
    ''' <remarks>
    ''' Note that while we do provide a lot of operator overloads to simplify working with native .net dates, some indirect calls that may invoke the .net's native Equals to compare 
    ''' with DateStruct will FAIL.  
    ''' </remarks>
    <Serializable()> _
    Public Structure TimeStruct
        Implements IComparable
        Implements IComparable(Of TimeStruct)
        Implements IEquatable(Of TimeStruct)
        Implements IEquatable(Of Date)
        Implements System.Runtime.Serialization.ISerializable

        Private _Date As Date

#Region "Readonly properties"
        Public ReadOnly Property Hour() As Integer
            Get
                Return _Date.Hour
            End Get
        End Property

        Public ReadOnly Property Minute() As Integer
            Get
                Return _Date.Minute
            End Get
        End Property

        Public ReadOnly Property Second() As Integer
            Get
                Return _Date.Second
            End Get
        End Property

        Public ReadOnly Property MilliSecond() As Integer
            Get
                Return _Date.Millisecond
            End Get
        End Property
#End Region

        ''' <summary>
        ''' Wrapper that will take in native .net date.  Please note the date portion will be discarded!
        ''' </summary>
        Sub New(ByVal poDate As Date)
            Me.New(poDate.Hour, poDate.Minute, poDate.Second, poDate.Millisecond)
        End Sub

        Sub New(ByVal hour As Integer, ByVal minute As Integer, ByVal second As Integer)
            _Date = New Date(1, 1, 1, hour, minute, second)
        End Sub

        Sub New(ByVal hour As Integer, ByVal minute As Integer, ByVal second As Integer, ByVal millisecond As Integer)
            _Date = New Date(1, 1, 1, hour, minute, second, millisecond)
        End Sub


        Public Shared Function Now() As TimeStruct
            Return New TimeStruct(DateTimeStruct.DateNow)
        End Function


        Public Shared ReadOnly MinValue As TimeStruct = New TimeStruct(Date.MinValue)
        Public Shared ReadOnly MaxValue As TimeStruct = New TimeStruct(Date.MaxValue)

#Region "Arithmetic routines"

        Public Function AddHours(ByVal pnValue As Integer) As TimeStruct
            Return New TimeStruct(_Date.AddHours(pnValue))
        End Function

        Public Function AddMinutes(ByVal pnValue As Integer) As TimeStruct
            Return New TimeStruct(_Date.AddMinutes(pnValue))
        End Function

        Public Function AddSeconds(ByVal pnValue As Integer) As TimeStruct
            Return New TimeStruct(_Date.AddSeconds(pnValue))
        End Function
#End Region


        ''' <summary>
        ''' Returns time information formatted as h:mm:ss tt (ie: 9:11:45 PM).   Note that sql smalldateTime does NOT have seconds portion.   
        ''' </summary>
        Public Overrides Function ToString() As String
            Return _Date.ToString("h:mm:ss tt")
        End Function

        ''' <summary>
        ''' Returns time information formatted as h:mm tt (ie: 9:11 PM).   This should mirror Date.ToShortTime() and sql smallDateTime.
        ''' </summary>
        ''' <remarks>
        ''' This is designed specifically if we know we're working with sql smallDateTime.  By default, it's better to show extra "seconds" field that may not be needed vs
        ''' not showing and potentially losing data.  That is why this doesn't share the same signature as ToString().
        ''' </remarks>
        Public Function ToStringWithoutSeconds() As String
            Return _Date.ToString("h:mm tt")
        End Function

        Public Overloads Function ToString(ByVal psFormat As String) As String
            Return _Date.ToString(psFormat)
        End Function


#Region "Conversion Overloads"
        'we're going to be losing time information so this is considered narrowing ... 
        Public Shared Narrowing Operator CType(ByVal d As Date) As TimeStruct
            Return New TimeStruct(d)
        End Operator

        Public Shared Narrowing Operator CType(ByVal s As String) As TimeStruct
            If IsDate(s) = False Then
                Throw New FormatException("String is not of date type:" & s)
            End If

            Dim d As Date = CDate(s)
            Return New TimeStruct(d)
        End Operator

        Public Shared Widening Operator CType(ByVal d As TimeStruct) As Date
            Return d._Date
        End Operator


        Public Shared Widening Operator CType(ByVal d As TimeStruct) As String
            Return d.ToString()
        End Operator
#End Region

#Region "Comparison Routines to mimic what normal date does"
        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            Return _Date.CompareTo(obj)
        End Function

        Public Function CompareTo(ByVal other As TimeStruct) As Integer Implements System.IComparable(Of TimeStruct).CompareTo
            Return _Date.CompareTo(other._Date)
        End Function
        Public Overrides Function Equals(ByVal obj As Object) As Boolean
            If TypeOf obj Is Date Then
                Return _Date.Equals((New TimeStruct(CDate(obj)))._Date) ' need to wrap so that we're only comparing time portion 
            Else
                Return _Date.Equals(CType(obj, TimeStruct)._Date)
            End If

        End Function
        Public Overloads Function Equals(ByVal other As TimeStruct) As Boolean Implements System.IEquatable(Of TimeStruct).Equals
            Return _Date.Equals(other._Date)
        End Function

        Public Overloads Function Equals(ByVal other As Date) As Boolean Implements System.IEquatable(Of Date).Equals
            Return _Date.Equals((New TimeStruct(other))._Date)
        End Function
#End Region

#Region "Operator Overloads to make comparison of TimeStruct and Date more seamless"
        Public Shared Operator =(ByVal lhs As TimeStruct, ByVal rhs As TimeStruct) As Boolean
            Return lhs.Equals(rhs)
        End Operator

        Public Shared Operator <>(ByVal lhs As TimeStruct, ByVal rhs As TimeStruct) As Boolean
            Return Not lhs.Equals(rhs)
        End Operator

        Public Shared Operator <(ByVal lhs As TimeStruct, ByVal rhs As TimeStruct) As Boolean
            Return lhs._Date < rhs._Date
        End Operator

        Public Shared Operator >(ByVal lhs As TimeStruct, ByVal rhs As TimeStruct) As Boolean
            Return lhs._Date > rhs._Date
        End Operator


        Public Shared Operator <=(ByVal lhs As TimeStruct, ByVal rhs As TimeStruct) As Boolean
            Return lhs._Date <= rhs._Date
        End Operator

        Public Shared Operator >=(ByVal lhs As TimeStruct, ByVal rhs As TimeStruct) As Boolean
            Return lhs._Date >= rhs._Date
        End Operator

#Region "Transparency equality check to make working with native dates easier."
        'equality ...   note we have to do things 2x to handle case of Date appearing on left or right
        Public Shared Operator =(ByVal lhs As TimeStruct, ByVal rhs As Date) As Boolean
            Return lhs.Equals(New TimeStruct(rhs))
        End Operator

        Public Shared Operator <>(ByVal lhs As TimeStruct, ByVal rhs As Date) As Boolean
            Return Not lhs.Equals(New TimeStruct(rhs))
        End Operator

        Public Shared Operator =(ByVal lhs As Date, ByVal rhs As TimeStruct) As Boolean
            Return rhs.Equals(New TimeStruct(lhs))
        End Operator

        Public Shared Operator <>(ByVal lhs As Date, ByVal rhs As TimeStruct) As Boolean
            Return Not rhs.Equals(New TimeStruct(lhs))
        End Operator


        'less than or greater ... 

        Public Shared Operator <(ByVal lhs As TimeStruct, ByVal rhs As Date) As Boolean
            Return lhs._Date < (New TimeStruct(rhs))._Date
        End Operator

        Public Shared Operator >(ByVal lhs As TimeStruct, ByVal rhs As Date) As Boolean
            Return lhs._Date > (New TimeStruct(rhs))._Date
        End Operator


        Public Shared Operator <=(ByVal lhs As TimeStruct, ByVal rhs As Date) As Boolean
            Return lhs._Date <= (New TimeStruct(rhs))._Date
        End Operator

        Public Shared Operator >=(ByVal lhs As TimeStruct, ByVal rhs As Date) As Boolean
            Return lhs._Date >= (New TimeStruct(rhs))._Date
        End Operator

        'inverse 
        Public Shared Operator <(ByVal lhs As Date, ByVal rhs As TimeStruct) As Boolean
            Return (New TimeStruct(lhs))._Date < rhs._Date
        End Operator

        Public Shared Operator >(ByVal lhs As Date, ByVal rhs As TimeStruct) As Boolean
            Return (New TimeStruct(lhs))._Date > rhs._Date
        End Operator


        Public Shared Operator <=(ByVal lhs As Date, ByVal rhs As TimeStruct) As Boolean
            Return (New TimeStruct(lhs))._Date <= rhs._Date
        End Operator

        Public Shared Operator >=(ByVal lhs As Date, ByVal rhs As TimeStruct) As Boolean
            Return (New TimeStruct(lhs))._Date >= rhs._Date
        End Operator


#End Region

#End Region


#Region "ISerializable Implementation (delegation)"
        Private Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)
            _Date = info.GetDateTime("_date")
        End Sub

        Public Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext) Implements System.Runtime.Serialization.ISerializable.GetObjectData
            info.AddValue("_date", _Date)
        End Sub
#End Region


    End Structure
End Namespace
