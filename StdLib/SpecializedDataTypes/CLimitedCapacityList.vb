Namespace SpecializedDataTypes
    ''' <summary>
    ''' A List which is limited to a maximum capacity.
    ''' </summary>
    <Serializable()> _
    Public Class CLimitedCapacityList(Of T)
        Implements IList(Of T)

        Private _MaxCapacity As Integer
        Private _List As New List(Of T)
        Private _IList As IList(Of T) = DirectCast(_List, IList(Of T))

        Public Class CapacityExceededException
            Inherits Exception
        End Class

        Public Sub Add(ByVal item As T) Implements System.Collections.Generic.ICollection(Of T).Add
            If Count + 1 > _MaxCapacity Then
                Throw New CapacityExceededException
            End If

            _IList.Add(item)
        End Sub

        Public Overridable Sub Clear() Implements System.Collections.Generic.ICollection(Of T).Clear
            _IList.Clear()
        End Sub

        Public Function Contains(ByVal item As T) As Boolean Implements System.Collections.Generic.ICollection(Of T).Contains
            Return _IList.Contains(Item)
        End Function

        Public Sub CopyTo(ByVal array() As T, ByVal arrayIndex As Integer) Implements System.Collections.Generic.ICollection(Of T).CopyTo
            _IList.CopyTo(Array, arrayIndex)
        End Sub

        Public ReadOnly Property Count() As Integer Implements System.Collections.Generic.ICollection(Of T).Count
            Get
                Return _IList.Count
            End Get
        End Property

        Public ReadOnly Property IsReadOnly() As Boolean Implements System.Collections.Generic.ICollection(Of T).IsReadOnly
            Get
                Return _IList.IsReadOnly
            End Get
        End Property

        Public Overridable Function Remove(ByVal item As T) As Boolean Implements System.Collections.Generic.ICollection(Of T).Remove
            Return _IList.Remove(Item)
        End Function

        Public Function GetEnumerator() As System.Collections.Generic.IEnumerator(Of T) Implements System.Collections.Generic.IEnumerable(Of T).GetEnumerator
            Return _IList.GetEnumerator
        End Function

        Public Function IndexOf(ByVal item As T) As Integer Implements System.Collections.Generic.IList(Of T).IndexOf
            Return _IList.IndexOf(item)
        End Function

        Public Overridable Sub Insert(ByVal index As Integer, ByVal item As T) Implements System.Collections.Generic.IList(Of T).Insert
            If Count + 1 > _MaxCapacity Then
                Throw New CapacityExceededException
            End If

            _IList.Insert(index, Item)
        End Sub

        Default Public Property Item(ByVal index As Integer) As T Implements System.Collections.Generic.IList(Of T).Item
            Get
                Return _IList.Item(index)
            End Get
            Set(ByVal value As T)
                _IList.Item(index) = value
            End Set
        End Property

        Public Overridable Sub RemoveAt(ByVal index As Integer) Implements System.Collections.Generic.IList(Of T).RemoveAt
            _IList.RemoveAt(index)
        End Sub

        Private Function GetEnumerator1() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Return _IList.GetEnumerator
        End Function

        Public ReadOnly Property MaxCapacity() As Integer
            Get
                Return _MaxCapacity
            End Get
        End Property

        Public Sub New(ByVal pnMaxCapacity As Integer)
            _MaxCapacity = pnMaxCapacity
        End Sub
    End Class
End Namespace
