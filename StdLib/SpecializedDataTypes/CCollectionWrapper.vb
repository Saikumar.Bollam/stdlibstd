Namespace SpecializedDataTypes
    ''' <summary>
    ''' Wraps a non-generic ICollection in a Generic.ICollection(Of T) interface.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <remarks>Throws NotSupportedExceptions for methods which are not provided by ICollection.</remarks>
    Public Class CCollectionWrapper(Of T)
        Implements ICollection(Of T)

        Private _Collection As ICollection

        Private Sub Add(ByVal item As T) Implements System.Collections.Generic.ICollection(Of T).Add
            Throw New NotSupportedException
        End Sub

        Private Sub Clear() Implements System.Collections.Generic.ICollection(Of T).Clear
            Throw New NotSupportedException
        End Sub

        Private Function Contains(ByVal item As T) As Boolean Implements System.Collections.Generic.ICollection(Of T).Contains
            Throw New NotSupportedException
        End Function

        Public Sub CopyTo(ByVal array() As T, ByVal arrayIndex As Integer) Implements System.Collections.Generic.ICollection(Of T).CopyTo
            _Collection.CopyTo(Array, arrayIndex)
        End Sub

        Public ReadOnly Property Count() As Integer Implements System.Collections.Generic.ICollection(Of T).Count
            Get
                Return _Collection.Count
            End Get
        End Property

        Public ReadOnly Property IsReadOnly() As Boolean Implements System.Collections.Generic.ICollection(Of T).IsReadOnly
            Get
                Return False
            End Get
        End Property

        Private Function Remove(ByVal item As T) As Boolean Implements System.Collections.Generic.ICollection(Of T).Remove
            Throw New NotSupportedException
        End Function

        Public Function GetEnumerator() As System.Collections.Generic.IEnumerator(Of T) Implements System.Collections.Generic.IEnumerable(Of T).GetEnumerator
            Return New CEnumeratorWrapper(Of T)(_Collection.GetEnumerator)
        End Function

        Public Function GetEnumerator1() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Return _Collection.GetEnumerator
        End Function

        Public Sub New(ByVal poCollection As ICollection)
            _Collection = poCollection
        End Sub
    End Class
End Namespace
