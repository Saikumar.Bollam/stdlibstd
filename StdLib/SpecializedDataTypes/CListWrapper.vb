Namespace SpecializedDataTypes
    ''' <summary>
    ''' Wraps a non-generic list in a generic interface.
    ''' </summary>
    ''' <typeparam name="T">The type of the elements within the list.</typeparam>
    ''' <remarks>This does not make a copy.  It references the list that it wraps.</remarks>
    Public Class CListWrapper(Of T)
        Implements IList(Of T)

        Private _List As IList

        Public Sub Add(ByVal item As T) Implements System.Collections.Generic.ICollection(Of T).Add
            _List.Add(item)
        End Sub

        Public Sub Clear() Implements System.Collections.Generic.ICollection(Of T).Clear
            _List.Clear()
        End Sub

        Public Function Contains(ByVal item As T) As Boolean Implements System.Collections.Generic.ICollection(Of T).Contains
            Return _List.Contains(Item)
        End Function

        Public Sub CopyTo(ByVal array() As T, ByVal arrayIndex As Integer) Implements System.Collections.Generic.ICollection(Of T).CopyTo
            _List.CopyTo(Array, arrayIndex)
        End Sub

        Public ReadOnly Property Count() As Integer Implements System.Collections.Generic.ICollection(Of T).Count
            Get
                Return _List.Count
            End Get
        End Property

        Public ReadOnly Property IsReadOnly() As Boolean Implements System.Collections.Generic.ICollection(Of T).IsReadOnly
            Get
                Return _List.IsReadOnly
            End Get
        End Property

        Public Function Remove(ByVal item As T) As Boolean Implements System.Collections.Generic.ICollection(Of T).Remove
            _List.Remove(Item)
        End Function

        Public Function GetEnumerator() As System.Collections.Generic.IEnumerator(Of T) Implements System.Collections.Generic.IEnumerable(Of T).GetEnumerator
            Return New CEnumeratorWrapper(Of T)(_List.GetEnumerator)
        End Function

        Public Function IndexOf(ByVal item As T) As Integer Implements System.Collections.Generic.IList(Of T).IndexOf
            Return _List.IndexOf(item)
        End Function

        Public Sub Insert(ByVal index As Integer, ByVal item As T) Implements System.Collections.Generic.IList(Of T).Insert
            _List.Insert(index, Item)
        End Sub

        Default Public Property Item(ByVal index As Integer) As T Implements System.Collections.Generic.IList(Of T).Item
            Get
                Return DirectCast(_List.Item(index), T)
            End Get
            Set(ByVal value As T)
                _List.Item(index) = value
            End Set
        End Property

        Public Sub RemoveAt(ByVal index As Integer) Implements System.Collections.Generic.IList(Of T).RemoveAt
            _List.RemoveAt(index)
        End Sub

        Public Function GetEnumerator1() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Return _List.GetEnumerator
        End Function

        Public Sub New(ByVal poList As IList)
            _List = poList
        End Sub
    End Class
End Namespace