﻿Option Strict On
Imports System.IO

Namespace SpecializedDataTypes
    Public Class CHybridStream
        Inherits Stream


        Protected _MemStream As New MemoryStream
        Protected _FileStream As FileStream

        ''' <summary>
        ''' Returns the Stream used in this Hybrid
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected ReadOnly Property SelectedStream As Stream
            Get
                If IsUsingFileStream Then
                    Return _FileStream
                Else
                    Return _MemStream
                End If
            End Get
        End Property

        Protected _TempFileName As String = "c:\temp\" & Guid.NewGuid.ToString
        Public ReadOnly Property TempFileName As String
            Get
                Return _TempFileName
            End Get
        End Property

        Protected _nWritten As Long = 0
        Public ReadOnly Property BytesWritten As Long
            Get
                Return _nWritten
            End Get
        End Property

        Private _disposed As Boolean = False

        Protected _mSizeThreshold As Integer = 64000
        Public Property SizeThreshold As Integer
            Get
                Return _mSizeThreshold
            End Get
            Set(value As Integer)
                _mSizeThreshold = value
            End Set
        End Property

        Public ReadOnly Property IsUsingFileStream() As Boolean
            Get
                Return _FileStream IsNot Nothing
            End Get
        End Property

        Protected log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CHybridStream))

        Public Sub New()

        End Sub

        Public Sub New(pThreshold As Integer)
            _mSizeThreshold = pThreshold
        End Sub

        Public Overrides ReadOnly Property CanRead As Boolean
            Get
                Return SelectedStream.CanRead
            End Get
        End Property

        Public Overrides ReadOnly Property CanSeek As Boolean
            Get
                Return SelectedStream.CanSeek
            End Get
        End Property

        Public Overrides ReadOnly Property CanWrite As Boolean
            Get
                Return SelectedStream.CanWrite
            End Get
        End Property

        Public Overrides Sub Flush()
            SelectedStream?.Flush()
        End Sub

        Public Overrides ReadOnly Property Length As Long
            Get
                Return SelectedStream.Length
            End Get
        End Property

        Public Overrides Property Position As Long
            Get
                Return SelectedStream.Position
            End Get
            Set(value As Long)
                SelectedStream.Position = value
            End Set
        End Property

        Public Overrides Function Read(buffer() As Byte, offset As Integer, count As Integer) As Integer
            Return SelectedStream.Read(buffer, offset, count)
        End Function

        Public Overrides Function Seek(offset As Long, origin As SeekOrigin) As Long
            Return SelectedStream.Seek(offset, origin)
        End Function

        Public Overrides Sub SetLength(value As Long)
            SelectedStream.SetLength(value)
        End Sub

        Public Overrides Sub Write(buffer() As Byte, offset As Integer, count As Integer)

            If _nWritten + count > SizeThreshold AndAlso _FileStream Is Nothing Then
                'log.Info("-----------Created filestream------------")
                _FileStream = New FileStream(_TempFileName, FileMode.Create, FileAccess.ReadWrite, FileShare.Read)
                _MemStream.Flush()
                _MemStream.Seek(0, SeekOrigin.Begin)
                _MemStream.CopyTo(_FileStream)
            End If

            If _FileStream IsNot Nothing Then
                'log.Info("-----------write file stream------------" & _nWritten)
                _FileStream.Write(buffer, offset, count)
                _nWritten += count
            Else
                'log.Info("-----------write mem stream------------" & _nWritten)
                _MemStream.Write(buffer, offset, count)
                _nWritten += count
            End If

        End Sub

        Protected Overrides Sub Dispose(disposing As Boolean)
            If _disposed Then
                Return
            End If

            If _MemStream IsNot Nothing Then
                _MemStream.Dispose()
                _MemStream = Nothing
            End If

            If _FileStream IsNot Nothing Then
                _FileStream.Dispose()
                _FileStream = Nothing
            End If

            Try
                If File.Exists(_TempFileName) Then
                    File.Delete(_TempFileName)
                End If
            Catch ex As Exception
                log.Info("Unable to delete temp file for hybrid stream", ex)
            End Try

            _disposed = True
            MyBase.Dispose(disposing)
        End Sub
    End Class
End Namespace
