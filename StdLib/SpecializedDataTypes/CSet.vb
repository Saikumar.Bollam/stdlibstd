Namespace SpecializedDataTypes
    ''' <summary>
    ''' A generic collection of unique elements.
    ''' </summary>
    ''' <remarks>This can probably be replaced with System.Collections.Generic.HashSet in .NET 3.5, but will still be needed by COrderdSet.</remarks>
    <Serializable()> _
    Public Class CSet(Of T)
        Implements ISet(Of T)

        Protected _Items As IDictionary(Of T, Object)

        Public Sub New()
            _Items = New Dictionary(Of T, Object)
        End Sub

        Public Sub New(ByVal pSource As IEnumerable(Of T))
            _Items = New Dictionary(Of T, Object)

            Me.UnionWith(pSource)
        End Sub

        Public Sub New(ByVal poComparer As IEqualityComparer(Of T))
            _Items = New Dictionary(Of T, Object)(poComparer)
        End Sub

        Public Sub New(ByVal pSource As IEnumerable(Of T), ByVal poComparer As IEqualityComparer(Of T))
            _Items = New Dictionary(Of T, Object)(poComparer)

            Me.UnionWith(pSource)
        End Sub

		Private Sub Add_ICollection(ByVal item As T) Implements ICollection(Of T).Add
			Me.Add(item)
		End Sub

		''' <summary>
		''' Adds the item to the set.
		''' </summary>
		''' <param name="poItem"></param>
		''' <returns>True if the item was added; false if it already exists within the set.</returns>
		''' <remarks></remarks>
		Public Function Add(ByVal poItem As T) As Boolean Implements ISet(Of T).Add
            If _Items.ContainsKey(poItem) Then
                Return False
            End If

            _Items.Add(poItem, Nothing)
            Return True
        End Function

		Public Sub Clear() Implements ICollection(Of T).Clear
			_Items.Clear()
		End Sub

		Public Function Contains(ByVal item As T) As Boolean Implements ICollection(Of T).Contains
			Return _Items.ContainsKey(item)
		End Function

		Public Sub CopyTo(ByVal array() As T, ByVal arrayIndex As Integer) Implements ICollection(Of T).CopyTo
			_Items.Keys.CopyTo(array, arrayIndex)
		End Sub

		Public ReadOnly Property Count() As Integer Implements ICollection(Of T).Count
			Get
				Return _Items.Count
			End Get
		End Property

		Public ReadOnly Property IsReadOnly() As Boolean Implements ICollection(Of T).IsReadOnly
			Get
				Return False
			End Get
		End Property

		Public Function Remove(ByVal item As T) As Boolean Implements ICollection(Of T).Remove
			_Items.Remove(item)
		End Function

		Public Function GetEnumerator() As IEnumerator(Of T) Implements IEnumerable(Of T).GetEnumerator
			Return _Items.Keys.GetEnumerator
		End Function

		Private Function GetEnumerator_NonGeneric() As IEnumerator Implements IEnumerable.GetEnumerator
			Return _Items.Keys.GetEnumerator
		End Function

		Public Function ToArray() As T() Implements ISet(Of T).ToArray
            Dim array(Me.Count - 1) As T
            CopyTo(array, 0)
            Return array
        End Function

        Public Sub IntersectWith(ByVal pOther As CSet(Of T))
            If Me.Count = 0 Then
                Return
            End If

            For Each item As T In pOther
                If Not Me.Contains(item) Then
                    Me.Remove(item)
                End If
            Next

            Dim toRemove As New List(Of T)
            For Each item As T In Me
                If Not pOther.Contains(item) Then
                    toRemove.Add(item) 'Mark item for later removal since we can't modify collection while enumerating
                End If
            Next
            For Each item As T In toRemove
                Me.Remove(item)
            Next
        End Sub

        Public Sub IntersectWith(ByVal pOther As IEnumerable(Of T)) Implements ISet(Of T).IntersectWith
            If Me.Count = 0 Then
                Return
            End If

            Dim otherSet As CSet(Of T)
            If TypeOf pOther Is CSet(Of T) Then
                otherSet = DirectCast(pOther, CSet(Of T))
            Else
                otherSet = New CSet(Of T)(pOther)
            End If

            Me.IntersectWith(otherSet)
        End Sub

        Public Shared Function Intersect(ByVal pFirst As IEnumerable(Of T), ByVal pSecond As IEnumerable(Of T)) As CSet(Of T)
            Dim firstCopy As New CSet(Of T)(pFirst)
            firstCopy.IntersectWith(pSecond)

            Return firstCopy
        End Function

        Public Sub UnionWith(ByVal pOther As IEnumerable(Of T)) Implements ISet(Of T).UnionWith
            For Each item As T In pOther
                Me.Add(item)
            Next
        End Sub

        ''' <summary>
        ''' This just calls UnionWith and is here just for convenience and having a similar interface to List(Of T)
        ''' </summary>
        Public Sub AddRange(ByVal pRange As IEnumerable(Of T))
            UnionWith(pRange)
        End Sub

        Public Shared Function Union(ByVal pFirst As IEnumerable(Of T), ByVal pSecond As IEnumerable(Of T)) As CSet(Of T)
            Dim firstCopy As New CSet(Of T)(pFirst)
            firstCopy.UnionWith(pSecond)

            Return firstCopy
        End Function

        Public Function Overlaps(ByVal pOther As IEnumerable(Of T)) As Boolean Implements ISet(Of T).Overlaps
            For Each item As T In pOther
                If Me.Contains(item) Then
                    Return True
                End If
            Next

            Return False
        End Function

        Public Function AsReadOnly() As CReadOnlyWrapper(Of T)
            Return New CReadOnlyWrapper(Of T)(Me)
        End Function

        Public Class CReadOnlyWrapper(Of TItem)
            Implements ISet(Of TItem)

            Private _Set As ISet(Of TItem)

            Public Sub New(ByVal pSet As ISet(Of TItem))
                _Set = pSet
            End Sub

			Private Sub Add_ICollection(ByVal item As TItem) Implements ICollection(Of TItem).Add
				Throw New NotSupportedException("Collection is read-only")
			End Sub

			Private Sub Clear() Implements ICollection(Of TItem).Clear
				Throw New NotSupportedException("Collection is read-only")
			End Sub

			Public Function Contains(ByVal item As TItem) As Boolean Implements ICollection(Of TItem).Contains
				Return _Set.Contains(item)
			End Function

			Public Sub CopyTo(ByVal array() As TItem, ByVal arrayIndex As Integer) Implements ICollection(Of TItem).CopyTo
				_Set.CopyTo(array, arrayIndex)
			End Sub

			Public ReadOnly Property Count() As Integer Implements ICollection(Of TItem).Count
				Get
					Return _Set.Count
				End Get
			End Property

			Public ReadOnly Property IsReadOnly() As Boolean Implements ICollection(Of TItem).IsReadOnly
				Get
					Return _Set.IsReadOnly
				End Get
			End Property

			Private Function Remove(ByVal item As TItem) As Boolean Implements ICollection(Of TItem).Remove
				Throw New NotSupportedException("Collection is read-only")
			End Function

			Public Function GetEnumerator() As IEnumerator(Of TItem) Implements IEnumerable(Of TItem).GetEnumerator
				Return _Set.GetEnumerator()
			End Function

			Private Overloads Function Add(ByVal item As TItem) As Boolean Implements ISet(Of TItem).Add
                Throw New NotSupportedException("Collection is read-only")
            End Function

			Private Sub IntersectWith(ByVal other As IEnumerable(Of TItem)) Implements ISet(Of TItem).IntersectWith
				Throw New NotSupportedException("Collection is read-only")
			End Sub

			Public Function Overlaps(ByVal other As IEnumerable(Of TItem)) As Boolean Implements ISet(Of TItem).Overlaps
				Return _Set.Overlaps(other)
			End Function

			Private Sub UnionWith(ByVal other As IEnumerable(Of TItem)) Implements ISet(Of TItem).UnionWith
				Throw New NotSupportedException("Collection is read-only")
			End Sub

			Private Function GetEnumerator_NonGeneric() As IEnumerator Implements IEnumerable.GetEnumerator
				Return DirectCast(_Set, IEnumerable).GetEnumerator()
			End Function

			Public Function ToArray() As TItem() Implements ISet(Of TItem).ToArray
                Return _Set.ToArray()
            End Function
        End Class
    End Class
End Namespace