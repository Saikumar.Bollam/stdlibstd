﻿Imports System.Collections.Specialized


Namespace SpecializedDataTypes
    ''' <summary>
    ''' This is used whenever you want to swap out the .NET StringDictionary with one that is case sensitive.
    ''' </summary>
    Public Class CCaseSensitiveStringDictionary
        Inherits StringDictionary

        Protected Dictionary As New Dictionary(Of String, String)

        Public Overrides Sub Add(ByVal key As String, ByVal value As String)
            Dictionary.Add(key, value)
        End Sub

        Public Overrides Sub Clear()
            Dictionary.Clear()
        End Sub

        Public Overrides Function ContainsKey(ByVal key As String) As Boolean
            Return Dictionary.ContainsKey(key)
        End Function

        Public Overrides Function ContainsValue(ByVal value As String) As Boolean
            Return Dictionary.ContainsValue(value)
        End Function

        Public Overrides ReadOnly Property Count() As Integer
            Get
                Return Dictionary.Count
            End Get
        End Property

        Public Overrides Property Item(ByVal key As String) As String
            Get
                Return Dictionary.Item(key)
            End Get
            Set(ByVal value As String)
                Dictionary.Item(key) = value
            End Set
        End Property

        Public Overrides ReadOnly Property Keys() As System.Collections.ICollection
            Get
                Return Dictionary.Keys
            End Get
        End Property

        Public Overrides Sub Remove(ByVal key As String)
            Dictionary.Remove(key)
        End Sub

        Public Overrides ReadOnly Property Values() As System.Collections.ICollection
            Get
                Return Dictionary.Values
            End Get
        End Property

        Public Overrides Function ToString() As String
            Return Dictionary.ToString()
        End Function

        Public Overrides Function GetEnumerator() As System.Collections.IEnumerator
            Return Dictionary.GetEnumerator()
        End Function
    End Class
End Namespace