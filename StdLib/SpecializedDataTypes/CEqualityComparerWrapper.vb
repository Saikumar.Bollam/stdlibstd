Namespace SpecializedDataTypes
    Public Class CEqualityComparerWrapper(Of T)
        Implements IEqualityComparer(Of T)

        Private _EqualityComparer As IEqualityComparer

        Public Function Equals1(ByVal x As T, ByVal y As T) As Boolean Implements System.Collections.Generic.IEqualityComparer(Of T).Equals
            Return _EqualityComparer.Equals(x, y)
        End Function

        Public Function GetHashCode1(ByVal obj As T) As Integer Implements System.Collections.Generic.IEqualityComparer(Of T).GetHashCode
            Return _EqualityComparer.GetHashCode(obj)
        End Function

        Private Sub New(ByVal poEqualityComparer As IEqualityComparer)
            _EqualityComparer = poEqualityComparer
        End Sub

        Public Shared Function Convert(ByVal poEqualityComparer As IEqualityComparer) As IEqualityComparer(Of T)
            If TypeOf poEqualityComparer Is IEqualityComparer(Of T) Then
                Return DirectCast(poEqualityComparer, IEqualityComparer(Of T))
            Else
                Return New CEqualityComparerWrapper(Of T)(poEqualityComparer)
            End If
        End Function
    End Class

    Public Class CEqualityComparerGenericWrapper(Of T)
        Implements IEqualityComparer

        Private _EqualityComparerGeneric As IEqualityComparer(Of T)

        Public Function Equals1(ByVal x As Object, ByVal y As Object) As Boolean Implements System.Collections.IEqualityComparer.Equals
            Return _EqualityComparerGeneric.Equals(DirectCast(x, T), DirectCast(y, T))
        End Function

        Public Function GetHashCode1(ByVal obj As Object) As Integer Implements System.Collections.IEqualityComparer.GetHashCode
            Return _EqualityComparerGeneric.GetHashCode(DirectCast(obj, T))
        End Function

        Private Sub New(ByVal poEqualityComparer As IEqualityComparer(Of T))
            _EqualityComparerGeneric = poEqualityComparer
        End Sub

        Public Shared Function Convert(ByVal poEqualityComparer As IEqualityComparer(Of T)) As IEqualityComparer
            If TypeOf poEqualityComparer Is IEqualityComparer Then
                Return DirectCast(poEqualityComparer, IEqualityComparer)
            Else
                Return New CEqualityComparerGenericWrapper(Of T)(poEqualityComparer)
            End If
        End Function
    End Class
End Namespace