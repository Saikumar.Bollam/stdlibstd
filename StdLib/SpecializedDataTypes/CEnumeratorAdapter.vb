Namespace SpecializedDataTypes
    ''' <summary>
    ''' Adapts an IEnumerable(Of SubT) into an IEnumerable(Of BaseT).
    ''' This is useful because in .NET 2.0, when a type SubT inherits from type BaseT, 
    '''  it doesn't mean that IEnumerable(Of SubT) inherits from IEnumerable(Of BaseT).
    ''' </summary>
    ''' <remarks>This class is only needed because .NET does not support covariance for Generics.</remarks>
    Public Class CEnumeratorAdapter(Of SubT As BaseT, BaseT)
        Implements IEnumerator(Of BaseT)

        Private _SubEnumerator As IEnumerator(Of SubT)

        Public ReadOnly Property Current() As BaseT Implements System.Collections.Generic.IEnumerator(Of BaseT).Current
            Get
                Return _SubEnumerator.Current
            End Get
        End Property

        Public ReadOnly Property Current1() As Object Implements System.Collections.IEnumerator.Current
            Get
                Return Me.Current
            End Get
        End Property

        Public Function MoveNext() As Boolean Implements System.Collections.IEnumerator.MoveNext
            Return _SubEnumerator.MoveNext
        End Function

        Public Sub Reset() Implements System.Collections.IEnumerator.Reset
            _SubEnumerator.Reset()
        End Sub

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    _SubEnumerator.Dispose()
                End If
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

        Public Sub New(ByVal poSubEnumerator As IEnumerator(Of SubT))
            _SubEnumerator = poSubEnumerator
        End Sub
    End Class
End Namespace
