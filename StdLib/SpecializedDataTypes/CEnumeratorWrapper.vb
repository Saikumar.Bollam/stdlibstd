Namespace SpecializedDataTypes
    Public Class CEnumeratorWrapper(Of T)
        Implements IEnumerator(Of T)

        Private _Enumerator As IEnumerator

        Public ReadOnly Property Current() As T Implements System.Collections.Generic.IEnumerator(Of T).Current
            Get
                Return DirectCast(_Enumerator.Current, T)
            End Get
        End Property

        Private ReadOnly Property Current1() As Object Implements System.Collections.IEnumerator.Current
            Get
                Return _Enumerator.Current
            End Get
        End Property

        Public Function MoveNext() As Boolean Implements System.Collections.IEnumerator.MoveNext
            Return _Enumerator.MoveNext()
        End Function

        Public Sub Reset() Implements System.Collections.IEnumerator.Reset
            _Enumerator.Reset()
        End Sub

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    'free managed resources when explicitly called
                    _Enumerator = Nothing
                End If

                ' TODO: free shared unmanaged resources
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

        Public Sub New(ByVal poEnumerator As IEnumerator)
            _Enumerator = poEnumerator
        End Sub
    End Class
End Namespace
