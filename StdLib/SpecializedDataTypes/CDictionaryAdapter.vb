Namespace SpecializedDataTypes
    ''' <summary>
    ''' Covariance adapter for IDictionary.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class CDictionaryAdapter(Of TKey, TSub As TBase, TBase)
        Implements IDictionary(Of TKey, TBase)

        Private _SubDictionary As IDictionary(Of TKey, TSub)
        Public Sub New(ByVal poDictionary As IDictionary(Of TKey, TSub))
            _SubDictionary = poDictionary
        End Sub

        Private Sub Add(ByVal item As System.Collections.Generic.KeyValuePair(Of TKey, TBase)) Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TBase)).Add
            _SubDictionary.Add(New KeyValuePair(Of TKey, TSub)(item.Key, DirectCast(item.Value, TSub)))
        End Sub

        Public Sub Clear() Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TBase)).Clear
            _SubDictionary.Clear()
        End Sub

        Private Function Contains(ByVal item As System.Collections.Generic.KeyValuePair(Of TKey, TBase)) As Boolean Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TBase)).Contains
            Return _SubDictionary.Contains(New KeyValuePair(Of TKey, TSub)(Item.Key, DirectCast(Item.Value, TSub)))
        End Function

        Private Sub CopyTo(ByVal array() As System.Collections.Generic.KeyValuePair(Of TKey, TBase), ByVal arrayIndex As Integer) Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TBase)).CopyTo
            For Each oPair As KeyValuePair(Of TKey, TBase) In Me
                array(arrayIndex) = oPair
                arrayIndex += 1
            Next
        End Sub

        Public ReadOnly Property Count() As Integer Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TBase)).Count
            Get
                Return _SubDictionary.Count
            End Get
        End Property

        Public ReadOnly Property IsReadOnly() As Boolean Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TBase)).IsReadOnly
            Get
                Return _SubDictionary.IsReadOnly
            End Get
        End Property

        Private Function Remove1(ByVal item As System.Collections.Generic.KeyValuePair(Of TKey, TBase)) As Boolean Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TBase)).Remove
            _SubDictionary.Remove(New KeyValuePair(Of TKey, TSub)(Item.Key, DirectCast(Item.Value, TSub)))
        End Function

        Private Sub Add1(ByVal key As TKey, ByVal value As TBase) Implements System.Collections.Generic.IDictionary(Of TKey, TBase).Add
            _SubDictionary.Add(key, DirectCast(value, TSub))
        End Sub

        Public Function ContainsKey(ByVal key As TKey) As Boolean Implements System.Collections.Generic.IDictionary(Of TKey, TBase).ContainsKey
            Return _SubDictionary.ContainsKey(key)
        End Function

        Default Public Property Item(ByVal key As TKey) As TBase Implements System.Collections.Generic.IDictionary(Of TKey, TBase).Item
            Get
                Return _SubDictionary(key)
            End Get
            Set(ByVal value As TBase)
                _SubDictionary(key) = DirectCast(value, TSub)
            End Set
        End Property

        Public ReadOnly Property Keys() As System.Collections.Generic.ICollection(Of TKey) Implements System.Collections.Generic.IDictionary(Of TKey, TBase).Keys
            Get
                Return _SubDictionary.Keys
            End Get
        End Property

        Public Function Remove(ByVal key As TKey) As Boolean Implements System.Collections.Generic.IDictionary(Of TKey, TBase).Remove
            Return _SubDictionary.Remove(key)
        End Function

        Public Function TryGetValue(ByVal key As TKey, ByRef value As TBase) As Boolean Implements System.Collections.Generic.IDictionary(Of TKey, TBase).TryGetValue
            Return _SubDictionary.TryGetValue(key, DirectCast(value, TSub))
        End Function

        Public ReadOnly Property Values() As System.Collections.Generic.ICollection(Of TBase) Implements System.Collections.Generic.IDictionary(Of TKey, TBase).Values
            Get
                Return New CCollectionAdapter(Of TSub, TBase)(_SubDictionary.Values)
            End Get
        End Property

        Public Function GetEnumerator() As System.Collections.Generic.IEnumerator(Of System.Collections.Generic.KeyValuePair(Of TKey, TBase)) Implements System.Collections.Generic.IEnumerable(Of System.Collections.Generic.KeyValuePair(Of TKey, TBase)).GetEnumerator
            Return New CKeyValuePairEnumeratorAdapter(Of TKey, TSub, TBase)(_SubDictionary.GetEnumerator())
        End Function

        Private Function GetEnumerator1() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Return GetEnumerator()
        End Function
    End Class
End Namespace