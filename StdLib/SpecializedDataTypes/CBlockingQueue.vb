﻿Imports System.Threading

Namespace SpecializedDataTypes

    ''' <summary>
    ''' A thread safe queue that will block if an enqueue operation is run when full or if a dequeue operation is run when empty
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <remarks></remarks>
    Public Class CBlockingQueue(Of T)
        Private _Queue As Queue(Of T)
        Private _MaxCapacity As Integer
        Private _Capacity As Semaphore
        Private _Count As Semaphore



        Private _Lock As New Object


        Sub New(ByVal pMaxCapacity As Integer)
            If pMaxCapacity < 1 Then
                Throw New ArgumentException("Max capacity must be at least 1")
            End If
            _Queue = New Queue(Of T)
            _MaxCapacity = pMaxCapacity
            _Capacity = New Semaphore(_MaxCapacity, _MaxCapacity)
            _Count = New Semaphore(0, _MaxCapacity)

        End Sub

        Public Sub Enqueue(ByVal pItem As T)
            _Capacity.WaitOne()
            SyncLock _Lock
                _Queue.Enqueue(pItem)
                _Count.Release()
                CAssert.IsTrue(_Queue.Count <= _MaxCapacity, "The queue is over capacity")
            End SyncLock
        End Sub

        Public Function Dequeue() As T
            _Count.WaitOne()
            Dim item As T
            SyncLock _Lock
                item = _Queue.Dequeue
                _Capacity.Release()
            End SyncLock

            Return item
        End Function
    End Class
End Namespace
