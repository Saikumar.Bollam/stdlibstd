Namespace SpecializedDataTypes
    ''' <summary>
    ''' Adapts an IEnumerable(Of SubT) into an IEnumerator(Of BaseT).
    ''' This is useful because in .NET 2.0, when a type SubT inherits from type BaseT, 
    '''  it doesn't mean that IEnumerable(Of SubT) inherits from IEnumerable(Of BaseT).
    ''' </summary>
    ''' <remarks>This class is only needed because .NET does not support covariance for Generics.</remarks>
    Public Class CEnumerableAdapter(Of SubT As BaseT, BaseT)
        Implements IEnumerable(Of BaseT)

        Private _SubEnumerable As IEnumerable(Of SubT)

        Public Function GetEnumerator() As System.Collections.Generic.IEnumerator(Of BaseT) Implements System.Collections.Generic.IEnumerable(Of BaseT).GetEnumerator
            Return New CEnumeratorAdapter(Of SubT, BaseT)(_SubEnumerable.GetEnumerator)
        End Function

        Private Function GetEnumerator1() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Return Me.GetEnumerator
        End Function

        Public Sub New(ByVal poSubEnumerable As IEnumerable(Of SubT))
            _SubEnumerable = poSubEnumerable
        End Sub
    End Class
End Namespace
