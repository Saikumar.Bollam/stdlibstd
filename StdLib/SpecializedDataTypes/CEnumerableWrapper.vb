Namespace SpecializedDataTypes
    Public Class CEnumerableWrapper(Of T)
        Implements IEnumerable(Of T)

        Private _Enumerable As IEnumerable

        Public Sub New(ByVal poEnumerable As IEnumerable)
            _Enumerable = poEnumerable
        End Sub

        Public Function GetEnumerator() As System.Collections.Generic.IEnumerator(Of T) Implements System.Collections.Generic.IEnumerable(Of T).GetEnumerator
            Return New CEnumeratorWrapper(Of T)(_Enumerable.GetEnumerator())
        End Function

        Public Function GetEnumerator1() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Return _Enumerable.GetEnumerator()
        End Function
    End Class
End Namespace
