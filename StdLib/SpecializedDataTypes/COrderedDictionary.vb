Imports System.Data
'test
Namespace SpecializedDataTypes
    ''' <summary>
    ''' Generic version of System.Collections.Specialized.OrderedDictionary
    ''' </summary>
    ''' <typeparam name="TKey"></typeparam>
    ''' <typeparam name="TValue"></typeparam>
    ''' <remarks>Referred to http://www.codeproject.com/KB/recipes/GenericOrderedDictionary.aspx during implementation.</remarks>
    <Serializable()>
    Public Class COrderedDictionary(Of TKey, TValue)
        Implements IOrderedDictionary(Of TKey, TValue)

        Private _Dictionary As Dictionary(Of TKey, TValue)
        Private _List As List(Of KeyValuePair(Of TKey, TValue))

#Region "TypeCast"
        Private ReadOnly Property _IDictionary() As IDictionary(Of TKey, TValue)
            Get
                Return _Dictionary
            End Get
        End Property
#End Region

        Public Sub New()
            _Dictionary = New Dictionary(Of TKey, TValue)
            _List = New List(Of KeyValuePair(Of TKey, TValue))
        End Sub

        Public Sub New(ByVal pnCapacity As Integer)
            _Dictionary = New Dictionary(Of TKey, TValue)(pnCapacity)
            _List = New List(Of KeyValuePair(Of TKey, TValue))(pnCapacity)
        End Sub

        Public Sub New(ByVal poEqualityComparer As IEqualityComparer(Of TKey))
            _Dictionary = New Dictionary(Of TKey, TValue)(poEqualityComparer)
            _List = New List(Of KeyValuePair(Of TKey, TValue))
        End Sub

        Public Sub New(ByVal pnCapacity As Integer, ByVal poEqualityComparer As IEqualityComparer(Of TKey))
            _Dictionary = New Dictionary(Of TKey, TValue)(pnCapacity, poEqualityComparer)
            _List = New List(Of KeyValuePair(Of TKey, TValue))(pnCapacity)
        End Sub

        ''' <summary>
        ''' Add a new item to the collection.
        ''' </summary>
        ''' <returns>The index of the newly added item.</returns>
        Public Function Add(ByVal key As TKey, ByVal value As TValue) As Integer
            _Dictionary.Add(key, value)
            _List.Add(New KeyValuePair(Of TKey, TValue)(key, value))
            Return Count - 1
        End Function

        Public Function IndexOf(ByVal key As TKey) As Integer Implements IOrderedDictionary(Of TKey, TValue).IndexOf
            If Not ContainsKey(key) Then
                Return -1
            End If

            For i As Integer = 0 To _List.Count - 1
                If _Dictionary.Comparer.Equals(_List(i).Key, key) Then
                    Return i
                End If
            Next

            Return -1 'Should never be hit
        End Function

        Private Sub Add_ICollection(ByVal item As System.Collections.Generic.KeyValuePair(Of TKey, TValue)) Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).Add
            Add(item.Key, item.Value)
        End Sub

        Public Sub Clear() Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).Clear
            _Dictionary.Clear()
            _List.Clear()
        End Sub

        Private Function Contains(ByVal item As System.Collections.Generic.KeyValuePair(Of TKey, TValue)) As Boolean Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).Contains
            Return _IDictionary.Contains(item)
        End Function

        Public Sub CopyTo(ByVal array() As System.Collections.Generic.KeyValuePair(Of TKey, TValue), ByVal arrayIndex As Integer) Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).CopyTo
            _List.CopyTo(array, arrayIndex)
        End Sub

        Public ReadOnly Property Count() As Integer Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).Count
            Get
                Return _List.Count
            End Get
        End Property

        Public ReadOnly Property IsReadOnly() As Boolean Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).IsReadOnly
            Get
                Return _IDictionary.IsReadOnly
            End Get
        End Property

        Public Function Remove(ByVal item As System.Collections.Generic.KeyValuePair(Of TKey, TValue)) As Boolean Implements System.Collections.Generic.ICollection(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).Remove
            If Not Contains(item) Then
                Return False
            End If

            Return Remove(item.Key)
        End Function

        Private Sub Add_IDictionary(ByVal key As TKey, ByVal value As TValue) Implements System.Collections.Generic.IDictionary(Of TKey, TValue).Add
            Add(key, value)
        End Sub

        Public Function ContainsKey(ByVal key As TKey) As Boolean Implements System.Collections.Generic.IDictionary(Of TKey, TValue).ContainsKey
            Return _Dictionary.ContainsKey(key)
        End Function

        Default Public Overloads Property Item(ByVal key As TKey) As TValue Implements System.Collections.Generic.IDictionary(Of TKey, TValue).Item
            Get
                Return _Dictionary(key)
            End Get
            Set(ByVal value As TValue)
                If (_Dictionary.ContainsKey(key)) Then
                    _Dictionary(key) = value
                    _List(IndexOf(key)) = New KeyValuePair(Of TKey, TValue)(key, value)
                Else
                    Add(key, value)
                End If
            End Set
        End Property

        Public ReadOnly Property Keys() As System.Collections.Generic.ICollection(Of TKey) Implements System.Collections.Generic.IDictionary(Of TKey, TValue).Keys
            Get
                Return New CKeyCollection(Me)
            End Get
        End Property

        Public Function Remove(ByVal key As TKey) As Boolean Implements System.Collections.Generic.IDictionary(Of TKey, TValue).Remove
            If key Is Nothing Then
                Throw New ArgumentNullException("key")
            End If

            Dim nIndex As Integer = IndexOf(key)
            If nIndex < 0 Then
                Return False
            End If

            _Dictionary.Remove(_List(nIndex).Key)
			_List.RemoveAt(nIndex)
			Return True
		End Function

        Public Function TryGetValue(ByVal key As TKey, ByRef value As TValue) As Boolean Implements System.Collections.Generic.IDictionary(Of TKey, TValue).TryGetValue
            Return _Dictionary.TryGetValue(key, value)
        End Function

        Public ReadOnly Property Values() As System.Collections.Generic.ICollection(Of TValue) Implements System.Collections.Generic.IDictionary(Of TKey, TValue).Values
            Get
                Return New CValueCollection(Me)
            End Get
        End Property

        Public Function GetEnumerator() As System.Collections.Generic.IEnumerator(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)) Implements System.Collections.Generic.IEnumerable(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).GetEnumerator
            Return _List.GetEnumerator()
        End Function

        Private Function GetEnumerator_NonGeneric() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Return _List.GetEnumerator()
        End Function

        Private Function IndexOf(ByVal item As System.Collections.Generic.KeyValuePair(Of TKey, TValue)) As Integer Implements System.Collections.Generic.IList(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).IndexOf
            Return _List.IndexOf(item)
        End Function

        Public Sub Insert(ByVal index As Integer, ByVal item As System.Collections.Generic.KeyValuePair(Of TKey, TValue)) Implements System.Collections.Generic.IList(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).Insert
            If index > Count OrElse index < 0 Then
                Throw New ArgumentOutOfRangeException("index", "index must be non-negative and less than the size of the collection")
            End If

            _IDictionary.Add(item)
            _List.Insert(index, item)
        End Sub

        ''' <summary>
        ''' Gets the KeyValuePair at the specified index, or removes the KeyValuePair at the specified index and inserts a the specified KeyValuePair at the same index.
        ''' </summary>
        Private Property Item_IList(ByVal index As Integer) As System.Collections.Generic.KeyValuePair(Of TKey, TValue) Implements System.Collections.Generic.IList(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).Item
            Get
                Return _List(index)
            End Get
            Set(ByVal item As System.Collections.Generic.KeyValuePair(Of TKey, TValue))
                Dim key As TKey = _List(index).Key

                _Dictionary.Remove(key)
                _Dictionary.Add(item.Key, item.Value)
                _List(index) = item
            End Set
        End Property

        Default Public Overloads Property Item(ByVal index As Integer) As TValue Implements IOrderedDictionary(Of TKey, TValue).Item
            Get
                Return _List(index).Value
            End Get
            Set(ByVal value As TValue)
                Dim key As TKey = _List(index).Key

                _List(index) = New KeyValuePair(Of TKey, TValue)(key, value)
                _Dictionary(key) = value
            End Set
        End Property

        Public Sub RemoveAt(ByVal index As Integer) Implements System.Collections.Generic.IList(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).RemoveAt
            Dim key As TKey = _List(index).Key

            _List.RemoveAt(index)
            _Dictionary.Remove(key)
        End Sub

#Region "Inner collection classes"
        Public Class CKeyCollection
            Implements ICollection(Of TKey)

            Private _OrderedDictionary As COrderedDictionary(Of TKey, TValue)

            Public Sub New(ByVal pOrderedDictionary As COrderedDictionary(Of TKey, TValue))
                _OrderedDictionary = pOrderedDictionary
            End Sub

            Private Sub Add(ByVal item As TKey) Implements System.Collections.Generic.ICollection(Of TKey).Add
                Throw New ReadOnlyException()
            End Sub

            Private Sub Clear() Implements System.Collections.Generic.ICollection(Of TKey).Clear
                Throw New ReadOnlyException()
            End Sub

            Public Function Contains(ByVal item As TKey) As Boolean Implements System.Collections.Generic.ICollection(Of TKey).Contains
                Return _OrderedDictionary.ContainsKey(item)
            End Function

            Public Sub CopyTo(ByVal array() As TKey, ByVal arrayIndex As Integer) Implements System.Collections.Generic.ICollection(Of TKey).CopyTo
                If array Is Nothing Then
                    Throw New ArgumentNullException("array is null.")
                End If
                If array.Rank > 1 Then
                    Throw New ArgumentException("array is multidimensional.")
                End If

                ' Ignore case where length = 0 and arrayindex = 0
                If Not (arrayIndex = 0 AndAlso array.Length = 0) AndAlso arrayIndex >= array.Length Then
                    Throw New ArgumentException("arrayIndex is equal to or greater than the length of array.")
                End If
                If array.Length - arrayIndex < Count Then
                    Throw New ArgumentException("The number of elements in the source System.Collections.Generic.ICollection(Of T) is greater than the available space from arrayIndex to the end of the destination array.")
                End If

                For Each item As TKey In Me
                    array(arrayIndex) = item
                    arrayIndex += 1
                Next
            End Sub

            Public ReadOnly Property Count() As Integer Implements System.Collections.Generic.ICollection(Of TKey).Count
                Get
                    Return _OrderedDictionary.Count
                End Get
            End Property

            Public ReadOnly Property IsReadOnly() As Boolean Implements System.Collections.Generic.ICollection(Of TKey).IsReadOnly
                Get
                    Return True
                End Get
            End Property

            Private Function Remove(ByVal item As TKey) As Boolean Implements System.Collections.Generic.ICollection(Of TKey).Remove
                Throw New ReadOnlyException()
            End Function

            Public Function GetEnumerator() As System.Collections.Generic.IEnumerator(Of TKey) Implements System.Collections.Generic.IEnumerable(Of TKey).GetEnumerator
                Return New CEnumerator(_OrderedDictionary.GetEnumerator())
            End Function

            Private Function GetEnumerator_NonGeneric() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
                Return GetEnumerator()
            End Function

            Public Class CEnumerator
                Implements IEnumerator(Of TKey)

                Private _Enumerator As IEnumerator(Of KeyValuePair(Of TKey, TValue))

                Public Sub New(ByVal pEnumerator As IEnumerator(Of KeyValuePair(Of TKey, TValue)))
                    _Enumerator = pEnumerator
                End Sub

                Public ReadOnly Property Current() As TKey Implements System.Collections.Generic.IEnumerator(Of TKey).Current
                    Get
                        Return _Enumerator.Current.Key
                    End Get
                End Property

                Private ReadOnly Property Current_NonGeneric() As Object Implements System.Collections.IEnumerator.Current
                    Get
                        Return Current
                    End Get
                End Property

                Public Function MoveNext() As Boolean Implements System.Collections.IEnumerator.MoveNext
                    Return _Enumerator.MoveNext()
                End Function

                Public Sub Reset() Implements System.Collections.IEnumerator.Reset
                    _Enumerator.Reset()
                End Sub

                Private disposedValue As Boolean = False        ' To detect redundant calls

                ' IDisposable
                Protected Overridable Sub Dispose(ByVal disposing As Boolean)
                    If Not Me.disposedValue Then
                        If disposing Then
                            _Enumerator.Dispose()
                        End If
                    End If
                    Me.disposedValue = True
                End Sub

#Region " IDisposable Support "
                ' This code added by Visual Basic to correctly implement the disposable pattern.
                Public Sub Dispose() Implements IDisposable.Dispose
                    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
                    Dispose(True)
                    GC.SuppressFinalize(Me)
                End Sub
#End Region

            End Class
        End Class

        Public Class CValueCollection
            Implements ICollection(Of TValue)

            Private _OrderedDictionary As COrderedDictionary(Of TKey, TValue)

            Public Sub New(ByVal pOrderedDictionary As COrderedDictionary(Of TKey, TValue))
                _OrderedDictionary = pOrderedDictionary
            End Sub

            Private Sub Add(ByVal item As TValue) Implements System.Collections.Generic.ICollection(Of TValue).Add
                Throw New ReadOnlyException()
            End Sub

            Private Sub Clear() Implements System.Collections.Generic.ICollection(Of TValue).Clear
                Throw New ReadOnlyException()
            End Sub

            Public Function Contains(ByVal item As TValue) As Boolean Implements System.Collections.Generic.ICollection(Of TValue).Contains
                Return _OrderedDictionary._IDictionary.Values.Contains(item)
            End Function

            Public Sub CopyTo(ByVal array() As TValue, ByVal arrayIndex As Integer) Implements System.Collections.Generic.ICollection(Of TValue).CopyTo
                If array Is Nothing Then
                    Throw New ArgumentNullException("array is null.")
                End If
                If array.Rank > 1 Then
                    Throw New ArgumentException("array is multidimensional.")
                End If

                ' Ignore case where length = 0 and arrayindex = 0
                If Not (array.Length = 0 AndAlso arrayIndex = 0) AndAlso arrayIndex >= array.Length Then
                    Throw New ArgumentException("arrayIndex is equal to or greater than the length of array.")
                End If
                If array.Length - arrayIndex < Count Then
                    Throw New ArgumentException("The number of elements in the source System.Collections.Generic.ICollection(Of T) is greater than the available space from arrayIndex to the end of the destination array.")
                End If

                For Each item As TValue In Me
                    array(arrayIndex) = item
                    arrayIndex += 1
                Next
            End Sub

            Public ReadOnly Property Count() As Integer Implements System.Collections.Generic.ICollection(Of TValue).Count
                Get
                    Return _OrderedDictionary.Count
                End Get
            End Property

            Public ReadOnly Property IsReadOnly() As Boolean Implements System.Collections.Generic.ICollection(Of TValue).IsReadOnly
                Get
                    Return True
                End Get
            End Property

            Private Function Remove(ByVal item As TValue) As Boolean Implements System.Collections.Generic.ICollection(Of TValue).Remove
                Throw New ReadOnlyException()
            End Function

            Public Function GetEnumerator() As System.Collections.Generic.IEnumerator(Of TValue) Implements System.Collections.Generic.IEnumerable(Of TValue).GetEnumerator
                Return New CEnumerator(_OrderedDictionary.GetEnumerator())
            End Function

            Private Function GetEnumerator_NonGeneric() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
                Return GetEnumerator()
            End Function

            Public Class CEnumerator
                Implements IEnumerator(Of TValue)

                Private _Enumerator As IEnumerator(Of KeyValuePair(Of TKey, TValue))

                Public Sub New(ByVal pEnumerator As IEnumerator(Of KeyValuePair(Of TKey, TValue)))
                    _Enumerator = pEnumerator
                End Sub

                Public ReadOnly Property Current() As TValue Implements System.Collections.Generic.IEnumerator(Of TValue).Current
                    Get
                        Return _Enumerator.Current.Value
                    End Get
                End Property

                Private ReadOnly Property Current_NonGeneric() As Object Implements System.Collections.IEnumerator.Current
                    Get
                        Return Current
                    End Get
                End Property

                Public Function MoveNext() As Boolean Implements System.Collections.IEnumerator.MoveNext
                    Return _Enumerator.MoveNext()
                End Function

                Public Sub Reset() Implements System.Collections.IEnumerator.Reset
                    _Enumerator.Reset()
                End Sub

                Private disposedValue As Boolean = False        ' To detect redundant calls

                ' IDisposable
                Protected Overridable Sub Dispose(ByVal disposing As Boolean)
                    If Not Me.disposedValue Then
                        If disposing Then
                            _Enumerator.Dispose()
                        End If
                    End If
                    Me.disposedValue = True
                End Sub

#Region " IDisposable Support "
                ' This code added by Visual Basic to correctly implement the disposable pattern.
                Public Sub Dispose() Implements IDisposable.Dispose
                    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
                    Dispose(True)
                    GC.SuppressFinalize(Me)
                End Sub
#End Region

            End Class
        End Class
#End Region
    End Class
End Namespace
