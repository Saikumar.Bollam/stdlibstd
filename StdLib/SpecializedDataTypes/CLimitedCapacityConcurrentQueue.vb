﻿Option Strict On

Imports System.Threading

Namespace SpecializedDataTypes

    ''' <summary>
    ''' Create a Queue of Arrays of T
    ''' Specific behavior: will try to not allow size > MAX.
    ''' will clear out queue to make room for entire input array and still not exceed max.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    Public Class CLimitedCapacityConcurrentArrayQueue(Of T)

        Private ObjectQueue As New Concurrent.ConcurrentQueue(Of T())
        Private ObjectCount As Long = 0
        Private MaximumCount As Long

        Public Sub New(ByVal pMaximumCount As Long)
            MaximumCount = pMaximumCount
            If MaximumCount < 1 Then
                MaximumCount = 1
            End If
        End Sub

        Public Function Count() As Integer
            Return ObjectQueue.Count
        End Function

        Public Sub Enqueue(ByVal inputArray As T())
            While ObjectCount + inputArray.Count > MaximumCount
                'we have capped, so we will first in first out the data.
                Dim obj As T() = Nothing
                If ObjectQueue.TryDequeue(obj) Then
                    Interlocked.Add(ObjectCount, -obj.Count)
                End If

                ''sanity check in case of desync??
                '' OR
                ''when the input array is TOO LARGE to fit in max count. we will enqueue it and just it.
                If ObjectQueue.Count = 0 Then
                    ObjectCount = 0
                    Exit While
                End If
            End While

            Interlocked.Add(ObjectCount, inputArray.Count)
            ObjectQueue.Enqueue(inputArray)

        End Sub

        Public Function TryDequeue(ByRef inputArray As T()) As Boolean
            ''save it to local array first in case of strange race conditions possible from OUTSIDE code.
            Dim resultArray As T() = Nothing
            Dim success As Boolean = ObjectQueue.TryDequeue(resultArray)
            If success Then
                inputArray = resultArray
                Interlocked.Add(ObjectCount, -resultArray.Count)

                ''sanity check in case of desync??
                If ObjectQueue.Count = 0 Then
                    ObjectCount = 0
                End If

            End If
            Return success
        End Function

    End Class
End Namespace