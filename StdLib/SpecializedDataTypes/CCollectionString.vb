Namespace SpecializedDataTypes


    Public Class CCollectionString
        Inherits System.Collections.Specialized.StringCollection

        'combines 2 set and returns a new one - originals are not affected
        Public Function union(ByVal oList As CCollectionString) As CCollectionString
            Dim newSet As New CCollectionString()
            Dim buff() As String = Nothing

            Me.CopyTo(buff, 0)
            newSet.AddRange(buff)

            oList.CopyTo(buff, 0)
            newSet.AddRange(buff)

            Return newSet
        End Function
    End Class
End Namespace