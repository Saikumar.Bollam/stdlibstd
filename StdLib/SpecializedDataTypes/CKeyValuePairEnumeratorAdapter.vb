Namespace SpecializedDataTypes
    ''' <summary>
    ''' Covariance adapter for KeyValuePair.
    ''' </summary>
    Public Class CKeyValuePairEnumeratorAdapter(Of TKey, TSub As TBase, TBase)
        Implements IEnumerator(Of KeyValuePair(Of TKey, TBase))

        Private _SubEnumerator As IEnumerator(Of KeyValuePair(Of TKey, TSub))
        Public Sub New(ByVal poEnumerator As IEnumerator(Of KeyValuePair(Of TKey, TSub)))
            _SubEnumerator = poEnumerator
        End Sub

        Public ReadOnly Property Current() As System.Collections.Generic.KeyValuePair(Of TKey, TBase) Implements System.Collections.Generic.IEnumerator(Of System.Collections.Generic.KeyValuePair(Of TKey, TBase)).Current
            Get
                Dim oCurrent As KeyValuePair(Of TKey, TSub) = _SubEnumerator.Current
                Return New KeyValuePair(Of TKey, TBase)(oCurrent.Key, oCurrent.Value)
            End Get
        End Property

        Private ReadOnly Property Current1() As Object Implements System.Collections.IEnumerator.Current
            Get
                Return Current
            End Get
        End Property

        Public Function MoveNext() As Boolean Implements System.Collections.IEnumerator.MoveNext
            Return _SubEnumerator.MoveNext
        End Function

        Public Sub Reset() Implements System.Collections.IEnumerator.Reset
            _SubEnumerator.Reset()
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            _SubEnumerator.Dispose()
        End Sub
    End Class
End Namespace
