Namespace SpecializedDataTypes
    ''' <summary>
    ''' Wraps an IEnumerator(Of DictionaryEntry) into an IEnumerator(Of KeyValuePair(Of TKey, TValue))
    ''' </summary>
    ''' <typeparam name="TKey"></typeparam>
    ''' <typeparam name="TValue"></typeparam>
    ''' <remarks>Assumes the DictionaryEntry's Key and Value members are of the correct type.</remarks>
    Class CDictionaryEntryEnumeratorWrapper(Of TKey, TValue)
        Implements IEnumerator(Of KeyValuePair(Of TKey, TValue))

        Private _Enumerator As IEnumerator(Of DictionaryEntry)

        Public ReadOnly Property Current() As System.Collections.Generic.KeyValuePair(Of TKey, TValue) Implements System.Collections.Generic.IEnumerator(Of System.Collections.Generic.KeyValuePair(Of TKey, TValue)).Current
            Get
                Return New KeyValuePair(Of TKey, TValue)(DirectCast(_Enumerator.Current.Key, TKey), DirectCast(_Enumerator.Current.Value, TValue))
            End Get
        End Property

        Public ReadOnly Property Current1() As Object Implements System.Collections.IEnumerator.Current
            Get
                Return Current()
            End Get
        End Property

        Public Function MoveNext() As Boolean Implements System.Collections.IEnumerator.MoveNext
            Return _Enumerator.MoveNext()
        End Function

        Public Sub Reset() Implements System.Collections.IEnumerator.Reset
            _Enumerator.Reset()
        End Sub

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' free managed resources when explicitly called
                    _Enumerator.Dispose()
                End If

                ' TODO: free shared unmanaged resources
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

        Public Sub New(ByVal poEnumerator As IEnumerator(Of DictionaryEntry))
            _Enumerator = poEnumerator
        End Sub
    End Class
End Namespace
