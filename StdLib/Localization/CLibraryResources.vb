Imports System.Configuration.ConfigurationManager
Imports System.Resources
Imports StdLib.CBaseStd
Imports StdLib.Exceptions

Namespace Localization
    ''' <summary>
    ''' Wraps the .NET resource manager to avoid some code ugliness. Please note that this is potentially quite a heavyweight class to create.
    ''' </summary>
    ''' <typeparam name="T">The class to get strings for.</typeparam>
    ''' <remarks></remarks>
    Public Class CLibraryResources(Of T)
        Implements IDisposable

        Private Shared sDir As String
        Private Shared _Type As System.Type = GetType(T)

        Shared Sub New()
            sDir = System.Configuration.ConfigurationManager.AppSettings("LIBRARY_RESOURCES_DIR")
            If CBaseStd.SafeString(sDir) = "" Then
                sDir = ConfigMapFolder & "\Resources"
            End If
        End Sub

        Private _Resources As ResourceManager

        Sub New()
            'my.Resources.ResourceManager.
            _Resources = ResourceManager.CreateFileBasedResourceManager(_Type.FullName, sDir, Nothing)
        End Sub

        Public Function getString(ByVal psKey As String) As String
            If psKey Is Nothing Then
                Throw New ArgumentNullException("psKey")
            End If

            Dim sReturn As String = _Resources.GetString(psKey)
            If sReturn Is Nothing Then
                Dim sThrow As String = SafeString(AppSettings.Item("loc_throw_error_missing_key"))
                If sThrow = "Y" Then
                    Throw New LocalizationKeyNotFoundException(psKey)
                Else
                    CAssert.FailGracefully("Cannot find translation key: " & psKey & " of Type: " & _Type.FullName)
                    Return "?????"
                End If
            End If

            Return sReturn
        End Function

        Public Function getString(ByVal key As String, ByVal ParamArray args() As Object) As String
            Return String.Format(getString(key), args)
        End Function


#Region " IDisposable Support "
        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    _Resources.ReleaseAllResources()
                    _Resources = Nothing
                End If
            End If
            Me.disposedValue = True
        End Sub

        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class
End Namespace