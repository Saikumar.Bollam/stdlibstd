Option Strict On
Imports System.Configuration.ConfigurationManager
Imports System.Xml
Imports StdLib.CBaseStd


Namespace Localization

    Public Class CustomStringTranslation
        Private Const CACHE_TIMEOUT_SECS As Integer = 30


        Protected Shared ReadOnly Property TranslationDoc() As XmlDocument
            Get
                'Static- lives from one call of the property to another.
                Static _TranslationDoc As XmlDocument
                Static _TranslationDocFetched As Date = Date.MinValue
                Static _TranslationPath As String
                'Double lock- saves the time of synching if we don't need to go into the critical section, then once locked makes
                'sure another process didn't beat us into the critical section.
                If _TranslationDoc Is Nothing OrElse _TranslationDocFetched.AddSeconds(CACHE_TIMEOUT_SECS) < Now Then
                    SyncLock GetType(CustomStringTranslation)
                        If _TranslationDoc Is Nothing OrElse _TranslationDocFetched.AddSeconds(CACHE_TIMEOUT_SECS) < Now Then
                            If _TranslationPath Is Nothing Then
                                _TranslationPath = SafeString(AppSettings.Get("LIBRARY_RESOURCES_DIR"))
                                If _TranslationPath = "" Then
                                    _TranslationPath = ConfigMapFolder & "\Resources"
                                End If
                                _TranslationPath &= "\CustomTranslations.xml"
                            End If

                            _TranslationDoc = New XmlDocument
                            _TranslationDoc.Load(_TranslationPath)

                            _TranslationDocFetched = Now
                        End If
                    End SyncLock
                End If

                Return _TranslationDoc
            End Get
        End Property

        Private sXPath As String
        Public Sub New()
            Dim sUICulture As String = Threading.Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName
            sXPath = "language[@code='" & sUICulture & "']/entry[english/text()={0}]/translated"
        End Sub



        Public Function translateString(ByVal psToTranslate As String) As String
            Dim oTranslateElement As XmlElement = DirectCast(TranslationDoc.DocumentElement.SelectSingleNode(String.Format(sXPath, XPathString(psToTranslate))), XmlElement)
            If oTranslateElement IsNot Nothing AndAlso oTranslateElement.InnerText <> "" Then
                Return oTranslateElement.InnerText
            Else
                Return psToTranslate
            End If
        End Function


    End Class
End Namespace
