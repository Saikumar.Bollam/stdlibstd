﻿Imports StdLib.CBaseStd

Namespace DBUtils
    <Serializable>
    Public Class CDBUtilsFactory
        Implements IDBUtilsFactory
        Public Function Create(ByVal psConnectionString As String) As IDBUtils Implements IDBUtilsFactory.Create

            If SafeString(psConnectionString) = "" Then
                Throw New ArgumentException("Connection string is blank")
            End If

            Return New CSQLDBUtils(psConnectionString)
        End Function

    End Class
End Namespace
