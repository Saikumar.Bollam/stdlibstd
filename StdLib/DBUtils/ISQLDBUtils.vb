﻿Imports System.Data

Namespace DBUtils
    Public Interface ISQLDBUtils
        Inherits IDBUtils
        Overloads Sub FillDataTable(ByVal dt As DataTable, ByVal poWhereClauseWithWhere As CSQLWhereStringBuilder,
                                    Optional ByVal psOrderClause As String = "")
    End Interface
End Namespace
