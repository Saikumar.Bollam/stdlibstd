﻿Imports Microsoft.AspNetCore.Http
Namespace Stdlib
    Public NotInheritable Class HttpContext
        Private Shared m_httpContextAccessor As IHttpContextAccessor

        Public Shared Sub Configure(httpContextAccessor As IHttpContextAccessor)
            m_httpContextAccessor = httpContextAccessor
        End Sub

        Public Shared ReadOnly Property Current As Microsoft.AspNetCore.Http.HttpContext
            Get
				Return m_httpContextAccessor?.HttpContext
			End Get
        End Property
    End Class
End Namespace