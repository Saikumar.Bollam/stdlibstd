﻿Namespace DBUtils
    Public Interface IDBUtilsFactory

        Function Create(ByVal psConnectionString As String) As IDBUtils
    End Interface
End Namespace
