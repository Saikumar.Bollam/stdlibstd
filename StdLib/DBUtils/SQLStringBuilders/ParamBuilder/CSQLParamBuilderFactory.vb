﻿Option Strict On

Namespace DBUtils
    Public Class CSQLParamBuilderFactory
        Implements ISQLParamBuilderFactory

        Public Function create() As ISQLParamBuilder Implements ISQLParamBuilderFactory.create
            Return New CSQLParamBuilder()
        End Function
    End Class
End Namespace
