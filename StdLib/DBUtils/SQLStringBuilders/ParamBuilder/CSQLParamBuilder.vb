﻿
Imports System.Data
Imports System.Data.SqlClient

Namespace DBUtils
    '***********************************************************************************************************
    ' CSQLParamBuilder
    '***********************************************************************************************************
    ' EXAMPLE USAGE (Suitable for small or simple queries):
    '   Protected Function Example_GetDataType(ByVal psTableName As String, ByVal psColumn As String) As String
    '    Dim sSQLQuery As String = "SELECT Columns.DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS Columns "
    '    Dim oWhere As New CSQLWhereStringBuilder()

    '    Dim prmTableName = CSQLParamBuilder.CreateParam("@tableName", psTableName)
    '    oWhere.AppendANDCondition($"Columns.TABLE_NAME={prmTableName}")

    '    Dim prmColumnName = CSQLParamBuilder.CreateParam("@columnName", psColumn)
    '    oWhere.AppendANDCondition($"Columns.COLUMN_NAME={prmColumnName}")

    '    Return _DBConn.getScalarValue(sSQLQuery & oWhere.SQL, prmTableName, prmColumnName)
    '   End Function
    '***********************************************************************************************************
    ' ALTERNATE EXAMPLE USAGE (Suitable for large or conditional queries):
    '   Protected Function Example_GetDataType(ByVal psTableName As String, ByVal psColumn As String) As String
    '    Dim sSQLQuery As String = "SELECT Columns.DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS Columns "
    '    Dim oWhere As New CSQLWhereStringBuilder()
    '    Dim oSqlParams As New CSQLParamBuilder()

    '    Dim prmTableName = CSQLParamBuilder.CreateParam("@tableName", psTableName)
    '    oSqlParams.AddParam(prmTableName)
    '    oWhere.AppendANDCondition($"Columns.TABLE_NAME={prmTableName}")

    '    If (_bSomeCondition) Then
    '       Dim prmColumnName = CSQLParamBuilder.CreateParam("@columnName", psColumn)
    '       oSqlParams.AddParam(prmColumnName)
    '       oWhere.AppendANDCondition($"Columns.COLUMN_NAME={prmColumnName}")
    '    End If

    '    Return _DBConn.getScalarValue(sSQLQuery & oWhere.SQL, oSqlParams.GetParams())
    '   End Function
    '***********************************************************************************************************
    ' CREATING MANY PARAMETERS FROM A DICTIONARY (Assumes Imports System.Linq, dicParams is already defined):
    '   dicParams.Select(Function(kvPair) CSQLParamBuilder.CreateParam(kvPair.Key, kvPair.Value))
    '***********************************************************************************************************
    Public Class CSQLParamBuilder
        Implements ISQLParamBuilder

        Public Const TABLE_PARAMETER_COLUMN_NAME = "Item"
        Private Const _TINYINT_MIN = 0
        Private Const _TINYINT_MAX = 255
        Private Const _DEFAULT_SIZE_BINARY = 8000
        Private Const _DEFAULT_SIZE_VARCHAR = 8000
        Private Const _DEFAULT_SIZE_NVARCHAR = 4000
        Private Const _SQL_MAX = -1

        Private _SqlParams As List(Of SqlParameter) = New List(Of SqlParameter)

        ''' <summary>
        ''' Returns the list of SqlParameters
        ''' </summary>
        Public Function GetParams() As List(Of SqlParameter) Implements ISQLParamBuilder.GetParams
            Return _SqlParams
        End Function

        ''' <summary>
        ''' Adds a SqlParameter to the list of params.
        ''' </summary>
        ''' <param name="pSqlParam">SqlParameter to add to the list</param>
        Public Sub AddParam(ByVal pSqlParam As SqlParameter) Implements ISQLParamBuilder.AddParam
            _SqlParams.Add(pSqlParam)
        End Sub

        ''' <summary>
        ''' Adds a list of SqlParameter to the list of params.
        ''' </summary>
        ''' <param name="pSqlParams">SqlParameters to add to the list</param>
        Public Sub AddParams(ByVal pSqlParams As IEnumerable(Of SqlParameter)) Implements ISQLParamBuilder.AddParams
            _SqlParams.AddRange(pSqlParams)
        End Sub

        ''' <summary>
        ''' Construct a DateTime SqlParameter 
        ''' </summary>
        ''' <param name="psParamName">SQL parameter name</param>
        ''' <param name="pdtParamValue">Date value</param>
        Public Shared Function CreateParam(ByVal psParamName As String, ByVal pdtParamValue? As Date) As SqlParameter
            If Not IsSQLParameterNameValid(psParamName) Then
                Throw New Exception($"SQL Parameter Name is not valid: {psParamName}")
            End If

            Dim sqlParam As SqlParameter = New SqlParameter(psParamName, SqlDbType.DateTime)

            If False = pdtParamValue.HasValue Then
                sqlParam.IsNullable = True
                sqlParam.Value = DBNull.Value
            Else
                If (Not IsValidSqlDateTime(pdtParamValue.Value)) Then
                    Throw New Exception($"Invalid SqlDateTime value: {pdtParamValue.ToString()}")
                End If

                sqlParam.Value = pdtParamValue.Value
            End If

            Return sqlParam
        End Function

        ''' <summary>
        ''' Validates a date for use in the database
        ''' </summary>
        ''' <param name="pdtValue">Date value to validate</param>
        Private Shared Function IsValidSqlDateTime(ByVal pdtValue As Date) As Boolean
            If (SqlTypes.SqlDateTime.MinValue.Value > pdtValue OrElse SqlTypes.SqlDateTime.MaxValue < pdtValue) Then
                Return False
            End If

            Return True
        End Function

        ''' <summary>
        ''' Construct a VarChar SqlParameter 
        ''' </summary>
        ''' <param name="psParamName">SQL parameter name</param>
        ''' <param name="psParamValue">String value</param>
        Public Shared Function CreateParam(ByVal psParamName As String, ByVal psParamValue As String) As SqlParameter
            If Not IsSQLParameterNameValid(psParamName) Then
                Throw New Exception($"SQL Parameter Name is not valid: {psParamName}")
            End If


            Dim sqlParam As SqlParameter = New SqlParameter(psParamName, SqlDbType.VarChar)
            If psParamValue Is Nothing Then
                sqlParam.IsNullable = True
                sqlParam.Value = DBNull.Value
            Else
                Dim size As Integer
                If (psParamValue.Length > _DEFAULT_SIZE_VARCHAR) Then
                    size = _SQL_MAX
                Else
                    size = _DEFAULT_SIZE_VARCHAR
                End If
                sqlParam.Size = size
                sqlParam.Value = psParamValue
            End If

            Return sqlParam
        End Function

        ''' <summary>
        ''' Construct a unique identifier SqlParameter
        ''' </summary>
        ''' <param name="psParamName">SQL parameter name</param>
        ''' <param name="psParamValue">String value</param>
        Public Shared Function CreateParam(ByVal psParamName As String, ByVal psParamValue As Guid?) As SqlParameter
            If Not IsSQLParameterNameValid(psParamName) Then
                Throw New Exception($"SQL Parameter Name is not valid: {psParamName}")
            End If


            Dim sqlParam As SqlParameter = New SqlParameter(psParamName, SqlDbType.UniqueIdentifier)
            If Not psParamValue.HasValue Then
                sqlParam.IsNullable = True
                sqlParam.Value = DBNull.Value
            Else
                sqlParam.Size = 16
                sqlParam.Value = psParamValue.Value
            End If

            Return sqlParam
        End Function



        Private Shared Function IsSQLParameterNameValid(ByVal psParamName As String) As Boolean
            If String.IsNullOrWhiteSpace(psParamName) Then
                Return False
            End If

            Return True
        End Function
    End Class
End Namespace
