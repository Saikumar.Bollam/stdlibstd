﻿Option Strict On

Imports System.Data.SqlClient

Namespace DBUtils
    Public Interface ISQLParamBuilder
        Sub AddParam(pSqlParam As SqlParameter)
        Sub AddParams(pSqlParams As IEnumerable(Of SqlParameter))
        Function GetParams() As List(Of SqlParameter)
    End Interface
End Namespace
