﻿Namespace DBUtils
    Public Interface ISQLParamBuilderFactory
        Function create() As ISQLParamBuilder
    End Interface
End Namespace
