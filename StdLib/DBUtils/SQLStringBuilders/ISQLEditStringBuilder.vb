Namespace DBUtils

    Public Interface ISQLEditStringBuilder
        ReadOnly Property SQL() As String
        Sub appendString(ByVal sField As String, ByVal sValue As String)
        Sub AppendValue(ByVal psField As String, ByVal poValue As CSQLParamValue)
        Sub AppendParameter(psFieldName As String, psParameterName As String)
        Sub Reset()
    End Interface

End Namespace
