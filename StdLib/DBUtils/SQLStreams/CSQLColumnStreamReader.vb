﻿Option Strict On

Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Namespace DBUtils

    ''' <summary>
    ''' Streams the content of a SINGLE column, and a SINGLE row.
    ''' NOTE: Each read hits DB, so buffer it.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class CSQLColumnStreamReader
        Inherits Stream

        Public Sub New(ByVal pCommand As SqlCommand)
            Dim sqlReader As SqlDataReader = pCommand.ExecuteReader(CommandBehavior.SequentialAccess)
            If False = sqlReader.Read() Then
                Throw New Exception("Data Not Found.")
            End If

            oReader = sqlReader
        End Sub

        Private oReader As SqlClient.SqlDataReader
        Private ColumnIndex As Integer = 0
        Public Overrides Property Position As Long

        Public Overrides ReadOnly Property CanRead As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property CanSeek As Boolean
            Get
                Return False
            End Get
        End Property

        Public Overrides ReadOnly Property CanWrite As Boolean
            Get
                Return False
            End Get
        End Property

        Public Overrides Sub Flush()
            Throw New NotImplementedException()
        End Sub

        Public Overrides ReadOnly Property Length As Long
            Get
                Throw New NotImplementedException()
            End Get
        End Property


        Public Overrides Function Read(buffer() As Byte, offset As Integer, count As Integer) As Integer
            Dim lBytesRead As Long = oReader.GetBytes(ColumnIndex, Position, buffer, offset, count)
            Position += lBytesRead
            Return CInt(lBytesRead)
        End Function

        Public Overrides Function Seek(offset As Long, origin As SeekOrigin) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Sub SetLength(value As Long)
            Throw New NotImplementedException()
        End Sub

        Public Overrides Sub Write(buffer() As Byte, offset As Integer, count As Integer)
            Throw New NotImplementedException()
        End Sub

        Protected Overrides Sub Dispose(disposing As Boolean)
            If disposing AndAlso oReader IsNot Nothing Then
                oReader.Dispose()
                oReader = Nothing
            End If
            MyBase.Dispose(disposing)
        End Sub
    End Class

End Namespace