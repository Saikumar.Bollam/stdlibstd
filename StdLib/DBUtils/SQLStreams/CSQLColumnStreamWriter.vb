﻿Option Strict On

Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Namespace DBUtils

    ''' <summary>
    ''' This class is guaranteed to smoothly stream BLOBs into SQL by stream. Minimal memory usage.
    ''' NOTE: Each write hits DB, so buffer it.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class CSQLColumnStreamWriter
        Inherits Stream

        Public Const DATA_PARAM As String = "@data"
        Public Const OFFSET_PARAM As String = "@offset"
        Public Const LENGTH_PARAM As String = "@length"

        Public UpdateCommand As SqlCommand
        ''' <summary>
        ''' @data of .WRITE(@data, @offset, @length). data to insert.
        ''' </summary>
        Public UpdateDataParam As SqlParameter
        ''' <summary>
        ''' @offset of .WRITE(@data, @offset, @length). Which character to start replacing on.
        ''' </summary>
        Public UpdateOffsetParam As SqlParameter
        ''' <summary>
        ''' @length of .WRITE(@data, @offset, @length). How many CHARACTERS to REPLACE with DATA 
        ''' </summary>
        Public UpdateLengthParam As SqlParameter

        ''' <summary>
        ''' DO NOT CONSTRUCT DIRECTLY!!
        ''' Use getColumnStreamWriter in CSQLDBUTILS to construct this class for everything to set up automatically!
        ''' </summary>
        Public Sub New(ByVal pCommand As SqlCommand)
            UpdateCommand = pCommand

            If Not UpdateCommand.Parameters.Contains(DATA_PARAM) Then
                UpdateCommand.Parameters.Add(DATA_PARAM, SqlDbType.VarBinary, -1)
            End If
            UpdateDataParam = UpdateCommand.Parameters(DATA_PARAM)

            If Not UpdateCommand.Parameters.Contains(OFFSET_PARAM) Then
                UpdateCommand.Parameters.Add(OFFSET_PARAM, SqlDbType.BigInt, -1)
            End If
            UpdateOffsetParam = UpdateCommand.Parameters(OFFSET_PARAM)

            If Not UpdateCommand.Parameters.Contains(LENGTH_PARAM) Then
                UpdateCommand.Parameters.Add(LENGTH_PARAM, SqlDbType.BigInt, -1)
            End If
            UpdateLengthParam = UpdateCommand.Parameters(LENGTH_PARAM)
        End Sub


        Public Overrides ReadOnly Property CanRead As Boolean
            Get
                Return False
            End Get
        End Property

        Public Overrides ReadOnly Property CanSeek As Boolean
            Get
                Return False
            End Get
        End Property

        Public Overrides ReadOnly Property CanWrite As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides Sub Flush()
        End Sub

        Public Overrides ReadOnly Property Length As Long
            Get
                Throw New NotImplementedException()
            End Get
        End Property

        ''' <summary>
        ''' CAN use this to set where you want to write!
        ''' </summary>
        Public Overrides Property Position As Long = 0

        Public Overrides Function Read(buffer() As Byte, offset As Integer, count As Integer) As Integer
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Seek(offset As Long, origin As SeekOrigin) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Sub SetLength(value As Long)
            Throw New NotImplementedException()
        End Sub

        Public Overrides Sub Write(buffer() As Byte, offset As Integer, count As Integer)
            Dim data As Byte() = buffer
            If offset <> 0 OrElse count <> buffer.Length Then
                ReDim data(count - 1)
                Array.Copy(buffer, offset, data, 0, count)
            End If
            UpdateDataParam.Value = data
            UpdateOffsetParam.Value = Position
            UpdateLengthParam.Value = count
            UpdateCommand.ExecuteNonQuery()
            Position += count
        End Sub
    End Class

End Namespace