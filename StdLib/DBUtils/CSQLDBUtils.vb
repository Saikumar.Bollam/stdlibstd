Imports System.Data
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports StdLib.CBaseStd
Imports StdLib.CTimeZoneManager
Imports StdLib.Logging
Imports StdLib.SpecializedDataTypes


Namespace DBUtils
    ''' <summary>
    ''' This class provides for more convenient access into the SQL Server.  
    ''' Sample Proper use that avoids connection leaks:
    ''' 
    ''' Using oDB As New CSQLDBUtils(std.CONNECTIONSTRING)
    ''' Dim s As String = oDB.getScalerValue("SELECT top 10 * From Loans")
    ''' End Using
    ''' 
    ''' </summary>
    ''' <remarks>
    ''' * when working w/ transactions, there is a potential deadlock (exception type  'System.Data.SqlClient.SqlException')
    ''' if we try to retrieve a field value, in a separate connection, from a row that's already being modified in an active transaction,
    ''' in a transacted connection.  The work around is if we detect a current transaction, we use that transacted connection
    ''' for all queries.
    ''' * also, we dont really need to worrry about opening a connection to retrieve data, in client 
    ''' code we should worry more about closing the connection asap so that it can be pooled
    ''' </remarks>
    Public Class CSQLDBUtils
        Implements IDBUtils
        Implements ISQLDBUtils
        Implements IDisposable

        Protected Shared Log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CSQLDBUtils))

        ''' <summary>
        ''' Keeps a copy of the cached data columns SELECT statements in shared variable for efficiency reasons.  
        ''' However, one should avoid enabling this as it could create concurrency and memory issues.  
        ''' It's best to only turn on for frequent used queries with high hit ratio (eg: app related tables).  
        ''' Content in here are not clered out by default so memory growth could become an issue.
        ''' </summary>
        Public EnableCacheMetaData As Boolean

        Private Shared _DefaultIsTransactionViolationFatal As Boolean = ("Y" = SafeString(System.Configuration.ConfigurationManager.AppSettings.Get("IS_TRANSACTION_VIOLATION_FATAL")))
        Private _IsTransactionViolationFatal As Nullable(Of Boolean) = Nothing
        ''' <summary>
        ''' Governs whether attempting to execute a non-transacted UpdateDataTable or executeNonQuery while the object has a
        ''' transaction should throw an exception, or only log an error.
        ''' </summary>
        ''' <remarks>There may be times when we don't want to enlist secondary queries in a transaction for performance reasons.  If that is the case, simply use a different instance of CDBUtils.</remarks>
        Public Property IsTransactionViolationFatal() As Boolean
            Get
                If _IsTransactionViolationFatal.HasValue Then
                    Return _IsTransactionViolationFatal.Value
                Else
                    Return _DefaultIsTransactionViolationFatal
                End If
            End Get
            Set(ByVal value As Boolean)
                _IsTransactionViolationFatal = value
            End Set
        End Property

#Region "Diagnostic / Profiling Routines"
        ''' <summary>
        ''' Note this is a low level configuration to maximize performance by turning off all tracing (no logs nor stopwatch is internally created).   
        ''' If not an emergency it's recommended to control what logs to capture via the log4net config file.
        ''' </summary>		
        ''' <remarks>
        ''' Having this information is useful for identifying unnecessary system bottlenecks.
        ''' </remarks>
        Private Shared EnableSQLProfile As Boolean = System.Configuration.ConfigurationManager.AppSettings.Get("DBUTILS.ENABLE_PROFILE") <> "N"
        Private _StopWatch As New System.Diagnostics.Stopwatch
        Private Sub startTimer()
            If EnableSQLProfile = False Then
                Return
            End If

            _StopWatch.Reset()
            _StopWatch.Start()
        End Sub
        ''' <summary>
        ''' Ends stopwatch in progress logs summary information.  This should be called after startTimer
        ''' </summary>
        ''' <param name="psPrefix"></param>
        ''' <param name="sql"></param>
        ''' <remarks></remarks>
        Private Sub endTimer(ByVal psPrefix As String, ByVal sql As String)
            Dim nSec As Double = _StopWatch.Elapsed().TotalSeconds
            _StopWatch.Stop()

            HoneyBee.CDrone.GetInstance().LogSingleLibraryMetric("SQL-Execution-Seconds", nSec, Nothing)

            If EnableSQLProfile = False Then
                Return
            End If

            Log.Debug(FormatNumber(nSec, 3) & If(nSec > 1, "!!", "") & " s: [" & psPrefix & "] " & sql)
            If Stdlib.HttpContext.Current IsNot Nothing AndAlso
                CBaseStd.YNStringToBool(SafeString(Stdlib.HttpContext.Current.Items("enable_user_sql_trace"))) Then
                Log.Info(FormatNumber(nSec, 3) & If(nSec > 1, "!!", "") & " s: [" & psPrefix & "] " & sql)
            End If
        End Sub

#End Region


        Private _connectionString As String
        Public Sub New(ByVal sConnection As String)
            _connectionString = sConnection
        End Sub

        Private _Conn As SqlConnection
        Private _ConnTransacted As SqlConnection
        Private _sharedTransaction As SqlTransaction 'associated w/ _ConnTransacted
        Private _PerfTrackerID As Guid = Guid.Empty
        Private _LastSQL As String = ""
        Public Const SQLConnectionTagName As String = "SQL_CONNECTION"
        Private _ConnectionOpenTime As DateTime? = Nothing

        'one of the problems with filling datatables is that dataset table has n fields defined and the db has n+1 fields defined, then when we fill the datatable using SELECT * and try to update, it will fail!
        'key of hashtable = table name 
        'Public Shared SelectCache As New Hashtable()

        Private _CommandTimeout As Integer = 90
        Public Property CommandTimeout() As Integer
            Get
                Return _CommandTimeout
            End Get
            Set(ByVal Value As Integer)
                _CommandTimeout = Value
            End Set
        End Property

        ''' <summary>
        ''' Returns true if the normal or transaction connection is open.  
        ''' </summary>
        ''' <remarks>
        ''' This is useful to identify connection leak
        ''' </remarks>
        Public ReadOnly Property HasOpenConnection As Boolean
            Get
                If IsNothing(_Conn) = False AndAlso _Conn.State <> ConnectionState.Closed Then
                    Return True
                End If

				If False = IsNothing(_ConnTransacted) AndAlso _ConnTransacted.State <> ConnectionState.Closed Then
					Return True
				End If
				Return False
			End Get
        End Property

        Public Sub closeConnection() Implements IDBUtils.closeConnection
            If IsNothing(_lastDataReader) = False AndAlso _lastDataReader.IsClosed = False Then
                _lastDataReader.Close()
            End If
            If IsNothing(_Conn) = False AndAlso _Conn.State <> ConnectionState.Closed Then
                _Conn.Close()
            End If
            _Conn = Nothing

            If False = IsNothing(_ConnTransacted) AndAlso _ConnTransacted.State <> ConnectionState.Closed Then
                _sharedTransaction = Nothing
                _ConnTransacted.Close()
            End If
            _ConnTransacted = Nothing

            LogConnectionCloseTime()
        End Sub

        Public Function openConnection() As System.Data.IDbConnection Implements IDBUtils.openConnection
            If IsNothing(_Conn) Then
                _Conn = New SqlConnection(_connectionString)
                _Conn.Open()

                LogConnectionOpenTime("Open connection")
            End If
            Return _Conn
        End Function

        ''' <summary>
        ''' Async version of the openConnection.
        ''' </summary>
        Public Async Function openConnectionAsync() As Task(Of System.Data.IDbConnection) Implements IDBUtils.openConnectionAsync
            If IsNothing(_Conn) Then
                _Conn = New SqlConnection(_connectionString)
                Await _Conn.OpenAsync()

                LogConnectionOpenTime("Open connection")
            End If
            Return _Conn
        End Function

        Public Function openConnectionTransacted() As System.Data.IDbConnection Implements IDBUtils.openConnectionTransacted
            If IsNothing(_ConnTransacted) Then
                _ConnTransacted = New SqlConnection(_connectionString)
                _ConnTransacted.Open()

                LogConnectionOpenTime("Open connection transacted")
            End If

            'ensure there is a valid transaction since the transaction might be invalidated after commit/rollback
            If IsNothing(_sharedTransaction) Then
                _sharedTransaction = _ConnTransacted.BeginTransaction
            ElseIf IsNothing(_sharedTransaction.Connection) Then
                _sharedTransaction = _ConnTransacted.BeginTransaction
            End If

            Return _ConnTransacted
        End Function

        ''' <summary>
        ''' Async version of the openConnectionTransacted.
        ''' </summary>
        Public Async Function openConnectionTransactedAsync() As Task(Of System.Data.IDbConnection) Implements IDBUtils.openConnectionTransactedAsync
            If IsNothing(_ConnTransacted) Then
                _ConnTransacted = New SqlConnection(_connectionString)
                Await _ConnTransacted.OpenAsync()

                LogConnectionOpenTime("Open connection transacted")
            End If

            'ensure there is a valid transaction since the transaction might be invalidated after commit/rollback
            If IsNothing(_sharedTransaction) Then
                _sharedTransaction = _ConnTransacted.BeginTransaction
            ElseIf IsNothing(_sharedTransaction.Connection) Then
                _sharedTransaction = _ConnTransacted.BeginTransaction
            End If

            Return _ConnTransacted
        End Function

        Private _LocalZoneCache As Zone?
        ''' <summary>
        ''' To make StdLib more portable, and avoid time zone fuss, we default things to LA if not specified on machine.
        ''' </summary>
        Private ReadOnly Property _LocalZone As Zone
            Get
                If _LocalZoneCache.HasValue = False Then
                    If System.Configuration.ConfigurationManager.AppSettings.Get("MACHINE_TIME_ZONE") = "" Then
                        _LocalZoneCache = Zone.LosAngeles
                    Else
                        _LocalZoneCache = CTimeZoneManager.LocalZone
                    End If
                End If

                Return _LocalZoneCache.Value
            End Get
        End Property

        ''' <summary>
        ''' This will disable the log message when a connection is open for too long. This should only be used if it is expectd and known that the connection will
        ''' be open for a long time. 
        ''' </summary>
        Public DisableStaleConnectionMessage As Boolean = False

        ''' <summary>
        ''' Maxiumum number of seconds a connection can be open before it will log a message
        ''' </summary>
        Private Shared SQL_CONNECTION_STALE_THRESHOLD_SECONDS As Integer = SafeInteger(System.Configuration.ConfigurationManager.AppSettings.Get("SQL_CONNECTION_STALE_THRESHOLD_SECONDS"))


        Private Sub LogConnectionCloseTime()
            If _PerfTrackerID <> Guid.Empty Then
                CPerfTracker.LogPerf(Nothing, DateTimeStruct.Now(_LocalZone), SQLConnectionTagName,
                _LastSQL, "", _PerfTrackerID)

                'Avoid duplicate log
                _PerfTrackerID = Guid.Empty
            End If


            If Not DisableStaleConnectionMessage AndAlso SQL_CONNECTION_STALE_THRESHOLD_SECONDS > 0 Then
                If _ConnectionOpenTime.HasValue Then
                    Dim connectionOpenDuration As TimeSpan = DateTime.UtcNow - _ConnectionOpenTime.Value
                    If connectionOpenDuration.TotalSeconds > SQL_CONNECTION_STALE_THRESHOLD_SECONDS Then
                        Log.Info(String.Format("SQL connection open time: {0} ms exceeds limit. Please close connection as early as possible to avoid connection saturation. LastRunSQL (first 100 char): {1} StackTrace: {2}.", connectionOpenDuration.TotalMilliseconds, Left(_LastSQL, 100), System.Environment.StackTrace))
                    End If
                End If
            End If
            _ConnectionOpenTime = Nothing

        End Sub

        Private Sub LogConnectionOpenTime(ByVal psMessage As String)

            'No need to check if connection is open or not. It will always be open because it's
            'opened right before entering this function

            _PerfTrackerID = CPerfTracker.LogPerf(DateTimeStruct.Now(_LocalZone), Nothing, SQLConnectionTagName,
              psMessage, "")

            If _ConnectionOpenTime Is Nothing Then
                _ConnectionOpenTime = DateTime.UtcNow
            End If
        End Sub

        'if there is no current transaction then that means no sql was executed or the connection was never opened -- 
        Public Sub commitTransaction() Implements IDBUtils.commitTransaction
            If IsNothing(_sharedTransaction) Then
                Throw New ApplicationException("No transaction to commit.  Please check that transacted connection was opened or there was some sql executed against a transacted connection!")
            End If
            _sharedTransaction.Commit()
            _sharedTransaction = Nothing 'turn off transaction 
        End Sub

        Public Sub rollbackTransaction() Implements IDBUtils.rollbackTransaction
            If IsNothing(_sharedTransaction) Then
                Throw New ApplicationException("No transaction to roll back.  Please check that transacted connection was opened or there was some sql executed against a transacted connection!")
            End If
            _sharedTransaction.Rollback()
            _sharedTransaction = Nothing 'turn off transaction 
        End Sub

        ''' <summary>
        ''' Calls rollbackTransaction, but instead of throwing an error if there isn't one open, just does nothing.
        ''' </summary>
        ''' <remarks>
        ''' Good for error handling situations where we want to roll back if any transaction is open, and don't really know if that's the case.
        ''' </remarks>
        Public Sub rollbackTransactionIfOpen() Implements IDBUtils.rollbackTransactionIfOpen
            If IsNothing(_sharedTransaction) Then
                Return
            End If

            rollbackTransaction()
        End Sub

        ''' <summary>
        ''' Calls commitTransaction, but instead of throwing an error if there isn't one open, just does nothing.
        ''' </summary>
        Public Sub commitTransactionIfOpen() Implements IDBUtils.commitTransactionIfOpen
            If IsNothing(_sharedTransaction) Then
                Return
            End If

            commitTransaction()
        End Sub


        Private Function isTransactionActive() As Boolean
            If IsNothing(_sharedTransaction) Then
                Return False
            ElseIf IsNothing(_sharedTransaction.Connection) Then
                Return False
            End If

            Return True
        End Function

        ''' <summary>
        ''' Writes Big Data to a VarBinary column using streaming. Use to Minimize memory usage. Each write = DB hit, so buffer input.
        ''' </summary>
        Public Function getColumnStreamWriter(ByVal psTableName As String, ByVal psColumnName As String, ByVal pWhere As CSQLWhereStringBuilder,
                                              Optional ByVal isTransacted As Boolean = False) As CSQLColumnStreamWriter
            Dim cmd As SqlCommand
            Dim sSQL As String = String.Format("UPDATE {0} SET {1} .WRITE({2}, {3}, {4})",
                psTableName, psColumnName,
                CSQLColumnStreamWriter.DATA_PARAM, CSQLColumnStreamWriter.OFFSET_PARAM, CSQLColumnStreamWriter.LENGTH_PARAM) &
                    pWhere.SQL

            Dim con As SqlConnection = Nothing

            If isTransacted Then

                con = DirectCast(openConnectionTransacted(), SqlConnection)
                cmd = New SqlCommand(sSQL, con, _sharedTransaction)

            Else

                If True = isTransactionActive() Then
                    Dim oException As New CSQLException("Executing non-transacted non-query SQL command through a CSQLDBUtils object in transacted mode. This may cause data consistency errors and deadlocks.", sSQL)
                    If True = IsTransactionViolationFatal Then
                        Throw oException
                    Else
                        Log.Info(oException.ToString, oException)
                    End If
                End If

                con = DirectCast(openConnection(), SqlConnection)
                cmd = New SqlCommand(sSQL, con)

            End If

            Return New CSQLColumnStreamWriter(cmd)
        End Function

        ''' <summary>
        ''' Reads Big Data to a VarBinary column using streaming. Use to Minimize memory usage. Each write = DB hit, so buffer input.
        ''' </summary>
        Public Function getColumnStreamReader(ByVal psTableName As String, ByVal psColumnName As String,
                                              ByVal pWhere As CSQLWhereStringBuilder) As CSQLColumnStreamReader
            Dim cmd As SqlCommand
            Dim sSQL As String = String.Format("SELECT {0} FROM {1}", psColumnName, psTableName) & pWhere.SQL

            If isTransactionActive() Then
                cmd = New SqlCommand(sSQL, DirectCast(openConnectionTransacted(), SqlConnection))
                cmd.Transaction = _sharedTransaction
            Else
                cmd = New SqlCommand(sSQL, DirectCast(openConnection(), SqlConnection))
            End If

            Return New CSQLColumnStreamReader(cmd)
        End Function

        ''' <summary>
        ''' Executes an sql statement straight to our connection. 
        ''' </summary>
        ''' <param name="sql">SQL string that will be run.  Highly recommended you use the CSQLStringBuilders to generate this string instead of creating manually by hand.</param>
        ''' <param name="isTransacted"></param>
        Public Function executeNonQuery(ByVal sql As String, Optional ByVal isTransacted As Boolean = False) As Integer Implements IDBUtils.executeNonQuery
            sql = UnescapeSQLBraces(sql)
            Dim oConnectionCommandTuple As Tuple(Of SqlConnection, SqlCommand) = getSQLConnectionAndCommandForExecuteNonQuery(sql, isTransacted)

            Dim poConnection As SqlConnection = oConnectionCommandTuple.Item1
            Dim poCommand As SqlCommand = oConnectionCommandTuple.Item2

            Return executeNonQueryHelper(sql, poConnection, poCommand)

        End Function

        Public Function executeNonQuery(ByVal psSQL As String, ByVal pbIsTransacted As Boolean, ByVal ParamArray poSQLParams As SqlParameter()) As Integer Implements IDBUtils.executeNonQuery
            psSQL = UnescapeSQLBraces(psSQL)

            Dim oConnectionCommandTuple As Tuple(Of SqlConnection, SqlCommand) = getSQLConnectionAndCommandForExecuteNonQuery(psSQL, pbIsTransacted)

            Dim poConnection As SqlConnection = oConnectionCommandTuple.Item1
            Dim poCommand As SqlCommand = oConnectionCommandTuple.Item2

            poCommand.Parameters.AddRange(poSQLParams)

            Return executeNonQueryHelper(psSQL, poConnection, poCommand)
        End Function

        Private Function getSQLConnectionAndCommandForExecuteNonQuery(ByVal psSQL As String, ByVal pbIsTransacted As Boolean) As Tuple(Of SqlConnection, SqlCommand)
            Dim poConnection As SqlConnection = Nothing
            Dim poCommand As SqlCommand = Nothing
            If pbIsTransacted Then
                poConnection = DirectCast(openConnectionTransacted(), SqlConnection)
                poCommand = New SqlCommand(psSQL, poConnection)

                poCommand.Transaction = _sharedTransaction
            Else
                If True = isTransactionActive() Then
                    Dim oException As New CSQLException("Executing non-transacted non-query SQL command through a CSQLDBUtils object in transacted mode. This may cause data consistency errors and deadlocks.", psSQL)
                    If True = IsTransactionViolationFatal Then
                        Throw oException
                    Else
                        Log.Info(oException.ToString, oException)
                    End If
                End If

                poConnection = DirectCast(openConnection(), SqlConnection)

                poCommand = New SqlCommand(psSQL, poConnection)
            End If
            Return New Tuple(Of SqlConnection, SqlCommand)(poConnection, poCommand)
        End Function

        Private Function executeNonQueryHelper(ByVal psSQL As String, ByVal poSQLConnection As SqlConnection, ByVal poSQLCommand As SqlCommand) As Integer
            startTimer()
            Try
                _LastSQL = psSQL
                poSQLCommand.CommandTimeout = CommandTimeout
                Return poSQLCommand.ExecuteNonQuery()
            Catch sqlEx As SqlException
                Trace.WriteLine("SQL Syntax Error:" & psSQL & vbCrLf & sqlEx.ToString(), "ERR")
                Dim sErrorMsg As String = psSQL

                If HasDataTruncationSQLException(sqlEx) Then
                    sErrorMsg = sErrorMsg & vbCrLf & getDataTruncationSummary(poSQLCommand.CommandText, poSQLConnection)
                End If

                Throw New CSQLException(sqlEx, sErrorMsg & getDeadlockSummary(sqlEx))
            Finally
                endTimer("ExecNonQuery", psSQL)
            End Try
        End Function

        ''' <summary>
        ''' Async version of executeNonQuery. The benefits of async function is that system will release the thread while waiting for external dependency like database.
        ''' The async - wait pattern can help to reduce the thread usuage.
        ''' </summary>
        Public Async Function executeNonQueryAsync(ByVal sql As String, Optional ByVal isTransacted As Boolean = False) As Task(Of Integer)
            sql = UnescapeSQLBraces(sql)

            Dim cmd As SqlCommand

            Dim con As SqlConnection = Nothing
            If isTransacted Then
                con = DirectCast(Await openConnectionTransactedAsync(), SqlConnection)
                cmd = New SqlCommand(sql, con)

                cmd.Transaction = _sharedTransaction
            Else
                If True = isTransactionActive() Then
                    Dim oException As New CSQLException("Executing non-transacted non-query SQL command through a CSQLDBUtils object in transacted mode. This may cause data consistency errors and deadlocks.", sql)
                    If True = IsTransactionViolationFatal Then
                        Throw oException
                    Else
                        Log.Info(oException.ToString, oException)
                    End If
                End If

                con = DirectCast(Await openConnectionAsync(), SqlConnection)

                cmd = New SqlCommand(sql, con)
            End If

            'Trace.WriteLine("ExecNonQuery:" & sql, "SQL")
            startTimer()
            Try
                _LastSQL = sql
                cmd.CommandTimeout = CommandTimeout
                Return Await cmd.ExecuteNonQueryAsync()
            Catch sqlEx As SqlClient.SqlException
                Trace.WriteLine("SQL Syntax Error:" & sql & vbCrLf & sqlEx.ToString(), "ERR")
                Dim sErrorMsg As String = sql

                If HasDataTruncationSQLException(sqlEx) Then
                    sErrorMsg = sErrorMsg & vbCrLf & getDataTruncationSummary(cmd.CommandText, con)
                End If

                Throw New CSQLException(sqlEx, sErrorMsg & getDeadlockSummary(sqlEx))
            Finally
                endTimer("ExecNonQuery", sql)
            End Try

        End Function

#Region "Data Truncation Summary"
        Protected Function HasDataTruncationSQLException(ByVal pSQLEx As SqlException) As Boolean
            Return pSQLEx.ToString.ToUpper.IndexOf("STRING OR BINARY DATA WOULD BE TRUNCATED") <> -1
        End Function

        ''' <summary>
        ''' Parses and determines which columns caused a data truncation exception
        ''' </summary>
        ''' <param name="psSQLQuery">SQL Query</param>
        ''' <param name="pConn">Current SQL Connection</param>
        ''' <returns>Summary of culprit column names with its respective max lengths</returns>
        ''' <remarks>
        ''' Only handles common UPDATE and INSERT query formats. This should cover most usage
        ''' scenarios. For more esoteric queries, we will still have to eye ball it.
        ''' 
        ''' Formats handled:
        ''' 
        ''' UPDATE [TABLE NAME] SET
        ''' [Column1 Name]=[Column1 Value], ...., [ColumnN Name]=[ColumnN Value] WHERE...
        ''' 
        ''' INSERT INTO [TABLE NAME] ([Column1 Name], ... ,[ColumnN Name])
        ''' VALUES ([Column1 Value],....,[ColumnN Value])
        ''' </remarks>
        Protected Function getDataTruncationSummary(ByVal psSQLQuery As String, ByVal pConn As SqlConnection) As String

            Dim sSQLParts As String() = SafeString(psSQLQuery).Split(" "c)
            Dim sSQLOperation As String = SafeString(sSQLParts(0)).ToUpper

            If sSQLOperation <> "UPDATE" AndAlso sSQLOperation <> "INSERT" Then
                Return ""
            End If

            Dim sUpdateRegexPattern As String = "^UPDATE (\w+) SET (.*) WHERE"
            Dim sInsertRegExPattern As String = "^INSERT INTO (\w+) \((.*?)\) VALUES \((.*?)\)$"
            Dim sTableName As String = ""
            Dim oMatches As MatchCollection = Nothing
            '[source column name->value]
            Dim SourceSQLColumnsDict As New Generic.Dictionary(Of String, String)

            Select Case sSQLOperation
                Case "UPDATE"
                    oMatches = Regex.Matches(psSQLQuery, sUpdateRegexPattern, RegexOptions.IgnoreCase)

                    'validate we have 3 groups found (of which 1 is the entire text)
                    If oMatches.Count > 0 AndAlso oMatches(0).Groups.Count = 3 Then
                        sTableName = SafeString(oMatches(0).Groups(1).Value)

                        Dim ColumnNamesAndValues As String() = SafeString(oMatches(0).Groups(2).Value).Split(","c)

                        Dim arrSplit As String() = Nothing
                        For Each sCol As String In ColumnNamesAndValues
                            arrSplit = sCol.Split("="c)
                            If arrSplit.Length = 2 Then
                                If Not SourceSQLColumnsDict.ContainsKey(SafeString(arrSplit(0))) Then
                                    SourceSQLColumnsDict.Add(SafeString(arrSplit(0)).ToUpper, stripQuotations(arrSplit(1)))
                                End If
                            End If
                        Next

                    End If

                Case "INSERT"
                    oMatches = Regex.Matches(psSQLQuery, sInsertRegExPattern, RegexOptions.IgnoreCase)

                    'validate we have 4 groups found (of which 1 is the entire text)
                    If oMatches.Count > 0 AndAlso oMatches(0).Groups.Count = 4 Then
                        sTableName = SafeString(oMatches(0).Groups(1).Value)

                        Dim ColumnNames As String() = SafeString(oMatches(0).Groups(2).Value).Split(","c)
                        Dim ColumnValues As String() = SafeString(oMatches(0).Groups(3).Value).Split(","c)
                        If ColumnNames.Length = ColumnValues.Length Then
                            For nCount As Integer = 0 To ColumnNames.Length - 1
                                SourceSQLColumnsDict.Add(SafeString(ColumnNames(nCount)).ToUpper, stripQuotations(ColumnValues(nCount)))
                            Next
                        End If
                    End If
            End Select

            If sTableName = "" OrElse SourceSQLColumnsDict.Count = 0 Then
                Return ""
            End If

            Dim ColumnMaxLengthDict As Generic.Dictionary(Of String, Integer) = getColumnsWithMaxLength(pConn, sTableName)

            If ColumnMaxLengthDict.Count = 0 Then
                Return ""
            End If

            Dim sErrFormat As String = "Column: {0} ({1}), Value: {2} ({3})" & vbCrLf
            Dim sb As New System.Text.StringBuilder

            For Each sSourceColumnName As String In SourceSQLColumnsDict.Keys
                If ColumnMaxLengthDict.ContainsKey(sSourceColumnName) Then
                    If SourceSQLColumnsDict(sSourceColumnName).Length > ColumnMaxLengthDict(sSourceColumnName) Then
                        sb.Append(String.Format(sErrFormat,
                         sSourceColumnName,
                         ColumnMaxLengthDict(sSourceColumnName),
                         SourceSQLColumnsDict(sSourceColumnName),
                        SourceSQLColumnsDict(sSourceColumnName).Length))
                    End If
                End If
            Next

            Return vbCrLf & "Data Truncation Summary" & vbCrLf & sb.ToString
        End Function

        ''' <summary>
        ''' Queries DB schema to get max length for corresponding columns
        ''' </summary>
        ''' <param name="poConn"></param>
        ''' <param name="psTableName"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Function getColumnsWithMaxLength(ByVal poConn As SqlConnection, ByVal psTableName As String) As Generic.Dictionary(Of String, Integer)
            'we could cache it here

            'populate schema for target table, [column name->max length]
            Dim ColumnMaxLengthDict As New Generic.Dictionary(Of String, Integer)

            Try
                Dim oAdapter As New SqlDataAdapter("SELECT * FROM " & psTableName, poConn)
                If isTransactionActive() Then
                    'http://msdn.microsoft.com/en-us/library/y53he2tz.aspx
                    'to handle transactions, the poConn.GetSchema() will not work since it will throw an exception
                    oAdapter.SelectCommand.Transaction = _sharedTransaction
                End If

                Dim schemaDt As DataTable = New DataTable
                oAdapter.FillSchema(schemaDt, SchemaType.Source)

                Dim nMaxLength As Integer = 0
                Dim sColName As String = ""
                For Each column As DataColumn In schemaDt.Columns
                    sColName = SafeString(column.ColumnName).ToUpper
                    nMaxLength = column.MaxLength
                    If sColName <> "" AndAlso nMaxLength > -1 Then
                        If Not ColumnMaxLengthDict.ContainsKey(sColName) Then
                            ColumnMaxLengthDict.Add(sColName, nMaxLength)
                        End If
                    End If
                Next

            Catch ex As Exception
                CAssert.FailGracefully(String.Format("Error occurred while comparing current row values against schema max length with message: {0}. ", ex.Message))
            End Try


            Return ColumnMaxLengthDict
        End Function

        ''' <summary>
        ''' Remove beginning and ending quotation marks
        ''' </summary>
        ''' <param name="psSource"></param>
        ''' <returns></returns>
        ''' <remarks>
        ''' This helper function is used to strip off a column value to properly calculate the length of a column value in a SQL string
        ''' </remarks>
        Protected Function stripQuotations(ByVal psSource As String) As String
            Dim sCleanedString As String = SafeString(psSource)
            sCleanedString = Regex.Replace(sCleanedString, "^['""]", String.Empty)
            sCleanedString = Regex.Replace(sCleanedString, "['""]$", String.Empty)

            Return sCleanedString
        End Function

#End Region
        ''' <summary>
        ''' Async version of getDataReader. The benefits of async function is that system will release the thread while waiting for external dependency like database.
        ''' The async - wait pattern can help to reduce the thread usuage.
        ''' </summary>
        Public Async Function getDataReaderAsync(ByVal sql As String) As Task(Of System.Data.IDataReader) Implements IDBUtils.getDataReaderAsync
            sql = UnescapeSQLBraces(sql)
            checkDataReaderStillOpen()

            Dim cmd As SqlCommand
            If isTransactionActive() Then
                cmd = New SqlCommand(sql, DirectCast(Await openConnectionTransactedAsync(), SqlConnection))
                cmd.Transaction = _sharedTransaction
            Else
                cmd = New SqlCommand(sql, DirectCast(Await openConnectionAsync(), SqlConnection))
            End If

            'Trace.WriteLine("GetDataReader:" & sql, "SQL")
            startTimer()
            Try
                _LastSQL = sql
                cmd.CommandTimeout = CommandTimeout
                _lastDataReader = Await cmd.ExecuteReaderAsync
                _LastDataReaderSQL = sql
            Catch sqlEx As SqlClient.SqlException
                Trace.WriteLine("SQL Syntax Error:" & sql & vbCrLf & sqlEx.ToString(), "ERR")
                Throw New CSQLException(sqlEx, sql & getDeadlockSummary(sqlEx))
            Finally
                endTimer("GetDataReader", sql)
            End Try

            Return _lastDataReader
        End Function

        ''' <summary>
        ''' Convenience function: Equivalent to calling getDataReaderAsync(CSQLParamString.Format(sqlFormat, poValues))
        ''' </summary>
        Public Async Function getDataReaderAsync(ByVal sqlFormat As String, ByVal ParamArray poValues() As CSQLParamValue) As Task(Of System.Data.IDataReader)
            Return Await getDataReaderAsync(CSQLParamString.Format(sqlFormat, poValues))
        End Function

        'last datareader sql that was executed, saved for debuggin purposes
        Private _LastDataReaderSQL As String
        Private _lastDataReader As IDataReader
        Public Function getDataReader(ByVal sql As String) As System.Data.IDataReader Implements IDBUtils.getDataReader
            sql = UnescapeSQLBraces(sql)
            checkDataReaderStillOpen()

            Dim cmd As SqlCommand
            If isTransactionActive() Then
                cmd = New SqlCommand(sql, DirectCast(openConnectionTransacted(), SqlConnection))
                cmd.Transaction = _sharedTransaction
            Else
                cmd = New SqlCommand(sql, DirectCast(openConnection(), SqlConnection))
            End If

            'Trace.WriteLine("GetDataReader:" & sql, "SQL")
            startTimer()
            Try
                _LastSQL = sql
                cmd.CommandTimeout = CommandTimeout
                _lastDataReader = cmd.ExecuteReader
                _LastDataReaderSQL = sql
            Catch sqlEx As SqlClient.SqlException
                Trace.WriteLine("SQL Syntax Error:" & sql & vbCrLf & sqlEx.ToString(), "ERR")
                Throw New CSQLException(sqlEx, sql & getDeadlockSummary(sqlEx))
            Finally
                endTimer("GetDataReader", sql)
            End Try

            Return _lastDataReader
        End Function

        ''' <summary>
        ''' Convenience function: Equivalent to calling getDataReader(CSQLParamString.Format(sqlFormat, poValues)) 
        ''' </summary>
        Public Function getDataReader(ByVal sqlFormat As String, ByVal ParamArray poValues() As CSQLParamValue) As System.Data.IDataReader
            Return getDataReader(CSQLParamString.Format(sqlFormat, poValues))
        End Function

        Public Function getDataTable(ByVal sql As String) As DataTable Implements IDBUtils.getDataTable
            sql = UnescapeSQLBraces(sql)
            checkDataReaderStillOpen()
            Dim ds As New DataSet()
            Dim da As SqlDataAdapter = GetDataTableGetSQLDataAdapter(sql)

            Return getDataTableHelper(ds, sql, da)
        End Function

        Public Function getDataTable(ByVal sql As String, ByVal ParamArray poSQLParams As SqlParameter()) As DataTable Implements IDBUtils.getDataTable
            sql = UnescapeSQLBraces(sql)
            checkDataReaderStillOpen()
            Dim ds As New DataSet()
            Dim da As SqlDataAdapter = GetDataTableGetSQLDataAdapter(sql)
            da.SelectCommand.Parameters.AddRange(poSQLParams)

            Return getDataTableHelper(ds, sql, da)
        End Function

        Private Function GetDataTableGetSQLDataAdapter(ByVal psSQL As String) As SqlDataAdapter
            Dim da As SqlDataAdapter
            If isTransactionActive() Then
                da = New SqlDataAdapter(psSQL, DirectCast(openConnectionTransacted(), SqlConnection))
                da.SelectCommand.Transaction = _sharedTransaction
            Else
                da = New SqlDataAdapter(psSQL, DirectCast(openConnection(), SqlConnection))
            End If
            Return da
        End Function

        Private Function getDataTableHelper(ByVal poDS As DataSet, ByVal psSQL As String, ByVal poDA As SqlDataAdapter) As DataTable
            'Trace.WriteLine("GetDataTable:" & sql, "SQL")
            startTimer()

            Try
                _LastSQL = psSQL
                poDA.SelectCommand.CommandTimeout = CommandTimeout
                poDA.Fill(poDS)
            Catch sqlEx As SqlClient.SqlException
                Trace.WriteLine("SQL Syntax Error:" & psSQL & vbCrLf & sqlEx.ToString(), "ERR")
                Throw New CSQLException(sqlEx, psSQL & getDeadlockSummary(sqlEx))
            Finally
                endTimer("GetDataTable", psSQL)
            End Try

            Return poDS.Tables(0)
        End Function
        ''' <summary>
        ''' Convenience function: Equivalent to calling getDataTable(CSQLParamString.Format(sqlFormat, poValues)) 
        ''' </summary>
        Public Function getDataTable(ByVal sqlFormat As String, ByVal ParamArray poValues() As CSQLParamValue) As System.Data.DataTable
            Return getDataTable(CSQLParamString.Format(sqlFormat, poValues))
        End Function

        ''' <summary>
        ''' .Net doesn't support fill datatable async.
        ''' GetDataTableAsync will use DataReaderAsync to read the database then fill the datatable
        ''' Please be carefully with this function since it may have the same side effect as datareader
        ''' </summary>
        ''' <param name="psSql"></param>
        ''' <returns></returns>
        Public Async Function getDataTableAsync(ByVal psSql As String) As Task(Of DataTable) Implements IDBUtils.getDataTableAsync
            Dim dt As DataTable = New DataTable
            Using oReader As IDataReader = Await getDataReaderAsync(psSql)
                dt.Load(oReader)
            End Using

            Return dt
        End Function

        ''' <summary>
        ''' Convenience function: Equivalent to calling getDataTable(CSQLParamString.Format(sqlFormat, poValues))
        ''' </summary>
        Public Async Function getDataTableAsync(ByVal sqlFormat As String, ByVal ParamArray poValues() As CSQLParamValue) As Task(Of System.Data.DataTable)
            Return Await getDataTableAsync(CSQLParamString.Format(sqlFormat, poValues))
        End Function

        Public Function getScalarValue(ByVal sql As String, ByVal sqlParams As CSQLParamBuilder) As String Implements IDBUtils.getScalarValue
            Return getScalarValue(sql, sqlParams.GetParams())
        End Function

        Public Function getScalarValue(ByVal sql As String, ByVal ParamArray sqlParams As SqlParameter()) As String Implements IDBUtils.getScalarValue
            checkDataReaderStillOpen()
            Dim cmd As SqlCommand

            If isTransactionActive() Then
                cmd = New SqlCommand(sql, DirectCast(openConnectionTransacted(), SqlConnection))
                cmd.Transaction = _sharedTransaction
            Else
                cmd = New SqlCommand(sql, DirectCast(openConnection(), SqlConnection))
            End If

            cmd.Parameters.AddRange(sqlParams)
            startTimer()

            Try
                _LastSQL = sql
                cmd.CommandTimeout = CommandTimeout

                Return SafeString(cmd.ExecuteScalar)
            Catch sqlEx As SqlClient.SqlException
                Trace.WriteLine("SQL Syntax Error:" & sql & vbCrLf & sqlEx.ToString(), "ERR")
                Throw New CSQLException(sqlEx, sql & getDeadlockSummary(sqlEx))
            Finally
                endTimer("GetScalarValue", sql)
            End Try
        End Function

        Public Function getScalarValue(ByVal sql As String, ByVal sqlParams As IEnumerable(Of SqlParameter)) As String
            Return getScalarValue(sql, sqlParams.ToArray())
        End Function

        <Obsolete("This method is deprecated due to security concerns. Please use getScalarValue instead.")>
        Public Function getScalerValue(ByVal sql As String) As String Implements IDBUtils.getScalerValue
            sql = UnescapeSQLBraces(sql)
            checkDataReaderStillOpen()
            Dim cmd As SqlCommand
            If isTransactionActive() Then
                cmd = New SqlCommand(sql, DirectCast(openConnectionTransacted(), SqlConnection))
                cmd.Transaction = _sharedTransaction
            Else
                cmd = New SqlCommand(sql, DirectCast(openConnection(), SqlConnection))
            End If

            'Trace.WriteLine("GetScalerValue:" & sql, "SQL")
            startTimer()

            Try
                _LastSQL = sql
                cmd.CommandTimeout = CommandTimeout
                'Dim execResult As Object = cmd.ExecuteScalar

                Return SafeString(cmd.ExecuteScalar)
            Catch sqlEx As SqlClient.SqlException
                Trace.WriteLine("SQL Syntax Error:" & sql & vbCrLf & sqlEx.ToString(), "ERR")
                Throw New CSQLException(sqlEx, sql & getDeadlockSummary(sqlEx))
            Finally
                endTimer("GetScalerValue", sql)
            End Try
        End Function

        ''' <summary>
        ''' Convenience function: Equivalent to calling getScalerValue(CSQLParamString.Format(sqlFormat, poValues)) 
        ''' </summary>
        <Obsolete("This method is deprecated due to security concerns. Please use getScalarValue instead.")>
        Public Function getScalerValue(ByVal sqlFormat As String, ByVal ParamArray poValues() As CSQLParamValue) As String
            Return getScalerValue(CSQLParamString.Format(sqlFormat, poValues))
        End Function

        Public Async Function getScalarValueAsync(ByVal sql As String, ByVal sqlParams As CSQLParamBuilder) As Task(Of String) Implements IDBUtils.getScalarValueAsync
            Return Await getScalarValueAsync(sql, sqlParams.GetParams())
        End Function

        ''' <summary>
        ''' Async version of getScalarValue. The benefits of async function is that system will release the thread while waiting for external dependency like database.
        ''' The async - wait pattern can help to reduce the thread usuage.
        ''' </summary>
        Public Async Function getScalarValueAsync(ByVal sql As String, ByVal ParamArray sqlParams As SqlParameter()) As Task(Of String) Implements IDBUtils.getScalarValueAsync
            checkDataReaderStillOpen()
            Dim cmd As SqlCommand

            If isTransactionActive() Then
                cmd = New SqlCommand(sql, DirectCast(Await openConnectionTransactedAsync(), SqlConnection))
                cmd.Transaction = _sharedTransaction
            Else
                cmd = New SqlCommand(sql, DirectCast(Await openConnectionAsync(), SqlConnection))
            End If

            cmd.Parameters.AddRange(sqlParams)
            startTimer()

            Try
                _LastSQL = sql
                cmd.CommandTimeout = CommandTimeout

                Return SafeString(Await cmd.ExecuteScalarAsync)
            Catch sqlEx As SqlClient.SqlException
                Trace.WriteLine("SQL Syntax Error:" & sql & vbCrLf & sqlEx.ToString(), "ERR")
                Throw New CSQLException(sqlEx, sql & getDeadlockSummary(sqlEx))
            Finally
                endTimer("GetScalarValue", sql)
            End Try
        End Function

        Public Async Function getScalarValueAsync(ByVal sql As String, ByVal sqlParams As IEnumerable(Of SqlParameter)) As Task(Of String)
            Return Await getScalarValueAsync(sql, sqlParams.ToArray())
        End Function

        <Obsolete("This method is deprecated due to security concerns. Please use getScalarValue instead.")>
        Public Async Function getScalarValueAsync(ByVal sql As String) As Task(Of String) Implements IDBUtils.getScalarValueAsync
            sql = UnescapeSQLBraces(sql)
            checkDataReaderStillOpen()
            Dim cmd As SqlCommand
            If isTransactionActive() Then
                cmd = New SqlCommand(sql, DirectCast(Await openConnectionTransactedAsync(), SqlConnection))
                cmd.Transaction = _sharedTransaction
            Else
                cmd = New SqlCommand(sql, DirectCast(Await openConnectionAsync(), SqlConnection))
            End If

            'Trace.WriteLine("GetScalerValue:" & sql, "SQL")
            startTimer()

            Try
                _LastSQL = sql
                cmd.CommandTimeout = CommandTimeout
                'Dim execResult As Object = cmd.ExecuteScalar

                Return SafeString(Await cmd.ExecuteScalarAsync)
            Catch sqlEx As SqlClient.SqlException
                Trace.WriteLine("SQL Syntax Error:" & sql & vbCrLf & sqlEx.ToString(), "ERR")
                Throw New CSQLException(sqlEx, sql & getDeadlockSummary(sqlEx))
            Finally
                endTimer("GetScalerValue", sql)
            End Try
        End Function

        Public Sub bulkCopy(ByVal pDT As DataTable, ByVal pDestinationTableName As String, ByVal isTransacted As Boolean, pBatchSize As Integer, pBatchCopyTimeoutSeconds As Integer, Optional ByVal pSqlBulkCopyOptions As SqlBulkCopyOptions = SqlBulkCopyOptions.Default)
            Dim bulkCopy As SqlClient.SqlBulkCopy = Nothing
            If isTransacted Then
                bulkCopy = New SqlClient.SqlBulkCopy(DirectCast(openConnectionTransacted(), SqlConnection), pSqlBulkCopyOptions, _sharedTransaction)
            Else
                If True = isTransactionActive() Then
                    Dim oException As New CSQLException("Executing non-transacted bulk copy through a CSQLDBUtils object in transacted mode. This may cause data consistency errors and deadlocks.")
                    If True = IsTransactionViolationFatal Then
                        Throw oException
                    Else
                        Log.Info(oException.ToString, oException)
                    End If
                End If
                bulkCopy = New SqlClient.SqlBulkCopy(DirectCast(openConnection(), SqlConnection))
            End If

            _LastSQL = "bulk copy to " & pDestinationTableName

            Using bulkCopy
                bulkCopy.BatchSize = pBatchSize
                bulkCopy.BulkCopyTimeout = pBatchCopyTimeoutSeconds
                bulkCopy.DestinationTableName = pDestinationTableName
                bulkCopy.WriteToServer(pDT)
            End Using
        End Sub



#Region "String Manip Routines"
        ''' <summary>
        '''READ UNCOMMITTED - Implements dirty read, or isolation level 0 locking, which means that no 
        ''' shared locks are issued and no exclusive locks are honored. When this option is set, it 
        ''' is possible to read uncommitted or dirty data; values in the data can be changed and rows 
        ''' can appear or disappear in the data set before the end of the transaction. This option 
        ''' has the same effect as setting NOLOCK on all tables in all SELECT statements in a 
        ''' transaction. This is the least restrictive of the four isolation levels.
        ''' </summary>
        ''' <remarks>
        ''' Please note that the results from here can be LESS than expected.  Please see this article for actual example scenarios and how to do a quick test:
        ''' http://sqlblogcasts.com/blogs/tonyrogerson/archive/2006/11/10/1280.aspx
        ''' A future change will be to use snapshot isolation levels: 
        ''' http://www.informit.com/articles/article.aspx?p=327394&amp;seqNum=2
        ''' http://msdn.microsoft.com/en-us/library/ms175492.aspx
        ''' </remarks>
        Public Shared Function SQLReadUncommitted(ByVal psSQL As String) As String
            Dim sResult As String = "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " & psSQL & " ; SET TRANSACTION ISOLATION LEVEL READ COMMITTED;"
            Return sResult
        End Function
        Public Overloads Shared Function Safestring(ByVal oNull As DBNull) As String
            Return ""
        End Function

        Public Overloads Shared Function SafeString(ByVal s As Object) As String
            ' special case to handle Recordset Field objects
            If s Is Nothing OrElse TypeOf s Is DBNull Then
                Return ""
            ElseIf TypeOf s Is Guid Then
                'we really don't want to add too many else clauses here, but this GUID check seems like it will keep our code base
                'more maintainable since it will help our code avoid having to know the format of the GUID.  We read from db as-is, we write as-is.
                Return s.ToString()
            Else
                Return Trim(CStr(s)) 'very odd: if u return trim(s.toString()), then for dates, it will append 12:00:00 AM if there is no time :-T
            End If
        End Function

        Public Overloads Shared Function SQLString(ByVal s As Object) As String
            Return "'" & SafeString(s).Replace("'", "''") & "'"
        End Function

        'only used when fine control is needed  over how a string should be escaped w/ or w/o trimming
        'avoid using if possible
        Public Overloads Shared Function SQLString(ByVal s As String, ByVal pbTrim As Boolean) As String
            If s = "" Then
                Return "''"
            End If
            If pbTrim Then
                Return "'" & s.Trim().Replace("'", "''") & "'"
            Else
                Return "'" & s.Replace("'", "''") & "'"
            End If
        End Function
#End Region

        Private Sub checkDataReaderStillOpen()
            If False = IsNothing(_lastDataReader) Then
                If _lastDataReader.IsClosed = False Then
                    Throw New InvalidOperationException("There is already a data reader open w/ the sql:" & _LastDataReaderSQL)
                End If
            End If
        End Sub

        Private Shared _cachedSelects As New Hashtable()
        Private Function buildSelect(ByVal poDataTable As DataTable) As String
            Dim sqlFields As New System.Text.StringBuilder()
            sqlFields.Append("SELECT ")
            Dim i As Integer
            For i = 0 To poDataTable.Columns.Count - 2
                Dim oCol As DataColumn = poDataTable.Columns(i)
                sqlFields.Append("[" & oCol.ColumnName & "], ")
            Next
            sqlFields.Append("[" & poDataTable.Columns(poDataTable.Columns.Count - 1).ColumnName & "]") 'handle last column
            sqlFields.Append(" FROM " & poDataTable.TableName & " ")

            Return sqlFields.ToString
        End Function

        Public Overloads Sub FillDataTable(ByVal dt As DataTable, ByVal poWhereClauseWithWhere As CSQLWhereStringBuilder,
                                           Optional ByVal psOrderClause As String = "") Implements ISQLDBUtils.FillDataTable
            If dt.Columns.Count <= 0 Then
                Throw New ArgumentException("Datatable passed in must already have columns defined!  Otherwise, please use other FillDataTable overload.")
            End If

            Dim sqlFields As New System.Text.StringBuilder()

            If Me.EnableCacheMetaData Then
                If False = _cachedSelects.ContainsKey(dt.TableName) Then
                    SyncLock (_cachedSelects.SyncRoot)
                        _cachedSelects(dt.TableName) = buildSelect(dt)
                    End SyncLock
                End If

                sqlFields.Append(CStr(_cachedSelects(dt.TableName)))
            Else
                sqlFields.Append(buildSelect(dt))
            End If


            If False = IsNothing(poWhereClauseWithWhere) Then
                sqlFields.Append(poWhereClauseWithWhere.SQL)
            End If
            sqlFields.Append(" " & psOrderClause)

            Trace.WriteLine("Dynamic SQL for Fill: " & sqlFields.ToString)

            Try
                FillDataTable(dt, sqlFields.ToString)
            Catch sqlEx As SqlException
                Throw New CSQLException(sqlEx, sqlFields.ToString & getDeadlockSummary(sqlEx))
            End Try
        End Sub

        Public Overloads Sub FillDataTable(ByVal dt As DataTable, ByVal sqlSelect As String) Implements IDBUtils.FillDataTable
            Dim oConn As SqlConnection = FillDataTableGetSQLConnection()

            Dim oAdapter As New SqlDataAdapter(sqlSelect, oConn)
            oAdapter.SelectCommand.Transaction = _sharedTransaction
            oAdapter.SelectCommand.CommandTimeout = Me.CommandTimeout

            FillDataTableHelper(dt, sqlSelect, oAdapter)
        End Sub

        Public Overloads Sub FillDataTable(ByVal poDT As DataTable, ByVal psSQL As String, ByVal ParamArray poSQLParams As SqlParameter()) Implements IDBUtils.FillDataTable
            Dim oConn As SqlConnection = FillDataTableGetSQLConnection()

            Dim oAdapter As New SqlDataAdapter(psSQL, oConn)
            oAdapter.SelectCommand.Parameters.AddRange(poSQLParams)
            oAdapter.SelectCommand.Transaction = _sharedTransaction
            oAdapter.SelectCommand.CommandTimeout = Me.CommandTimeout

            FillDataTableHelper(poDT, psSQL, oAdapter)
        End Sub

        Private Function FillDataTableGetSQLConnection() As SqlConnection
            Dim oConn As SqlConnection
            If isTransactionActive() Then
                oConn = DirectCast(openConnectionTransacted(), SqlConnection)
            Else
                oConn = DirectCast(openConnection(), SqlConnection)
            End If

            Return oConn
        End Function

        Private Sub FillDataTableHelper(ByVal poDT As DataTable, ByVal psSQL As String, ByVal poSQLDataAdapter As SqlDataAdapter)
            Try
                _LastSQL = "FillDataTable: " & psSQL
                startTimer()
                poSQLDataAdapter.Fill(poDT)
            Catch sqlEx As SqlException
                Throw New CSQLException(sqlEx, psSQL & getDeadlockSummary(sqlEx))
            Finally
                endTimer("FillDataTable", psSQL)
            End Try
        End Sub

        Private _CurrentUpdatingRow As DataRow

        Private Sub RowUpdatingHandler(sender As Object, pArgs As SqlRowUpdatingEventArgs)
            _CurrentUpdatingRow = pArgs.Row
        End Sub

        Private Function IsNumeric(pType As Type) As Boolean
            Select Case Type.GetTypeCode(pType)
                Case TypeCode.Byte,
                TypeCode.Decimal,
                TypeCode.Double,
                TypeCode.Int16,
                TypeCode.Int32,
                TypeCode.Int64,
                TypeCode.SByte,
                TypeCode.Single,
                TypeCode.UInt16,
                TypeCode.UInt32,
                TypeCode.UInt64
                    Return True
            End Select
            Return False
        End Function

        Private Function GetRowSortedByValuesToString(pRow As DataRow) As String
            Dim oNumerics = New List(Of DataColumn)
            Dim oDates = New List(Of DataColumn)
            Dim oOthers = New List(Of DataColumn)

            ' Group by DataType
            For Each oColumn As DataColumn In pRow.Table.Columns
                ' even if DataType is double or date time, if null just place in oOthers 
                ' and don't bother sorting column
                If (pRow.IsNull(oColumn)) Then
                    oOthers.Add(oColumn)
                ElseIf (IsDate(pRow(oColumn))) Then
                    oDates.Add(oColumn)
                ElseIf (IsNumeric(oColumn.DataType)) Then
                    ' convert all numerics to double
                    oNumerics.Add(oColumn)
                Else
                    oOthers.Add(oColumn)
                End If
            Next

            'Sort desc
            oNumerics.Sort(
                Function(a As DataColumn, b As DataColumn)
                    Return CType(pRow(b), Double).CompareTo(CType(pRow(a), Double))
                End Function)
            oDates.Sort(
                Function(a As DataColumn, b As DataColumn)
                    Return CType(pRow(b), DateTime).CompareTo(CType(pRow(a), DateTime))
                End Function)

            ' generate output message
            Dim sResult As New System.Text.StringBuilder()

            For Each oColumn As DataColumn In oNumerics
                sResult.AppendLine(String.Format("{0}: {1},", oColumn.ColumnName, SafeString(pRow(oColumn))))
            Next
            For Each oColumn As DataColumn In oDates
                sResult.AppendLine(String.Format("{0}: {1},", oColumn.ColumnName, New CSQLParamValue(SafeString(pRow(oColumn))).SQLValue))
            Next
            For Each oColumn As DataColumn In oOthers
                If (pRow.IsNull(oColumn)) Then
                    sResult.AppendLine(oColumn.ColumnName & ": NULL,")
                ElseIf oColumn.DataType Is GetType(Byte()) Then
                    Dim sValue = VarBinaryLiteral(CType(pRow(oColumn), Byte()))
                    If (sValue.Length > 20) Then
                        sResult.AppendLine(String.Format("{0}: {1} ... ({2}),", oColumn.ColumnName, sValue.Substring(0, 20), sValue.Length))
                    Else
                        sResult.AppendLine(String.Format("{0}: {1},", oColumn.ColumnName, SafeString(pRow(oColumn))))
                    End If
                Else
                    Dim sValue As String = SafeString(pRow(oColumn))
                    If (sValue.Length > 20) Then
                        sResult.AppendLine(String.Format("{0}: {1} ... ({2}),", oColumn.ColumnName, sValue.Substring(0, 20), sValue.Length))
                    Else
                        sResult.AppendLine(String.Format("{0}: {1},", oColumn.ColumnName, SafeString(pRow(oColumn))))
                    End If
                End If
            Next

            If sResult.Length > 0 Then
                sResult.Append(" [Where SafeGuardClauseHere]") 'append this so we don't accidently screw things up when doing Update XXX Set .... 
            End If
            Return sResult.ToString
        End Function

        'note: the command builder generates code that will account for db concurrency violations.  if no row is updated, that means the row has been
        'modified and a dbexception will be thrown
        'refer to this for more info on concurrency: http://msdn.microsoft.com/msdnmag/issues/03/04/DataConcurrency/default.aspx
        Public Overloads Function UpdateDataTable(ByVal dt As DataTable, ByVal sqlSelectMetaData As String, ByVal isTransacted As Boolean) As Integer Implements IDBUtils.UpdateDataTable
            Dim oConn As SqlConnection
            If isTransacted Then
                oConn = DirectCast(openConnectionTransacted(), SqlConnection)
            Else
                If True = isTransactionActive() Then
                    Dim oException As New CSQLException("Executing non-transacted non-query SQL command through a CSQLDBUtils object in transacted mode. This may cause data consistency errors and deadlocks.", "Select Metadata: " & sqlSelectMetaData)
                    If True = IsTransactionViolationFatal Then
                        Throw oException
                    Else
                        Log.Info(oException.ToString, oException)
                    End If
                End If
                oConn = DirectCast(openConnection(), SqlConnection)
            End If

            Dim oAdapter As New SqlDataAdapter(sqlSelectMetaData, oConn)
            AddHandler oAdapter.RowUpdating, AddressOf Me.RowUpdatingHandler

            Dim cm As New SqlCommandBuilder(oAdapter)
            cm.QuotePrefix = "["
            cm.QuoteSuffix = "]"
            If isTransactionActive() Then
                'this must be done prior to update so that the command builder will auto enlist the other command objects into the transaction!
                '    'REF: http://groups.google.com/groups?hl=en&lr=&ie=UTF-8&oe=UTF-8&threadm=olb8du423jcslrjp78m1p09lufhktncagd%404ax.com&rnum=5&prev=/groups%3Fhl%3Den%26lr%3D%26ie%3DUTF-8%26oe%3DUTF-8%26q%3Dcommandbuilder%2Btransaction%2B%2522Execute%2Brequires%2Bthe%2Bcommand%2Bto%2Bhave%2Ba%2Btransaction%2Bobject%2Bwhen%2Bthe%2Bconnection%2Bassigned%2Bto%2Bthe%2Bcommand%2Bis%2Bin%2Ba%2Bpending%2Blocal%2Btransaction.%2B%2BThe%2BTransaction%2Bproperty%2Bof%2Bthe%2Bcommand%2Bhas%2Bnot%2Bbeen%2Binitialized.%2522
                oAdapter.SelectCommand.Transaction = _sharedTransaction
            End If
            Try
                oAdapter.InsertCommand = cm.GetInsertCommand
                oAdapter.UpdateCommand = cm.GetUpdateCommand
                oAdapter.DeleteCommand = cm.GetDeleteCommand

                oAdapter.InsertCommand.CommandTimeout = Me.CommandTimeout
                oAdapter.UpdateCommand.CommandTimeout = Me.CommandTimeout
                oAdapter.DeleteCommand.CommandTimeout = Me.CommandTimeout
            Catch sqlEx As SqlException
                Throw New CSQLException(sqlEx.Message, sqlEx)
            End Try

            'retrieve the update/insert commands so that we can change their timeouts.  Once that's done, the 
            Dim nAffectedCount As Integer
            startTimer()

            Try
                _LastSQL = "UpdateDataTable: " & sqlSelectMetaData
                nAffectedCount = oAdapter.Update(dt)
            Catch dbErr As DBConcurrencyException
                Throw dbErr
            Catch sqlEx As SqlException
                Dim maxLengthError As New System.Text.StringBuilder
                'First, check if any errrors were caused by String values that are longer than the column size
                'Fill the schema, and then check each column of each row
                Try
                    Dim schemaDt As DataTable = New DataTable
                    oAdapter.FillSchema(schemaDt, SchemaType.Source)
                    For Each row As DataRow In dt.Rows
                        'Unchanged rows should be fine since they were that size when pulled from the database
                        'We might not be able to access the row fields of deleted columns
                        If row.RowState = DataRowState.Deleted OrElse row.RowState = DataRowState.Unchanged Then
                            Continue For
                        End If
                        For Each column As DataColumn In schemaDt.Columns
                            If column.DataType.Equals(GetType(String)) AndAlso column.MaxLength > -1 Then
                                If SafeString(row(column.ColumnName)).Length > column.MaxLength Then
                                    maxLengthError.AppendFormat("Unable to update row in datatable ({0}). The value would be truncated." & vbCrLf & "Column: {1} ({4})" & vbCrLf & "Value: {2} ({3})" & vbCrLf, dt.TableName, column.ColumnName, row(column.ColumnName).ToString, row(column.ColumnName).ToString.Length, column.MaxLength)
                                End If
                            End If
                        Next
                    Next
                Catch err As Exception
                    'If this section throw an exception, it's probably a coding error that needs to be corrected.
                    'On Demo/Production, we'll get a warn/info before the following section throws a CSQLException
                    CAssert.FailGracefully(String.Format("Error occurred while comparing current row values against schema max length with message: {0}. Inner Exception: {1}", err.Message, sqlEx.Message))
                End Try
                If maxLengthError.Length > 0 Then
                    Throw New CSQLException(maxLengthError.ToString, sqlEx)
                End If

                If dt.Rows.Count = 1 Then
                    Dim sRowString As String
                    Try
                        sRowString = getRowTostring(dt.Rows(0))
                    Catch ex As Exception
                        'Don't want a failure prettying up the error to shadow the real problem...
                        'This is most likely to happen if there are deleted rows in the update, since you can't get
                        'information from them. 
                        sRowString = "--Can't access row information--"
                    End Try
                    Throw New CSQLException("Unable to update row in datatable (" & dt.TableName & ") :" & sRowString & vbCrLf & " Error Message:" & sqlEx.Message, sqlEx)
                Else
                    Dim sError As New System.Text.StringBuilder()
                    sError.Append("Unable to update row in datatable (" & dt.TableName & ") :")
                    Dim oRow As DataRow
                    For Each oRow In dt.Rows
                        Try
                            sError.Append(getRowTostring(oRow) & vbCrLf)
                        Catch ex As Exception
                            'Don't want a failure prettying up the error to shadow the real problem...
                            'This is most likely to happen if there are deleted rows in the update, since you can't get
                            'information from them. 
                            sError.Append("--Can't access row information--" & vbCrLf)
                        End Try
                    Next
                    sError.Append("SQL Error Message: " & sqlEx.Message)

                    Throw New CSQLException(sError.ToString, sqlEx)
                End If
            Catch overFlowEx As System.OverflowException
                Dim sError As New System.Text.StringBuilder()
                sError.Append("Unable to update row in datatable (" & dt.TableName & "): " & vbNewLine)
                Try
                    sError.Append(GetRowSortedByValuesToString(_CurrentUpdatingRow) & vbNewLine)
                Catch
                    'Don't want a failure prettying up the error to shadow the real problem...
                    'This is most likely to happen if there are deleted rows in the update, since you can't get
                    'information from them. 
                    sError.Append("--Can't access row information--" & vbCrLf)
                End Try
                sError.Append(" Error Message: " & overFlowEx.Message)
                Throw New CSQLException(sError.ToString, overFlowEx)
            Finally
                endTimer("UpdateDataTable", sqlSelectMetaData)
            End Try
            Return nAffectedCount
        End Function

        'safe method to update datatable when the db may have more fields than the datatable we are trying to update
        'otherwise you will get invalid operation/missing datacolumn errors
        Public Overloads Function UpdateDataTable(ByVal dt As DataTable, ByVal isTransacted As Boolean) As Integer Implements IDBUtils.UpdateDataTable
            If dt.Columns.Count <= 0 Then
                Throw New ArgumentException("Datatable passed in must already have columns defined!  Otherwise, please use other FillDataTable overload.")
            End If

            Dim sqlFields As New System.Text.StringBuilder()

            If Me.EnableCacheMetaData Then
                If False = _cachedSelects.ContainsKey(dt.TableName) Then
                    SyncLock (_cachedSelects.SyncRoot)
                        _cachedSelects(dt.TableName) = buildSelect(dt)
                    End SyncLock
                End If

                sqlFields.Append(CStr(_cachedSelects(dt.TableName)))
            Else
                sqlFields.Append(buildSelect(dt))
            End If

            Return UpdateDataTable(dt, sqlFields.ToString, isTransacted)
        End Function

        Private Function getRowTostring(ByVal oRow As DataRow) As String
            Dim sResult As String = ""
            Dim tbl As DataTable = oRow.Table
            Dim col As DataColumn
            For Each col In tbl.Columns
                Trace.WriteLine("type: " & col.ColumnName & ": " & col.DataType.ToString)

                Dim oVal As Object = oRow(col.ColumnName)
                If oRow(col.ColumnName) Is Nothing OrElse TypeOf oRow(col.ColumnName) Is DBNull Then
                    sResult += col.ColumnName & "=NULL,"
                Else
                    'handle conversion 
                    If col.DataType.Equals(GetType(String)) Then
                        sResult += col.ColumnName & "=" & SQLString(SafeString(oRow(col.ColumnName))) & ","
                    ElseIf col.DataType.Equals(GetType(Date)) Then
                        sResult += col.ColumnName & "=" & SQLString(SafeString(oRow(col.ColumnName))) & ","
                    ElseIf col.DataType.Equals(GetType(Byte())) Then
                        sResult += col.ColumnName & "=" & VarBinaryLiteral(CType(oRow(col.ColumnName), Byte())) & ","
                    Else
                        sResult += col.ColumnName & "=" & SafeString(oRow(col.ColumnName)) & ", "
                    End If
                End If
            Next
            If sResult <> "" Then
                sResult = sResult.TrimEnd(","c)
                sResult += " [Where SafeGuardClauseHere]" 'append this so we don't accidently screw things up when doing Update XXX Set .... 
            End If
            Return sResult
        End Function

        Protected Function ByteConvert(ByVal input As Byte) As String
            Return input.ToString("X2")
        End Function
        Protected Function VarBinaryLiteral(ByVal input As Byte()) As String
            Return "0x" & String.Join("", Array.ConvertAll(Of Byte, String)(input, AddressOf ByteConvert))
        End Function

        Private Function getDeadlockSummaryFromDB(ByVal pConnectionString As String) As DataTable
            If String.IsNullOrWhiteSpace(pConnectionString) Then
                Throw New ArgumentException("Cannot be null or empty", NameOf(pConnectionString))
            End If

            Dim sqlDeadlockSummary As String = String.Format("exec {0}.dbo.GetDeadlockSummary", pConnectionString)

            Using cn As New SqlConnection(_connectionString)
                Dim ds As New DataSet()

                Dim da As New SqlDataAdapter(sqlDeadlockSummary, cn)
                da.SelectCommand.CommandTimeout = 30
                da.Fill(ds)

                Return ds.Tables(0)
            End Using
        End Function

        Private Function getDeadlockSummary(ByVal ex As SqlException) As String
            Dim sMsg As String = ex.Message.ToUpper
            Dim bTraceAll As Boolean = True
            If sMsg.IndexOf("DEADLOCKED") <> -1 Then
                'green to go forward!
            ElseIf sMsg.IndexOf("TIMEOUT EXPIRED") <> -1 Then 'sMsg.IndexOf("DEADLOCKED") = -1 And 
                'continue and show ENTIRE activity summary;  unfortunately, if we're timed out, the activity will not count show correct locking process... 
            Else
                Return ""
            End If

            Dim dt As DataTable
            Try
                If UseTransactionScopeForDeadlockSummary Then
                    'we want to use a diff isolated connection/transaction to avoid MSDTC issues w/ any current transactions.
                    Using tx As New Transactions.TransactionScope(Transactions.TransactionScopeOption.RequiresNew)
                        dt = getDeadlockSummaryFromDB(CSQLConnectionString.CONNECTION_DBName)
                    End Using
                Else
                    dt = getDeadlockSummaryFromDB(CSQLConnectionString.CONNECTION_DBName)
                End If

                Dim rowsOfInterest As New System.Collections.Generic.List(Of DataRow)

                Dim dr As DataRow
                Dim sBuffer As New System.Text.StringBuilder()
                Dim nCount As Integer
                If bTraceAll Then
                    rowsOfInterest.AddRange(dt.Select())
                    nCount = rowsOfInterest.Count
                Else
                    For Each dr In dt.Select("BlockingSPID >  0 ")
                        rowsOfInterest.Add(dr)
                        nCount += 1
                        rowsOfInterest.AddRange(dt.Select(String.Format("UserSPID={0}", dr("BlockingSPID"))))
                    Next
                End If

                Dim dtRowsOfInterest As New DataTable
                Dim tmp As New System.Collections.Generic.List(Of DataRow)
                tmp.AddRange(dt.Select())
                For Each r As DataRow In tmp
                    If rowsOfInterest.Contains(r) = False Then
                        dt.Rows.Remove(r)
                    End If
                Next

                Dim sb As New System.Text.StringBuilder()

                'print header 
                sb.AppendLine()
                sb.AppendLine("<TABLE border=""1""> <TR>")

                For i As Integer = 0 To dt.Columns.Count - 1
                    sb.AppendLine("<TH>" & dt.Columns(i).ColumnName & "</TH>")
                Next
                sb.AppendLine("</TR>")

                'print rows 
                Dim bAlternate As Boolean
                For Each drFinal As DataRow In dt.Rows
                    bAlternate = Not bAlternate
                    sb.AppendLine(String.Format("<TR {0}>", If(bAlternate, "bgcolor=""#eaeaea""", "")))
                    For i As Integer = 0 To dt.Columns.Count - 1
                        sb.AppendLine("<TD>" & SafeString(drFinal.Item(i)) & "</TD>")
                    Next
                    sb.AppendLine("</TR>")
                Next

                sb.AppendLine("</TABLE>")

                'this is useful in case of unit testing whereby nUnit will not call ToString and we need to look at logs to see this info...
                Log.Debug("Deadlock Summary Table : " & sb.ToString)

                Return vbCrLf & " Deadlock summary ( " & nCount & " are being blocked " & ") : " & sb.ToString

            Catch exDead As System.Exception
                Log.Debug("Unable to retrieve deadlock info: " & exDead.ToString)
                Return vbCrLf & "Unable to obtain deadlock info: " & exDead.Message
            End Try

        End Function

        Private Function getDeadlockSummaryOld(ByVal ex As SqlException) As String
            Dim sMsg As String = ex.Message.ToUpper
            If sMsg.IndexOf("TIMEOUT EXPIRED") = -1 Then 'sMsg.IndexOf("DEADLOCKED") = -1 And 
                Return ""
            End If

            Dim dt As DataTable
            Try
                dt = getDataTable("exec sp_lock")
            Catch exDead As System.Exception
                Debug.Write("Unable to retrieve deadlock info: " & exDead.ToString)
                Return vbCrLf & "Unable to obtain deadlock info."
            End Try

            Dim dr As DataRow
            Dim sBuffer As New System.Text.StringBuilder()
            Dim nCount As Integer
            For Each dr In dt.Rows
                If SafeString(dr("Mode")) = "X" Then 'exclusive lock 
                    nCount += 1
					Dim sObjName As String = getScalarValue("Select Name From sysObjects where id=" & SafeString(dr("objID")))
					Dim sPID As String = SafeString(dr("SPID"))
                    Dim sBuff As String
                    Try
                        Dim dtBuff As DataTable = getDataTable(String.Format("dbcc inputbuffer ({0})", sPID))
                        sBuff = SafeString(dtBuff.Rows(0)("EventInfo"))
                    Catch exBuff As System.Exception
                        sBuff = "Unable to retrieve buffer: " & exBuff.ToString
                    End Try

                    sBuffer.AppendFormat("SPID: {0} Object:{1} SQL:{2}" & vbCrLf, sPID, sObjName, sBuff)

                End If
            Next

            Return vbCrLf & " Deadlock summary ( " & nCount & " blockings " & ") : " & sBuffer.ToString
        End Function

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: free unmanaged resources when explicitly called
                    closeConnection()
                End If

                ' TODO: free shared unmanaged resources
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

        Private ReadOnly Property UseTransactionScopeForDeadlockSummary() As Boolean
            Get
                Return YNStringToBool(System.Configuration.ConfigurationManager.AppSettings.Get("ENABLE_DBUTILS_DEADLOCK_TRANSACTIONSCOPE"))
            End Get
        End Property

        Public Shared Function UnescapeSQLBraces(ByVal pSql As String) As String
            Dim leftCurlyReplace As String = System.Configuration.ConfigurationManager.AppSettings.Get("SQL_LEFT_CURLY_REPLACEMENT")
            Dim rightCurlyReplace As String = System.Configuration.ConfigurationManager.AppSettings.Get("SQL_RIGHT_CURLY_REPLACEMENT")
            If leftCurlyReplace = "" Then
                leftCurlyReplace = "{"
            End If
            If rightCurlyReplace = "" Then
                rightCurlyReplace = "}"
            End If
            Return pSql.Replace(leftCurlyReplace, "{").Replace(rightCurlyReplace, "}")
        End Function

    End Class
End Namespace
