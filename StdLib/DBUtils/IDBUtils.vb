Imports System.Data
Imports System.Data.SqlClient

Namespace DBUtils
    'interface that provides functions to encapsulate the dirty day to day routines involving
    'commands, data readers, dataset, connection , and scalerexecution
    Public Interface IDBUtils
        Inherits IDisposable

        Function openConnection() As IDbConnection
        Function openConnectionAsync() As Task(Of IDbConnection)

        Function openConnectionTransacted() As IDbConnection
        Function openConnectionTransactedAsync() As Task(Of IDbConnection)
        ''' <summary>
        ''' Commits the transaction, throws an exception if none is open.
        ''' </summary>
        Sub commitTransaction()
        ''' <summary>
        ''' Rolls back the transaction, throws an exception if none is open.
        ''' </summary>
        Sub rollbackTransaction()

        ''' <summary>
        ''' Rolls back the transaction, does nothing if none is open.
        ''' </summary>
        Sub rollbackTransactionIfOpen()
        ''' <summary>
        ''' Commits the transaction, does nothing if none is open.
        ''' </summary>
        Sub commitTransactionIfOpen()

        'closes both connection and transacted connection
        'in asp, this method should be invoked on page unload
        Sub closeConnection()

        'used to execute update/insert statements, return -1 if not applicable
        Function executeNonQuery(ByVal sql As String, Optional ByVal isTransacted As Boolean = False) As Integer
        Function executeNonQuery(psSQL As String, pbIsTransacted As Boolean, ParamArray poSQLParams As SqlParameter()) As Integer

        'uses the non transacted conn to return single value from SELECT sql statement
        <Obsolete("This method is deprecated due to security concerns. Please use getScalarValue instead.")>
        Function getScalerValue(ByVal sql As String) As String
        Function getScalarValue(ByVal sql As String, ByVal sqlParams As CSQLParamBuilder) As String
        Function getScalarValue(ByVal sql As String, ByVal ParamArray sqlParams As SqlParameter()) As String

        'uses the non transacted conn to return single value from SELECT sql statement
        <Obsolete("This method is deprecated due to security concerns. Please use getScalarValueAsync instead.")>
        Function getScalarValueAsync(ByVal sql As String) As Task(Of String)
        Function getScalarValueAsync(ByVal sql As String, ByVal sqlParams As CSQLParamBuilder) As Task(Of String)
        Function getScalarValueAsync(ByVal sql As String, ByVal ParamArray sqlParams As SqlParameter()) As Task(Of String)

        Function getDataReader(ByVal sql As String) As IDataReader
        Function getDataReaderAsync(ByVal sql As String) As Task(Of IDataReader)
        Function getDataTable(ByVal sql As String) As DataTable
        Function getDataTableAsync(ByVal sql As String) As Task(Of DataTable)
        Function getDataTable(sql As String, ParamArray poSQLParams As SqlParameter()) As DataTable

        'usefull when working with strong typed dataset.
        Sub FillDataTable(ByVal dt As DataTable, ByVal sqlSelect As String)
        Sub FillDataTable(dt As DataTable, sqlSelect As String, ParamArray poSQLParams As SqlParameter())
        Function UpdateDataTable(ByVal dt As DataTable, ByVal sqlSelectMetaData As String, ByVal isTransacted As Boolean) As Integer
        Function UpdateDataTable(ByVal dt As DataTable, ByVal isTransacted As Boolean) As Integer
    End Interface
End Namespace
