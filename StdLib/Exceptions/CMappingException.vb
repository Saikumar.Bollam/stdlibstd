
Namespace Exceptions
    Public Class CMappingException
        Inherits Exception
        Private Sub New(ByVal srcType As Type, ByVal srcVal As Object, ByVal targetType As Type)
            MyBase.New(String.Format("Unable to map from type: {0} with value:{1} to target type:{2}", srcType.Name, safeVal(srcVal), targetType.Name))
        End Sub

        Sub New(ByVal srcVal As Object, ByVal targetType As Type)
            Me.new(srcVal.GetType, safeVal(srcVal), targetType)
        End Sub

        Private Shared Function safeVal(ByVal srcVal As Object) As String
            Dim tmpVal As String
            If IsNothing(srcVal) Then
                tmpVal = "[Nothing]"
            Else
                tmpVal = srcVal.ToString
            End If

            Return tmpVal
        End Function
    End Class
End Namespace