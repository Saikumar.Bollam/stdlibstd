Namespace Exceptions
    Public Class LocalizationKeyNotFoundException
        Inherits Exception

        Public Sub New(ByVal sKey As String)
            MyBase.New(String.Format("No resource found with key '{0}'.", sKey))
        End Sub

    End Class
End Namespace
