﻿Namespace Exceptions
    Public Class CFormatException
        Inherits System.Exception

        Public ExpectedFormat As String = ""
        Public Sub New(ByVal psMessage As String)
            MyBase.New(psMessage)
        End Sub

        Public Sub New(ByVal psMessage As String, ByVal poInnerException As Exception)
            MyBase.New(psMessage, poInnerException)
        End Sub

        Public Sub New(ByVal psMessage As String, ByVal psExpectedFormat As String)
            MyBase.New(psMessage & " Expected Format: " & psExpectedFormat)
            ExpectedFormat = psExpectedFormat
        End Sub

        Public Sub New(ByVal psMessage As String, ByVal psExpectedFormat As String, ByVal poInnerException As Exception)
            MyBase.New(psMessage & " Expected Format: " & psExpectedFormat, poInnerException)
            ExpectedFormat = psExpectedFormat
        End Sub
    End Class

End Namespace