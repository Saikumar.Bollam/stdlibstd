Namespace Exceptions
    Public Class CSetupException
        Inherits SystemException

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal psMessage As String)
            MyBase.New(psMessage)
        End Sub
    End Class
End Namespace