Namespace Exceptions


    ''' <summary>
    ''' Exception class to use for general vaidation failures.   
    ''' Usability note: If you are performing many validations, please consider
    ''' merging validation failures to end user can see and fix multiple issues at once.
    ''' </summary>
    Public Class CValidationException
        Inherits System.Exception

        Public Sub New(ByVal psMessage As String)
            MyBase.New(psMessage)
        End Sub

        Public Sub New(ByVal psMessage As String, ByVal poInnerException As Exception)
            MyBase.New(psMessage, poInnerException)
        End Sub
    End Class
End Namespace
