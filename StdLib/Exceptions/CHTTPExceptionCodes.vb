Namespace Exceptions
    ''' <summary>
    ''' This class contains our Custom HTTP Exception Code constants that map out to the web.config.
    ''' 
    ''' Only 400-500s are standard HTTP Status Codes. 600-> are custom defined by LPQ.
    ''' 
    ''' If you are adding a new code, please sure the value is unique.
    ''' </summary>
    Public Class CHTTPExceptionCodes

#Region "400s"
        Public Const FORBIDDEN As Integer = 403
        Public Const NOT_FOUND As Integer = 404
#End Region

#Region "500s"
        Public Const INTERNAL_SERVER_ERROR As Integer = 500
#End Region

#Region "600s"
        Public Const CORRUPT_SERVER_VIEWSTATE As Integer = 600
        Public Const INVALID_URL_REQUEST As Integer = 601

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        Public Const INVALID_URL As Integer = 602

        ''' <summary>
        ''' Somebody put something in an input field that might be markup or code injection
        ''' </summary>
        ''' <remarks></remarks>
        Public Const HTTP_REQUEST_VALIDATION As Integer = 604

        Public Const IP_RESTRICTED As Integer = 605

        ''' <summary>
        ''' Validation of Viewstate MAC failed
        ''' </summary>
        ''' <remarks>
        ''' Failure due to:
        ''' 1) Invalid session id. Session Id is used as ViewStateUserKey for signing _ViewState, the session id needs
        ''' to be the same when signing and when decoding _ViewState. (http://msdn.microsoft.com/en-us/library/ms972969.aspx)
        ''' 2) Corrupted _ViewState data due to network or malicious tampering
        ''' </remarks>
        Public Const VIEWSTATE_MAC_VALIDATION As Integer = 606
#End Region

#Region "700s"
        ''' <summary>
        ''' Consumer tried to access a resource they weren't suppose to.
        ''' </summary>        
        Public Const UNAUTHORIZED_ACCESS As Integer = 701
        Public Const UNDERWRITING_ERROR As Integer = 750
        Public Const UNDERWRITING_ERR_CC As Integer = 751
        Public Const UNDERWRITING_ERR_ML As Integer = 752
        Public Const UNDERWRITING_ERR_PL As Integer = 753
        Public Const UNDERWRITING_ERR_VL As Integer = 754
        Public Const UNDERWRITING_ERR_BL As Integer = 755
        Public Const UNDERWRITING_ERR_HE As Integer = 756
#End Region

#Region "800s"
        Public Const HANDLE_TEST_ERROR As Integer = 811
#End Region

    End Class
End Namespace

