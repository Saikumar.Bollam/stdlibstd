Imports System.Xml
Imports StdLib.CBaseStd
Imports StdLib.SpecializedDataTypes

Namespace XMLTools

    ''' <summary>
    ''' !! DEPRACATED !!.  Please use CXmlElement for any future development!
    ''' </summary>
    ''' <remarks></remarks>
    Public Class COptionalXMLElement

        'utility class to assist in writing attributes that are optional. Most usefull when writing optional  attributes that are tokens 
        'and do not accept empty values
        'sample usage:
        'dim e as xmlelement = ... 
        'dim eOp as new COptionalXMLElement( e)
        'e.setAttribute("x", "1") 
        'eOp.setAttribute ("y", "2")
        'eOp.setAttribute ("z", "") 'this attrib will never b written 

        Protected log As log4net.ILog = log4net.LogManager.GetLogger(Me.GetType)

        Private _Elem As XmlElement
        Public Sub New(ByVal poElem As XmlElement)
            _Elem = poElem
        End Sub

        'will have to force to use the wrap function
        Public Sub New()

        End Sub
        Public Sub wrap(ByVal poElement As XmlElement)
            _Elem = poElement
        End Sub

        Public Function GetAttribute(ByVal psName As String, Optional ByVal psDefault As String = "") As String
            If _Elem.Attributes(psName) Is Nothing Then
                Return psDefault
            Else
                Return _Elem.GetAttribute(psName)
            End If
        End Function

        ''' <summary>
        ''' Parses out the specified named attribute and returns a date result (NO time information).  Use this 
        ''' if the attribute is of datatype xs:date
        ''' </summary>
        ''' <param name="psName"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetAttributeDate(ByVal psName As String) As String
            Dim sValue As String = _Elem.GetAttribute(psName)
            If sValue = "" Then
                Return ""
            End If

            Dim dDate As Nullable(Of DateStruct) = ParseDateStruct(sValue)
            If dDate.HasValue Then
                Return dDate.Value.ToShortDateString
            Else
                log.Warn("Unable to parse attribute: " & psName & " with expected date value:" & sValue)
                Return ""
            End If
        End Function

        ''' <summary>
        ''' Parses out the specified named attribute and returns a date result with time information.  Use this 
        ''' if the attribute is of datatype xs:dateTime
        ''' </summary>
        ''' <param name="psName"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetAttributeDateTime(ByVal psName As String) As String
            Dim sValue As String = _Elem.GetAttribute(psName)
            If sValue = "" Then
                Return ""
            End If

            Dim dDate As Nullable(Of DateTimeStruct) = ParseDateTimeStruct(sValue, CTimeZoneManager.LocalZone)
            If dDate.HasValue Then
                Return dDate.Value.ToDateTime.ToString
            Else
                log.Warn("Unable to parse attribute: " & psName & " with expected date value:" & sValue)
                Return ""
            End If
        End Function


        Public Sub setAttributeRate(ByVal psAttribName As String, ByVal pnValue As Double)
            _Elem.SetAttribute(psAttribName, FormatNumber(pnValue, 5, , TriState.False, TriState.False))
        End Sub

        Public Sub setAttributeRate(ByVal psAttribName As String, ByVal pnValue As Double, pPrecision As Integer)
            _Elem.SetAttribute(psAttribName, FormatNumber(pnValue, pPrecision, , TriState.False, TriState.False))
        End Sub

        Public Sub setAttributeMoney(ByVal psAttribName As String, ByVal pnValue As Double)
            _Elem.SetAttribute(psAttribName, FormatNumber(pnValue, 2, , TriState.False, TriState.False))
        End Sub
        Public Sub setAttributeMoney(ByVal psAttribName As String, ByVal pnValue As Nullable(Of Double))
            If pnValue.HasValue Then
                _Elem.SetAttribute(psAttribName, FormatNumber(pnValue.Value, 2, , TriState.False, TriState.False))
            End If
        End Sub

        Public Sub SetAttributeDateTime(ByVal psAttribName As String, ByVal psValue As String)
            SetAttribute(psAttribName, mapDateTimeToXml(ParseDateTimeStruct(psValue, CTimeZoneManager.LocalZone)))
        End Sub
        Public Sub SetAttributeDateTimeUTC(ByVal psAttribName As String, ByVal psValue As String)
            SetAttribute(psAttribName, mapDateTimeUTCToXml(ParseDateTimeStruct(psValue, CTimeZoneManager.Zone.UTC)))
        End Sub
        Public Sub SetAttributeDate(ByVal psAttribName As String, ByVal psValue As String)
            SetAttribute(psAttribName, mapDateToXml(ParseDateStruct(psValue)))
        End Sub
        Public Sub SetAttributeDateTime(ByVal psAttribName As String, ByVal poValue As DateTime)
            SetAttribute(psAttribName, mapDateTimeToXml(New DateTimeStruct(poValue)))
        End Sub
        Public Sub SetAttributeDateTimeUTC(ByVal psAttribName As String, ByVal poValue As DateTime)
            SetAttribute(psAttribName, mapDateTimeUTCToXml(New DateTimeStruct(poValue, CTimeZoneManager.Zone.UTC)))
        End Sub
        Public Sub SetAttributeDate(ByVal psAttribName As String, ByVal poValue As DateTime)
            SetAttribute(psAttribName, mapDateToXml(New DateStruct(poValue)))
        End Sub

        Private Function mapDateTimeUTCToXml(ByVal pdDate As Nullable(Of DateTimeStruct)) As String
            If pdDate.HasValue Then
                Dim dDate As DateTimeOffset = pdDate.Value.ToTimeZone(CTimeZoneManager.Zone.UTC).ToDateTimeOffset

                Return System.Xml.XmlConvert.ToString(dDate)
            End If

            Return "0000-00-00T00:00:00"
        End Function

        Private Function mapDateTimeToXml(ByVal pdDate As Nullable(Of DateTimeStruct)) As String
            If pdDate.HasValue Then
                Dim dDate As DateTimeOffset = pdDate.Value.ToTimeZone(CTimeZoneManager.LocalZone).ToDateTimeOffset

                Return System.Xml.XmlConvert.ToString(dDate)
            End If

            Return "0000-00-00T00:00:00"
        End Function

        Private Function mapDateToXml(ByVal pdDate As Nullable(Of DateStruct)) As String
            If pdDate.HasValue Then
                Return pdDate.Value.ToString("yyyy-MM-dd")
            End If

            Return "0000-00-00"
        End Function

        Public Sub SetAttribute(ByVal psAttribName As String, ByVal psValue As String)
            If psAttribName = "" Then
                'this check is needed or else our code maybe setting empty values and we might catch the bug until real data is entered
                Throw New System.ArgumentException("psAttribName cannot be empty!")
            End If

            Select Case Trim(psValue) 'we trim b/c schema's don't allow for "" or " " tokens anyhow
                Case "", "0000-00-00T00:00:00", "0000-00-00", CBaseStd.EMPTY_GUID_STRING, "00000000000000000000000000000000"
                    Return
            End Select

            _Elem.SetAttribute(psAttribName, psValue)
        End Sub

        Public Function appendXMLChildWithText(ByVal psName As String, ByVal psInnerText As String, Optional ByVal skipIfValueMissing As Boolean = True, Optional ByVal psDefaultNameSpace As String = "") As XmlElement
            Dim oTempElement As XmlElement
            Dim oDoc As XmlDocument = _Elem.OwnerDocument

            If psDefaultNameSpace = "" Then
                oTempElement = oDoc.CreateElement(psName)
            Else
                oTempElement = oDoc.CreateElement(psName, psDefaultNameSpace)
            End If

            If Not (skipIfValueMissing AndAlso psInnerText = "") Then
                oTempElement.InnerText = psInnerText
                _Elem.AppendChild(oTempElement)
            End If
            Return oTempElement
        End Function
    End Class

End Namespace
