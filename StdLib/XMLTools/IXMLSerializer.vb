﻿Namespace XMLTools
    Public Interface IXMLSerializer(Of T)
        Function Serialize(pObj As T) As String
        Function Deserialize(ByVal pSerializedData As String) As T
        Function SerializeToFragment(pObj As T) As String
    End Interface
End Namespace
