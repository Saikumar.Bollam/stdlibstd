﻿Imports System.Text

Namespace XMLTools

    Public Class CXmlToHtmlTableConverter
        ''' <summary>
        ''' Converts a given Html file into a Html table.
        ''' Returns a String of Html.
        ''' This is a recurssive function.
        ''' </summary>
        Public Shared Function ConvertXmlToHtmlTable(ByVal psXML As String) As String
            Dim log As log4net.ILog = log4net.LogManager.GetLogger("XmlToHtmlConverter")

            Dim html As New StringBuilder("<table border='1'>")
            Try
                html.Append("<tr><td><b>Field</b></td><td><b>Value</b></td></tr>")
                Dim xDocument As XDocument = XDocument.Parse(psXML)
                Dim root As XElement = xDocument.Root

                html.Append(ConvertXMLToHTMLRows(root, 0))

                html.Append("</table>")
            Catch ex As Exception
                log.Error("There was an issue converting XML To HTML: " & ex.Message, ex)
            End Try

            Return html.ToString
        End Function

        ''' <summary>
        ''' XML to Html table helper function to assist in row creation.
        ''' </summary>
        Protected Shared Function ConvertXMLToHTMLRows(ByVal poElement As XElement, ByVal pnDepth As Integer) As String
            Dim html As New StringBuilder()

            Dim sPadding As String = ""
            For nCount = 1 To pnDepth
                sPadding &= "&nbsp;&nbsp;&nbsp;"
            Next
            Dim sName As String = ""
            sName = sPadding & poElement.Name.LocalName

            If poElement.HasElements Then
                html.Append("<tr>")
                html.Append("<td>" & sName & "</td>")
                html.Append("<td>&nbsp;</td>")
                html.Append("</tr>")

                For Each oChildElement As XElement In poElement.Elements
                    html.Append(ConvertXMLToHTMLRows(oChildElement, pnDepth + 1))
                Next
            Else
                html.Append("<tr>")
                html.Append("<td>" & sName & "</td>")
                html.Append("<td>" & poElement.Value & "</td>")
                html.Append("</tr>")
            End If

            Return html.ToString
        End Function

    End Class
End Namespace
