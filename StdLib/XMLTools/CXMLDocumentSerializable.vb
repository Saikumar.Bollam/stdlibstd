Imports System.Configuration.ConfigurationManager
Imports System.IO
Imports System.Runtime.Serialization
Imports System.Xml
Imports StdLib.XMLTools.CZipSharpCompression

Namespace XMLTools
    <Serializable()> _
    Public Class CXMLDocumentSerializable
        Inherits XmlDocument
        Implements ISerializable

        Private _XMLCompressionLevel As Integer

        Public ReadOnly Property XMLCompressionLevel() As Integer
            Get
                If AppSettings.Get("XML_COMPRESSION_LEVEL") <> "" Then
                    Return CBaseStd.SafeInteger(AppSettings.Get("XML_COMPRESSION_LEVEL"))
                Else
                    Return -1
                End If
            End Get
        End Property

        Sub New()
            MyBase.New()
        End Sub
        Sub New(ByVal nt As System.Xml.XmlNameTable)
            MyBase.New(nt)
        End Sub
        Sub New(ByVal imp As System.Xml.XmlImplementation)
            MyBase.New(imp)
        End Sub
        Sub New(ByVal xmlDoc As XmlDocument)
            If Not xmlDoc Is Nothing Then
                LoadXml(xmlDoc.OuterXml)
            End If
        End Sub
        Private Sub New(ByVal Info As SerializationInfo, ByVal Context As StreamingContext)
            If XMLCompressionLevel < 0 Then
                If Info.GetString("XML") <> "" Then
                    Me.LoadXml(Info.GetString("XML"))
                End If
            Else
                Dim InfoByte As Byte() = CType(Info.GetValue("XML", GetType(Byte())), Byte())
                If InfoByte IsNot Nothing AndAlso InfoByte.Length > 0 Then
                    Using InStream As New MemoryStream(InfoByte)
                        Using DecompressedStream As Stream = DeCompressToStream(New CCompressedData(InStream))
                            Me.Load(DecompressedStream)
                        End Using
                    End Using
                End If
            End If

        End Sub

        Public Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext) Implements System.Runtime.Serialization.ISerializable.GetObjectData
            If XMLCompressionLevel < 0 Then
                info.AddValue("XML", Me.OuterXml)
            Else
                Using InStream As New MemoryStream()
                    Me.Save(InStream)
                    InStream.Flush()
                    Using oCompression As CCompressedData = CompressDataToCCompressedData(InStream, XMLCompressionLevel)
                        info.AddValue("XML", oCompression.CompressedByte, GetType(Byte()))
                    End Using
                End Using
            End If
        End Sub
    End Class
End Namespace
