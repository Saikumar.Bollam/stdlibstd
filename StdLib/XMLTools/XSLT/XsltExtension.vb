﻿Namespace XMLTools.XsltUtils

    ''' <summary>
    ''' Extension objects are used to extend the functionality of style sheets.
    ''' </summary>	
    Public Class XsltExtension
        Public Sub New()
        End Sub

        Public Function SplitString(ByVal a_sIn As String, ByVal a_nMaxSize As Integer, ByVal a_cTabs As Integer) As String
            If a_nMaxSize >= a_sIn.Length Then
                Return a_sIn
            End If

            Dim delim As Char() = New Char() {" "c}
            Dim parts As String() = a_sIn.Split(delim)

            Dim result As String = ""

            Dim working As String
            Dim partNum As Integer = 0
            While partNum < parts.Length
                working = parts(System.Math.Max(System.Threading.Interlocked.Increment(partNum), partNum - 1))
                While (partNum < parts.Length) AndAlso (working.Length + parts(partNum).Length <= a_nMaxSize)
                    working += " " & parts(System.Math.Max(System.Threading.Interlocked.Increment(partNum), partNum - 1))
                End While

                result += working & vbLf
                For i As Integer = 0 To a_cTabs - 1
                    result += vbTab
                Next
            End While

            Return result
        End Function

        Public Function SplitStringWithIndent(ByVal a_cIndentChars As Integer, ByVal a_sIn As String, ByVal a_nMaxSize As Integer, ByVal a_cTabs As Integer) As String
            If a_nMaxSize - a_cIndentChars >= a_sIn.Length Then
                Return a_sIn
            End If

            Dim delim As Char() = New Char() {" "c}
            Dim parts As String() = a_sIn.Split(delim)

            Dim result As String = ""

            Dim working As String
            Dim partNum As Integer = 0
            Dim bFirstLine As Boolean = True
            While partNum < parts.Length
                working = parts(System.Math.Max(System.Threading.Interlocked.Increment(partNum), partNum - 1))
                Dim nCurrLineLen As Integer = working.Length
                If bFirstLine Then
                    nCurrLineLen += a_cIndentChars
                End If
                While (partNum < parts.Length) AndAlso (nCurrLineLen + parts(partNum).Length <= a_nMaxSize)
                    working += " " & parts(System.Math.Max(System.Threading.Interlocked.Increment(partNum), partNum - 1))
                    nCurrLineLen = working.Length
                    If bFirstLine Then
                        nCurrLineLen += a_cIndentChars
                    End If
                End While

                result += working & vbLf
                bFirstLine = False
                For i As Integer = 0 To a_cTabs - 1
                    result += vbTab
                Next
            End While

            Return result
        End Function

        Public Function FormatDate(ByVal a_sIn As String) As String
            If 4 = a_sIn.Length Then
                Return a_sIn.Substring(0, 2) & "/" & a_sIn.Substring(2, 2)
            ElseIf 6 = a_sIn.Length Then
                Return a_sIn.Substring(0, 2) & "/" & a_sIn.Substring(2, 2) & "/" & a_sIn.Substring(4, 2)
            ElseIf 8 = a_sIn.Length Then
                Return a_sIn.Substring(0, 2) & "/" & a_sIn.Substring(2, 2) & "/" & a_sIn.Substring(4, 4)
            Else
                Return a_sIn
            End If
        End Function

        ''' <summary>
        ''' Returns the formatted Date using the MM/DD/YYYY format
        ''' </summary>
        ''' <param name="a_sIn">String representation of date</param>
        ''' <returns>Formatted date</returns>
        Public Function FormatDateForTUCCLR(ByVal a_sIn As String) As String
            If 8 = a_sIn.Length Then
                Return a_sIn.Substring(4, 2) & "/" & a_sIn.Substring(6, 2) & "/" & a_sIn.Substring(0, 4)
            Else
                Return a_sIn
            End If
        End Function

        Public Function FormatTime(ByVal a_sIn As String) As String
            If 6 = a_sIn.Length Then
                Return a_sIn.Substring(0, 2) & ":" & a_sIn.Substring(2, 2) & ":" & a_sIn.Substring(4, 2)
            Else
                Return a_sIn
            End If
        End Function

        Public Function FormatSSN(ByVal a_sIn As String) As String
            Return a_sIn.Substring(0, 3) & "-" & a_sIn.Substring(3, 2) & "-" & a_sIn.Substring(5, 4)
        End Function

        Public Function FormatZip(ByVal a_sIn As String) As String
            If 9 = a_sIn.Length Then
                Return a_sIn.Substring(0, 5) & "-" & a_sIn.Substring(5, 4)
            Else
                Return a_sIn
            End If
        End Function

        Public Function FormatPhone(ByVal a_sIn As String) As String
            If 10 = a_sIn.Length Then
                Return "(" & a_sIn.Substring(0, 3) & ")" & a_sIn.Substring(3, 3) & "-" & a_sIn.Substring(6, 4)
            ElseIf 7 = a_sIn.Length Then
                Return a_sIn.Substring(0, 3) & "-" & a_sIn.Substring(3, 4)
            Else
                Return a_sIn
            End If
        End Function
    End Class
End Namespace
