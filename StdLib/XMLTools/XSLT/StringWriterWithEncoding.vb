﻿Imports System.IO
Imports System.Text

Namespace XMLTools.XsltUtils

    Public Class StringWriterWithEncoding
        Inherits StringWriter
        Private m_encoding As Encoding

        Public Sub New(ByVal encoding As Encoding)
            Me.m_encoding = encoding
        End Sub

        Public Overrides ReadOnly Property Encoding() As Encoding
            Get
                Return m_encoding
            End Get
        End Property
    End Class
End Namespace