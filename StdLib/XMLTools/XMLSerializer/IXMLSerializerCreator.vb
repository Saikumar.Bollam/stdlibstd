﻿Option Strict On

Namespace XMLTools
    Public Interface IXMLSerializerCreator
        Function Create(Of T)() As IXMLSerializer(Of T)
    End Interface
End Namespace
