﻿Imports System.Xml.Serialization

Namespace XMLTools
    ''' <summary>
    ''' The default .net XmlSerializer only caches for basic use cases  XmlSerializer(type) and XmlSerializer(type, defaultNameSpace).  
    ''' If you use any of the other overloads, then caching is up to you.  This class provides 
    ''' abstraction to this limitation and provides caching to common scenarios we actually use. 
    ''' </summary>
    ''' <remarks>
    ''' See here for more info:
    ''' http://www.theroks.com/using-the-xmlserializer-in-multithreaded-applications/
    ''' http://blogs.msdn.com/b/tess/archive/2006/02/15/532804.aspx
    ''' http://support.microsoft.com/kb/886385/en-us
    ''' The XmlSerializer IS threadsafe: http://msdn.microsoft.com/en-us/library/system.xml.serialization.xmlserializer.aspx
    ''' </remarks>
    Public Class CXmlSerializerFactory

        Private Shared _CacheSerializers As New Generic.Dictionary(Of String, XmlSerializer)
        Private Shared _CacheLock As New Object

        Public Shared Function Create(Of T)(ByVal psRootNodeName As String) As System.Xml.Serialization.XmlSerializer
            Return Create(GetType(T), psRootNodeName)
        End Function

        Public Shared Function Create(ByVal pType As Type, ByVal psRootNodeName As String) As System.Xml.Serialization.XmlSerializer
            Dim sKey As String = pType.FullName & psRootNodeName

            Dim oResult As XmlSerializer
            SyncLock _CacheLock
                If _CacheSerializers.ContainsKey(sKey) Then
                    oResult = _CacheSerializers.Item(sKey)
                Else

                    Dim attributeOverrides As New XmlAttributeOverrides()
                    Dim xmlAttributes As New XmlAttributes()
                    xmlAttributes.XmlRoot = New XmlRootAttribute(psRootNodeName)
                    attributeOverrides.Add(pType, xmlAttributes)

                    oResult = New XmlSerializer(pType, attributeOverrides)
                    _CacheSerializers.Add(sKey, oResult)


                End If


            End SyncLock

            Return oResult
        End Function

        Public Shared Function Create(Of T)() As XmlSerializer
            'this is one of the basic one that already support caching so don't bother double caching.. 
            Return New XmlSerializer(GetType(T))
        End Function
        Public Shared Function Create(ByVal pType As Type) As XmlSerializer
            'this is one of the basic one that already support caching so don't bother double caching.. 
            Return New XmlSerializer(pType)
        End Function
    End Class

End Namespace
