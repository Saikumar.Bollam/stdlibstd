Option Strict On

Imports System.IO
Imports System.Xml
Imports System.Xml.Serialization

Namespace XMLTools
    Public Class CXMLSerializer(Of T)
        Implements IXMLSerializer(Of T)

        Private _Serializer As XmlSerializer

        ''' <summary>
        ''' Initialize a new instance of the CXMLSerializer class
        ''' </summary>
        Public Sub New()
            _Serializer = CXmlSerializerFactory.Create(Of T)()

            _Namespaces.Add(String.Empty, String.Empty)
        End Sub

        ''' <summary>
        ''' Initialize a new instance of the CXMLSerializer class, overriding the root node name.
        ''' </summary>
        ''' <remarks>This is used if the class in question does not have an XmlRootAttribute.</remarks>
        Public Sub New(ByVal pRootNodeName As String)

            _Serializer = CXmlSerializerFactory.Create(Of T)(pRootNodeName)
            _Namespaces.Add(String.Empty, String.Empty)
        End Sub

        'For fragment serialization
        Private _Namespaces As New Serialization.XmlSerializerNamespaces()

        ''' <summary>
        ''' Serialize an object to XML using default encoding.
        ''' </summary>
        Public Function Serialize(ByVal pObj As T) As String Implements IXMLSerializer(Of T).Serialize
            Dim stringWriter As New StringWriter
            Serialize(pObj, stringWriter)

            Return stringWriter.ToString()
        End Function

        Public Sub Serialize(ByVal pObj As T, ByVal pWriter As TextWriter)
            Dim writerSettings As New XmlWriterSettings
            writerSettings.Encoding = pWriter.Encoding 'The underlying writer MUST support the encoding type of the XML writer
            Dim xmlWriter As XmlWriter = System.Xml.XmlWriter.Create(pWriter, writerSettings)

            _Serializer.Serialize(xmlWriter, pObj)
        End Sub

        ''' <summary>
        ''' Serialize an object to XML fragment using default encoding.
        ''' </summary>
        Public Function SerializeToFragment(ByVal pObj As T) As String Implements IXMLSerializer(Of T).SerializeToFragment
            Dim stringWriter As New StringWriter()
            SerializeToFragment(pObj, stringWriter)

            Return stringWriter.ToString
        End Function

        Public Sub SerializeToFragment(ByVal pObj As T, ByVal pWriter As TextWriter)
            Dim writerSettings As New XmlWriterSettings
            writerSettings.OmitXmlDeclaration = True
            writerSettings.Encoding = pWriter.Encoding 'The underlying writer MUST support the encoding type of the XML writer
            Dim xmlWriter As XmlWriter = System.Xml.XmlWriter.Create(pWriter, writerSettings)

            _Serializer.Serialize(xmlWriter, pObj, _Namespaces)
        End Sub

        Public Function Deserialize(ByVal pSerializedData As TextReader) As T
            Return DirectCast(_Serializer.Deserialize(pSerializedData), T)
        End Function

        Public Function Deserialize(ByVal pSerializedData As XmlReader) As T
            Return DirectCast(_Serializer.Deserialize(pSerializedData), T)
        End Function

        Public Function Deserialize(ByVal pSerializedData As String) As T Implements IXMLSerializer(Of T).Deserialize
            Return Deserialize(New StringReader(pSerializedData))
        End Function

        Public Function Deserialize(ByVal pSerializedData As XmlNode) As T
            Return Deserialize(New XmlNodeReader(pSerializedData))
        End Function
    End Class
End Namespace
