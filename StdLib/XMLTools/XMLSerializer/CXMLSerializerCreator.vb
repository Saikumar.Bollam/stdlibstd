﻿Option Strict On

Namespace XMLTools
    Public Class CXMLSerializerCreator
        Implements IXMLSerializerCreator

        Public Function Create(Of T)() As IXMLSerializer(Of T) Implements IXMLSerializerCreator.Create
            Return New CXMLSerializer(Of T)
        End Function
    End Class
End Namespace
