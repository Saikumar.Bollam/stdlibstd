Imports system.xml
Namespace XMLTools
    ''' <summary>
    ''' This class provides quicker access to an XML document that requires namespace access
    ''' </summary>
    ''' <remarks>
    ''' Sample Usage:
    ''' _namespaceManager = New System.Xml.XmlNamespaceManager(poSourceDoc.NameTable)
    ''' _namespaceManager.AddNamespace("default", CLFMapping.CLF_NAMESPACE)  'we could use clf as prefix but then we'd have to construct CXMLElementNSManager w/ additional "clf" prefix as well :-P
    ''' Dim oRoot As New CXMLElementNSManager(poSourceDoc.DocumentElement, _namespaceManager)
    ''' importApplicants(oRoot.SelectSingleNode("APPLICANTS").ChildNodes)
    ''' </remarks>
    Public Class CXMLElementNSManager
        Public Function SelectNodes(ByVal xPath As String) As XmlNodeList
            Return _Elem.SelectNodes(getCorrectPath(xPath), _NamespaceManager)
        End Function

        Public Function SelectSingleElement(ByVal xpath As String) As XmlElement
            Return CType(_Elem.SelectSingleNode(getCorrectPath(xpath), _NamespaceManager), XmlElement)
        End Function

        Public Function SelectSingleElementNSManager(ByVal xpath As String) As CXMLElementNSManager
            Dim oElement As XmlElement
            oElement = CType(_Elem.SelectSingleNode(getCorrectPath(xpath), _NamespaceManager), XmlElement)

            Return New CXMLElementNSManager(oElement, _NamespaceManager, _NamespaceUsed)
        End Function

        Public Function SelectSingleNode(ByVal xpath As String) As XmlNode
            Return _Elem.SelectSingleNode(getCorrectPath(xpath), _NamespaceManager)
        End Function

        Public Function GetAttribute(ByVal name As String, Optional ByVal xpath As String = "") As String
            If xpath = "" Then
                Return _Elem.GetAttribute(name)
            Else
                Dim oElement As XmlElement
                oElement = SelectSingleElement(xpath)
                If IsNothing(oElement) Then
                    Return ""
                Else
                    Return oElement.GetAttribute(name)
                End If
            End If
        End Function

        Private Function getCorrectPath(ByVal sXPath As String) As String
            Dim xPathWithNS As String = ""
            Dim arrXPath() As String
            arrXPath = sXPath.Split("/"c)
            For i As Integer = 0 To arrXPath.Length - 1
                xPathWithNS += _NamespaceUsed & ":" & arrXPath(i)
                If i <> arrXPath.Length - 1 Then
                    xPathWithNS += "/"
                End If
            Next
            Return xPathWithNS
        End Function

        Public Sub New(ByVal poElement As XmlElement, ByVal poNamespaceManager As XmlNamespaceManager, Optional ByVal psNamespaceUsed As String = "default")
            _Elem = poElement
            _NamespaceManager = poNamespaceManager
            _NamespaceUsed = psNamespaceUsed
        End Sub

        Public Sub New(ByVal poElement As XmlNode, ByVal poNamespaceManager As XmlNamespaceManager, Optional ByVal psNamespaceUsed As String = "default")
            _Elem = CType(poElement, XmlElement)
            _NamespaceManager = poNamespaceManager
            _NamespaceUsed = psNamespaceUsed
        End Sub

        'Public ReadOnly Property NamespaceURI() As String
        '    Get
        '        Return _Elem.NamespaceURI
        '    End Get
        'End Property

        Private _NamespaceUsed As String
        Public Property NamespaceUsed() As String
            Get
                Return _NamespaceUsed
            End Get
            Set(ByVal Value As String)
                _NamespaceUsed = Value
            End Set
        End Property

        Public ReadOnly Property NameSpaceManager() As XmlNamespaceManager
            Get
                Return _NamespaceManager
            End Get
        End Property

        Public ReadOnly Property _XmlElement() As XmlElement
            Get
                Return _Elem
            End Get
        End Property

        Private _Elem As XmlElement
        Private _NamespaceManager As XmlNamespaceManager
    End Class
End Namespace