Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Xml
Imports System.Xml.XPath
Imports System.Xml.Xsl
Imports StdLib.CBaseStd

Namespace XMLTools

    ''' <summary>
    ''' Utility to handle xml processing.   A lot of the routines in here are obsolete (as commented) and should be replaced by CXmlElement usage.
    ''' </summary>	
    Public Class CXMLUtils

        ''' <summary>
        ''' It's frequent that we like to print XML indented for enhanced readability instead of just using OuterXml which returns condensed version.
        ''' </summary>
        ''' <param name="poXml">Can be XmlDocument, but routine designed to be more generic to handle any xmlNode (includes xmlDocument)</param>
        Public Shared Function FormatXml(ByVal poXml As XmlNode) As String
            Using sw As New System.IO.StringWriter
                Using xtw As New XmlTextWriter(sw)
                    xtw.Formatting = Formatting.Indented
                    poXml.WriteTo(xtw)
                    Return sw.ToString
                End Using
            End Using


        End Function



        ''' <summary>
        ''' returns the inner text value of a specified xpath starting from any parent node
        ''' </summary>
        ''' <remarks>
        ''' Depracted:  Please use CXmlElement!
        ''' </remarks>
        Public Shared Function GetChildValue(ByVal poParent As XmlNode, ByVal psXPath As String, Optional ByVal pnMaxLength As Integer = -1) As String
            Dim xmlChild As XmlNode = poParent.SelectSingleNode(psXPath)
            If Not IsNothing(xmlChild) Then
                If pnMaxLength > -1 Then
                    Return Left(SafeString(xmlChild.InnerText), pnMaxLength)
                Else
                    Return SafeString(xmlChild.InnerText)
                End If
            End If

            Return ""
        End Function

        ''' <summary>
        ''' Depracted:  Please use CXmlElement!
        ''' </summary>
        Public Shared Sub SetChildValue(ByVal poParent As XmlNode, ByVal psChildName As String, ByVal psValue As String)
            Dim xmlChild As XmlNode = poParent.SelectSingleNode(psChildName)
            If IsNothing(xmlChild) Then
                AddChildElement(poParent, psChildName, psValue)
            Else
                xmlChild.InnerText = psValue
            End If
        End Sub

        ''' <summary>
        ''' PLEASE AVOID USING THIS ROUTINE SINCE IT'S NOT VERY OO AND CREATES MORE DIFFICULT TO READ CODE (EXCESSIVE PARAMETER BLOAT)!   PREFERRED WAY IS TO USE CXMLELEMENT 
        ''' CLASS AS IT PROVIDES SIMPLER NAMESPACE SUPPORT AND MORE CLEAN API. 
        ''' 
        ''' creates and appends a child element to the specified parent, and returns a pointer to the child		
        ''' </summary>
        Public Shared Function AddChildElement(ByVal poParent As XmlNode,
           ByVal psChildName As String,
           ByVal psChildInnerText As String,
           Optional ByVal psNodePrefix As String = "",
           Optional ByVal psNodeNamespaceURI As String = "",
           Optional ByVal pbOmitIfEmpty As Boolean = True,
           Optional ByVal pnMaxLength As Integer = -1) As XmlElement

            If psChildInnerText Is Nothing Then
                psChildInnerText = ""
            End If

            If pbOmitIfEmpty AndAlso psChildInnerText = "" Then
                Return Nothing
            End If

            Dim xmlel As XmlElement

            If psNodeNamespaceURI = "" And psNodePrefix = "" Then
                xmlel = poParent.OwnerDocument.CreateElement(psChildName, poParent.NamespaceURI)
            ElseIf psNodePrefix = "" Then
                xmlel = poParent.OwnerDocument.CreateElement(psChildName, psNodeNamespaceURI)
            Else
                xmlel = poParent.OwnerDocument.CreateElement(psNodePrefix, psChildName, psNodeNamespaceURI)
            End If

            If psChildInnerText.Length > 0 AndAlso pnMaxLength > 0 Then
                xmlel.InnerText = Left(psChildInnerText, pnMaxLength)
            Else
                xmlel.InnerText = psChildInnerText
            End If

            poParent.AppendChild(xmlel)

            Return xmlel
        End Function

        Public Shared Function mapDateToXml(ByVal psDate As String, Optional ByVal pbIncludeTime As Boolean = False) As String
            If IsDate(psDate) = False Then
                If pbIncludeTime Then
                    Return "0000-00-00T00:00:00"
                Else
                    Return "0000-00-00"
                End If
            End If
            Return mapDateToXml(CDate(psDate), pbIncludeTime)
        End Function

        Public Shared Function mapDateToXml(ByVal pdDate As Date, Optional ByVal pbIncludeTime As Boolean = False) As String
            If pbIncludeTime Then
                Return pdDate.ToString("yyyy-MM-dd") & "T" & pdDate.ToString("T", System.Globalization.DateTimeFormatInfo.InvariantInfo)     'System.Xml.XmlConvert.ToDateTime(psDate)
            Else
                Return pdDate.ToString("yyyy-MM-dd") 'System.Xml.XmlConvert.ToDateTime(psDate)
            End If
        End Function

        Private Shared _QuoteRegex As System.Text.RegularExpressions.Regex = New System.Text.RegularExpressions.Regex("([\'\""])")
        ''' <summary>
        ''' Takes a .NET string and returns it as a safe XPath literal. 
        ''' </summary>
        ''' <param name="psString"></param>
        ''' <returns></returns>
        ''' <remarks>
        ''' XPath (stupidly) contains no provision for escaping single/double quotes other than to use the other type for the string in question.
        ''' Thus, the only safe way to use a string which may contain both single and double quotes is to build up a call to the XPath concat() function.
        '''</remarks>
        Public Shared Function XPathString(ByVal psString As String) As String
            If Not psString.Contains("'"c) Then
                Return "'" & psString & "'"
            Else
                Dim tokens As String() = _QuoteRegex.Split(psString)
                Dim sToReturn As String = "concat("
                For Each sToken As String In tokens
                    If sToken = """" Then 'Literal string "
                        sToReturn &= "'""', "   ' literal string '"',
                    ElseIf sToken = "'" Then
                        sToReturn &= """'"", " 'literal string "'", 
                    Else
                        sToReturn &= "'" & sToken & "', "
                    End If
                Next
                sToReturn = sToReturn.TrimEnd(New Char() {","c, " "c})
                sToReturn &= ")"
                Return sToReturn
            End If
        End Function
#Region "From Underwrite Data Transfer"

        Public IgnoreColumns As New System.Collections.Specialized.StringCollection()

        Protected Sub AddElement(ByVal poParentNode As XmlElement, ByVal psElementName As String, ByVal psValue As String, ByVal psDataType As String)
            Dim oXMLElem As XmlElement
            Dim cFirstChar As Char

            'If element starts with a number, put a "_" at the beginning
            'will need to be handled on the import
            cFirstChar = psElementName.ToCharArray()(0)
            If True = Char.IsDigit(cFirstChar) Then
                psElementName = "_" & psElementName
            End If

            oXMLElem = CType(poParentNode.SelectSingleNode(psElementName), XmlElement)

            If IsNothing(oXMLElem) Then
                oXMLElem = poParentNode.OwnerDocument.CreateElement(psElementName)
                oXMLElem.SetAttribute("type", psDataType)
            End If

            oXMLElem.InnerText = psValue
            poParentNode.AppendChild(oXMLElem)
        End Sub

        Public Sub ExportRecordToElement(ByVal poXmlEl As XmlElement, ByVal dr As DataRow)
            Dim column As DataColumn

            For Each column In dr.Table.Columns
                If IgnoreColumns.Contains(UCase(column.ColumnName)) = False Then
                    Select Case UCase(column.DataType.ToString())
                        Case "SYSTEM.DOUBLE", "SYSTEM.DECIMAL"
                            AddElement(poXmlEl, UCase(column.ColumnName), FormatNumber(SafeDouble(dr(column.ColumnName)), 5), column.DataType.ToString())
                        Case "SYSTEM.INT32"
                            AddElement(poXmlEl, UCase(column.ColumnName), SafeInteger(dr(column.ColumnName)).ToString, column.DataType.ToString())
                        Case "SYSTEM.GUID"
                            AddElement(poXmlEl, UCase(column.ColumnName), SafeStringGuid(dr(column.ColumnName)), column.DataType.ToString())
                        Case "SYSTEM.DATE", "SYSTEM.DATETIME"
                            AddElement(poXmlEl, UCase(column.ColumnName), SafeDate(dr(column.ColumnName).ToString()), column.DataType.ToString())
                        Case "SYSTEM.BYTE[]"
                            Dim sValue As String = ""
                            If TypeOf (dr(column.ColumnName)) IsNot System.DBNull Then
                                Dim oBytes As Byte() = CType(dr(column.ColumnName), Byte())
                                sValue = Convert.ToBase64String(oBytes)
                            End If

                            AddElement(poXmlEl, UCase(column.ColumnName), sValue, column.DataType.ToString())

                        Case Else
                            AddElement(poXmlEl, UCase(column.ColumnName), SafeString(dr(column.ColumnName)), column.DataType.ToString())
                    End Select
                End If
            Next
        End Sub

        Public Sub ImportElementToRecord(ByVal poXmlEl As XmlElement, ByRef poRecord As DataRow)
            Dim column As DataColumn

            For Each column In poRecord.Table.Columns
                Dim sColumnName As String = UCase(column.ColumnName)

                If IgnoreColumns.Contains(sColumnName) = False Then
                    Dim xmlChild As XmlElement = CType(poXmlEl.SelectSingleNode(sColumnName), XmlElement)

                    If Not IsNothing(xmlChild) Then
                        Select Case UCase(xmlChild.GetAttribute("type"))
                            Case "SYSTEM.DOUBLE", "SYSTEM.DECIMAL"
                                poRecord(sColumnName) = SafeDouble(xmlChild.InnerText)
                            Case "SYSTEM.INT32", "SYSTEM.INT16"
                                poRecord(sColumnName) = SafeInteger(xmlChild.InnerText)
                            Case "SYSTEM.GUID"
                                poRecord(sColumnName) = convertToDSGuid(xmlChild.InnerText)
                            Case "SYSTEM.DATE", "SYSTEM.DATETIME"
                                poRecord(sColumnName) = IIf(SafeDate(xmlChild.InnerText) = "", DBNull.Value, SafeDate(xmlChild.InnerText))
                            Case "SYSTEM.BYTE[]"
                                If xmlChild.InnerText <> "" Then
                                    poRecord(sColumnName) = Convert.FromBase64String(xmlChild.InnerText)

                                End If
                            Case Else
                                poRecord(sColumnName) = xmlChild.InnerText
                        End Select
                    End If
                End If
            Next
        End Sub
#End Region


        ''' <summary>
        ''' Creates a readable audit trail of changes between an old XML document and a new XML document.  It isn't
        ''' perfect, because we have no way of knowing which nodes correspond to other nodes, but it should be good 
        ''' enough for the majority of use cases (e.g. changing an attribute here, adding a node there).
        ''' 
        ''' It makes the assumption that order matters.  That is, if you have 10 nodes in the old XML and 11 nodes
        ''' in the new XML, the new node added needs to be the last node in the series, because it's going to compare
        ''' the 1st node of the old XML against the 1st node of the new XML.  If the new XML added the node at the
        ''' beginning, it'll detect a lot more conflicts than there actually were.
        ''' </summary>
        Public Shared Function CompareXML(ByVal pOldXml As XmlDocument, ByVal pNewXml As XmlDocument) As List(Of String)
            Dim differences As New List(Of String)
            Dim oldDataset As New DataSet("Old")
            Dim stringReader As New StringReader(pOldXml.OuterXml)
            oldDataset.ReadXml(stringReader)
            oldDataset.AcceptChanges() ' Accept changes so that any changes we make from here on out will be noted

            ' Load new XML into a dataset
            Dim newDataset As New DataSet("New")
            stringReader = New StringReader(pNewXml.OuterXml)
            newDataset.ReadXml(stringReader)

            ' Add audits depending on whether tables/rows/columns were added/removed/updated
            For Each table As DataTable In oldDataset.Tables
                If newDataset.Tables(table.TableName) Is Nothing Then
                    ' Table / Node was deleted
                    differences.Add(String.Format("{0} deleted", table.TableName))
                Else
                    For Each column As DataColumn In table.Columns
                        If newDataset.Tables(table.TableName).Columns(column.ColumnName) Is Nothing Then
                            ' Column / Attribute was deleted
                            differences.Add(String.Format("{0} - {1} deleted", table.TableName, column.ColumnName))
                        End If
                    Next
                End If
            Next

            For Each table As DataTable In newDataset.Tables
                If oldDataset.Tables(table.TableName) Is Nothing Then
                    ' Table / Node was created
                    differences.Add(String.Format("{0} added", table.TableName))
                Else
                    For Each column As DataColumn In table.Columns
                        If oldDataset.Tables(table.TableName).Columns(column.ColumnName) Is Nothing Then
                            ' Column / Attribute was created
                            differences.Add(String.Format("{0} - {1} added", table.TableName, column.ColumnName))
                        End If
                    Next

                    ' Determine if any rows were added/removed
                    Dim numRowsDiff As Integer = table.Rows.Count - oldDataset.Tables(table.TableName).Rows.Count

                    If numRowsDiff > 0 Then
                        differences.Add(String.Format("{0} {1} node(s) added", numRowsDiff, table.TableName))
                    ElseIf numRowsDiff < 0 Then
                        differences.Add(String.Format("{0} {1} node(s) deleted", Math.Abs(numRowsDiff), table.TableName))
                    End If

                    ' Determine what values in each row changed. Note that this is NOT perfect. We don't have primary keys
                    ' so it's impossible for us to determine whether we're comparing the same rows between the old and new
                    ' datasets.  We're going to go ahead and make the assumption that order determines ID.  That is, the 
                    ' first row in this table in the old dataset is the first row in this table in the new dataset.  If someone
                    ' were to add a new row above this one, then they're going to see a lot of modification changes. :-(
                    For i As Integer = 0 To table.Rows.Count - 1
                        If oldDataset.Tables(table.TableName).Rows.Count <= i Then
                            Exit For
                        End If

                        ' Copy new values into old dataset for auditing
                        Dim oldRow As DataRow = oldDataset.Tables(table.TableName).Rows(i)
                        Dim newRow As DataRow = table.Rows(i)

                        Dim editedFields As New System.Text.StringBuilder
                        For Each column As DataColumn In oldDataset.Tables(table.TableName).Columns
                            If newRow.Table.Columns(column.ColumnName) Is Nothing Then
                                ' This column doesn't exist in the new table, so skip it
                                Continue For
                            End If

                            'Check if the old value is equal to the new value
                            Dim origVal As Object = oldRow(column.ColumnName)
                            Dim curVal As Object = newRow(column.ColumnName)

                            If Not SafeString(origVal).Equals(SafeString(curVal)) Then
                                editedFields.AppendFormat("{0}:{1}->{2} ", column.ColumnName, origVal, curVal)
                            End If
                        Next

                        If editedFields.Length > 0 Then
                            differences.Add(table.TableName & " - " & editedFields.ToString)
                        End If

                    Next
                End If
            Next
            Return differences
        End Function

        ''' <summary>
        ''' This function is used to transform XML data using an XSL style sheet
        ''' </summary>
        Public Shared Function XslTransform(ByVal psXslFileName As String, ByVal pDocToTransform As XmlDocument, ByVal pArgs As XsltArgumentList) As String
            Dim oStyle As New XmlDocument()
            Try
                oStyle.Load(psXslFileName)
            Catch ex As Exception
                Throw New ApplicationException("Failed to load stylesheet", ex)
            End Try

            Return XslTransformFromDocument(oStyle, pDocToTransform, pArgs)
        End Function

        Public Shared Function XslTransformFromDocument(ByVal pStyleDoc As XmlDocument, ByVal pDocToTransform As XmlDocument, ByVal args As XsltArgumentList) As String
            Dim pathdoc As New XPathDocument(New XmlNodeReader(pDocToTransform))
            Dim trans As New XslCompiledTransform()
            trans.Load(New XmlNodeReader(pStyleDoc))
            Dim writer As StringWriter = New XsltUtils.StringWriterWithEncoding(Encoding.UTF8)
            trans.Transform(pathdoc, args, writer)

            Return writer.ToString()
        End Function

        ''' <summary>
        ''' This function is used to validate the xml schema for input stream. If the request fails schema validation, an exception will be thrown containing the parseerror message
        ''' </summary>
        ''' <param name="pInputStream"></param>
        ''' <param name="psInputSchemaPath"></param>
        ''' <remarks></remarks>
        Public Shared Sub ValidateInputStream(ByVal pInputStream As Stream, ByVal psInputSchemaPath As String)
            Dim oLog As log4net.ILog = log4net.LogManager.GetLogger(GetType(CXMLUtils))

            If File.Exists(psInputSchemaPath) = False Then
                oLog.Warn("Skipping validation of schema for service b/c could not find file: " & psInputSchemaPath)
                Return
            End If

            pInputStream.Seek(0, SeekOrigin.Begin)
            Dim oSchema As New CSchemaValidator()
            If oSchema.validate(Xml.XmlReader.Create(pInputStream), New System.Xml.XmlTextReader(psInputSchemaPath)) = False Then
                oLog.WarnFormat("Request failed schema validation for {0}. Warnings: {1}. Error: {2}.", psInputSchemaPath.Replace(AppSettings("LocalLPQFolder"), ""), Text.CArrayFormatter.FormatAsTextComma(oSchema.Warnings), oSchema.ParseError)
                Throw New Exception("Failed schema validation: " & oSchema.ParseError)
            End If

            pInputStream.Seek(0, SeekOrigin.Begin)
        End Sub

        ''' <summary>
        ''' This is basically safer way to check for invalid characters in case of out of memory execptions it will just return the orignal node.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub RemoveInvalidXMLCharactersSafe(ByVal pNode As XmlNode)
            Dim oLog As log4net.ILog = log4net.LogManager.GetLogger(GetType(CXMLUtils))
            Try
                RemoveInvalidXMLCharacters(pNode)
            Catch ex As Exception
                oLog.Error(ex)
            End Try
        End Sub

        Public Shared Sub RemoveInvalidXMLCharacters(ByVal pNode As XmlNode)
            If pNode Is Nothing Then
                Return
            End If
            If TypeOf pNode Is XmlText Then
                pNode.InnerText = RemoveInvalidXMLCharactersHelper(pNode.InnerText)
            End If
            If pNode.Attributes IsNot Nothing Then
                For Each oAttribute As XmlAttribute In pNode.Attributes
                    oAttribute.Value = RemoveInvalidXMLCharactersHelper(oAttribute.Value)
                Next
            End If
            If pNode.ChildNodes IsNot Nothing Then
                For Each oNode As XmlNode In pNode.ChildNodes
                    RemoveInvalidXMLCharacters(oNode)
                Next
            End If
        End Sub

        Public Shared Function RemoveInvalidXMLCharactersHelper(ByVal pString As String) As String
            Dim validXmlChars = pString.Where(Function(ch) XmlConvert.IsXmlChar(ch)).ToArray()
            Return New String(validXmlChars)
        End Function


        ''' <summary>
        ''' XmlDocument normally does not provide a rename feature.  This makes refactoring a bit more complicated.  To suppor it, 
        ''' we'll have to do a deep copy.
        ''' </summary>		
        Public Shared Function RenameNode(src As XmlNode, newName As String) As XmlNode
            Dim doc As XmlDocument = src.OwnerDocument
            Dim newNode As XmlNode = doc.CreateNode(src.NodeType, newName, Nothing)
            While src.HasChildNodes
                newNode.AppendChild(src.FirstChild)
            End While
            Dim ac As XmlAttributeCollection = src.Attributes
            While ac.Count > 0
                newNode.Attributes.Append(ac(0))
            End While
            Dim parent As XmlNode = src.ParentNode
            parent.ReplaceChild(newNode, src)
            Return newNode
        End Function
    End Class
End Namespace
