﻿Imports System.Configuration.ConfigurationManager
Imports System.IO
Imports ICSharpCode.SharpZipLib

Namespace XMLTools

    Public Class CZipSharpCompression


        Public Shared ReadOnly Property BlockSize() As Integer
            Get
                If AppSettings.Get("COMPRESSION_BLOCK_SIZE") <> "" Then
                    Return CBaseStd.SafeInteger(AppSettings.Get("COMPRESSION_BLOCK_SIZE"))
                Else
                    Return 8192
                End If
            End Get
        End Property

        Public Shared Function CompressDataToCCompressedData(ByVal pStream As Stream, ByVal pCompressionLevel As Integer) As CCompressedData
            pStream.Position = 0
            Dim OutStream As New MemoryStream
            Dim gz As New Zip.ZipOutputStream(OutStream)
            gz.SetLevel(pCompressionLevel)
            Dim zipEntry As New Zip.ZipEntry("ZipEntry")
            gz.PutNextEntry(zipEntry)
            Dim buffer As Byte() = New Byte(BlockSize) {}
            Dim numRead As Integer
            numRead = pStream.Read(buffer, 0, buffer.Length)
            Do While numRead <> 0
                gz.Write(buffer, 0, numRead)
                numRead = pStream.Read(buffer, 0, buffer.Length)
            Loop
            gz.Finish()
            OutStream.Flush()
            Return New CCompressedData(OutStream)
        End Function

        ''' <summary>
        ''' Make sure to close the stream after usage.
        ''' </summary>
        Public Shared Function DeCompressToStream(ByVal pCompressedData As CCompressedData) As Stream
            Dim OutStream As New MemoryStream()
            Using gz As New Zip.ZipInputStream(pCompressedData.CompressedStream)
                Dim oEntry As Zip.ZipEntry = gz.GetNextEntry
                If oEntry Is Nothing Then
                    Throw New Exception("CSharpzip entry returned nothing")
                End If
                OutStream.Position = 0
                Dim buffer As Byte() = New Byte(BlockSize) {}
                Dim numRead As Integer
                numRead = gz.Read(buffer, 0, buffer.Length)
                Do While numRead <> 0
                    OutStream.Write(buffer, 0, numRead)
                    numRead = gz.Read(buffer, 0, buffer.Length)
                Loop
            End Using
            OutStream.Flush()
            OutStream.Position = 0
            Return OutStream
        End Function


        Public Class CCompressedData
            Implements IDisposable


            Private _compressedStream As MemoryStream
            Public ReadOnly Property CompressedByte As Byte()
                Get
                    If _compressedStream IsNot Nothing Then
                        Return _compressedStream.ToArray
                    Else
                        Return Nothing
                    End If
                End Get
            End Property

            Public ReadOnly Property CompressedStream As MemoryStream
                Get
                    If _compressedStream IsNot Nothing Then
                        _compressedStream.Position = 0
                        Return _compressedStream
                    Else
                        Return Nothing
                    End If
                End Get
            End Property

            Public Sub New(ByVal pStream As MemoryStream)
                _compressedStream = pStream
            End Sub



#Region "IDisposable Support"
            Private disposedValue As Boolean ' To detect redundant calls

            ' IDisposable
            Protected Overridable Sub Dispose(disposing As Boolean)
                If Not Me.disposedValue Then
                    If disposing Then
                        ' TODO: dispose managed state (managed objects).
                        If _compressedStream IsNot Nothing Then
                            _compressedStream.Close()
                        End If
                    End If

                    ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
                    ' TODO: set large fields to null.
                End If
                Me.disposedValue = True
            End Sub

            ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
            'Protected Overrides Sub Finalize()
            '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            '    Dispose(False)
            '    MyBase.Finalize()
            'End Sub

            ' This code added by Visual Basic to correctly implement the disposable pattern.
            Public Sub Dispose() Implements IDisposable.Dispose
                ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
                Dispose(True)
                GC.SuppressFinalize(Me)
            End Sub
#End Region

        End Class

    End Class

End Namespace
