Imports System.Configuration.ConfigurationManager
Imports StdLib.CBaseStd
Imports StdLib.CTimeZoneManager
Imports StdLib.SpecializedDataTypes

Namespace Logging
    ''' <summary>
    ''' This class is used for tracking the star/end time for a process e.g. report running. This class could also be used to
    ''' detect connection leaks. (https://intranet/index.php?title=Projects/LoansPQ_Developer%27s_Corner/How_to_Detect_SQL_Connection_Leaks#section_3)
    ''' The stats and recent entries for each tag could be viewed in Performance Report when is avaiable in Portal
    '''
    ''' Sample use:
    ''' 
    ''' Using CPerfTracker.CreateInstance(tag, message, orgname)
    '''      'your code
    ''' End Using
    '''
    ''' By adding the using block outside your code, this class will automatically log the start/end time when your code is executed.
    ''' Or you can call LogPerf to manually generate the perf tracker message.
    ''' 
    ''' For each tag, app setting key ENABLE_PERF_TRACKER_FOR_TAGNAME could be used to control the frequency of logging
    ''' Possible values: (default value is ON)
    ''' OFF 
    ''' ON
    ''' RANDOM:10 (10% chance fire, possible value 0~100)
    ''' 
    ''' App setting key ATTACH_STACK_TRACE_TO_PERF_MESSAGE_FOR_TAGNAME value (Y/N) could be used when you want to attach the detailed trace info, 
    ''' Server should be forced to take less load in order to enable this key.
    ''' </summary>
    Public Class CPerfTracker
        Implements IDisposable

        Protected Shared Log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CPerfTracker))
        Private _oPerfTrackerMessage As CPerfTrackerMessage = Nothing

        ''' <summary>
        ''' make sure you wrap with using block!
        ''' </summary>
        Public Shared Function CreateInstance(ByVal psTag As String, ByVal psMessage As String, ByVal psOrgname As String) As CPerfTracker
            Return New CPerfTracker(psTag, psMessage, psOrgname)
        End Function

        Private Sub New(ByVal psTag As String, ByVal psMessage As String, ByVal psOrgname As String)
            CAssert.IsTrue(Not String.IsNullOrEmpty(psTag), "Tags are missing")

            'TO DO: need to use new type
            If LogPerfTrackerMessage(psTag) Then
                _oPerfTrackerMessage = New CPerfTrackerMessage(DateTimeStruct.Now(LocalZone), Nothing, psTag, psMessage, psOrgname)
                Log.Debug(_oPerfTrackerMessage.GenerateMessage)
            End If
        End Sub

        Shared Sub LogPerf(ByVal pStart As DateTimeStruct?, ByVal pEnd As DateTimeStruct?, ByVal psTag As String, ByVal psMessage As String, ByVal psOrgname As String, ByVal pID As Guid)
            'TO DO: need to use new type
            If pID <> Guid.Empty Then
                Dim oPerfTrackerMessage As CPerfTrackerMessage = New CPerfTrackerMessage(pStart, pEnd, psTag, psMessage, psOrgname)
                oPerfTrackerMessage.ID = pID
                Log.Debug(oPerfTrackerMessage.GenerateMessage)
            End If
        End Sub

        Shared Function LogPerf(ByVal pStart As DateTimeStruct?, ByVal pEnd As DateTimeStruct?, ByVal psTag As String, ByVal psMessage As String, ByVal psOrgname As String) As Guid
            Dim oPerfTrackerMessage As CPerfTrackerMessage = New CPerfTrackerMessage(pStart, pEnd, psTag, psMessage, psOrgname)

            'TO DO: need to use new type
            If LogPerfTrackerMessage(psTag) Then
                Log.Debug(oPerfTrackerMessage.GenerateMessage)
                Return oPerfTrackerMessage.ID
            Else
                Return Guid.Empty
            End If

        End Function

        'Function to determine whether to log the perf tracker message, default value ON
        Private Shared Function LogPerfTrackerMessage(ByVal psTageName As String) As Boolean
            Try
                If SafeString(AppSettings("ENABLE_PERF_TRACKER_FOR_" & psTageName)) = "" Then
                    Return True
                End If

                Dim sSetting = SafeString(AppSettings("ENABLE_PERF_TRACKER_FOR_" & psTageName)).ToUpper

                Select Case sSetting
                    Case "ON"
                        Return True
                    Case "OFF"
                        Return False
                    Case Else
                        Dim iChance As Integer = SafeInteger(sSetting.Replace("RANDOM:", ""))
                        If iChance > 0 Then
                            Return CInt(Math.Ceiling(Rnd() * 100)) <= iChance
                        Else
                            Return False
                        End If
                End Select
            Catch ex As Exception
                Log.Error("Unable to parse app settings ENABLE_PERF_TRACKER_FOR_" & psTageName & ": " & ex.Message, ex)
            End Try

            Return False
        End Function

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    If _oPerfTrackerMessage IsNot Nothing Then
                        _oPerfTrackerMessage.END = DateTimeStruct.Now(LocalZone)

                        'TO DO: need to use new type
                        Log.Debug(_oPerfTrackerMessage.GenerateMessage)
                    End If
                End If
            End If

            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region
    End Class

    Class CPerfTrackerMessage
        Public ID As Guid = Guid.NewGuid
        Public ORG As String = ""
        Public Tag As String = ""
        Public Message As String = ""
        Public START As DateTimeStruct? = Nothing
        Public [END] As DateTimeStruct? = Nothing

        Public Sub New(ByVal pStart As DateTimeStruct?, ByVal pEnd As DateTimeStruct?, ByVal psTag As String, ByVal psMessage As String, ByVal psOrgname As String)
            Tag = psTag
            Message = psMessage
            START = pStart
            [END] = pEnd
            ORG = psOrgname
        End Sub

        Public Function GenerateMessage() As String
            Dim oDoc As New Xml.XmlDocument
            oDoc.LoadXml("<PERF></PERF>")
            oDoc.DocumentElement.SetAttribute("ID", ID.ToString)

            If Not String.IsNullOrEmpty(ORG) Then
                oDoc.DocumentElement.SetAttribute("ORG", ORG)
            End If

            If START.HasValue Then
                oDoc.DocumentElement.SetAttribute("START", START.ToString)
            End If

            If [END].HasValue Then
                oDoc.DocumentElement.SetAttribute("END", [END].ToString)
            End If

            Dim oTAG = oDoc.CreateElement("TAG")
            oTAG.InnerText = Tag
            oDoc.DocumentElement.AppendChild(oTAG)
            Dim oMessage = oDoc.CreateElement("MESSAGE")
            'in case there're 2 cdata sections, escape the second one
            oMessage.AppendChild(oDoc.CreateCDataSection(SafeString(Message).Replace("]]>", "")))
            oDoc.DocumentElement.AppendChild(oMessage)

            'Note that the default value of ATTACH_STACK_TRACE_TO_PERF_MESSAGE_FOR_TAGNAME should be N.
            'Server should be forced to take less load in order to enable this key
            If YNStringToBool(AppSettings("ATTACH_STACK_TRACE_TO_PERF_MESSAGE_FOR_" & Tag)) Then
                Dim oStackTrace = oDoc.CreateElement("STACK_TRACE")

                oStackTrace.AppendChild(oDoc.CreateCDataSection(System.Environment.StackTrace))
                oDoc.DocumentElement.AppendChild(oStackTrace)
            End If

            Return "[" & oDoc.OuterXml & "]"
        End Function
    End Class
End Namespace