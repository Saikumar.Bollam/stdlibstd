Option Strict On
Imports System.IO
Imports StdLib.CBaseStd
Imports StdLib.Text

Namespace Logging
    Public Class CLoggingUtils
        Protected Shared Log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CLoggingUtils))

        ''' <summary>
        ''' This function is used to log the first pMaxLogLength characters of request from inputstream.
        ''' If you want full request, simply set pMaxLogLength to 0. Set any negtive number e.g. -1 for hiding all content.
        ''' </summary>
        ''' <param name="pRequest"></param>
        ''' <param name="pMaxLogLength"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetStreamLog(ByVal pRequest As Stream, pMaxLogLength As Integer) As String
            If pMaxLogLength < 0 Then
                Return ""
            End If

            pRequest.Seek(0, SeekOrigin.Begin)
            Dim textreader As New IO.BinaryReader(pRequest)

            Dim sRequest As String = textreader.ReadChars(If(pMaxLogLength <> 0, pMaxLogLength, SafeInteger(pRequest.Length)))

            sRequest = CSecureStringFormatter.MaskSensitiveData(sRequest)

            pRequest.Seek(0, SeekOrigin.Begin)

            Return sRequest
        End Function
    End Class
End Namespace



