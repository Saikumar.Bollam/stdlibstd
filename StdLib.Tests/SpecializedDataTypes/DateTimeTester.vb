﻿Option Strict On

Imports NUnit.Framework
Imports StdLib.SpecializedDataTypes
Imports System.Configuration.ConfigurationManager

' C:\TOOLS\dotcover\dotCover.exe cover /Output="c:\Temp\codecoverage\AppCoverageReport.html" /ReportType="HTML" /TargetExecutable="C:\TOOLS\NUNIT\NUnit-2.6.1\bin\nunit-console.exe" /TargetArguments="C:\loanspq2\StdLib.Tests\bin\Debug\StdLib.Tests.dll /fixture=StdLib.Tests.SpecializedDataTypesTests.DateTimeTester"

Namespace SpecializedDataTypesTests
	<TestFixture()> Public Class DateTimeTester


		''' <summary>
		''' ticks is an interval of 100-nanoseconds, 10 million per secon
		''' </summary>
		''' <param name="val1"></param>
		''' <param name="val2"></param>
		''' <param name="maxDiff">default to 1 second</param>
		''' <returns></returns>
		Private Function AreTicksClose(ByVal val1 As Long, ByVal val2 As Long, Optional ByVal maxDiff As Long = 10000000) As Boolean
			Return Math.Abs(val1 - val2) < maxDiff
		End Function

		' i will use ticks to compare because ticks are based off 12:00:00 january 1, 0001, and it doesnt take into acount time zone.
		<Test()> Public Sub AreTicksClose_ActuallyTellSecondsAccurately()
			Dim date1 As DateTime = DateTime.UtcNow

			Dim date2 As DateTime = DateTime.UtcNow
			Assert.IsTrue(AreTicksClose(date1.Ticks, date2.Ticks))

			date2 = DateTime.UtcNow.AddSeconds(2)
			Assert.IsFalse(AreTicksClose(date1.Ticks, date2.Ticks))

			date2 = DateTime.UtcNow.AddSeconds(-2)
			Assert.IsFalse(AreTicksClose(date1.Ticks, date2.Ticks))
		End Sub


#Region "DateTimeStruct"


		<Test()> Public Sub DateTimeStruct_DateNow_UsesAppSetting()
			SimpleDateNowTest("America/Los_Angeles", "Pacific Standard Time")
			SimpleDateNowTest("Pacific/Honolulu", "Hawaiian Standard Time")
			SimpleDateNowWithMilliSecondTest("America/Los_Angeles", "Pacific Standard Time")
			SimpleDateNowWithMilliSecondTest("Pacific/Honolulu", "Hawaiian Standard Time")
		End Sub
		<Test()> Public Sub DateTimeStruct_Now_UsesAppSetting()
			AppSettings("MACHINE_TIME_ZONE") = "America/Los_Angeles"
			Dim structDate As DateTimeStruct = DateTimeStruct.Now()
			Dim outputted As DateTime = structDate.ToDateTime()
			Dim actualDate As DateTime = DateTime.Now
			Assert.IsTrue(AreTicksClose(outputted.Ticks, actualDate.Ticks))
		End Sub
		<Test()> Public Sub DateTimeStruct_NowWithMilliSecond_UsesAppSetting()
			AppSettings("MACHINE_TIME_ZONE") = "America/Los_Angeles"
			Dim structDate As DateTimeStruct = DateTimeStruct.NowWithMilliSecond()
			Dim outputted As DateTime = structDate.ToDateTime()
			Dim actualDate As DateTime = DateTime.Now
			Assert.IsTrue(AreTicksClose(outputted.Ticks, actualDate.Ticks))
		End Sub


		Private Sub SimpleDateNowWithMilliSecondTest(ByVal lpqZoneName As String, ByVal dotnetZoneName As String)
			AppSettings("MACHINE_TIME_ZONE") = lpqZoneName
			Dim localZone As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(dotnetZoneName)
			Dim expectedDate As DateTime = TimeZoneInfo.ConvertTime(DateTime.UtcNow, localZone)
			Dim actualDate As DateTime = DateTimeStruct.DateNowWithMilliSecond()
			Assert.IsTrue(AreTicksClose(actualDate.Ticks, expectedDate.Ticks))
		End Sub

		Private Sub SimpleDateNowTest(ByVal lpqZoneName As String, ByVal dotnetZoneName As String)
			AppSettings("MACHINE_TIME_ZONE") = lpqZoneName
			Dim localZone As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(dotnetZoneName)
			Dim expectedDate As DateTime = TimeZoneInfo.ConvertTime(DateTime.UtcNow, localZone)
			Dim actualDate As DateTime = DateTimeStruct.DateNow()
			Assert.IsTrue(AreTicksClose(actualDate.Ticks, expectedDate.Ticks))
		End Sub

		<Test()> Public Sub DateTimeStruct_TestToStringTemplates()
			AppSettings("MACHINE_TIME_ZONE") = "America/Los_Angeles"
			Dim oDate As DateTime = New Date(2000, 1, 12, 10, 11, 12)
			Dim oLocalStruct As DateTimeStruct = New DateTimeStruct(oDate)

			Assert.AreEqual("1/12/2000", oLocalStruct.ToShortDateString())
			Assert.AreEqual("Wednesday, January 12, 2000", oLocalStruct.ToLongDateString())
			Assert.AreEqual("10:11 AM", oLocalStruct.ToShortTimeString())
			Assert.AreEqual("10:11:12 AM", oLocalStruct.ToLongTimeString())
			Assert.AreEqual("Wednesday, January 12, 2000 10:11:12 AM", oLocalStruct.ToFullDateTimeString())
			Assert.AreEqual("1/12/2000 10:11:12 AM PST", oLocalStruct.ToStringWithZone())
			Assert.AreEqual("1/12/2000 11:11:12 AM MST", oLocalStruct.ToStringWithZone(CTimeZoneManager.Zone.Mountain))

		End Sub
		<Test()> Public Sub DateTimeStruct_TestDateValuesMap()
			''using utc versus utc to avoid dayligh savings issues

			Dim oDate As DateTime = DateTime.UtcNow
			Dim oStruct As DateTimeStruct = New DateTimeStruct(oDate, CTimeZoneManager.Zone.UTC)

			Assert.AreEqual(oDate.Year, oStruct.Year)
			Assert.AreEqual(oDate.Month, oStruct.Month)
			Assert.AreEqual(oDate.Day, oStruct.Day)
			Assert.AreEqual(oDate.Hour, oStruct.Hour)
			Assert.AreEqual(oDate.Minute, oStruct.Minute)
			Assert.AreEqual(oDate.Second, oStruct.Second)
			Assert.AreEqual(0, oStruct.MilliSecond)

			'TODO when we add a param to not strip millis, update the unit test

		End Sub

		<Test()> Public Sub DateTimeStruct_TestAddFunctions()
			Dim oDate As DateTime = New Date(2000, 1, 1, 1, 1, 1, 1)
			Dim oStruct As DateTimeStruct = New DateTimeStruct(oDate, CTimeZoneManager.Zone.Mountain)

			Assert.AreEqual(2010, oStruct.AddYears(10).Year)
			Assert.AreEqual(6, oStruct.AddMonths(5).Month)
			Assert.AreEqual(13, oStruct.AddDays(12).Day)
			Assert.AreEqual(2, oStruct.AddHours(1).Hour)
			Assert.AreEqual(21, oStruct.AddMinutes(20).Minute)
			Assert.AreEqual(41, oStruct.AddSeconds(40).Second)
		End Sub

		<Test()> Public Sub DateTimeStruct_TestComparisonOperators()
			Dim oDate As DateTime = New Date(2000, 1, 1, 1, 1, 1, 1)
			Dim oStruct As DateTimeStruct = New DateTimeStruct(oDate, CTimeZoneManager.Zone.Mountain)

			' ADD 1 TO MINUTES  AND START COMPARING
			Dim oOpponent As DateTime = oStruct.ToDateTime().AddMinutes(1)
			Dim oOpponentStruct As DateTimeStruct = oStruct.AddMinutes(1)

			Assert.IsTrue(oStruct < oOpponentStruct)
			Assert.IsTrue(oStruct < oOpponent)
			Assert.IsFalse(oOpponent < oStruct)

			Assert.IsTrue(oStruct <= oOpponentStruct)
			Assert.IsTrue(oStruct <= oOpponent)
			Assert.IsFalse(oOpponent <= oStruct)

			Assert.IsFalse(oStruct > oOpponentStruct)
			Assert.IsFalse(oStruct > oOpponent)
			Assert.IsTrue(oOpponent > oStruct)

			Assert.IsFalse(oStruct >= oOpponentStruct)
			Assert.IsFalse(oStruct >= oOpponent)
			Assert.IsTrue(oOpponent >= oStruct)

			Assert.IsFalse(oStruct = oOpponentStruct)
			Assert.IsFalse(oStruct = oOpponent)
			Assert.IsFalse(oOpponent = oStruct)
			Assert.IsTrue(oOpponent = oOpponentStruct)

			Assert.IsTrue(oStruct <> oOpponentStruct)
			Assert.IsTrue(oStruct <> oOpponent)
			Assert.IsTrue(oOpponent <> oStruct)
			Assert.IsFalse(oOpponent <> oOpponentStruct)
		End Sub
		<Test()> Public Sub DateTimeStruct_UTCTypeStaysConsistent()
			Dim oDate As DateTime = New Date(2000, 5, 5, 5, 5, 5, 500)
			Dim oUtcDate As DateTime = DateTime.SpecifyKind(oDate, DateTimeKind.Utc)
			Dim oStruct As DateTimeStruct = New DateTimeStruct(oUtcDate, CTimeZoneManager.Zone.UTC)

			Dim oOutputDate As DateTime = oStruct.ToDateTime()

			Assert.AreEqual(5, oOutputDate.Minute)
		End Sub

		<Test()> Public Sub DateTimeStruct_TestActionOperators_Subtract()
			Dim oDate As DateTime = New Date(2000, 5, 5, 5, 5, 5, 500)
			Dim oStruct As DateTimeStruct = New DateTimeStruct(oDate)

			' ADD 1 TO MINUTES  AND START COMPARING
			Dim oOpponent As DateTime = oStruct.ToDateTime().AddMinutes(1)
			Dim oOpponentStruct As DateTimeStruct = oStruct.AddMinutes(1)

			Dim oSpan As TimeSpan = oOpponentStruct - oStruct

			Assert.AreEqual(60, oSpan.TotalSeconds)

			Dim oSetSpan As TimeSpan = New TimeSpan(0, 0, 3, 0, 0)

			Dim oResult As DateTimeStruct = oStruct - oSetSpan

			Assert.AreEqual(2, oResult.Minute)

		End Sub
		<Test()> Public Sub DateTimeStruct_TestActionOperators_Add()
			Dim oDate As DateTime = New Date(2000, 5, 5, 5, 5, 5, 500)
			Dim oStruct As DateTimeStruct = New DateTimeStruct(oDate)

			Dim oSetSpan As TimeSpan = New TimeSpan(0, 0, 3, 0, 0)

			Dim oResult As DateTimeStruct = oStruct + oSetSpan

			Assert.AreEqual(8, oResult.Minute)
		End Sub
		<Test()> Public Sub DateTimeStruct_TestConvertTimeZoneUTC()
			Dim oDate As DateTime = DateTime.Now
			Dim oStruct As DateTimeStruct = New DateTimeStruct(oDate, CTimeZoneManager.LocalZone)
			Dim oUTCStruct As DateTimeStruct = oStruct.ToTimeZone(CTimeZoneManager.Zone.UTC)

			Dim oOutput As DateTime = oUTCStruct.ToDateTime()
			Dim oActual As DateTime = DateTime.UtcNow

			Assert.IsTrue(AreTicksClose(oActual.Ticks, oOutput.Ticks))

		End Sub
		<Test()> Public Sub DateTimeStruct_TestEqualsFunctions()
			Dim oDate As DateTime = DateTime.Now
			Dim oStruct As DateTimeStruct = New DateTimeStruct(oDate)
			Dim oStruct2 As DateTimeStruct = New DateTimeStruct(oDate)
			Dim oStruct3 As Object = New DateTimeStruct(oDate)
			Dim oDateStruct As DateStruct = New DateStruct(oDate)

			Assert.IsTrue(oStruct.Equals(oStruct2))
			Assert.IsTrue(oStruct.Equals(oDate))
			Assert.IsTrue(oStruct.Equals(oStruct3))
			Assert.IsTrue(oStruct.Equals(oDateStruct))
			oStruct = oStruct.AddMinutes(1)
			Assert.IsFalse(oStruct.Equals(oStruct2))
			Assert.IsFalse(oStruct.Equals(oDate))
			Assert.IsFalse(oStruct.Equals(oStruct3))
			oStruct = oStruct.AddMinutes(-1)
			Assert.IsTrue(oStruct.Equals(oStruct2))
			Assert.IsTrue(oStruct.Equals(oDate))
			Assert.IsTrue(oStruct.Equals(oStruct3))

			oStruct = oStruct.AddDays(1)
			Assert.IsFalse(oStruct.Equals(oDateStruct))
			oStruct = oStruct.AddDays(-1)
			Assert.IsTrue(oStruct.Equals(oDateStruct))

		End Sub
		<Test()> Public Sub DateTimeStruct_TestGetHashCode()
			'have in utc and no millis to get around hashing differences
			Dim oDate As DateTime = New DateTime(2000, 1, 2, 3, 4, 5, 0, DateTimeKind.Utc)
			Dim oStruct As DateTimeStruct = New DateTimeStruct(oDate, CTimeZoneManager.Zone.UTC)
			Assert.AreEqual(oDate.GetHashCode, oStruct.GetHashCode)
		End Sub
		<Test()> Public Sub DateTimeStruct_TestCompareTo()
			Dim oDate As DateTime = New Date(2000, 5, 5, 5, 5, 5, 500)
			Dim oStruct As DateTimeStruct = New DateTimeStruct(oDate)
			Dim oStructDupe As DateTimeStruct = New DateTimeStruct(oDate)
			Assert.AreEqual(0, oStruct.CompareTo(oDate))
			Assert.AreEqual(0, oStruct.CompareTo(oStructDupe))

			Dim oDate2 As DateTime = oDate.AddDays(1)
			Dim oStruct2 As DateTimeStruct = New DateTimeStruct(oDate2)
			Assert.Less(oStruct.CompareTo(oDate2), 0)
			Assert.Less(oStruct.CompareTo(oStruct2), 0)

			Dim oDate3 As DateTime = oDate.AddDays(-1)
			Dim oStruct3 As DateTimeStruct = New DateTimeStruct(oDate3)
			Assert.Greater(oStruct.CompareTo(oDate3), 0)
			Assert.Greater(oStruct.CompareTo(oStruct3), 0)
		End Sub
		<Test()> Public Sub DateTimeStruct_TestCompareToDateStruct()
			Dim oDate As DateTime = New Date(2000, 5, 5, 5, 5, 5, 500)
			Dim oStruct As DateTimeStruct = New DateTimeStruct(oDate)
			Dim oDateStruct As DateStruct = New DateStruct(oDate)

			Assert.AreEqual(0, oStruct.CompareTo(oDateStruct))

			Dim oDate2 As DateTime = oDate.AddDays(1)
			Dim oDateStruct2 As DateStruct = New DateStruct(oDate2)
			Assert.Less(oStruct.CompareTo(oDateStruct2), 0)

			Dim oDate3 As DateTime = oDate.AddDays(-1)
			Dim oDateStruct3 As DateStruct = New DateStruct(oDate3)
			Assert.Greater(oStruct.CompareTo(oDate3), 0)
			Assert.Greater(oStruct.CompareTo(oDateStruct3), 0)

			' finally add a miniscule amount and date struct and the date will not change.
			' ONLY WORKS BECAUSE WE HARDCODED DATE, NOT USE DATE NOW, OTHERWISE FLAKEY
			Dim oDate4 As DateTime = oDate.AddMinutes(1)
			Dim oDateStruct4 As DateStruct = New DateStruct(oDate4)

			Assert.AreEqual(0, oStruct.CompareTo(oDateStruct4))
		End Sub
		<Test()> Public Sub DateTimeStruct_TestConvertTimeToUTCSafe_ThrowsAndFallsBack()
			Dim oDateInvalid As DateTime = New DateTime(2007, 3, 11, 2, 30, 0)
			Dim oStruct As DateTimeStruct = New DateTimeStruct(oDateInvalid)
			'the exception is caught but as a result it adds an hour
			Assert.AreEqual(3, oStruct.Hour)
		End Sub
		<Test()> Public Sub DateTimeStruct_ZoneName()
			Dim oStruct As DateTimeStruct = DateTimeStruct.Now().ToTimeZone(CTimeZoneManager.Zone.UTC)
			' utc happens to only be called UTC, and doesnt have a daylight time version of it.
			Assert.AreEqual("UTC", oStruct.ZoneDisplayName)
			Assert.AreEqual("UTC", oStruct.ZoneStandardName)
			Assert.AreEqual("UTC", oStruct.ZoneDaylightName)
			Assert.AreEqual("UTC", oStruct.ZoneCurrentName)

			'11/4/2007 1:30:00 AM is when daylight savings ends
			Dim oDaylightSavings As DateTime = New Date(2007, 11, 4, 0, 30, 0)
			Dim oDaylightSavingsStruct As DateTimeStruct = New DateTimeStruct(oDaylightSavings).ToTimeZone(CTimeZoneManager.Zone.Pacific)
			Assert.AreEqual("Pacific Time", oDaylightSavingsStruct.ZoneDisplayName)
			Assert.AreEqual("Pacific Standard Time", oDaylightSavingsStruct.ZoneStandardName)
			Assert.AreEqual("Pacific Daylight Time", oDaylightSavingsStruct.ZoneDaylightName)
			Assert.AreEqual("Pacific Daylight Time", oDaylightSavingsStruct.ZoneCurrentName)
			oDaylightSavingsStruct = oDaylightSavingsStruct.AddDays(1)
			Assert.AreEqual("Pacific Standard Time", oDaylightSavingsStruct.ZoneCurrentName)
		End Sub

		<Test()> Public Sub DateTimeStruct_TestDateTimeOffset()
			Dim oStruct As DateTimeStruct = DateTimeStruct.Now(CTimeZoneManager.Zone.UTC)
			Dim oDateTime As DateTime = oStruct.ToDateTime
			Dim dto As DateTimeOffset = oStruct.ToDateTimeOffset

			Assert.AreEqual(oDateTime.Ticks, dto.Ticks)
			Assert.AreEqual(New TimeSpan(0, 0, 0), dto.Offset)
		End Sub

		<Test()> Public Sub DateTimeStruct_TestDateTimeOffsetWithMilliSecond()
			Dim oStruct As DateTimeStruct = DateTimeStruct.NowWithMilliSecond(CTimeZoneManager.Zone.UTC)
			Dim oDateTime As DateTime = oStruct.ToDateTime
			Dim dto As DateTimeOffset = oStruct.ToDateTimeOffset

			Assert.AreEqual(oDateTime.Ticks, dto.Ticks)
			Assert.AreEqual(New TimeSpan(0, 0, 0), dto.Offset)
		End Sub

		<Test()> Public Sub DateTimeStruct_ConvertUserShortDateStringToDateTime_Success()
			Dim oDate As Date = DateTimeStruct.ConvertUserShortDateStringToDateTime("2021-10-21")
			Dim oExpectedDate As Date = New Date(2021, 10, 21, 12, 0, 0)
			Dim oDiff As TimeSpan = oExpectedDate.Subtract(oDate)
			Assert.AreEqual(oDiff.TotalDays, 0)
			Assert.AreEqual(oDiff.TotalSeconds, 0)
		End Sub

		<Test()> Public Sub DateTimeStruct_ConvertUserShortDateStringToDateTime_InvalidDateString()
			Dim oException = Assert.Throws(Of Exception)(Sub() DateTimeStruct.ConvertUserShortDateStringToDateTime("2021-13-21"))

			Assert.IsTrue(oException.Message.Contains("Error in ConvertStringDateToDateTime"))
		End Sub

		<Test()> Public Sub DateTimeStruct_LoadDateWithFallbackIfDateIsInvalid_Success()
			Dim oPrimaryDate As Date = New Date(2021, 10, 21, 12, 0, 0)
			Dim oFallbackDate As Date = New Date(2020, 8, 1, 12, 0, 0)
			Dim oLoadedDate = DateTimeStruct.LoadDateWithFallbackIfDateIsInvalid(oPrimaryDate, oFallbackDate)

			Dim oDiff As TimeSpan = oLoadedDate.Subtract(oPrimaryDate)
			Assert.AreEqual(oDiff.TotalDays, 0)
			Assert.AreEqual(oDiff.TotalSeconds, 0)
		End Sub

		<Test()> Public Sub DateTimeStruct_LoadDateWithFallbackIfDateIsInvalid_FailAndFallback()
			Dim oPrimaryDate As Date = New Date(1, 1, 1, 12, 0, 0)
			Dim oFallbackDate As Date = New Date(2020, 8, 1, 12, 0, 0)
			Dim oLoadedDate = DateTimeStruct.LoadDateWithFallbackIfDateIsInvalid(oPrimaryDate, oFallbackDate)

			Dim oDiff As TimeSpan = oLoadedDate.Subtract(oFallbackDate)
			Assert.AreEqual(oDiff.TotalDays, 0)
			Assert.AreEqual(oDiff.TotalSeconds, 0)
		End Sub

#End Region

#Region "DateSTruct"

		<Test()> Public Sub DateStruct_TestComparisonOperators()
			Dim oDate As Date = New Date(2000, 1, 1, 1, 1, 1, 1)
			Dim oStruct As DateStruct = New DateStruct(oDate)
			Dim oOpponent As Date = oDate.AddDays(1)
			Dim oOpponentStruct As DateStruct = New DateStruct(oOpponent)

			' ADD 1 TO MINUTES  AND START COMPARING

			Assert.IsTrue(oStruct <oOpponentStruct)
			Assert.IsTrue(oStruct < oOpponent)
			Assert.IsFalse(oOpponent < oStruct)

			Assert.IsTrue(oStruct <= oOpponentStruct)
			Assert.IsTrue(oStruct <= oOpponent)
			Assert.IsFalse(oOpponent <= oStruct)

			Assert.IsFalse(oStruct > oOpponentStruct)
			Assert.IsFalse(oStruct > oOpponent)
			Assert.IsTrue(oOpponent > oStruct)

			Assert.IsFalse(oStruct >= oOpponentStruct)
			Assert.IsFalse(oStruct >= oOpponent)
			Assert.IsTrue(oOpponent >= oStruct)

			Assert.IsFalse(oStruct = oOpponentStruct)
			Assert.IsFalse(oStruct = oOpponent)
			Assert.IsFalse(oOpponent = oStruct)
			Assert.IsTrue(oOpponent = oOpponentStruct)

			Assert.IsTrue(oStruct <> oOpponentStruct)
			Assert.IsTrue(oStruct <> oOpponent)
			Assert.IsTrue(oOpponent <> oStruct)
			Assert.IsFalse(oOpponent <> oOpponentStruct)
		End Sub
		<Test()> Public Sub DateStruct_TestEqualsFunctions()
			Dim oDate As DateTime = DateTime.Now
			Dim oStruct As DateStruct = New DateStruct(oDate)
			Dim oStruct2 As DateStruct = New DateStruct(oDate)
			Dim oStruct3 As Object = New DateStruct(oDate)
			Dim oDate2 As Object = oDate

			Assert.IsTrue(oStruct.Equals(oStruct2))
			Assert.IsTrue(oStruct.Equals(oDate))
			Assert.IsTrue(oStruct.Equals(oStruct3))
			oStruct = oStruct.AddDays(1)
			Assert.IsFalse(oStruct.Equals(oStruct2))
			Assert.IsFalse(oStruct.Equals(oDate))
			Assert.IsFalse(oStruct.Equals(oStruct3))
			oStruct = oStruct.AddDays(-1)
			Assert.IsTrue(oStruct.Equals(oStruct2))
			Assert.IsTrue(oStruct.Equals(oDate))
			Assert.IsTrue(oStruct.Equals(oStruct3))

			Assert.IsTrue(oStruct.Equals(oDate2))
		End Sub
		<Test()> Public Sub DateStruct_TestCompareTo()
			Dim oDate As DateTime = New Date(2000, 5, 5, 0, 0, 0, 0)
			Dim oStruct As DateStruct = New DateStruct(oDate)
			Dim oStructDupe As DateStruct = New DateStruct(oDate)
			Assert.AreEqual(0, oStruct.CompareTo(oDate))
			Assert.AreEqual(0, oStruct.CompareTo(oStructDupe))

			Dim oDate2 As DateTime = oDate.AddDays(1)
			Dim oStruct2 As DateStruct = New DateStruct(oDate2)
			Assert.Less(oStruct.CompareTo(oDate2), 0)
			Assert.Less(oStruct.CompareTo(oStruct2), 0)

			Dim oDate3 As DateTime = oDate.AddDays(-1)
			Dim oStruct3 As DateStruct = New DateStruct(oDate3)
			Assert.Greater(oStruct.CompareTo(oDate3), 0)
			Assert.Greater(oStruct.CompareTo(oStruct3), 0)
		End Sub
		<Test()> Public Sub DateStruct_TestAddFunctions()
			Dim oStruct As DateStruct = New DateStruct(2000, 1, 1)

			Assert.AreEqual(2010, oStruct.AddYears(10).Year)
			Assert.AreEqual(6, oStruct.AddMonths(5).Month)
			Assert.AreEqual(13, oStruct.AddDays(12).Day)
		End Sub
		<Test()> Public Sub DateStruct_TestCTypes()
			Dim oStruct As DateStruct = New DateStruct(2000, 10, 20)

			Assert.AreEqual("10/20/2000 12:00:00 AM", CType(oStruct, Date).ToString)
			Assert.AreEqual("10/20/2000", CType(oStruct, String))

			Dim oDate As Date = New Date(2000, 10, 20)
			Dim oStruct2 As DateStruct = CType(oDate, DateStruct)
			Assert.AreEqual(oStruct, oStruct2)

			Try
				Dim obj = CType("thisfakedate", DateStruct)
				Assert.Fail("Should have thrown a Format Exception")
			Catch ex As FormatException
				'this is expected
			End Try

			Dim validDate As DateStruct = CType("2000/3/20", DateStruct)

			Assert.AreEqual(2000, validDate.Year)
			Assert.AreEqual(3, validDate.Month)
			Assert.AreEqual(20, validDate.Day)

		End Sub
		<Test()> Public Sub DateStruct_ToString()
			Dim oStruct As DateStruct = New DateStruct(2000, 10, 20)

			Assert.AreEqual("October 20", oStruct.ToString("MMMM dd"))
			Assert.AreEqual("10/20/2000", oStruct.ToShortDateString)

		End Sub
		<Test()> Public Sub DateStruct_Now()
			Dim oStruct As DateStruct = DateStruct.Now()
			Dim oDate As DateTime = DateTime.Now()

			Assert.AreEqual(oDate.Year, oStruct.Year)
			Assert.AreEqual(oDate.Month, oStruct.Month)
			Assert.AreEqual(oDate.Day, oStruct.Day)

			Dim oStructUTC As DateStruct = DateStruct.Now(CTimeZoneManager.Zone.UTC)
			Dim oDateUTC As DateTime = DateTime.UtcNow()

			Assert.AreEqual(oDateUTC.Year, oStructUTC.Year)
			Assert.AreEqual(oDateUTC.Month, oStructUTC.Month)
			Assert.AreEqual(oDateUTC.Day, oStructUTC.Day)

		End Sub

		<Test()> Public Sub CTimeZoneManager_DefaultTimeZone()
			' test unset
			AppSettings("MACHINE_TIME_ZONE") = ""
			Assert.AreEqual(CTimeZoneManager.Zone.LosAngeles, CTimeZoneManager.LocalZone)

		End Sub
		<Test()> Public Sub CTimeZoneManager_UnhandledEnum()
			Try
				CTimeZoneManager.GetTimeZoneAbbreviation(CType(123456, CTimeZoneManager.Zone), False)
				Assert.Fail("There probably shouldnt be an enum withh value 123456")
			Catch ex As Exception
				'pass
			End Try

			Try
				CTimeZoneManager.MapTimeZoneToID(CType(123456, CTimeZoneManager.Zone))
				Assert.Fail("There probably shouldnt be an enum withh value 123456")
			Catch ex As Exception
				'pass
			End Try

		End Sub
		'

#End Region

	End Class
End Namespace
