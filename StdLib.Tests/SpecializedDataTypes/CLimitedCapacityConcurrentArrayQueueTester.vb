﻿Option Strict On
Imports NUnit.Framework
Imports StdLib.SpecializedDataTypes

Namespace SpecializedDataTypesTests

	<TestFixture()> Public Class CLimitedCapacityConcurrentArrayQueueTester

		<Test()> Public Sub TestQueueSizeCapped()

			Dim QueueArray As New CLimitedCapacityConcurrentArrayQueue(Of Integer)(20)

			For count = 1 To 10
				Dim arr As Integer() = {}
				ReDim arr(2)
				Assert.AreEqual(3, arr.Count)
				For value = 1 To 3
					arr(value - 1) = value
				Next
				QueueArray.Enqueue(arr)
			Next
			''after this runs we expect a queue of arrays {1,2,3} 6 times.
			'' 18 objects just less than 20

			Dim sum As Integer = 0
			While QueueArray.Count > 0
				Dim arr As Integer() = Nothing
				If QueueArray.TryDequeue(arr) Then
					For Each value As Integer In arr
						sum += value
					Next
				End If
			End While
			Assert.AreEqual(36, sum)

		End Sub


		<Test()> Public Sub TestFIFO()

			Dim QueueArray As New CLimitedCapacityConcurrentArrayQueue(Of Integer)(10)

			QueueArray.Enqueue({1, 1, 1, 1, 1})
			QueueArray.Enqueue({2, 2, 2, 2, 2})
			QueueArray.Enqueue({3, 3, 3, 3, 3})

			Dim sum As Integer = 0

			Dim arr As Integer() = Nothing
			If Not QueueArray.TryDequeue(arr) Then
				Assert.Fail("There should be an array in here")
			End If
			For index = 0 To 4
				Assert.AreEqual(arr(index), 2)
			Next
			If Not QueueArray.TryDequeue(arr) Then
				Assert.Fail("There should be an array in here")
			End If
			For index = 0 To 4
				Assert.AreEqual(arr(index), 3)
			Next

		End Sub

		<Test()> Public Sub TestTooBigDude()

			Dim QueueArray As New CLimitedCapacityConcurrentArrayQueue(Of Integer)(5)

			''make sure no infinite loop as 
			' it Is impossible to loop while this queue.count + input.count < max
			QueueArray.Enqueue({0, 1, 2, 3, 4, 5, 6, 7, 8, 9})


			Dim arr As Integer() = Nothing
			If Not QueueArray.TryDequeue(arr) Then
				Assert.Fail("There should be an array in here")
			End If
			For index = 0 To 9
				Assert.AreEqual(arr(index), index)
			Next

		End Sub

		<Test()> Public Sub TestSpamInsert()

			Dim QueueArray As New CLimitedCapacityConcurrentArrayQueue(Of Byte)(1000)

			For index = 0 To 100000
				QueueArray.Enqueue({0, 1, 2, 3, 4, 5, 6, 7, 8, 9})
			Next

			Assert.IsTrue(QueueArray.Count <= 100,
						  "There should not be that more than 100 items in the queue")

			Dim numCount As Integer = 0
			While QueueArray.Count > 0
				Dim arr As Byte() = Nothing
				If QueueArray.TryDequeue(arr) Then
					numCount += arr.Count
				End If
			End While
			Assert.AreEqual(1000, numCount)

		End Sub

	End Class

End Namespace