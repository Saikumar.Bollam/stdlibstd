Imports NUnit.Framework
Imports StdLib.Text
Imports StdLib.Text.CPhoneNumber

Namespace TextTests

	<TestFixture()>
	Public Class CPhoneParserTester
		<Test()> Public Sub testPhoneParser()
			Dim oParser As New CPhoneNumber("(123)456-1234")

			Assert.IsTrue(oParser.AreaCode = "123", "Failed to parse:" & oParser.FullPhoneNumber)
			Assert.IsTrue(oParser.First3 = "456", "Failed to parse:" & oParser.FullPhoneNumber)
			Assert.IsTrue(oParser.Second4 = "1234", "Failed to parse:" & oParser.FullPhoneNumber)


			oParser.FullPhoneNumber = "(123) 456-1234x555"
			Assert.IsTrue(oParser.AreaCode = "123", "Failed to parse:" & oParser.FullPhoneNumber)
			Assert.IsTrue(oParser.First3 = "456", "Failed to parse:" & oParser.FullPhoneNumber)
			Assert.IsTrue(oParser.Second4 = "1234", "Failed to parse:" & oParser.FullPhoneNumber)
			Assert.IsTrue(oParser.Extension = "555", "Failed to parse:" & oParser.FullPhoneNumber)

			oParser.FullPhoneNumber = "(123) 456-1234"
			Assert.IsTrue(oParser.AreaCode = "123", "Failed to parse:" & oParser.FullPhoneNumber)
			Assert.IsTrue(oParser.First3 = "456", "Failed to parse:" & oParser.FullPhoneNumber)
			Assert.IsTrue(oParser.Second4 = "1234", "Failed to parse:" & oParser.FullPhoneNumber)

			oParser.FullPhoneNumber = "123-456-1234"
			Assert.IsTrue(oParser.AreaCode = "123", "Failed to parse:" & oParser.FullPhoneNumber)
			Assert.IsTrue(oParser.First3 = "456", "Failed to parse:" & oParser.FullPhoneNumber)
			Assert.IsTrue(oParser.Second4 = "1234", "Failed to parse:" & oParser.FullPhoneNumber)

			oParser.FullPhoneNumber = "(123) 456-1234"
			Assert.IsTrue(oParser.AreaCode = "123", "Failed to parse:" & oParser.FullPhoneNumber)
			Assert.IsTrue(oParser.First3 = "456", "Failed to parse:" & oParser.FullPhoneNumber)
			Assert.IsTrue(oParser.Second4 = "1234", "Failed to parse:" & oParser.FullPhoneNumber)

			oParser.FullPhoneNumber = "1234561234"
			Assert.IsTrue(oParser.AreaCode = "123", "Failed to parse:" & oParser.FullPhoneNumber)
			Assert.IsTrue(oParser.First3 = "456", "Failed to parse:" & oParser.FullPhoneNumber)
			Assert.IsTrue(oParser.Second4 = "1234", "Failed to parse:" & oParser.FullPhoneNumber)

			'bad #'s
			Assert.IsTrue(isPhoneNumber("() 456-1234") = False, "Should have fail to parse:" & oParser.FullPhoneNumber)
			Assert.IsTrue(isPhoneNumber("(222 456-1234") = False, "Should have fail to parse:" & oParser.FullPhoneNumber)
			Assert.IsTrue(isPhoneNumber("-456-1234") = False, "Should have fail to parse:" & oParser.FullPhoneNumber)
			'Assert.IsTrue(oParser.isPhoneNumber("153456-1234") = False, "Should have fail to parse:" & oParser.FullPhoneNumber)

		End Sub
	End Class
End Namespace
