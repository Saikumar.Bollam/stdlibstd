Option Strict On
Imports NUnit.Framework
Imports System.Collections.Generic
Imports StdLib.Text

Namespace TextTests

	<TestFixture()> _
	Public Class CSecureStringFormatterTester

		<Test()> _
		Public Sub testMaskSensitiveData()
			Dim oTestData As New List(Of KeyValuePair(Of String, String))
			With oTestData
				.Add(New KeyValuePair(Of String, String)("UID=flaguser;PWD=secret;DATABASE=Vehicle", "UID=flaguser;PWD=xxxxxt;DATABASE=Vehicle"))
				.Add(New KeyValuePair(Of String, String)("UID=flaguser;PWD=mine;DATABASE=Vehicle", "UID=flaguser;PWD=xxxxx;DATABASE=Vehicle"))
				.Add(New KeyValuePair(Of String, String)("UID=flaguser;PWD: mine", "UID=flaguser;PWD: xxxxx"))
				.Add(New KeyValuePair(Of String, String)("UID=flaguser;password=mine" & vbCrLf & "SUFFIX", "UID=flaguser;password=xxxxx" & vbCrLf & "SUFFIX"))
				.Add(New KeyValuePair(Of String, String)("test=nomatch;" & vbCrLf & "SUFFIX", "test=nomatch;" & vbCrLf & "SUFFIX"))
				.Add(New KeyValuePair(Of String, String)("test=multimatch;password=123;repeat=0;pwd=222;pWD=333;password=444", "test=multimatch;password=xxxxx;repeat=0;pwd=xxxxx;pWD=xxxxx;password=xxxxx"))
			End With

			For Each oPair As KeyValuePair(Of String, String) In oTestData
				Assert.AreEqual(oPair.Value, CSecureStringFormatter.MaskSensitiveData(oPair.Key))
			Next
		End Sub

		<Test()>
		Public Sub testMaskSensitiveXMLDataCDATA()
			Dim xTestDoc As XElement = <DOC password="test" encoding="CDATA">
										   <![CDATA[<TEST password="123"><PASSWORD>test</PASSWORD></TEST>]]>
										   <![CDATA[test=string;password=test;pwd=testquotes;]]>
									   </DOC>

			Dim xExpectedResult As XElement = <DOC password="xxxxx" encoding="CDATA">
												  <![CDATA[<TEST password="xxxxx"><PASSWORD>xxxxx</PASSWORD></TEST>]]>
												  <![CDATA[test=string;password=xxxxx;pwd=xxxxxuotes;]]>
											  </DOC>

			Assert.AreEqual(xExpectedResult.ToString(), CSecureStringFormatter.MaskSensitiveXMLData(xTestDoc.ToString()))
		End Sub

		<Test()>
		Public Sub MaskSensitiveXMLData_VerifyCDATA_TagNameContainsSensitiveTagName_ShouldMask()
			Dim sXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?><ICS xmlns=""http://xml.equifax.com/XMLSchema/Test""><PlainText><![CDATA[<USIdentificationSSN><SubjectSSN>123456789</SubjectSSN><InquirySubjectSSN>123456789</InquirySubjectSSN></USIdentificationSSN>]]></PlainText></ICS>"

			Dim sExpectedResult As String = "<ICS xmlns=""http://xml.equifax.com/XMLSchema/Test""><PlainText><![CDATA[<USIdentificationSSN><SubjectSSN>xxxxx</SubjectSSN><InquirySubjectSSN>xxxxx</InquirySubjectSSN></USIdentificationSSN>]]></PlainText></ICS>"

			Dim sResult As String = CSecureStringFormatter.MaskSensitiveXMLData(sXML)

			Assert.AreEqual(sExpectedResult, sResult)
		End Sub


		<Test()>
		Public Sub MaskSensitiveXMLData_VerifyXML_TagNameContainsSensitiveTagName_ShouldMask()
			Dim sXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?><ICS xmlns=""http://xml.equifax.com/XMLSchema/Test""><PlainTextSSN>123456789</PlainTextSSN></ICS>"

			Dim sExpectedResult As String = "<ICS xmlns=""http://xml.equifax.com/XMLSchema/Test""><PlainTextSSN>xxxxx</PlainTextSSN></ICS>"

			Dim sResult As String = CSecureStringFormatter.MaskSensitiveXMLData(sXML)

			Assert.AreEqual(sExpectedResult, sResult)
		End Sub

		<Test()> _
		Public Sub testMaskSensitiveXMLData()
			Dim sTestDoc As String = _
			 "<doc>" & _
			 "	<PASSWORD>" & _
			 "		secret" & _
			 "	</PASSWORD>" & _
			 "	<pwd>" & _
			 "		<nested>" & _
			 "			not a secret" & _
			 "		</nested>" & _
			 "	</pwd>" & _
			 "	<deeper>" & _
			 "		<pW>secret</pW>" & _
			 "	</deeper>" & _
			 "	<attribute PasSWoRd=""secret""/>" & _
			"	<attribute PWD=""secret""/>" & _
			 "</doc>"

			Dim sExpectedResult As String = _
			 "<doc>" & _
			 "	<PASSWORD>xxxxx</PASSWORD>" & _
			 "	<pwd>" & _
			 "		<nested>" & _
			 "			not a secret" & _
			 "		</nested>" & _
			 "	</pwd>" & _
			 "	<deeper>" & _
			 "		<pW>xxxxx</pW>" & _
			 "	</deeper>" & _
			 "	<attribute PasSWoRd=""xxxxx""/>" & _
			 "	<attribute PWD=""xxxxx""/>" & _
			 "</doc>"

			Dim oExpected As New Xml.XmlDocument
			oExpected.LoadXml(sExpectedResult)
			Assert.AreEqual(oExpected.InnerXml, CSecureStringFormatter.MaskSensitiveXMLData(sTestDoc))

		End Sub

		<Test()> _
		Public Sub testMaskSensitiveXMLDataWithNoSensitiveData()
			Dim sTestDoc As String = _
			 "<doc>" & _
			 "	<Duh>" & _
			 "		not secret" & _
			 "	</Duh>" & _
			 "	<Foo>" & _
			 "		<nested>" & _
			 "			not a secret" & _
			 "		</nested>" & _
			 "	</Foo>" & _
			 "</doc>"

			Dim sExpectedResult As String = _
			  "<doc>" & _
			 "	<Duh>" & _
			 "		not secret" & _
			 "	</Duh>" & _
			 "	<Foo>" & _
			 "		<nested>" & _
			 "			not a secret" & _
			 "		</nested>" & _
			 "	</Foo>" & _
			 "</doc>"

			Dim oExpected As New Xml.XmlDocument
			oExpected.LoadXml(sExpectedResult)
			Assert.AreEqual(oExpected.InnerXml, CSecureStringFormatter.MaskSensitiveXMLData(sTestDoc))

		End Sub

	End Class
End Namespace
