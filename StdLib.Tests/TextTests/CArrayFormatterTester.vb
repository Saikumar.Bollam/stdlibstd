Imports NUnit.Framework
Imports StdLib.Text

Namespace TextTests

	<NUnit.Framework.TestFixture()> _
	Public Class CArrayFormatterTester

		Private _List As System.Collections.ArrayList

		<SetUpAttribute()> _
		 Public Sub setupTest()
			_List = New System.Collections.ArrayList()
			_List.Add("Orange")
			_List.Add("Peach")
			_List.Add("Apple")

		End Sub

		<NUnit.Framework.Test()> Public Sub testGenerateHTMLList()

			NUnit.Framework.Assert.AreEqual("<li>Orange</li><li>Peach</li><li>Apple</li>", CArrayFormatter.FormatAsHTMLItems(_List))

			NUnit.Framework.Assert.AreEqual("<ul><li>Orange</li><li>Peach</li><li>Apple</li></ul>", CArrayFormatter.FormatAsHTMList(_List, "ul"))


		End Sub


		<NUnit.Framework.Test()> Public Sub testJoinSimpleComma()


			NUnit.Framework.Assert.AreEqual("Orange,Peach,Apple", CArrayFormatter.FormatAsTextComma(_List))

			_List.Clear()
			NUnit.Framework.Assert.AreEqual("", CArrayFormatter.FormatAsTextComma(_List))

			_List.Add("Apple")
			NUnit.Framework.Assert.AreEqual("Apple", CArrayFormatter.FormatAsTextComma(_List))

		End Sub



	End Class


End Namespace