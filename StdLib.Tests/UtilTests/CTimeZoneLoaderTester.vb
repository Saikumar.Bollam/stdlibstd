﻿Option Strict On
Imports NUnit.Framework

Namespace UtilTests
    <TestFixture()>
    <Category("UNIT")>
    Public Class CTimeZoneLoaderTester

        Private _sut As ITimeZoneLoader

#Region "SetUp"
        <SetUp>
        Public Sub Setup()
            _sut = New CTimeZoneLoader()
        End Sub
#End Region

        <Test()>
        Public Sub IsTimeZoneConfigFileExists_InvalidPath_ReturnsFalse()
            Dim bIsFileExists As Boolean = _sut.IsTimeZoneConfigFileExists("C:\FileNotFoundPath.config")
            Assert.AreEqual(False, bIsFileExists)
        End Sub

        <Test()>
        Public Sub LoadTimeZoneInfo_ValidXML_ReturnsExpectedTimeZoneInfo()
            Dim oXDocument As XDocument = getSampleTimeZoneConfig()
            Dim oTimeZoneInfo As TimeZoneInfo = _sut.LoadTimeZoneInfo("America/New_York", oXDocument)
            Assert.AreEqual("-05:00:00", oTimeZoneInfo.BaseUtcOffset.ToString())
            Assert.AreEqual("Eastern Daylight Time", oTimeZoneInfo.DaylightName)
            Assert.AreEqual("Eastern Time", oTimeZoneInfo.DisplayName)
            Assert.AreEqual("America/New_York", oTimeZoneInfo.Id)
            Assert.AreEqual("Eastern Standard Time", oTimeZoneInfo.StandardName)
            Assert.AreEqual(True, oTimeZoneInfo.SupportsDaylightSavingTime)
        End Sub

        <Test()>
        Public Sub LoadTimeZoneInfo_ZoneNotFoundInXML_ThrowException()
            Dim oXDocument As XDocument = <?xml version="1.0" encoding="utf-8"?>
                                          <TIME_ZONES xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="C:\LoansPQ2\ConfigMap\TimeZones.xsd">
                                              <TIME_ZONE id="America/New_York" base_utc_offset="-5" display_name="Eastern Time" standard_display_name="Eastern Standard Time" daylight_display_name="Eastern Daylight Time">
                                              </TIME_ZONE>
                                          </TIME_ZONES>
            Assert.Throws(Of Exception)(Function() _sut.LoadTimeZoneInfo("America/Chicago", oXDocument))
        End Sub

        <Test()>
        Public Sub LoadTimeZoneInfo_MoreThanOneZoneFoundInXML_ThrowException()
            Dim oXDocument As XDocument = <?xml version="1.0" encoding="utf-8"?>
                                          <TIME_ZONES xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="C:\LoansPQ2\ConfigMap\TimeZones.xsd">
                                              <TIME_ZONE id="America/New_York" base_utc_offset="-5" display_name="Eastern Time" standard_display_name="Eastern Standard Time" daylight_display_name="Eastern Daylight Time">
                                              </TIME_ZONE>
                                              <TIME_ZONE id="America/New_York" base_utc_offset="-5" display_name="Eastern Time" standard_display_name="Eastern Standard Time" daylight_display_name="Eastern Daylight Time">
                                              </TIME_ZONE>
                                          </TIME_ZONES>
            Assert.Throws(Of Exception)(Function() _sut.LoadTimeZoneInfo("America/New_York", oXDocument))
        End Sub

        <Test()>
        Public Sub LoadTimeZoneInfo_InvalidDateRuleNode_ThrowException()
            Dim oXDocument As XDocument = <?xml version="1.0" encoding="utf-8"?>
                                          <TIME_ZONES xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="C:\LoansPQ2\ConfigMap\TimeZones.xsd">
                                              <TIME_ZONE id="America/New_York" base_utc_offset="-5" display_name="Eastern Time" standard_display_name="Eastern Standard Time" daylight_display_name="Eastern Daylight Time">
                                                  <ADJUSTMENT_RULES>
                                                      <ADJUSTMENT_RULE date_start="1974-01-01" date_end="1974-12-31">
                                                          <DAYLIGHT_TRANSITION_START>
                                                              <INVALID_DATE_RULE time_of_day='02:00:00' month='10' day='28'/>
                                                          </DAYLIGHT_TRANSITION_START>
                                                      </ADJUSTMENT_RULE>
                                                  </ADJUSTMENT_RULES>
                                              </TIME_ZONE>
                                          </TIME_ZONES>

            Assert.Throws(Of Exception)(Function() _sut.LoadTimeZoneInfo("America/New_York", oXDocument))
        End Sub

#Region "Helper Functions"
        Private Function getSampleTimeZoneConfig() As XDocument
            Dim oXDocument As XDocument = <?xml version="1.0" encoding="utf-8"?>
                                          <TIME_ZONES xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="C:\LoansPQ2\ConfigMap\TimeZones.xsd">
                                              <TIME_ZONE id="America/New_York" base_utc_offset="-5" display_name="Eastern Time" standard_display_name="Eastern Standard Time" daylight_display_name="Eastern Daylight Time">
                                                  <!--Standard US rules-->
                                                  <ADJUSTMENT_RULES>
                                                      <ADJUSTMENT_RULE date_start="1974-01-01" date_end="1974-12-31">
                                                          <DAYLIGHT_TRANSITION_START>
                                                              <FIXED_DATE_RULE time_of_day="02:00:00" month="1" day="6"/>
                                                          </DAYLIGHT_TRANSITION_START>
                                                          <DAYLIGHT_TRANSITION_END>
                                                              <FLOATING_DATE_RULE time_of_day="02:00:00" month="10" week="5" day_of_week="Sunday"/>
                                                          </DAYLIGHT_TRANSITION_END>
                                                      </ADJUSTMENT_RULE>
                                                  </ADJUSTMENT_RULES>
                                              </TIME_ZONE>
                                          </TIME_ZONES>

            Return oXDocument
        End Function
#End Region
    End Class
End Namespace
