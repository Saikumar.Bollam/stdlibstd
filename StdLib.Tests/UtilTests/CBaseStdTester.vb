Imports NUnit.Framework
Imports StdLib.CBaseStd
Imports StdLib.Exceptions
Imports StdLib.SpecializedDataTypes
Imports System.Threading.Tasks

Namespace UtilTests

	''' <summary>
	''' This class to privde test code for 
	''' </summary>
	''' <remarks></remarks>
	<NUnit.Framework.TestFixture()> _
	Public Class CBaseStdTester
		<Test()> Public Sub testAgeAt()
			Assert.AreEqual(18, CDateUtils.AgeAt(#6/24/1989#, #12/7/2007#))
			Assert.AreEqual(17, CDateUtils.AgeAt(#6/24/1989#, #6/7/2007#))
			Assert.AreEqual(18, CDateUtils.AgeAt(#6/24/1989#, #6/24/2007#))
			Assert.AreEqual(17, CDateUtils.AgeAt(#6/24/1989#, #5/7/2007#))
			Assert.AreEqual(18, CDateUtils.AgeAt(#6/24/1989#, #2/7/2008#))
			Assert.AreEqual(18, CDateUtils.AgeAt(#6/24/1989#, #6/7/2008#))
			Assert.AreEqual(19, CDateUtils.AgeAt(#6/24/1989#, #6/24/2008#))


			Assert.AreEqual(0, CDateUtils.AgeAt(#6/24/1989#, #6/24/1989#))
			Assert.AreEqual(0, CDateUtils.AgeAt(#6/24/1989#, #9/24/1989#))

			Assert.AreEqual(1, CDateUtils.AgeAt(#6/24/1989#, #6/24/1990#))

		End Sub

		<Test()> Public Sub testGetOneWayHashValue()
			Dim hashBlank As String = getOneWayHashValue("")
			Dim hashABC As String = getOneWayHashValue("ABC")

			Assert.AreNotEqual(hashBlank, hashABC)


		End Sub


		<Test()> Public Sub testFormatYear()
			Assert.AreEqual("2000", FormatYear(0, -1)) ' this is to account for scenarios where we store years as numerics
			Assert.AreEqual("", FormatYear(0, 0))
			Assert.AreEqual("2000", FormatYear(2000, 0))
			Assert.AreEqual("2001", FormatYear(1, -1))
			Assert.AreEqual("1919", FormatYear(19, -1))

			Assert.AreEqual("1980", FormatYear(1980, -1))
			Assert.AreEqual("1980", FormatYear(80, -1))

			Assert.AreEqual("", FormatYear(-1, -1))
			Assert.AreEqual("", FormatYear(5, 5))

		End Sub

		<Test()> Public Sub testValidateMultiThreadURL()
			'in this test we basically want to test that generatewhitelist is only called initially, meaning that most of the threads that are going to fire should            'finish relatively fast, a typical fresh compiled stdlibtest that runs validate url for first time can take between 4 million to 8 million ticks
			'while it finishes it causes however many threads to be blocked, causing those threads to also finish slow
			'usually 6 million ticks will block at most 2 other threads before the first validate url can finish meaning that if we run this with 10 threads at 
			'least 7 threads should finish in time. In this case will give some extra cushion (6) in case it takes longer so at least 4 threads needs to finish fast
			'else there might be some problem in the code, in case it takes too long well just exit and ignore the test.
			Dim oTimeList As New Concurrent.ConcurrentBag(Of Integer)
			Dim Totaltick As Long
			Dim oStopWatch As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
			Dim parallelOptions As New ParallelOptions

			parallelOptions.MaxDegreeOfParallelism = 10
			Parallel.For(0, 10, parallelOptions, Sub(i)
													 oStopWatch.Start()
													 ValidateURL("/login.aspx")
													 Totaltick = oStopWatch.ElapsedTicks
													 oTimeList.Add(Totaltick)
													 oStopWatch.Restart()
												 End Sub)

			Dim TICKTHRESHHOLDLOW As Integer = 2000000
			Dim TICKTHRESHHOLDHIGH As Integer = 40000000
			Dim TotalOverThreshHoldTicks As Integer = 0
			For Each iTicks As Integer In oTimeList
				'This check might potentially render the unit test useless in the event that this unit test is always tested right after high compile time,
				'if we ever transition into a consistent compile->test schematic we should probably revisit the threshhold constants to account for it.
				If iTicks > TICKTHRESHHOLDHIGH Then
					Assert.Ignore("One of the threads took too long this unit test is probably no longer valid")
					Return
				End If
				If iTicks > TICKTHRESHHOLDLOW Then
					TotalOverThreshHoldTicks += 1
				End If
			Next
			Assert.IsTrue(TotalOverThreshHoldTicks <= 6, "URL threads are taking too long, either a recent compile is causing sloweness or something wrong with threading")

		End Sub

		<Test()> Public Sub testValidateSingleThreadURL()

			Try
				CBaseStd.IsRelativePathDelegate = Function(pURL)
																  pURL = CBaseStd.CleanRelativeSubPathForValidateURL(pURL)
																  If pURL.StartsWith("/login") OrElse pURL.StartsWith("/images") OrElse _
																	  pURL.StartsWith("~/login") Then
																	  Return True
																  Else
																	  Return False
																  End If
															  End Function

				'valids these will pass because we allow /
				ValidateURL("/login.aspx")
				ValidateURL("~/login.aspx")
				ValidateURL("/login.aspx?logout=Y")
				ValidateURL("/login.aspx?javascript=blah")
				ValidateURL("/images/mygif.gif")

				'these should fail due to the security regex
				validateURLFailHelper("/login.aspx?javascript:alert('moo');")
				validateURLFailHelper("/login.aspx?x=<script>")
				validateURLFailHelper("/login.aspx?body_url=vBsCrIpT:MsgBox(56671)")

				'this should fail because it doesn't start with correct slash
				validateURLFailHelper("\login.aspx")

				'should pass because relative path check is turned off
				ValidateURL("\loginZthebest.aspx", False)

			Finally
				CBaseStd.IsRelativePathDelegate = Nothing
			End Try

		End Sub


		<Test()> Public Sub testCompareNumber_Double()
			Assert.IsTrue(CompareNumber(1 / 3, 1 / 3) = NumberCompareResults.EqualBoth)
			Assert.IsTrue(CompareNumber(0, 0) = NumberCompareResults.EqualBoth)
			Assert.IsTrue(CompareNumber(0.00001, 0.00001) = NumberCompareResults.EqualBoth)
			Assert.IsTrue(CompareNumber(-7, 7) = NumberCompareResults.BIsBigger)
			Assert.IsTrue(CompareNumber(0.00001, 0.00003) = NumberCompareResults.BIsBigger)
			Assert.IsTrue(CompareNumber(0.000005, 0.000003) = NumberCompareResults.AIsBigger)
			Assert.IsTrue(CompareNumber(0.00005, 1.00003) = NumberCompareResults.BIsBigger)
			Assert.IsTrue(CompareNumber(0.00005, 0.000055) = NumberCompareResults.BIsBigger)
			Assert.IsTrue(CompareNumber(0.0005, 0.0005) = NumberCompareResults.EqualBoth)
			Assert.IsTrue(CompareNumber(Round(1 / 3, 6), 1 / 3) = NumberCompareResults.BIsBigger)
			Assert.IsTrue(CompareNumber(Round(1 / 3, 7), 1 / 3) = NumberCompareResults.EqualBoth)
			Assert.IsTrue(CompareNumber(0.33334, 1 / 3) = NumberCompareResults.AIsBigger)

			Assert.IsTrue(CompareNumber(0.33333, 1 / 3) = NumberCompareResults.BIsBigger)

			'In floating terms, 1.05 - 1.05 = 0.010000000000000.......9;  this comparison against 0.01 is NOT equal unless we compare against epslion
			Assert.IsTrue(CompareNumber(1.05 - 1.04, 0.01, 2) = NumberCompareResults.EqualBoth)

			'these 2 as percentage displayed to 2 precision are logically the same..
			Assert.IsTrue(CompareNumber(1.052, 1.049, 2) = NumberCompareResults.EqualBoth)
			Assert.IsTrue(CompareNumber(1.055, 1.049, 2) = NumberCompareResults.AIsBigger) 'since 1.055 should get rounded to 1.06
		End Sub

		<Test()> Public Sub testYNStringToBool()
			Assert.AreEqual(True, YNStringToBool("Y"))
			Assert.AreEqual(False, YNStringToBool("N"))
			Assert.AreEqual(False, YNStringToBool(Nothing))
			Assert.AreEqual(False, YNStringToBool(String.Empty))
			Assert.AreEqual(False, YNStringToBool("blah"))
			Assert.AreEqual(False, YNStringToBool(DBNull.Value))
		End Sub

		<Test()> Public Sub testGetCommonType()
			'Direct inheritance
			Dim types As Type() = {GetType(CSet(Of Integer)), GetType(COrderedSet(Of Integer))}
			Assert.AreEqual(GetType(CSet(Of Integer)), GetCommonType(types))

			'Common base type
			types = New Type() {GetType(CURLParamException), GetType(CSetupException), GetType(CValidationException)}
			Assert.AreEqual(GetType(System.Exception), GetCommonType(types))
		End Sub

		<Test()> Public Sub testParseNumbers()
			'integer
			Assert.IsFalse(ParseInteger("abc").HasValue)
			Assert.IsFalse(ParseInteger("").HasValue)
			Assert.IsFalse(ParseInteger(System.DBNull.Value).HasValue)

			Assert.AreEqual(100, ParseInteger("100"))
			Assert.AreEqual(100, ParseInteger(CObj("100")))

			'long 
			Assert.IsFalse(ParseLong("abc").HasValue)
			Assert.IsFalse(ParseLong("").HasValue)
			Assert.IsFalse(ParseLong(System.DBNull.Value).HasValue)

			Assert.AreEqual(100, ParseLong("100"))
			Assert.AreEqual(100, ParseLong(CObj("100")))

			'double 
			Assert.IsFalse(ParseDouble("abc").HasValue)
			Assert.IsFalse(ParseDouble("").HasValue)
			Assert.IsFalse(ParseDouble(System.DBNull.Value).HasValue)

			Assert.AreEqual(100, ParseDouble("100"))
			Assert.AreEqual(100, ParseDouble(CObj("100")))

			'decimal 
			Assert.IsFalse(ParseDecimal("abc").HasValue)
			Assert.IsFalse(ParseDecimal("").HasValue)
			Assert.IsFalse(ParseDecimal(System.DBNull.Value).HasValue)

			Assert.AreEqual(100, ParseDecimal("100"))
			Assert.AreEqual(100, ParseDecimal(CObj("100")))
		End Sub




		<Test()> Public Sub testParseDate()
			'Complete date formats
			Dim testDate As New Date(2009, 7, 9)
			parseDateHelper(testDate, "2009-07-09")	'ISO 8601 sortable date (see http://en.wikipedia.org/wiki/ISO_8601#Calendar_dates)
			parseDateHelper(testDate, "09 Jul 2009") 'RFC 822 (As Updated by RFC 1123) Date (see http://www.hackcraft.net/web/datetime/#rfc822)
			parseDateHelper(testDate, "7/9/2009")
			parseDateHelper(testDate, "07/09/2009")
			parseDateHelper(testDate, "07/09/09")
			parseDateHelper(testDate, "7/9/09")
			parseDateHelper(Nothing, "13/9/09")	'Month-first is not handled

			'Month-day formats
			Dim januaryFirst As New Date(Date.Now.Year, 1, 1)
			parseDateHelper(januaryFirst, "1/1")
			parseDateHelper(januaryFirst, "1/01")
			parseDateHelper(januaryFirst, "01/1")
			parseDateHelper(januaryFirst, "January 01")

			'Complete date and time formats
			Dim testDateTime As New Date(2009, 7, 8, 12, 34, 56, DateTimeKind.Local) '12:34:56 on 7/8/09
			parseDateHelper(testDateTime, "07/08/2009 12:34:56 PM")
			parseDateHelper(testDateTime, "07/08/2009 12:34:56") 'This is the format generally returned from MS SQL Server
			parseDateHelper(testDateTime, "7/8/09 12:34:56 PM")

			'Complete date and time formats (no seconds)
			testDateTime = testDateTime.Subtract(New TimeSpan(0, 0, 56))
			parseDateHelper(testDateTime, "07/08/2009 12:34 PM")
			parseDateHelper(testDateTime, "07/08/2009 12:34")

			'Time formats (current date assumed)
			Dim testTime As Date = New Date(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 13, 23, 0)
			parseDateHelper(testTime, "1:23:00 PM")
			parseDateHelper(testTime, "1:23 PM")
			parseDateHelper(testTime, "13:23")

			'Restricted by range
			Assert.AreEqual(testDateTime, convertToDSDate(testDateTime)) 'Should be a valid Date
			Assert.IsTrue(Convert.IsDBNull(convertToDSDate(ParseDate("1/2/1750"))))
			parseDateHelper(New Date(9999, 12, 31, 12, 34, 0), "9999-12-31 12:34") 'Verify that this date actually works normally
			Assert.IsTrue(Convert.IsDBNull(convertToDSDate(ParseDate("9999-12-31 12:34"))))
		End Sub

		Public Sub parseDateHelper(ByVal pExpected As Nullable(Of Date), ByVal pDateString As String)
			Dim actual As Nullable(Of Date) = ParseDate(pDateString)
			Assert.AreEqual(pExpected, actual, "Expected: {0} | Actual: {1} | Original string: {2}", pExpected, actual, pDateString)
		End Sub

		Private Sub validateURLFailHelper(ByVal psURL As String)
			Dim bFailed As Boolean
			Try
				ValidateURL(psURL)
				bFailed = False
			Catch ex As Exception
				'good 
				bFailed = True
			End Try

			Assert.IsTrue(bFailed, "Validation should have failed for URL: " & psURL)

		End Sub


		<Test()> Public Sub testSafeInteger()
			Assert.AreEqual(0, SafeInteger("abc"))
			Assert.AreEqual(0, SafeInteger("1531351351353553135"))
			Assert.AreEqual(16, SafeInteger("15.5"))
			Assert.AreEqual(16, SafeInteger(15.5))
			Assert.AreEqual(15, SafeInteger(15.0001))
			Assert.AreEqual(15, SafeInteger("15.0001"))
			Assert.AreEqual(16, SafeInteger("15.99999"))
			Assert.AreEqual(16, SafeInteger(15.99999))
		End Sub


		<Test()> Public Sub testSafeLong()
			Assert.AreEqual(0, SafeLong("abc"))
			Assert.AreEqual(0, SafeLong("153135135135355151351531351351531533135"))
			Assert.AreEqual(16, SafeLong("15.5"))
			Assert.AreEqual(16, SafeLong(15.5))
			Assert.AreEqual(15, SafeLong(15.0001))
			Assert.AreEqual(15, SafeLong("15.0001"))
			Assert.AreEqual(16, SafeLong("15.99999"))
			Assert.AreEqual(16, SafeLong(15.99999))
		End Sub


		<Test()> Public Sub testSafeDouble()
			Assert.AreEqual(0, SafeDouble("abc"))
			'max for double is 1.79769313486232E+308 ! 
			Assert.AreEqual(0, SafeDouble("153135135135355151351531351351531533135123123123123153135135135355151351531351351531533135123123123123153135135135355151351531351351531533135123123123123153135135135355151351531351351531533135123123123123153135135135355151351531351351531533135123123123123153135135135355151351531351351531533135123123123123153135135135355151351531351351531533135123123123123153135135135355151351531351351531533135123123123123"))
			Assert.AreEqual(15.5, SafeDouble("15.5"))
			Assert.AreEqual(15.5, SafeDouble(15.5))
			Assert.AreEqual(15.0001, SafeDouble(15.0001))
			Assert.AreEqual(15.0001, SafeDouble("15.0001"))
			Assert.AreEqual(15.99999, SafeDouble("15.99999"))
			Assert.AreEqual(15.99999, SafeDouble(15.99999))
		End Sub

		<Test()> Public Sub testSafeSanitizedString()
			Assert.AreEqual("&lt;script&gt;alert(\&quot;Attack!\&quot;)&lt;/script&gt;", SafeSanitizedString("<script>alert(\""Attack!\"")</script>"))
			Assert.AreEqual("&lt;a&gt;http:xx.com?test=1&amp;test2=2&lt;a&gt;", SafeSanitizedString("<a>http:xx.com?test=1&test2=2<a>"))
		End Sub

		<Test()>
		Public Sub SafeSubString_InputLongerThanExpectedLength_ReturnsSubstring()
			Assert.AreEqual("st", SafeSubString("string", 2))
		End Sub

		<Test()>
		Public Sub SafeSubString_ExpectedLength0_ReturnsEmptyString()
			Assert.AreEqual("", SafeSubString("string", 0))
		End Sub


		<Test()>
		Public Sub SafeSubString_ExpectedLengthNegative_ReturnsOriginalInput()
			Assert.AreEqual("original string", SafeSubString("original string", -10))
		End Sub

		<TestCase("")>
		<TestCase(" ")>
		<TestCase(Nothing)>
		Public Sub SafeSubString_EmptyWhitespaceOrNullString_ReturnsOriginalInput(ByVal pInput As String)
			Assert.AreEqual(pInput, SafeSubString(pInput, 10))
		End Sub

		<Test>
		Public Sub SafeSubString_ExpectedLengthLongerThanInput_ReturnsOriginalInput()
			Assert.AreEqual("long string", SafeSubString("long string", 50))
		End Sub

		<Test>
		Public Sub SafeSubString_WhitespacePreserved_ReturnsInputWithWhitespace()
			Assert.AreEqual("\tlong string\n\r", SafeSubString("\tlong string\n\r", 50))
		End Sub
	End Class
End Namespace
