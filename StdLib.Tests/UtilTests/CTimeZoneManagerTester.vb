﻿Option Strict On

Imports System.Configuration.ConfigurationManager
Imports Moq
Imports StdLib.CTimeZoneManager
Imports System.TimeZoneInfo
Imports NUnit.Framework

Namespace UtilTests
    <TestFixture()>
    <Category("INTEGRATION")>
    Public Class CTimeZoneManagerTester

        <Test()> Public Sub TestLoadTimeZones()
            Dim oLAZone As TimeZoneInfo = ZoneInfo(Zone.LosAngeles)
            Dim oNYZone As TimeZoneInfo = ZoneInfo(Zone.NewYork)
            Dim oCHIZone As TimeZoneInfo = ZoneInfo(Zone.Chicago)
            Dim oDENZone As TimeZoneInfo = ZoneInfo(Zone.Denver)
            Dim oANCHZone As TimeZoneInfo = ZoneInfo(Zone.Anchorage)

            Dim oHONZone As TimeZoneInfo = ZoneInfo(Zone.Honolulu)
            Dim oPHOZone As TimeZoneInfo = ZoneInfo(Zone.Phoenix)
            Dim oCETZone As TimeZoneInfo = ZoneInfo(Zone.CentralEurope)

            '2007 Day Light Savings
            Dim oDLSStart As New DateTime(2007, 3, 11, 9, 59, 0, DateTimeKind.Utc)
            Dim oDLSEnd As New DateTime(2007, 11, 4, 8, 59, 0, DateTimeKind.Utc)
            DayLightSavingsTestHelper(oLAZone, oDLSStart, oDLSEnd)

            '1974 DLS
            oDLSStart = New DateTime(1974, 1, 6, 9, 59, 0, DateTimeKind.Utc)
            oDLSEnd = New DateTime(1974, 10, 27, 8, 59, 0, DateTimeKind.Utc)
            DayLightSavingsTestHelper(oLAZone, oDLSStart, oDLSEnd)

            '2007 DLS
            oDLSStart = New DateTime(2007, 3, 11, 6, 59, 0, DateTimeKind.Utc)
            oDLSEnd = New DateTime(2007, 11, 4, 5, 59, 0, DateTimeKind.Utc)
            DayLightSavingsTestHelper(oNYZone, oDLSStart, oDLSEnd)

            '1974 DLS
            oDLSStart = New DateTime(1974, 1, 6, 6, 59, 0, DateTimeKind.Utc)
            oDLSEnd = New DateTime(1974, 10, 27, 5, 59, 0, DateTimeKind.Utc)
            DayLightSavingsTestHelper(oNYZone, oDLSStart, oDLSEnd)

            '2007 DLS
            oDLSStart = New DateTime(2007, 3, 11, 7, 59, 0, DateTimeKind.Utc)
            oDLSEnd = New DateTime(2007, 11, 4, 6, 59, 0, DateTimeKind.Utc)
            DayLightSavingsTestHelper(oCHIZone, oDLSStart, oDLSEnd)

            '1974 DLS
            oDLSStart = New DateTime(1974, 1, 6, 7, 59, 0, DateTimeKind.Utc)
            oDLSEnd = New DateTime(1974, 10, 27, 6, 59, 0, DateTimeKind.Utc)
            DayLightSavingsTestHelper(oCHIZone, oDLSStart, oDLSEnd)

            '2007 DLS
            oDLSStart = New DateTime(2007, 3, 11, 8, 59, 0, DateTimeKind.Utc)
            oDLSEnd = New DateTime(2007, 11, 4, 7, 59, 0, DateTimeKind.Utc)
            DayLightSavingsTestHelper(oDENZone, oDLSStart, oDLSEnd)

            '1974 DLS
            oDLSStart = New DateTime(1974, 1, 6, 8, 59, 0, DateTimeKind.Utc)
            oDLSEnd = New DateTime(1974, 10, 27, 7, 59, 0, DateTimeKind.Utc)
            DayLightSavingsTestHelper(oDENZone, oDLSStart, oDLSEnd)

            '2007 DLS
            oDLSStart = New DateTime(2007, 3, 11, 10, 59, 0, DateTimeKind.Utc)
            oDLSEnd = New DateTime(2007, 11, 4, 9, 59, 0, DateTimeKind.Utc)
            DayLightSavingsTestHelper(oANCHZone, oDLSStart, oDLSEnd)

            '1974 DLS
            oDLSStart = New DateTime(1974, 1, 6, 10, 59, 0, DateTimeKind.Utc)
            oDLSEnd = New DateTime(1974, 10, 27, 9, 59, 0, DateTimeKind.Utc)
            DayLightSavingsTestHelper(oANCHZone, oDLSStart, oDLSEnd)

            '1996 DLS
            oDLSStart = New DateTime(1996, 3, 31, 0, 59, 0, DateTimeKind.Utc)
            oDLSEnd = New DateTime(1996, 10, 26, 23, 59, 0, DateTimeKind.Utc)
            DayLightSavingsTestHelper(oCETZone, oDLSStart, oDLSEnd)

            '1978 DLS
            oDLSStart = New DateTime(1978, 4, 2, 0, 59, 0, DateTimeKind.Utc)
            oDLSEnd = New DateTime(1978, 9, 30, 23, 59, 0, DateTimeKind.Utc)
            DayLightSavingsTestHelper(oCETZone, oDLSStart, oDLSEnd)
        End Sub

        Private Sub DayLightSavingsTestHelper(ByVal poTimeZone As TimeZoneInfo, ByVal poDayLightSavingsStart As DateTime, ByVal poDayLightSavingsEnd As DateTime)
            'standard -> daylight transition
            Dim oTestTimeUTC As DateTime = poDayLightSavingsStart
            Assert.AreEqual(1, ConvertTimeFromUtc(oTestTimeUTC, poTimeZone).Hour, poTimeZone.DisplayName & " Zone Info is incorrect before spring forward ")
            Assert.IsFalse(poTimeZone.IsDaylightSavingTime(oTestTimeUTC), "Time should not be in DaylightSavings")

            oTestTimeUTC = oTestTimeUTC.AddHours(1)
            Assert.AreEqual(3, ConvertTimeFromUtc(oTestTimeUTC, poTimeZone).Hour, poTimeZone.DisplayName & " Zone Info is incorrect after spring forward")
            Assert.IsTrue(poTimeZone.IsDaylightSavingTime(oTestTimeUTC), "Time should be in DaylightSavings")

            'daylight -> standard transition
            oTestTimeUTC = poDayLightSavingsEnd
            Assert.AreEqual(1, ConvertTimeFromUtc(oTestTimeUTC, poTimeZone).Hour, poTimeZone.DisplayName & " Zone Info is incorrect before fall backward")
            Assert.IsTrue(poTimeZone.IsDaylightSavingTime(oTestTimeUTC), "Time should be in DaylightSavings")

            oTestTimeUTC = oTestTimeUTC.AddHours(1)
            Assert.AreEqual(1, ConvertTimeFromUtc(oTestTimeUTC, poTimeZone).Hour, poTimeZone.DisplayName & " Zone Info is incorrect after fall backward")
            Assert.IsFalse(poTimeZone.IsDaylightSavingTime(oTestTimeUTC), "Time should not be in DaylightSavings")
        End Sub

        <Test()> Public Sub LocalZoneInfo_MachineTimeZoneKeyNotFoundInAppSettings_ReturnsLosAngelesTimeZoneInfo()
            AppSettings.Set("MACHINE_TIME_ZONE", "")
            Dim oCurrentTimeUTC As DateTime = DateTime.UtcNow
            Dim oCurrentTimeLocal As DateTime = oCurrentTimeUTC.ToLocalTime

            Dim oLocalTimeZone As TimeZoneInfo = LocalZoneInfo()
            Dim oConvertedTimeLocal As DateTime = ConvertTimeFromUtc(oCurrentTimeUTC, oLocalTimeZone)

            Dim sFormat As String = "MM/dd/yyyy hh:mm:ss"
            Assert.AreEqual(oCurrentTimeLocal.ToString(sFormat), oConvertedTimeLocal.ToString(sFormat), "System local time does not match Time Zone Manager's local time")
        End Sub

        <TestCase("Europe/London", Zone.London)>
        <TestCase("America/Los_Angeles", Zone.LosAngeles)>
        <TestCase("America/Phoenix", Zone.Phoenix)>
        <TestCase("Etc/UTC", Zone.UTC)>
        <TestCase("America/New_York", Zone.NewYork)>
        <TestCase("America/Chicago", Zone.Chicago)>
        <TestCase("America/Denver", Zone.Denver)>
        <TestCase("America/Anchorage", Zone.Anchorage)>
        <TestCase("Pacific/Honolulu", Zone.Honolulu)>
        <TestCase("Central Europe", Zone.CentralEurope)>
        <TestCase("Chamorro", Zone.Chamorro)>
        Public Sub MapTimeZoneIDToZone_ValidZoneIDString_ReturnsExpectedZone(ByVal psZone As String, ByVal peExpectedZone As Zone)
            Dim eZone As Zone = MapTimeZoneIDToZone(psZone)
            Assert.AreEqual(peExpectedZone, eZone)
        End Sub

        <Test>
        Public Sub MapTimeZoneIDToZone_InvalidZoneIDString_ThrowException()
            Assert.Throws(Of Exception)(Function() MapTimeZoneIDToZone("InvalidZoneID"))
        End Sub

        <TestCase(Zone.London, True, "BST")>
        <TestCase(Zone.London, False, "GMT")>
        <TestCase(Zone.LosAngeles, True, "PDT")>
        <TestCase(Zone.LosAngeles, False, "PST")>
        <TestCase(Zone.Phoenix, True, "MDT")>
        <TestCase(Zone.Phoenix, False, "MST")>
        <TestCase(Zone.UTC, True, "UTC")>
        <TestCase(Zone.NewYork, True, "EDT")>
        <TestCase(Zone.NewYork, False, "EST")>
        <TestCase(Zone.Chicago, True, "CDT")>
        <TestCase(Zone.Chicago, False, "CST")>
        <TestCase(Zone.Anchorage, True, "AKDT")>
        <TestCase(Zone.Anchorage, False, "AKST")>
        <TestCase(Zone.Honolulu, True, "HADT")>
        <TestCase(Zone.Honolulu, False, "HAST")>
        <TestCase(Zone.CentralEurope, True, "CEST")>
        <TestCase(Zone.CentralEurope, False, "CET")>
        <TestCase(Zone.Chamorro, True, "ChST")>
        Public Sub GetTimeZoneAbbreviation_ValidZone_ReturnsExpectedTimeZoneAbbreviation(ByVal peZone As Zone, ByVal pbIsDaylightTime As Boolean, ByVal psExpectedTimeZoneAbbreviation As String)
            Dim sTimeZoneAbbreviation As String = GetTimeZoneAbbreviation(peZone, pbIsDaylightTime)
            Assert.AreEqual(psExpectedTimeZoneAbbreviation, sTimeZoneAbbreviation)
        End Sub

        <TestCase(Zone.London, "Europe/London")>
        <TestCase(Zone.LosAngeles, "America/Los_Angeles")>
        <TestCase(Zone.Phoenix, "America/Phoenix")>
        <TestCase(Zone.UTC, "Etc/UTC")>
        <TestCase(Zone.NewYork, "America/New_York")>
        <TestCase(Zone.Chicago, "America/Chicago")>
        <TestCase(Zone.Denver, "America/Denver")>
        <TestCase(Zone.Anchorage, "America/Anchorage")>
        <TestCase(Zone.Honolulu, "Pacific/Honolulu")>
        <TestCase(Zone.CentralEurope, "Central Europe")>
        <TestCase(Zone.Chamorro, "Chamorro")>
        Public Sub MapTimeZoneToID_ValidZone_ReturnsExpectedZoneID(ByVal peZone As Zone, ByVal psExpectedZoneID As String)
            Dim sZoneID As String = MapTimeZoneToID(peZone)
            Assert.AreEqual(psExpectedZoneID, sZoneID)
        End Sub

        <Test()> Public Sub LocalZoneInfo_MachineTimeZoneKeyFoundInAppSettings_ReturnsTimeZoneInfoAsPerAppSettingsKey()
            AppSettings.Set("MACHINE_TIME_ZONE", "America/Los_Angeles")

            Dim oCurrentTimeUTC As DateTime = DateTime.UtcNow
            Dim oCurrentTimeLocal As DateTime = oCurrentTimeUTC.ToLocalTime

            Dim oLocalTimeZone As TimeZoneInfo = LocalZoneInfo()
            Dim oConvertedTimeLocal As DateTime = ConvertTimeFromUtc(oCurrentTimeUTC, oLocalTimeZone)

            Dim sFormat As String = "MM/dd/yyyy hh:mm:ss"
            Assert.AreEqual(oCurrentTimeLocal.ToString(sFormat), oConvertedTimeLocal.ToString(sFormat), "System local time does not match Time Zone Manager's local time")
        End Sub

        <Test()> Public Sub LocalZone_MachineTimeZoneKeyFoundInAppSettings_ReturnsExpectedZone()
            AppSettings.Set("MACHINE_TIME_ZONE", "America/Denver")
            Dim oZone As Zone = LocalZone()
            Assert.AreEqual(Zone.Denver, oZone)
        End Sub

        <Test()> Public Sub ZoneInfo_UsingDefaultTimeZoneConfig_ReturnsExpectedTimeZoneInfo()
            Dim oTimeZoneInfo As TimeZoneInfo = GetSampleTimeZoneInfo()
            Dim oMockTimeZoneLoader As Mock(Of ITimeZoneLoader) = New Mock(Of ITimeZoneLoader)
            oMockTimeZoneLoader.Setup(Function(c) c.IsTimeZoneConfigFileExists(It.IsAny(Of String))).Returns(False)
            oMockTimeZoneLoader.Setup(Function(c) c.LoadTimeZoneInfo(It.IsAny(Of String), It.IsAny(Of XDocument))).Returns(oTimeZoneInfo)
            TimeZoneLoader = oMockTimeZoneLoader.Object

            Dim oZoneInfo As TimeZoneInfo = ZoneInfo(Zone.Chamorro)

            oMockTimeZoneLoader.Verify(Function(c) c.IsTimeZoneConfigFileExists(It.IsAny(Of String)), Times.Once)
            oMockTimeZoneLoader.Verify(Function(c) c.LoadTimeZoneInfo(It.IsAny(Of String), It.IsAny(Of XDocument)), Times.Once)

            Assert.AreEqual(oTimeZoneInfo.BaseUtcOffset, oZoneInfo.BaseUtcOffset)
            Assert.AreEqual(oTimeZoneInfo.DaylightName, oZoneInfo.DaylightName)
            Assert.AreEqual(oTimeZoneInfo.DisplayName, oZoneInfo.DisplayName)
            Assert.AreEqual(oTimeZoneInfo.Id, oZoneInfo.Id)
            Assert.AreEqual(oTimeZoneInfo.StandardName, oZoneInfo.StandardName)
            Assert.AreEqual(oTimeZoneInfo.SupportsDaylightSavingTime, oZoneInfo.SupportsDaylightSavingTime)
        End Sub

        <Test()> Public Sub ZoneInfo_UTCZone_ReturnsExpectedUTCTimeZoneInfo()
            AssertUTCTimeZoneInfo(ZoneInfo(Zone.UTC))
        End Sub

        <Test()> Public Sub UTCZoneInfo_ValidData_ReturnsExpectedUTCTimeZoneInfo()
            AssertUTCTimeZoneInfo(UTCZoneInfo())
        End Sub

#Region "Helper Functions"

        Private Sub AssertUTCTimeZoneInfo(ByVal poTimeZoneInfo As TimeZoneInfo)
            Assert.AreEqual(New TimeSpan(0, 0, 0), poTimeZoneInfo.BaseUtcOffset)
            Assert.AreEqual("UTC", poTimeZoneInfo.DaylightName)
            Assert.AreEqual("UTC", poTimeZoneInfo.DisplayName)
            Assert.AreEqual("UTC", poTimeZoneInfo.Id)
            Assert.AreEqual("UTC", poTimeZoneInfo.StandardName)
            Assert.AreEqual(False, poTimeZoneInfo.SupportsDaylightSavingTime)
        End Sub

        Private Function GetSampleTimeZoneInfo() As TimeZoneInfo
            Dim oAdjustments As New List(Of AdjustmentRule)
            Return CreateCustomTimeZone("Chamorro", New TimeSpan(10, 0, 0), "Chamorro Time", "Chamorro Standard Time", "Chamorro Time", oAdjustments.ToArray())
        End Function

#End Region
    End Class
End Namespace
