﻿Imports NUnit.Framework

Namespace XmlToolTests

    ''' <summary>
    ''' Note: This test should be run inside nUnit.   When run inside the IDE, it may have unexpected
    ''' failure;  the IDE maybe doing something funny?   
    ''' 
    ''' Additionally, when run inside nUnit, the test can only be run ONCE!  Conseqetive runs 
    ''' won't work b/c the data types are cached from prior runs.
    ''' </summary>
    ''' <remarks></remarks>
    <TestFixture()> Public Class CXmlSerializerFactoryTest
        'this routine is example of how to cause a memory leak from abusive use of the XmlSerializer

        Private log As log4net.ILog = log4net.LogManager.GetLogger(GetType(CXmlSerializerFactoryTest))



        <Test()> Public Sub CreateCacheWithRootsSmallLeak()
            Dim nBeforeCount As Int16 = getAssemblyCount()

            Dim attributeOverrides As New System.Xml.Serialization.XmlAttributeOverrides()
            Dim xmlAttributes As New System.Xml.Serialization.XmlAttributes()
            xmlAttributes.XmlRoot = New System.Xml.Serialization.XmlRootAttribute("A")
            attributeOverrides.Add(GetType(CGarbage), xmlAttributes)
            Dim serializer As New System.Xml.Serialization.XmlSerializer(GetType(CGarbage), attributeOverrides)

            Assert.AreEqual(getAssemblyCount, nBeforeCount + 1)

            serializer = New System.Xml.Serialization.XmlSerializer(GetType(CGarbage), attributeOverrides)
            Assert.AreEqual(getAssemblyCount, nBeforeCount + 2)
        End Sub

        Public Class CGarbage
            Public Garbo1 As Integer
        End Class

        Private Shared _CreateCacheWithRootsNoLeakAlreadyRun As Boolean
        <Test()> Public Sub CreateCacheWithRootsNoLeak()
            If _CreateCacheWithRootsNoLeakAlreadyRun Then
                Assert.Fail("Test already run -- you need to reload app domain to re-run test!")
            Else
                _CreateCacheWithRootsNoLeakAlreadyRun = True
            End If


            Dim nBeforeCount As Int16 = getAssemblyCount()

            Dim serializer = XMLTools.CXmlSerializerFactory.Create(Of CGarbage)("B")

            If nBeforeCount + 1 <> getAssemblyCount() Then
                logAssemblies()

                Assert.AreEqual(nBeforeCount + 1, getAssemblyCount, "Check Logs for assemblies loaded.")
            End If


            serializer = XMLTools.CXmlSerializerFactory.Create(Of CGarbage)("B")

            If nBeforeCount + 1 <> getAssemblyCount() Then
                logAssemblies()

                Assert.AreEqual(nBeforeCount + 1, getAssemblyCount, "Check Logs for assemblies loaded.")
            End If


        End Sub

        Private Shared _CreateDefaultNoLeak As Boolean
        <Test()> Public Sub CreateDefaultNoLeak()
            If _CreateDefaultNoLeak Then
                Assert.Fail("Test already run -- you need to reload app domain to re-run test!")
            Else
                _CreateDefaultNoLeak = True
            End If

            Dim nBeforeCount As Int16 = getAssemblyCount()

            Dim serializer = XMLTools.CXmlSerializerFactory.Create(Of CGarbage)()

            If nBeforeCount + 1 <> getAssemblyCount() Then
                logAssemblies()

                Assert.AreEqual(nBeforeCount + 1, getAssemblyCount, "Check Logs for assemblies loaded.")
            End If



            serializer = XMLTools.CXmlSerializerFactory.Create(Of CGarbage)()

            If nBeforeCount + 1 <> getAssemblyCount() Then
                logAssemblies()

                Assert.AreEqual(nBeforeCount + 1, getAssemblyCount)
            End If

        End Sub

        Private Sub logAssemblies()
            Dim sDebug As New System.Text.StringBuilder
            sDebug.AppendLine("Assemblies Count: " & AppDomain.CurrentDomain.GetAssemblies().Length)

            For Each asm In AppDomain.CurrentDomain.GetAssemblies()
                sDebug.AppendLine("Assembly: " & asm.FullName)


                'only print the types for dynamic libraries with funny names short names. eg: 
                'kdsub3um, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
                If asm.FullName.IndexOf(",") <= 9 Then

                    sDebug.AppendLine("Types: ")

                    'printing too much types will likely make things unreadable... 
                    For i As Integer = 0 To Math.Min(40, asm.GetTypes.Length - 1)
                        Dim t = asm.GetTypes(i)
                        sDebug.AppendLine("  " & t.Name)
                    Next
                End If
            Next
            log.Info(sDebug)

        End Sub


        Private Function getAssemblyCount() As Integer
            Return AppDomain.CurrentDomain.GetAssemblies.Length
        End Function

    End Class

End Namespace