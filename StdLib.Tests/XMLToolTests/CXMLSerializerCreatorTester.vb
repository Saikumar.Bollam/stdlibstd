﻿Option Strict On

Imports NUnit.Framework
Imports StdLib.XMLTools

Namespace XmlToolTests

    <TestFixture()>
    <Category("UNIT")>
    Public Class CXMLSerializerCreatorTester


        Private _sut As CXMLSerializerCreator

        <SetUp>
        Public Sub setup()
            _sut = New CXMLSerializerCreator
        End Sub

        <Test>
        Public Sub create_HappyPath_ObjectNotNull()
            Assert.IsNotNull(_sut.Create(Of Object)())
        End Sub
    End Class
End Namespace
