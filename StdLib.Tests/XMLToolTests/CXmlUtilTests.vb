﻿Imports System.Text
Imports NUnit.Framework
Imports StdLib.XMLTools

Namespace XmlToolTests

    <TestFixture()>
    Public Class CXmlUtilTests
        <Test> Public Sub RenameChild()
            Dim s1 As String = "<ROOT><CHILD1><SUB1/></CHILD1><CHILD2/></ROOT>"
            Dim doc1 As New Xml.XmlDocument
            doc1.LoadXml(s1)

            Dim expected1 As String = doc1.OuterXml.Replace("CHILD1", "CHILD_A")
            CXMLUtils.RenameNode(doc1.DocumentElement.FirstChild, "CHILD_A")
            Assert.AreEqual(expected1, doc1.OuterXml)


        End Sub

        <Test> Public Sub RenameRoot()
            Dim s1 As String = "<ROOT><CHILD1><SUB1/></CHILD1><CHILD2/></ROOT>"
            Dim doc1 As New Xml.XmlDocument
            doc1.LoadXml(s1)

            Dim expected1 As String = doc1.OuterXml.Replace("ROOT", "NEW_ROOT")
            CXMLUtils.RenameNode(doc1.DocumentElement, "NEW_ROOT")
            Assert.AreEqual(expected1, doc1.OuterXml)


        End Sub

        <Test> Public Sub ExportRecordToElement_ExportFromRow_ValuesShouldMatch()
            Dim oUtils As New CXMLUtils()

            Dim oDoc As New Xml.XmlDocument()
            Dim oElm As Xml.XmlElement = oDoc.CreateElement("TEST")

            Dim oDT As DataTable = GetTestTable()

            Dim oDR As DataRow = oDT.NewRow()
            oDR("TestString") = "Test string"
            oDR("TestDouble") = 555.555
            oDR("TestInt") = 66
            Dim oGuid As Guid = Guid.NewGuid()
            oDR("TestGUID") = oGuid
            Dim oDate As DateTime = DateTime.Now
            oDR("TestDate") = oDate
            oDR("TestByteArr") = Encoding.UTF8.GetBytes("test bytes")

            oUtils.ExportRecordToElement(oElm, oDR)

            Dim oTestElm As Xml.XmlElement = oElm.GetElementsByTagName("TESTSTRING")(0)
            Assert.AreEqual("Test string", oTestElm.InnerText, "Exporting string failed")
            oTestElm = oElm.GetElementsByTagName("TESTDOUBLE")(0)
            Assert.AreEqual(555.555, CBaseStd.SafeDouble(oTestElm.InnerText), "Exporting double failed")
            oTestElm = oElm.GetElementsByTagName("TESTINT")(0)
            Assert.AreEqual("66", oTestElm.InnerText, "Exporting int failed")
            oTestElm = oElm.GetElementsByTagName("TESTGUID")(0)
            Assert.AreEqual(oGuid.ToString, oTestElm.InnerText, "Exporting guid failed")
            oTestElm = oElm.GetElementsByTagName("TESTDATE")(0)
            Assert.AreEqual(oDate.ToString, oTestElm.InnerText, "Exporting date failed")
            oTestElm = oElm.GetElementsByTagName("TESTBYTEARR")(0)
            Assert.AreEqual(Convert.ToBase64String(Encoding.UTF8.GetBytes("test bytes")), oTestElm.InnerText, "Exporting byte failed")
        End Sub

        <Test> Public Sub ImportElementToRecord_ImportXML_ValuesShouldMatch()
            Dim oUtils As New CXMLUtils()

            Dim oDoc As New Xml.XmlDocument()
            oDoc.LoadXml("<TEST>" &
              "<TESTSTRING type=""System.String"">Test string</TESTSTRING>" &
              "<TESTDOUBLE type=""System.Double"">555.55500</TESTDOUBLE>" &
              "<TESTINT type=""System.Int32"">66</TESTINT>" &
              "<TESTGUID type=""System.Guid"">0b64f0b3-fb1c-4daf-b60c-944f2c58bc10</TESTGUID>" &
              "<TESTDATE type=""System.DateTime"">6/29/2020 7:20:57 PM</TESTDATE>" &
              "<TESTBYTEARR type=""System.Byte[]"">dGVzdCBieXRlcw==</TESTBYTEARR>" &
              "</TEST>")

            Dim oElm As Xml.XmlElement = oDoc.DocumentElement

            Dim oDT As DataTable = GetTestTable()
            Dim oDR As DataRow = oDT.NewRow()

            oUtils.ImportElementToRecord(oElm, oDR)

            Assert.AreEqual("Test string", CBaseStd.SafeString(oDR("TestString")), "importing string failed")
            Assert.AreEqual(555.555, CBaseStd.SafeDouble(oDR("TestDouble")), "importing double failed")
            Assert.AreEqual(66, CBaseStd.SafeInteger(oDR("TestInt")), "importing int failed")
            Assert.AreEqual("0b64f0b3-fb1c-4daf-b60c-944f2c58bc10", CBaseStd.SafeString(oDR("TestGUID")), "importing guid failed")
            Assert.AreEqual("6/29/2020 7:20:57 PM", CBaseStd.SafeString(oDR("TestDate")), "importing date failed")

            Dim oBytes As Byte() = CType(oDR("TestByteArr"), Byte())
            Dim sValue As String = Convert.ToBase64String(oBytes)
            Assert.AreEqual("dGVzdCBieXRlcw==", sValue, "importing byte failed")
        End Sub

        Private Function GetTestTable() As DataTable
            Dim oDT As New DataTable()
            oDT.Columns.Add(New DataColumn("TestString", GetType(String)))
            oDT.Columns.Add(New DataColumn("TestDouble", GetType(Double)))
            oDT.Columns.Add(New DataColumn("TestInt", GetType(Integer)))
            oDT.Columns.Add(New DataColumn("TestGUID", GetType(Guid)))
            oDT.Columns.Add(New DataColumn("TestDate", GetType(DateTime)))
            oDT.Columns.Add(New DataColumn("TestByteArr", GetType(Byte())))
            Return oDT
        End Function
    End Class

End Namespace
