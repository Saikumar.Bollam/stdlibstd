﻿Imports NUnit.Framework
Imports StdLib.XMLTools

Namespace XmlToolTests


    Public Class CXmlToHtmlConverterTester

        <Test> Public Sub ConvertXMLToHTML()
            Dim xmlString As String = "<root><responseHeader><requestType>PreciseIdOnly</requestType><clientReferenceId>PIDInitialML</clientReferenceId>" &
                                    "<expRequestId>RB000100970082</expRequestId><messageTime>2020-04-14T17:54:25Z</messageTime><overallResponse /><responseCode>R0201</responseCode>" &
                                    "<responseType>INFO</responseType><responseMessage>Workflow Complete.</responseMessage><tenantID>T25GKJ7T</tenantID></responseHeader></root>"

            Dim xml As New Xml.XmlDocument
            xml.LoadXml(xmlString)

            Dim htmlString As String = ""
            htmlString = CXmlToHtmlTableConverter.ConvertXmlToHtmlTable(xmlString)

            Assert.IsNotEmpty(htmlString)
            Assert.True(htmlString.Contains("<table border='1'>"), "HTML string expected to contain table element")
            Assert.True(htmlString.Contains("</table>"), "HTML string expected to contain end table element")
            Assert.True(htmlString.Contains("<tr>"), "HTML string expected to contain table row element")
            Assert.True(htmlString.Contains("</tr>"), "HTML string expected to contain end table row element")


        End Sub

    End Class

End Namespace
