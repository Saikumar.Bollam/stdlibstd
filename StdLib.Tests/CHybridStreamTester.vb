﻿Option Strict On
Imports NUnit.Framework
Imports StdLib.SpecializedDataTypes
<TestFixture()> Public Class CHybridStreamTester

	<Test()> Sub testHybridStream()
		Dim b(1023) As Byte
		For i As Integer = 0 To 1023
			b(i) = CByte(i Mod 256)
		Next

		Dim tempFileName As String = ""
		Using h As New CHybridStream(1024)
			h.Write(b, 0, 1024)
			Assert.IsFalse(h.IsUsingFileStream, "Should not be using filestream yet")

			h.Write(b, 0, 1024)
			Assert.IsTrue(h.IsUsingFileStream, "Should be using filestream")

			h.Flush()

			h.Seek(3, IO.SeekOrigin.Begin)
			Assert.IsTrue(h.ReadByte() = 3, "Should be 3 at position 3")


			h.Seek(1075, IO.SeekOrigin.Begin)
			Assert.IsTrue(h.ReadByte() = CByte(1075 Mod 256))

			tempFileName = h.TempFileName
			Assert.IsTrue(System.IO.File.Exists(h.TempFileName), "Temp file should exist")
		End Using

		Assert.IsTrue(tempFileName <> "", "Temp file name should not be blank")

		Assert.IsFalse(System.IO.File.Exists(tempFileName), "Temp file should be gone")
	End Sub
End Class
