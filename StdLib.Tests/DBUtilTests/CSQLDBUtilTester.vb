﻿Option Strict On

Imports StdLib.CBaseStd
Imports StdLib.DBUtils
Imports NUnit.Framework
Imports System.Threading.Tasks

Namespace DBUtils
    'This class has been around for some time, and it's used extensively, so the common test cases would not
    'be priority at this point.
    <TestFixture> Public Class CSQLDBUtilTester

        'Run these unit tests on a local mdf database
        Private Const UnitTestDBConnectionString As String = "Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\UnitTestsDB.mdf;Integrated Security=True;Connect Timeout=30;Trusted_Connection=Yes;"

        <Test> Public Sub testDataTruncate()
            Using db As New CSQLDBUtils(UnitTestDBConnectionString)
                Dim sTest As String = "Create Table #tmpA ( " &
                " FirstName [char](5) NOT NULL " &
                 ") " &
                 " INSERT INTO #tmpA (FirstName) Values ('(B)OBBBY ((BBB))OOBLE') "

                Try
                    db.executeNonQuery(sTest)
                Catch ex As CSQLException When ex.Message.IndexOf("truncated") <> -1
                    'OK!
                    Return
                End Try


                Assert.Fail("Expected to cause truncation exception!")
            End Using
        End Sub

        <Test> Public Sub testDataTruncateBadInsert()
            Using db As New CSQLDBUtils(UnitTestDBConnectionString)
                Dim sTest As String = "Create Table #tmpA ( " &
                " FirstName [char](5) NOT NULL " &
                 ") " &
                 " INSERT INTO #tmpA (FirstName) Values ('(B)OBBBY ((BBB))OOBLE', 'MOO') "

                Try
                    db.executeNonQuery(sTest)
                Catch ex As CSQLException When ex.Message.IndexOf("There are fewer columns in the") <> -1
                    'OK!
                    Return
                End Try


                Assert.Fail("Expected to cause truncation exception!")
            End Using
        End Sub


        <Test> Public Sub testDataTruncateUpdate()
            Using db As New CSQLDBUtils(UnitTestDBConnectionString)
                Dim sTest As String = "Create Table #tmpA ( " &
                " FirstName [char](5) NOT NULL " &
                 ");" &
                 "INSERT INTO #tmpA (FirstName) Values ('OK')"
                db.executeNonQuery(sTest)


                Dim sqlTruncate As String = "INSERT INTO #tmpA (FirstName) Values ('(B)O)BBBY (B)OOBLE')"
                Try
                    db.executeNonQuery(sqlTruncate)
                Catch ex As CSQLException When ex.Message.IndexOf("truncated") <> -1
                    'OK!
                    Return
                End Try



                Assert.Fail("Expected to cause truncation exception!")
            End Using
        End Sub

        ''' <summary>
        ''' write using normal insert, reads using stream reader.
        ''' </summary>
        <Test> Public Sub testSQLColumnStreamRead()

            Using db As New CSQLDBUtils(UnitTestDBConnectionString)

                'id 36 because i dont want to deal with taking dashes out of guid
                Dim sTest As String = "Create Table #tmpA ( " &
                " ID [varchar](36) NOT NULL, " &
                " Dat [varbinary](max) NOT NULL " &
                 ")"
                db.executeNonQuery(sTest)

                Dim sID As String = Guid.NewGuid.ToString
                Dim bWriteVal() As Byte = {1, 2, 3, 4, 5, 6}

                Dim insertBuilder As New cSQLInsertStringBuilder("#tmpA")
                insertBuilder.AppendValue("ID", New CSQLParamValue(sID))
                insertBuilder.AppendValue("Dat", New CSQLParamValue(bWriteVal))
                db.executeNonQuery(insertBuilder.SQL)

                Dim where As New CSQLWhereStringBuilder()
                where.AppendAndCondition("ID = {0}", New CSQLParamValue(sID))

                Dim bReadVal(bWriteVal.Length - 1) As Byte
                Using colstream As CSQLColumnStreamReader = db.getColumnStreamReader("#tmpA", "Dat", where)
                    Using ms As New IO.MemoryStream(bReadVal)
                        colstream.CopyTo(ms)
                    End Using
                End Using

                TestArraysEqual(bReadVal, bWriteVal)

            End Using
        End Sub

        ''' <summary>
        ''' write using stream writer, reads using get datatable
        ''' </summary>
        <Test> Public Sub testSQLColumnStreamWrite()

            Using db As New CSQLDBUtils(UnitTestDBConnectionString)

                'id 36 because i dont want to deal with taking dashes out of guid
                Dim sTest As String = "Create Table #tmpA ( " &
                " ID [varchar](36) NOT NULL, " &
                " Dat [varbinary](max) NOT NULL " &
                 ")"
                db.executeNonQuery(sTest)

                Dim sID As String = Guid.NewGuid.ToString
                Dim bWriteVal() As Byte = {1, 2, 3, 4, 5, 6}

                Dim insertBuilder As New cSQLInsertStringBuilder("#tmpA")
                insertBuilder.AppendValue("ID", New CSQLParamValue(sID))
                insertBuilder.AppendValue("Dat", New CSQLParamValue({0}))
                db.executeNonQuery(insertBuilder.SQL)

                Dim where As New CSQLWhereStringBuilder()
                where.AppendAndCondition("ID = {0}", New CSQLParamValue(sID))

                Dim sReadVal(bWriteVal.Length - 1) As Byte
                Using colstream As CSQLColumnStreamWriter = db.getColumnStreamWriter("#tmpA", "Dat", where)
                    colstream.Write(bWriteVal, 0, bWriteVal.Length)
                End Using

                Dim dt As DataTable = db.getDataTable("SELECT * FROM #tmpA" & where.SQL)
                Dim dr As DataRow = dt.Rows(0)
                Dim bReadVal As Byte() = DirectCast(dr("Dat"), Byte())

                TestArraysEqual(bReadVal, bWriteVal)

            End Using
        End Sub

        ''' <summary>
        ''' Test if the transaction works with the other CSQLDBUTILS stuff
        ''' </summary>
        <Test> Public Sub testSQLColumnStreamTransactions()

            Using db As New CSQLDBUtils(UnitTestDBConnectionString)

                'id 36 because i dont want to deal with taking dashes out of guid
                Dim sTest As String = "Create Table #tmpA ( " &
                " ID [varchar](36) NOT NULL, " &
                " Tag [varchar](10) NOT NULL, " &
                " Dat [varbinary](max) NOT NULL " &
                    ")"
                db.executeNonQuery(sTest, True)

                Dim sID As String = Guid.NewGuid.ToString
                Dim bWriteVal() As Byte = {1, 2, 3, 4, 5, 6}

                Dim insertBuilder As New cSQLInsertStringBuilder("#tmpA")
                insertBuilder.AppendValue("ID", New CSQLParamValue(sID))
                insertBuilder.AppendValue("Tag", New CSQLParamValue("endo"))
                insertBuilder.AppendValue("Dat", New CSQLParamValue({0}))
                db.executeNonQuery(insertBuilder.SQL, True)

                Dim where As New CSQLWhereStringBuilder()
                where.AppendAndCondition("ID = {0}", New CSQLParamValue(sID))

                Dim dt As DataTable
                Dim dr As DataRow
                Dim bReadVal As Byte()

                Dim sReadVal(bWriteVal.Length - 1) As Byte
                Using colstream As CSQLColumnStreamWriter = db.getColumnStreamWriter("#tmpA", "Dat", where, True)

                    'write some
                    colstream.Write(bWriteVal, 0, 3)

                    dt = db.getDataTable("SELECT * FROM #tmpA" & where.SQL)
                    dr = dt.Rows(0)
                    bReadVal = DirectCast(dr("Dat"), Byte())
                    TestArraysEqual(bReadVal, {1, 2, 3})

                    'use an updatebuilder
                    Dim updateBuilder As New cSQLUpdateStringBuilder("#tmpA", where)
                    updateBuilder.AppendValue("Tag", New CSQLParamValue("yoko"))

                    db.executeNonQuery(updateBuilder.SQL, True)

                    'write some more
                    colstream.Write(bWriteVal, 3, 3)

                    dt = db.getDataTable("SELECT * FROM #tmpA" & where.SQL)
                    dr = dt.Rows(0)
                    bReadVal = DirectCast(dr("Dat"), Byte())
                    TestArraysEqual(bReadVal, bWriteVal)

                End Using

                TestArraysEqual(bReadVal, bWriteVal)

            End Using
        End Sub

        Public Sub TestArraysEqual(ByVal BA1 As Byte(), ByVal BA2 As Byte())
            Assert.IsTrue(BA1.Length = BA2.Length, "Arrays lengths are not the same")
            For i = 0 To BA1.Length - 1
                Assert.IsTrue(BA1(i) = BA2(i), "Arrays did not match at index {0}. left[{0}] = {1}  <> right[{0}] = {2} ", i, BA1(i), BA2(i))
            Next
        End Sub

        ''' <summary>
        ''' Test async
        ''' </summary>
        <Test> Public Sub TestAsyncFunction()
            Using db As New CSQLDBUtils(UnitTestDBConnectionString)

                'id 36 because i dont want to deal with taking dashes out of guid
                Dim sTest As String = "Create Table #tmpA ( " &
                " ID [varchar](36) NOT NULL, " &
                " Tag [varchar](10) NOT NULL, " &
                " Dat [varbinary](max) NOT NULL " &
                    ")"

                Dim oExecuteNonQueryTask As Task(Of Integer) = db.executeNonQueryAsync(sTest)
                oExecuteNonQueryTask.Wait()

                Dim sID As String = Guid.NewGuid.ToString
                Dim bWriteVal() As Byte = {1, 2, 3, 4, 5, 6}

                Dim insertBuilder As New cSQLInsertStringBuilder("#tmpA")
                insertBuilder.AppendValue("ID", New CSQLParamValue(sID))
                insertBuilder.AppendValue("Tag", New CSQLParamValue("endo"))
                insertBuilder.AppendValue("Dat", New CSQLParamValue({0}))

                oExecuteNonQueryTask = db.executeNonQueryAsync(insertBuilder.SQL)
                oExecuteNonQueryTask.Wait()

                Dim where As New CSQLWhereStringBuilder()
                where.AppendAndCondition("ID = {0}", New CSQLParamValue(sID))

                Dim dt As DataTable = New DataTable
                Dim dr As DataRow
                Using oExecuteDataReadyerTask As Task(Of IDataReader) = db.getDataReaderAsync("SELECT * FROM #tmpA" & where.SQL)
                    oExecuteDataReadyerTask.Wait()
                    dt.Load(oExecuteDataReadyerTask.Result)
                    dr = dt.Rows(0)
                End Using

                Assert.AreEqual(sID, SafeString(dr("ID")), "Fail to insert ID")
                Assert.AreEqual("endo", SafeString(dr("Tag")), "Fail to insert ID")

				'Test getScalarValueAsync
				Dim oGetScalarValueTask As Task(Of String) = db.getScalarValueAsync("Select count(*) from #tmpA", New CSQLParamBuilder())
				oGetScalarValueTask.Wait()
                Assert.AreEqual("1", oGetScalarValueTask.Result, "Fail to execute get scalar value")
            End Using
        End Sub
    End Class
End Namespace
