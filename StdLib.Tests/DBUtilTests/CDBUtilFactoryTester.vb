﻿Option Strict On

Imports StdLib.DBUtils
Imports StdLib.CBaseStd
Imports NUnit.Framework

Namespace DBUtils
    'This class has been around for some time, and it's used extensively, so the common test cases would not
    'be priority at this point.
    <TestFixture> Public Class CDBUtilFactoryTester
        'Run these unit tests on a local mdf database
        Private Const UnitTestDBConnectionString As String = "Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\UnitTestsDB.mdf;Integrated Security=True;Connect Timeout=30;Trusted_Connection=Yes;"


        <Test> Public Sub Create_ConnectionString_GoodDBUtils()
            Dim oDBFactory As IDBUtilsFactory = New CDBUtilsFactory()
            Dim oDB As IDBUtils = oDBFactory.Create(UnitTestDBConnectionString)

            Assert.Pass("No Errors in creation.  pass")

        End Sub

        <Test> Public Sub Create_EmptyConnectionString_Error()
            Dim oDBFactory As IDBUtilsFactory = New CDBUtilsFactory()

            Try
                Dim oDB As IDBUtils = oDBFactory.Create("")
                Assert.Fail("An error should have been thrown for empty connection string")
            Catch ex As ArgumentException
                Assert.IsTrue(SafeString(ex.Message).ToUpper.Contains("BLANK"), "Error message should indicate connection string is blank")

            End Try


        End Sub

    End Class
End Namespace
