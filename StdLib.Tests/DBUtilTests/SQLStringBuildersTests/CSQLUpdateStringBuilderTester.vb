﻿Option Strict On


Imports StdLib.DBUtils
Imports NUnit.Framework

Namespace DBUtilTests.SQLStringBuildersTests

    <TestFixture>
    Public Class CSQLUpdateStringBuilderTester

        <Test>
        Public Sub SQL_EmptyTableName_ThrowsException()
            Assert.Throws(Of Exception)(AddressOf SQLEmptyTableNameHelper)
        End Sub

        Private Sub SQLEmptyTableNameHelper()
            Dim oWhere As New CSQLWhereStringBuilder
            oWhere.AppendANDCondition("LenderID = @LenderID")
            Dim oSQLUpdateStringBuilder As ISQLEditStringBuilder = New cSQLUpdateStringBuilder("", oWhere)
            Dim sSQL As String = oSQLUpdateStringBuilder.SQL()
        End Sub

        <Test>
        Public Sub SQL_EmptyWhereClauseAllowEmptyWhereFalse_ThrowsException()
            Assert.Throws(Of Exception)(AddressOf SQLEmptyWhereClauseAllowEmptyWhereFalseHelper)
        End Sub

        Private Sub SQLEmptyWhereClauseAllowEmptyWhereFalseHelper()
            Dim oWhere As New CSQLWhereStringBuilder
            Dim oSQLUpdateStringBuilder As ISQLEditStringBuilder = New cSQLUpdateStringBuilder("", oWhere)
            Dim sSQL As String = oSQLUpdateStringBuilder.SQL()
        End Sub

        <TestCase("")>
        <TestCase("     ")>
        <TestCase(Nothing)>
        Public Sub AppendParameter_NullFieldName_ThrowsArgumentException(ByVal psFieldName As String)
            Dim oWhere As New CSQLWhereStringBuilder
            oWhere.AppendANDCondition("LenderID = @LenderID")
            Dim oSQLUpdateStringBuilder As ISQLEditStringBuilder = New cSQLUpdateStringBuilder("TestTable", oWhere)
            Assert.Throws(Of ArgumentException)(Sub() oSQLUpdateStringBuilder.AppendParameter(psFieldName, "testValue"))
        End Sub

        <TestCase("")>
        <TestCase("     ")>
        <TestCase(Nothing)>
        Public Sub AppendParameter_NullParameterName_ThrowsArgumentException(ByVal psParameterName As String)
            Dim oWhere As New CSQLWhereStringBuilder
            oWhere.AppendANDCondition("LenderID = @LenderID")
            Dim oSQLUpdateStringBuilder As ISQLEditStringBuilder = New cSQLUpdateStringBuilder("TestTable", oWhere)
            Assert.Throws(Of ArgumentException)(Sub() oSQLUpdateStringBuilder.AppendParameter("TestFieldName", psParameterName))
        End Sub

        <Test>
        Public Sub AppendParameter_ValidInput_AppendsParameter()
            Dim oWhere As New CSQLWhereStringBuilder
            oWhere.AppendANDCondition("LenderID = @LenderID")
            Dim oSQLUpdateStringBuilder As ISQLEditStringBuilder = New cSQLUpdateStringBuilder("TestTable", oWhere)
            oSQLUpdateStringBuilder.AppendParameter("TestFieldName", "TestFieldName")
            Dim sExpectedSQL As String = "UPDATE TestTable SET TestFieldName = @TestFieldName  WHERE LenderID = @LenderID"
            Assert.AreEqual(sExpectedSQL, oSQLUpdateStringBuilder.SQL)
        End Sub

        <Test>
        Public Sub AppendParameter_ValidInputs_AppendsParameters()
            Dim oWhere As New CSQLWhereStringBuilder
            oWhere.AppendANDCondition("LenderID = @LenderID")
            Dim oSQLUpdateStringBuilder As ISQLEditStringBuilder = New cSQLUpdateStringBuilder("TestTable", oWhere)
            oSQLUpdateStringBuilder.AppendParameter("TestFieldName", "TestFieldName")
            oSQLUpdateStringBuilder.AppendParameter("TestFieldName2", "TestFieldName2")
            oSQLUpdateStringBuilder.AppendParameter("TestFieldName3", "TestFieldName3")
            Dim sExpectedSQL As String = "UPDATE TestTable SET TestFieldName = @TestFieldName, TestFieldName2 = @TestFieldName2, TestFieldName3 = @TestFieldName3  WHERE LenderID = @LenderID"
            Assert.AreEqual(sExpectedSQL, oSQLUpdateStringBuilder.SQL)
        End Sub

        <Test>
        Public Sub Reset_NoInputs_ResetsFieldsAndValues()
            Dim oWhere As New CSQLWhereStringBuilder
            oWhere.AppendANDCondition("LenderID = @LenderID")
            Dim oSQLUpdateStringBuilder As ISQLEditStringBuilder = New cSQLUpdateStringBuilder("TestTable", oWhere)
            oSQLUpdateStringBuilder.AppendParameter("TestFieldName", "TestFieldName")
            oSQLUpdateStringBuilder.AppendParameter("TestFieldName2", "TestFieldName2")
            oSQLUpdateStringBuilder.AppendParameter("TestFieldName3", "TestFieldName3")
            oSQLUpdateStringBuilder.Reset()
            Dim sExpectedSQL As String = "UPDATE TestTable SET   WHERE LenderID = @LenderID"
            Assert.AreEqual(sExpectedSQL, oSQLUpdateStringBuilder.SQL)
        End Sub

        <Test>
        Public Sub AppendString_ValidInput_AppendsString()
            Dim oWhere As New CSQLWhereStringBuilder
            oWhere.AppendANDCondition("LenderID = @LenderID")
            Dim oSQLUpdateStringBuilder As ISQLEditStringBuilder = New cSQLUpdateStringBuilder("TestTable", oWhere)
            oSQLUpdateStringBuilder.appendString("TestFieldName", "TestFieldName")
            Dim sExpectedSQL As String = "UPDATE TestTable SET TestFieldName=TestFieldName  WHERE LenderID = @LenderID"
            Assert.AreEqual(sExpectedSQL, oSQLUpdateStringBuilder.SQL)
        End Sub

        <Test>
        Public Sub AppendString_ValidInputs_AppendsStrings()
            Dim oWhere As New CSQLWhereStringBuilder
            oWhere.AppendANDCondition("LenderID = @LenderID")
            Dim oSQLUpdateStringBuilder As ISQLEditStringBuilder = New cSQLUpdateStringBuilder("TestTable", oWhere)
            oSQLUpdateStringBuilder.appendString("TestFieldName", "TestValue")
            oSQLUpdateStringBuilder.appendString("TestFieldName2", "TestValue2")
            oSQLUpdateStringBuilder.appendString("TestFieldName3", "TestValue3")
            Dim sExpectedSQL As String = "UPDATE TestTable SET TestFieldName=TestValue, TestFieldName2=TestValue2, TestFieldName3=TestValue3  WHERE LenderID = @LenderID"
            Assert.AreEqual(sExpectedSQL, oSQLUpdateStringBuilder.SQL)
        End Sub

        <Test>
        Public Sub AppendValue_ValidInput_AppendsValue()
            Dim oWhere As New CSQLWhereStringBuilder
            oWhere.AppendANDCondition("LenderID = @LenderID")
            Dim oSQLUpdateStringBuilder As ISQLEditStringBuilder = New cSQLUpdateStringBuilder("TestTable", oWhere)
            oSQLUpdateStringBuilder.AppendValue("TestFieldName", New CSQLParamValue("TestParamValue"))
            Dim sExpectedSQL As String = "UPDATE TestTable SET TestFieldName='TestParamValue'  WHERE LenderID = @LenderID"
            Assert.AreEqual(sExpectedSQL, oSQLUpdateStringBuilder.SQL)
        End Sub

        <Test>
        Public Sub AppendValue_ValidInputs_AppendsValues()
            Dim oWhere As New CSQLWhereStringBuilder
            oWhere.AppendANDCondition("LenderID = @LenderID")
            Dim oSQLUpdateStringBuilder As ISQLEditStringBuilder = New cSQLUpdateStringBuilder("TestTable", oWhere)
            oSQLUpdateStringBuilder.AppendValue("TestFieldName", New CSQLParamValue("TestParamValue"))
            oSQLUpdateStringBuilder.AppendValue("TestFieldName2", New CSQLParamValue("TestParamValue2"))
            oSQLUpdateStringBuilder.AppendValue("TestFieldName3", New CSQLParamValue("TestParamValue3"))
            Dim sExpectedSQL As String = "UPDATE TestTable SET TestFieldName='TestParamValue', TestFieldName2='TestParamValue2', TestFieldName3='TestParamValue3'  WHERE LenderID = @LenderID"
            Assert.AreEqual(sExpectedSQL, oSQLUpdateStringBuilder.SQL)
        End Sub

    End Class

End Namespace
