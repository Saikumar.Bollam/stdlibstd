﻿Option Strict On

Imports StdLib.DBUtils
Imports NUnit.Framework

Namespace DBUtilTests.SQLStringBuildersTests

    <TestFixture>
    Public Class CSQLInsertStringBuilderTester

        <Test>
        Public Sub SQL_EmptyTableName_ThrowsException()
            Assert.Throws(Of Exception)(AddressOf SQLEmptyTableNameHelper)
        End Sub

        Private Sub SQLEmptyTableNameHelper()
            Dim oSQLInsertStringBuilder As ISQLEditStringBuilder = New cSQLInsertStringBuilder("")
            Dim sSQL As String = oSQLInsertStringBuilder.SQL()
        End Sub

        <TestCase("")>
        <TestCase("     ")>
        <TestCase(Nothing)>
        Public Sub AppendParameter_NullFieldName_ThrowsArgumentException(ByVal psFieldName As String)
            Dim oSQLInsertStringBuilder As ISQLEditStringBuilder = New cSQLInsertStringBuilder("TestTable")
            Assert.Throws(Of ArgumentException)(Sub() oSQLInsertStringBuilder.AppendParameter(psFieldName, "testValue"))
        End Sub

        <TestCase("")>
        <TestCase("     ")>
        <TestCase(Nothing)>
        Public Sub AppendParameter_NullParameterName_ThrowsArgumentException(ByVal psParameterName As String)
            Dim oSQLInsertStringBuilder As ISQLEditStringBuilder = New cSQLInsertStringBuilder("TestTable")
            Assert.Throws(Of ArgumentException)(Sub() oSQLInsertStringBuilder.AppendParameter("TestFieldName", psParameterName))
        End Sub

        <Test>
        Public Sub AppendParameter_ValidInput_AppendsParameter()
            Dim oSQLInsertStringBuilder As ISQLEditStringBuilder = New cSQLInsertStringBuilder("TestTable")
            oSQLInsertStringBuilder.AppendParameter("TestFieldName", "TestFieldName")
            Dim sExpectedSQL As String = "INSERT INTO TestTable (TestFieldName) VALUES (@TestFieldName)"
            Assert.AreEqual(sExpectedSQL, oSQLInsertStringBuilder.SQL)
        End Sub

        <Test>
        Public Sub AppendParameter_ValidInputs_AppendsParameters()
            Dim oSQLInsertStringBuilder As ISQLEditStringBuilder = New cSQLInsertStringBuilder("TestTable")
            oSQLInsertStringBuilder.AppendParameter("TestFieldName", "TestFieldName")
            oSQLInsertStringBuilder.AppendParameter("TestFieldName2", "TestFieldName2")
            oSQLInsertStringBuilder.AppendParameter("TestFieldName3", "TestFieldName3")
            Dim sExpectedSQL As String = "INSERT INTO TestTable (TestFieldName, TestFieldName2, TestFieldName3) VALUES (@TestFieldName, @TestFieldName2, @TestFieldName3)"
            Assert.AreEqual(sExpectedSQL, oSQLInsertStringBuilder.SQL)
        End Sub

        <Test>
        Public Sub Reset_NoInputs_ResetsFieldsAndValues()
            Dim oSQLInsertStringBuilder As ISQLEditStringBuilder = New cSQLInsertStringBuilder("TestTable")
            oSQLInsertStringBuilder.AppendParameter("TestFieldName", "TestFieldName")
            oSQLInsertStringBuilder.AppendParameter("TestFieldName2", "TestFieldName2")
            oSQLInsertStringBuilder.AppendParameter("TestFieldName3", "TestFieldName3")
            oSQLInsertStringBuilder.Reset()
            Dim sExpectedSQL As String = "INSERT INTO TestTable () VALUES ()"
            Assert.AreEqual(sExpectedSQL, oSQLInsertStringBuilder.SQL)
        End Sub

        <Test>
        Public Sub AppendString_ValidInput_AppendsString()
            Dim oSQLInsertStringBuilder As ISQLEditStringBuilder = New cSQLInsertStringBuilder("TestTable")
            oSQLInsertStringBuilder.appendString("TestFieldName", "TestFieldName")
            Dim sExpectedSQL As String = "INSERT INTO TestTable (TestFieldName) VALUES (TestFieldName)"
            Assert.AreEqual(sExpectedSQL, oSQLInsertStringBuilder.SQL)
        End Sub

        <Test>
        Public Sub AppendString_ValidInputs_AppendsStrings()
            Dim oSQLInsertStringBuilder As ISQLEditStringBuilder = New cSQLInsertStringBuilder("TestTable")
            oSQLInsertStringBuilder.appendString("TestFieldName", "TestValue")
            oSQLInsertStringBuilder.appendString("TestFieldName2", "TestValue2")
            oSQLInsertStringBuilder.appendString("TestFieldName3", "TestValue3")
            Dim sExpectedSQL As String = "INSERT INTO TestTable (TestFieldName, TestFieldName2, TestFieldName3) VALUES (TestValue, TestValue2, TestValue3)"
            Assert.AreEqual(sExpectedSQL, oSQLInsertStringBuilder.SQL)
        End Sub

        <Test>
        Public Sub AppendValue_ValidInput_AppendsValue()
            Dim oSQLInsertStringBuilder As ISQLEditStringBuilder = New cSQLInsertStringBuilder("TestTable")
            oSQLInsertStringBuilder.AppendValue("TestFieldName", New CSQLParamValue("TestParamValue"))
            Dim sExpectedSQL As String = "INSERT INTO TestTable (TestFieldName) VALUES ('TestParamValue')"
            Assert.AreEqual(sExpectedSQL, oSQLInsertStringBuilder.SQL)
        End Sub

        <Test>
        Public Sub AppendValue_ValidInputs_AppendsValues()
            Dim oSQLInsertStringBuilder As ISQLEditStringBuilder = New cSQLInsertStringBuilder("TestTable")
            oSQLInsertStringBuilder.AppendValue("TestFieldName", New CSQLParamValue("TestParamValue"))
            oSQLInsertStringBuilder.AppendValue("TestFieldName2", New CSQLParamValue("TestParamValue2"))
            oSQLInsertStringBuilder.AppendValue("TestFieldName3", New CSQLParamValue("TestParamValue3"))
            Dim sExpectedSQL As String = "INSERT INTO TestTable (TestFieldName, TestFieldName2, TestFieldName3) VALUES ('TestParamValue', 'TestParamValue2', 'TestParamValue3')"
            Assert.AreEqual(sExpectedSQL, oSQLInsertStringBuilder.SQL)
        End Sub

    End Class

End Namespace
