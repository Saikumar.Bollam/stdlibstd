﻿Option Strict On
Imports System.Data.SqlClient
Imports StdLib.DBUtils
Imports NUnit.Framework

Namespace DBUtils
    Public Class CSQLParamBuilderTester



#Region "Create String SQL Parameter"
        <Test>
        Public Sub CreateStrParamWithNullNameThrow()
            Dim paramValue As String = "TEST"

            Assert.Throws(Of Exception)(Function() CSQLParamBuilder.CreateParam(Nothing, paramValue))
        End Sub

        <TestCase("")>
        <TestCase("    ")>
        Public Sub CreateStrParamWithEmptyOrWhiteSpaceNameThrow(ByVal psParamName As String)
            Dim paramValue As String = "TEST"

            Assert.Throws(Of Exception)(Function() CSQLParamBuilder.CreateParam(psParamName, paramValue))
        End Sub

        <Test>
        Public Sub CreateStrParamWithNullValue()
            Dim nullParamValue As String = Nothing

            Dim parameter As SqlParameter = CSQLParamBuilder.CreateParam("NullValue", nullParamValue)

            Assert.IsTrue(parameter.IsNullable, "SQL Parameter should be null")
            Assert.AreEqual("NullValue", parameter.ParameterName)
            Assert.AreEqual(DBNull.Value, parameter.Value)
        End Sub

        <Test>
        Public Sub CreateStrParamWithLongValue()
            Dim parameterValueWith8001Char As String = "Z2Eznz7miLdtTzYnvJpFuIwnwX0JRbgPmxHoAzK0HNcA50RmqqMvxCGjllyQEBrbi7dwVcXri586p0c1ZsChi4lsyaq7FgiKvFpttod5vr3GUFfFL0TPM1MjYPXWEsJbnZu09ljDntdKoONY4yZKckaDwY7eWgTLfaSqoC1QVj8emEbVSRUtibLw0vLmvGo8g8YrVYokDIp47VYqYGBk2AsJoTlZmGd2q24KRJg1I5n0uNjVAXZudrolrlte6gilSPPjTZTPX9tkdRBW8WaUoyeTtMRZHupuPQ0N9jdKE3w5LeSs0h6CFEPIOHOyxvUYv8umcs82uIKysdB4I77C3rUAB3MayI0T1KcrHWD9SM4RGuJCMtV6mtQTqDi4oAcEG2iV7ZyleGcEiPCCyDk6xmvryMmKLV8auqSPBVZIPuB6xoBxOZC0gHOfBrfJpxRWynxKsxWEZIU1QsCATPjP0zXwF5IcylOgqUxudJ9zsJqM6saVXvHRXsfqeYLRBccM3vPOq9Ss9JzURa9UJLHHO3CGM1z1uJAxrerxANoMOuuXqniqEQ9VlP4qwS9dtsNprbRfbsfOFwwUZmuYA9CpS8cBC0uj5lCEcfkR9xepqEFe7Nzv1a35LjqPU4YcHH8L0tSQgVT72lwwjCHls5wPh1HubgtFsWKK5F2n1F7AlIXHBWkTjjvafhmvNV5ST8LVslsQtayX03LMsVyZ1plIt1EIV6igjEzdis4XQWVvOEBXk68db2vcSQf3wg7NnadrqzhkxQkVeHo7FFikFRsiF1ptqUouOvvpwZC7YXJFOfFVr8Jrx2aMh60RAm0O9yscPXCsNrYSFm0JJjTAttGia3fcNu3mazJuaQ1b2BFl15RutHXtZSqyxHrxK44LTZcJD30MGu7mZHta4UQns4Y7XlsUCV9X8uKksyrWfIlrX2pCgLiUg9VhF9uqQ1RO9TtqXFdbXViTz5EElBvtI2lVZYITjFzPsZaySvvBQUq0sl6RZK3OpWjcwBC2vi8BRR4EBKMUfqWaZP5p9BwlfVs77pjEqfB0DYUgm18BiqXX1E8jYTlEF6TtuIU1jmw6fmwfyO0vfpScJF1cJbpvSvFVtZbCzC3b97uOlB5kGdPnbrWDJPvUABH3f8HIR8gFl2rxQR8VwjtQMaIcPeDIfKMN15Vzwl6yJnyAwzBwCiYs475pv4nDl8BVWyw5MJvHfVcP81HBaHQ28uIA3uxFLH0RnmuV1gWPZeWdWDehsrc4i5rjqgT1sCeQXuF1H6mWaS3rRadT536xYN1YTfkmKae6Fn4omwPYRJh1MseqgPW7tPZtHAsQcLareRq43i5qqDYQuWQFcAEFs9ZpAokUXR2nQRjMksBBJbMc7HR1GEUEJ1KPirWbkIug1SlOuklbKyb1uCf4cB7vtvrctHMLwMa4DMK0eyPCbzCQlXc7bibfyE34gxOf8JPbo1QInQwqlLStaEwCdOchwDrmETZr6Wqg6RpKCGMD4pmlqwzHPY6M3HzyjizG9wIPEPz9u2szv4xGftydXHdFnxu6KzUi1QWZfk5WTbcIrhKIlEMOfxUlIntBszXoppr4elICwDlZV9cZwl30hLbTSjvsb3onxJDh0mu1imcUF16WfhLGJtMffcnDxQYMFXVmwgXtLlGElUiynG8a8bXVWeM84Hqrej8T8rKK5MPyv3bL6jxO3RcmWlKS5hyRo76BTaiubOlGyHhcEotkeChiSRo3xlC8tol6R26O1Yv0mt2iur0adtxlEFxAAeHKgGcVnWpKvJQEm124wcfpgVhofNzw65W8pVchtKvXferqKGsXdCWhpo1GAb1qWRqlGaFuME3mBnyPBdjZDq0RjIrOjcaqlFPZF88EQ4fx1Nk6vQ4AnUpZYupAYEJ1lEnQoawjYNDROXEhr0I08DiHWW0NHk064Lqr3yi53aWzLYjWFC2ThC9V5ewOF9CCdgWuvlkZOCf7BerQ1GnRcp4rw3PDtSpPjr12f6EZ24dqBOxAzyjvOwLh8m3MDwGkmmqKNH8WiiJdfiKnaN2wycgHtxDn8CnfMmig7gUD5TruASdJZNVrEsbOtJSuhmy5LNBuqVmky0gAuqt4OjYuQdOU65U0IqGl7EiRKYayZbaxvDct3rydzKB23vcuwa7WQwFsp5Pi8IRJuLO0ARPDYaBg8VtOjfRy0iVczUhVAm92celIBnmPuPEApAo0Vx7dgWhn1fygCW7bdkzl85mwxw7DvwnjZNcUzC64pFYEGF0kOlarE6kkOxYloXn5IJwyXDzDLrkpPxY7LWiTt6y1omv7dVTskYtEHkkOlBYUMjYVdK2Lo52hjH7GRFxoFltPXH1EcZM4KbzWtCE50NuS5ACHeJCxF0v8CcEKWXfxj2liwRsFd6bKH4ZPfZULrS2XV9HuY70JMumdiQWQnhQuEuOirCB6wTOUCyGc6ucDhyyFiU39qNyQxXbEolpNOjjPKPZceNKaMR0MfNjQSZjYltOXl2qvwKWNJ7J189lFIaJjn6OMITzqHFXNrw0reBMEicMBLSQ5NVbSsoDRHsruZGGUOGFLOQRYDRh0iSoXNbtdTO4UpOar2YhJ1mfwzpmdyGjI9ylxtsNCRc3jXIS4TDngeB7W6sCa2gLWmAwsLtajVq55wMYh0gaLwKnYmchFXsDhn2yrKZ0UqxItWHqvwKO8QVOHVBt8sTB3KuwV8yAiBOPmLw3Zwyl4BgIGXeNg0DTd4z4fCVrdIqIARH9lE9V5DDjgaYhVH2oQ5jYQybu7NxlEKBbvKH30V677xoW8dJAalKsALIpz958eu015t2DWxt1BClQpTDJj88hsY1pKwCqRoy1zuDyIVlR2a86qORjJQ1BiD0UvWjhg0BhGV03dfNDC4u8K94Qyw6rMZ1YY6yyWjW00cCD7Lfp5MthOS59cgL1gkwnzDTQCRTxt7VavPydNM9rBfuwDOKpUGo3u02dSD6dTPfbVeTfKB8e3KVUulHgYal2SI0gWOaXwl59vLvCGuYcHYslN7Rz58dPgLg1RTLo0kfeKDV2qVb43Bw9funZOgGMGv5o7U1CXAWWZMQaEtjHvXcyoOK4X0iqRqfxiOcx0CLrg0LGeUnAkCmW2biPgilRY3Ant4nAo4CE3tfsbH83nNrdOqf16QEJF4rAvzkNCVIFZy2Jh8nc0UV1Rnc8fJnbB1u3dVMGXPMLlAaeZylv1JMkdr9n9FElhXwWGwAH5Wom6gqPPbViqzUQf2YypxLsLpnLIND0nyy5XfPo0602b4jTSw4DMI80QzbVK9eTCEpm0vzuWspliMWysPGSFd7VMe52DcaJqNCfVTmLK8uLDjBubP8kcyo5YukRZSOvSNm7z8nByJJ74PMjX6Q49wnK8YqGl6WK81qd97NQFtuHqhuFYrArmivrLP6ifHjpOxW1QL8AVgwOjJwzpELAs3YE9doTcZ9AGEI0j8nESzDipcrKQXYpeNGw1oDrQorzkqzqsIEewTPNikZsD4XfyITLSvwFX8symoV8AeDI18gwNpjatdi4xHIAjHtsQ6ipf16JdoxeNOTy1Ew7GICUGRDxoBxFXs8eTP2Z9GJwtVz27mohvWoZ6db5nxt7nM4e2RNcOJhLg4Ads7AdHSKWH7aKfnO5XyHULg09TLjEl8teUb1FwrstWciStjxNHOD6w4jhQj447Tp4PqNDK1DfElEyzM9F9l7KFkFOTWu1vcXRQh3iUhqpVbAkOwAOTCVgKD5pbA3IoEM563gBfBVjDVBF69oKJL5wen6nakjIrVX9PsrktjIxUEvdqMTOfnCEZrWepd3fCQKFTdMzYf3VaHK3N4Aqg57W8dTPv1DtefciECRCQxXH4GLAskS0iLPBWwmpRmea67PCzP5jmhrAHwQcGu3AtEAoxUQATAMjI7KM6yOuXhnk3fvBlAIoCTjuUghh1CJ6WLMG4GLaOYRTprYPKfZjS9Q9CxWvWcmJENsqMfFhf7MqRsps8Hu7dFT4c0dwXiOlvjkSJA8hUS44eYGcfqecDStJEXNptqCwWzACHfvM8RWJCeyirFyWacqbASYGGpoBnMjm0pQlaNBVPXMBGpYssuOOGjjFWnPRJBTeaHgTO21Q7HLD44mWoAM8c4RnHgWgfbBeon5UhRlJSThxKJYRuKBm92rEadGMSu19jmFYZ9OQXf5QkEg6WyZMO6hF76Sac5PdJhTBiU4PLae7QT8BGrOhxzXZG0a9ugjdxMdpCnBBBS2kSRBRBcWvEtvov4zMZHZGxsgPdnn1qZ1qViOui0Brf3AyW5aNAJ7Z1fMsKXFNL7fpRHwQbUjFIV5bjbJqwy2oUbdmTHUBIKhejgAbwIZQZ3Opi0hmE5L565RMXdayt3h7FvBm3UQHYcyI5fB2DvbTiOMkX9cbxlcBlypU47F6U4AHSzvIDGw8Sm8gerFKXt81GFgpYkDzzE1vd38V453QGcC4F2qgdKVIHiqRnQBu99W1ZvSFBeIJoR2WQ9xLnY7sLIUBdIkZLZ6PU1NItiT7sLyxTVI8dp82zuzc1l3jp3Hd7zFAD2yUnURE4L1xvCLWpkjgpea8Qxo94SW9TUb68b3iY97h2UnPQvN9abZaLXstqKySEUyXEE8Kxp6NlIo2fgZfinRpPXQ2uZfMhss8dm9REuOo5EfqqsEGlqwC4TEQWw03MHxW075XQmogLlJsKf3uuYrYEc6xNjMfv9dqQ4Br1CamfilT4ffm7StVuSxB74opI8RGDMO6ff5zJx50PP20c19uDZRBifBQ0IMsfApxuDST2YgQiyjwEu0pFTgaRtgIoXzSmDTo8SkmM0AM0IaJTAzdwRCJT4QfAzLpCbVxflho0F3JH6toU9S2h8O4aiclUabizp4srRnzwf0DuSzBGAxnSDkjF6XKGSs1gPeu2LV4roJPB2L3tUXi8BosjODXyfCMFYcEab3MYra0vrumfpxnhxaHez4xTurR85YYR7uN98MAbNK2gnelwHsddg0nIGTLtcDMPjivfg1scdXDLELPmS4pbUDS5FmKdNcyufKaTHL2FCEamYayg2vWYphGyWsawzlnPl5Vxa3fMviPyHs728A98jL6EF2lC3CWn89lW7ZtYzrm1Q4lxb0bGPytuLtr7PO6vIe0dNevqhOj8XyzBP3FufhKa1fMMUthzOKcQCFod2kGkz5BCkmtvq7tCIi4PMJMMUw8hUwiJnjrkQx9LBdzOCspbxLitlbGyolUpYNgrXMSo8gMPyp8vl03RnF1rk0VMuFrcYjbHAhJmpG6sQCrVwVGehtwvovgxkZYHlLrFthRuLlcBRENXiMIIfWCdPu5YmFcs1jqYcvueA5i23RnGrF3xCnasr5IEFyfau3Zz7BzFmVziQXnmbR2ufRITwsb4nsA9q7xzFvdbw4JR3po4ArCmEXZIdpK3pIEjUl55s8Nn1MucTRX1xb7FiDCcwFHV61BD6ZROzCXsAIU0imZHQa9fgEEuOCTVpVQQtecRXwQRzP63zExi5gR9kktH3lvGrAn7f5tdXQbe9DPMZJhjJa3lPyraiaLUeFr6pH8ceY6JhCaTkoxXzSAQA2kqfaCNxc7KGNoMqrnEoIhwfMkKotO7D2PAt4GjfGxzdlP0LFmnDhj5lyGYxRm3KQdLqKmXP4bvJXwtO4Fq3GK9vDfe7qAQLhBxWCXJdOO8GrqtK8MZnhJnp4Oud53HVD7n7bpj2s76tfoIJ5YpuWW5v0rxewkKawcQuGDjPHRcl4NJH6tQT6HbOrdfJglPRkvVsDWzwFx2iDoAVKNlNLJC9pasNwxD6rbwzoc78OqTYUPicrQT3ABw4hsxtTuDefA2wGnuDM1Z6C1l6faFnKxQebODvNlu4bAKdBmD6uUE6HdLI5llauIF6LzffCLfcSGsaaNLnjx9CjYUTN0TktUn7P7OLog265FQRhCHz9QDTMpdBmyMHkBo2Em9wfztLruEd71Dkpw14FuFMP5dUQyD7N5NSuOaxN7F3SAPd1NfjBrnjmSa9JIbPc8m9KiK1UXxmRAz7S0Zt8Pxkv3nBX7BWNOnMenzckgLjrriHyuwufKxsO7jEKcPT2gUirOTV1vq0t05k5D70lebvp8UVJrXcI8mvdnVdLUyUoqyg6VaMesmzzFbQoD0BHBz0CO8K9fiYUJh4bGfwfjbJEJ2iIXlWRERKNDCkjXHLkZ1Zpe2fTXt7rYF9CbML7gJPjWaPEkBAwr3b2sI6WQoZzc1EX8BgPfYbYehhfFivKL6ag2TzNfYm3xjKmKPLpFXb0j6w1YJBCJ3WHJIM551iZijcSbGGXsMBGmJa2mO3hFfHKXMCXGGKGdHb0GwT7b7a5TfMJPJVtJcTnZkPRW517xkD1n7QBtCHg2WTqndMJiYiyOrhBDyCz7aAtzyepun5pMYvZWywE2obrn4Pdzh1V1RZ2zy29xhcHreRxrQNyDxRDiXUWG31KuSb0zaqwtWupndh1Yj2IPhAtmGO87njLpwIoV9uODIASPsgsAyltaqLHebqTelOWSPagF5ELr1NowBZZQd9bvT476IqlkCetyZbnavc0UneevY9RtRBV93SrNSn9NGg8J5y4ahBmLZDgoYwT1EUjimh0iJmEnwQEXbfLDNuEd0UlVULxBrmTCYnCwbW4WE7Zr8eVJtmQ1BLs3iorYzB7Tq9y45wpAgK1itb6kTE1JVW4XPqROP0ntCvXXaL3Wnh8JJeMpohNojg6HdGeS29awXVu269XneX27LSsedK8FOwjFIKd9tHd0A9DWhgsPLlOgBCXQqGz7cqr7YHiiiab7BizCgPn55i2H1Mq1hCg1lH0YKrf53D5RtclVS7m6kUqvIVONJ8GlQ3tCFrsFG3SPa4R01oZrrVdkup4GTB3znWJYwlSzZCxyGRNyOjrFSiXoVzRwXQ9ht75epM7BSYGdawUykEwgv4683bSKguiSsWFi556poagzqhOLiXZpIFf8INKjEs3gfug2aXcHultiKzeZKfWm2NFef2CCEcaFjNDTGy3leYglVuEhi35VMJF3kfejDJNZBNfeGILx12FYqC9C4cM7vaC8LDvT8cRD1DlVtHs1s5N6pkNlXMltRwLJd9qoIo0cdFp8iyKaBqGCXqNm9MuTSK6KinnG7sZsGlph99jPhc2y9YHLBOo6pGGZVJQBoocPdOrb4qWCaTqbvrNwJttOzF5YHSf4XMEkABzNzQBFH8Un0MS4I747qQfYjboVrb5Q6bxJo6MQVWmUYSgiVaQt6FBkyk3OiWBP7v184nT77aiAsYDL6YL51LQSoDph1abWgJdjISuFrEUGOUnRfcyjIoZXPaC6O5c2kgQjKld0pb1fYoKbINSE0M1CO6AluVr9EBvpeWUBH5Bn9E10YQFzqJ854VNPWcJ2oQ5WqupwkwBNxjpt37ie2YHZS5AI2wi6n7VP9GTHdIy99O0F5qbvFKWXiMfrcsPRj9j2NuxrKpg7jJhXzMu0FMTQED3sy4ehnmAIFDEJV6x9YbKKG0NrgO0OGtPxP7T6HoiQMiPteiqoQ3w1u7Ri0vNJBUaOanJm1HwipCs2lA7vNFsTwElZbFSuWnSfuU8GxwcSzCaLLtMRxPui8B956fkkU9LAvmPjrMz8OQYFfmxyVeBPrFpsixCnQgZ4esfz8x6wp1m2OnQJyz5FCIeFuL32yrlONgiUhO0ZMazhWTJ6h8BgbPr4gd9sbMIIaqlgYTTjnKIlp08VwzdojLzqe9b4qSglegESwqYJ5SgVLsHwJlBPNDdmvldkCbaZ9UgxBjqSV0wZNViqwHLS06NBgLxgdlTlA7AvJ5ayHeES6qpAe7w4LuMMVuFsTyHg82lzPVUPEF0y78Fd60CmRzAIWXzc6v1Mh1zpaitJcKBg55gzqwLTaURcZp5e9bsd8gCVKRFFUNgnEjNiWjQ7IFN6ExLLw1WmF5Ao6VU3AoKBMPLSfWwEZjjFRZbwv7qpwoNzrAHRjgomq6kYsuwcjZiGTJowHJ7CoF"

            Dim parameter As SqlParameter = CSQLParamBuilder.CreateParam("LongValue", parameterValueWith8001Char)

            Assert.AreEqual(-1, parameter.Size)
            Assert.AreEqual("LongValue", parameter.ParameterName)
            Assert.AreEqual(parameterValueWith8001Char, parameter.Value)
        End Sub

        <Test>
        Public Sub CreateStrParamWithValidParameter()
            Dim paramValueWithLessThan8000Char As String = "Valid Parameter"

            Dim parameter As SqlParameter = CSQLParamBuilder.CreateParam("ValidValue", paramValueWithLessThan8000Char)

            Assert.AreEqual(8000, parameter.Size)
            Assert.AreEqual("ValidValue", parameter.ParameterName)
            Assert.AreEqual(paramValueWithLessThan8000Char, parameter.Value)
        End Sub
#End Region


#Region "Create Date SQL Parameter"
        <Test>
        Public Sub CreateDateParamWithNullNameThrow()
            Dim paramValue As Date = Date.Now

            Assert.Throws(Of Exception)(Function() CSQLParamBuilder.CreateParam(Nothing, paramValue))
        End Sub

        <TestCase("")>
        <TestCase("    ")>
        Public Sub CreateDateParamWithEmptyOrWhiteSpaceNameThrow(ByVal psParamName As String)
            Dim paramValue As Date = Date.Now

            Assert.Throws(Of Exception)(Function() CSQLParamBuilder.CreateParam(psParamName, paramValue))
        End Sub

        <Test>
        Public Sub CreateDateParamWithNullValue()
            Dim nullDate As Date? = Nothing

            Dim parameter As SqlParameter = CSQLParamBuilder.CreateParam("NullValue", nullDate)

            Assert.IsTrue(parameter.IsNullable, "SQL Parameter should be null")
            Assert.AreEqual("NullValue", parameter.ParameterName)
            Assert.AreEqual(DBNull.Value, parameter.Value)
        End Sub

        <Test>
        Public Sub CreateDateParamWithInvalidDateThrow()
            Dim invalidDate As Date = SqlTypes.SqlDateTime.MinValue.Value.AddYears(-1)

            Assert.Throws(Of Exception)(Sub() CSQLParamBuilder.CreateParam("InvalidDate", invalidDate))
        End Sub

        <Test>
        Public Sub CreateDateParamWithValidParameter()
            Dim currentDate As Date = Date.Now

            Dim parameter As SqlParameter = CSQLParamBuilder.CreateParam("ValidValue", currentDate)

            Assert.AreEqual("ValidValue", parameter.ParameterName)
            Assert.AreEqual(currentDate, parameter.Value)
        End Sub
#End Region

#Region "Create GUID SQL Parameter"
        <TestCase("")>
        <TestCase("    ")>
        <TestCase(Nothing)>
        Public Sub CreateParam_GuidNameIsEmptyWhiteSpaceOrNothing_Throws(ByVal psParamName As String)
            Dim gParamValue As Guid? = Guid.NewGuid

            Assert.Throws(Of Exception)(Function() CSQLParamBuilder.CreateParam(psParamName, gParamValue))
        End Sub

        <Test>
        Public Sub CreateParam_NullGuid_ParamValueIsDBNull()
            Dim sParamName As String = "TestGuidParam"
            Dim gParamValue As Guid? = Nothing

            Dim oSQLParameter As SqlParameter = CSQLParamBuilder.CreateParam(sParamName, gParamValue)

            Assert.AreEqual(sParamName, oSQLParameter.ParameterName)
            Assert.AreEqual(DBNull.Value, oSQLParameter.Value)
        End Sub

        <Test>
        Public Sub CreateParam_NormalGuid_ParamValueIsGuid()
            Dim sParamName As String = "TestGuidParam"
            Dim gParamValue As Guid? = Guid.NewGuid

            Dim oSQLParameter As SqlParameter = CSQLParamBuilder.CreateParam(sParamName, gParamValue)

            Assert.AreEqual(sParamName, oSQLParameter.ParameterName)
            Assert.AreEqual(gParamValue.Value, oSQLParameter.Value)
        End Sub
#End Region


    End Class
End Namespace
