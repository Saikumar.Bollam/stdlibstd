﻿Option Strict On

Imports StdLib.DBUtils
Imports NUnit.Framework

Namespace DBUtilTests

    <TestFixture()>
    <Category("UNIT")>
    Public Class CSQLParamBuilderFactoryTester
        Private _sut As CSQLParamBuilderFactory

        <SetUp>
        Public Sub setup()
            _sut = New CSQLParamBuilderFactory
        End Sub

        <Test>
        Public Sub create_HappyPath_ObjectNotNull()
            Assert.IsNotNull(_sut.create())
        End Sub
    End Class
End Namespace
